<?php
ini_set('default_charset', 'UTF-8');
//date_default_timezone_set("Etc/GMT-6");
defined('YII_DEBUG') or define('YII_DEBUG', true);
// include Yii bootstrap file
error_reporting(0);
require_once(dirname(__FILE__) . '/framework/yiilite.php');
$config = dirname(__FILE__) . '/apps/love/config/backend.php';

// create a Web application instance and run
Yii::createWebApplication($config)->run();
