<?php $this->pageTitle='Мои Cообщения';?>
<?php
$this->breadcrumbs=array('Сообщения');
?>

<div class="foto_fon_top"></div>
<div class="otsup_kraya">
    <div class="zagolovok_lider4">Мои Cообщения</div>
    <div style="clear:left;"></div>

    <div class="silki_message4 <?= !(isset($_GET['folder']) && $_GET['folder'] == 'outbox') ? 'active' : '' ;?>"><a href="<?=CHtml::normalizeUrl(array('messages/index')); ?>">Полученные</a></div>
    <div class="silki_message4 <?= (isset($_GET['folder']) && $_GET['folder'] == 'outbox') ? 'active' : '' ;?>"><a href="<?=CHtml::normalizeUrl(array('messages/index', 'folder'=>'outbox')); ?>">Отправленные</a></div>


    <?php if (!$messages): ?>

        <div style="margin:150px;" align="center">Здесь будет выводиться список Ваших сообщений.

            <br><br><br>
            Пожалуйста, выберите <a href="<?=CHtml::normalizeUrl(array('search/index')); ?>"><b>пользователя</b></a>, чтобы начать общение.
        </div>

    <?php else: ?>

        <div style="clear:both;"></div>

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'mailForm',
        )); ?>
        <div class="select_all">
            <div class="text_select_all">
                Выделить: <span><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">все</a>,</span> <span><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">прочитанные</a>,</span> <span><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">новые</a></span>

            </div>

        </div>
        <ul class="bttns right" style="display:none;" id="control_panel">
            <li><input type="submit" value="Удалить" name="action" class="btn small" /></li>
            <li><a href="#" class="btn wide">ОТМЕТИТЬ КАК ПРОЧИТАННЫЕ</a></li>
        </ul>

        <div class="number_message5"><?=$_GET['folder']=='outbox' ? 'Вы отправили '.Yii::t('app','{n} сообщение|{n} сообщения|{n} сообщений', $count) : 'Вы получили '.Yii::t('app','{n} сообщение|{n} сообщения|{n} сообщений', $count)?></div>


        <?php foreach($messages as $row):?>
            <div class="all_users2 message" id="msg<?php echo $row->id;?>">
                <input id="to_message_<?php echo $row->id;?>" type="checkbox" name="to_message[]" value="<?php echo $row->id;?>" type="checkbox" class="checkbox galochka4">
                <div class="img_all_us"><img src="<?php echo $row->dialogueUser->getImage(87,87, 'camera_s.png');?>" alt="" width="87" height="87"></div>
                <div class="all_text_us">
                    <div class="name_all_tx_us"><a href="<?=$row->dialogueUser->url; ?>"><?=$row->dialogueUser->id ? $row->dialogueUser->name : 'DELETED'?></a></div>
                    <?php if ($row->dialogueUser->isOnline):?>
                        <div class="online_all_us"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/circle.png" alt="" width="11" height="11" class="img1_all_us">Online</div>
                    <?php endif;?>
                    <div class="txt_all_us"><?php echo Html::date($row->time);?></div>

                </div>

                <div class="right_block_text" onclick="location='<?=CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$row->user->id)); ?>';" style="cursor:pointer;">
                    <?=$row->message ?>
                </div>
                <div style="clear:both;"></div>
                <? $this->widget('CLinkPager', array(
                    //'id'=>'wallPages',
                    'pages'=>$pages,
                    // 'cssFile'=>Html::cssUrl('pager.css'),
                    'htmlOptions'=>array('class'=>'pagination'),
                    'header'=>'',
                    //'maxButtonCount'=>5,
                    'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
                    'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
                    'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
                    'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
                ));
                ?>
            </div>
        <?php endforeach;?>
        <?php $this->endWidget(); ?>
        <div class="form-actions1" style="display:none;">
            <?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'danger', 'buttonType'=>'submit', 'label'=>'Удалить', 'icon'=>'remove')); ?>
        </div>

    <?php endif; ?>

    <div style="clear:both;"></div>
</div>
<div class="foto_fon_bot"></div>



<script type="text/javascript">
    function checkAllCheckboxes(object, prefix){

        var re = new RegExp('^' + prefix);
        var checkboxes = document.getElementsByTagName('input');
        var total = checkboxes.length;
        for(i = 0; i < total; i++){
            if(checkboxes.item(i).id.match(re)){
                checkboxes.item(i).checked = true;
            }
        }

        $("#control_panel").show();


    }
    $(document).ready(function(){
        $('#mailForm').ajaxForm({
            dataType: 'json',
            url: $(this).attr("action"),
            type: "POST",
            cache: false,
            beforeSend: function(){

            },
            success: function(data) {

            }
        });

        $("#mailForm").submit(function() {
            $("#mailForm").change();
            $('input[type=checkbox]').each(function () {
                if (this.checked) {
                    console.log($(this).val());
                    $('#msg'+$(this).val()).hide();

                }
            });

        });

        $("#mailForm").change(function() {

            var boxes = $(":checkbox:checked");
            if (boxes.length==1) {
                $("#control_panel").show();
            }
            if (boxes.length==0) {
                $("#control_panel").hide();
            }


        });

    });
</script>