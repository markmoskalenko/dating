<?php $this->pageTitle='Написать сообщение';?>
<?php 		$this->breadcrumbs=array(
			'Написать сообщение',
		);
?>

<div class="foto_fon_top"></div>
<div class="otsup_kraya">
    <div class="zagolovok_lider">Новое сообщение</div>
    <div class="close box-modal_close arcticmodal-close"></div>
    <?php if(Yii::app()->user->hasFlash('sent')): ?>
        <div class="alert flash-success">
            Сообщение отправлено.
        </div>
    <?php endif; ?>
    <div style="clear:left;"></div>


    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'messageForm',
        'htmlOptions'=>array('class'=>'well1'),
        'enableClientValidation'=>false,
        'enableAjaxValidation'=>false,
    )); ?>
        <div id="errorSummary" class="alert alert-danger" style="display:none;">
        </div>
        <?php echo CHtml::errorSummary($model);?>


        <div class="foto_message3"><img src="<?php echo $user->have_photo==1 ? ImageHelper::thumb(87, 87, $user->id, $user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('camera_s.png');?>" alt="" width="87" height="87"></div>
        <div class="text_who">Кому: <span><a href="<?php echo $user->url; ?>"><?php echo $user->name; ?></a></span></div>

        <?php if (Yii::app()->user->model->isReal):?>
            <div class="otstup_area2">
                <?php echo $form->textArea($model, 'message', array('class'=>'text_pole2')); ?>
            </div>
            <div class="silka_dialog"><a href="<?=CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$user->id)); ?>">перейти в диалог</a></div>
            <input type="submit" value="Отправить" class="bet2"  onclick="sendMessage($('#messageForm')); return false;" >
        <?php elseif (!Yii::app()->user->isGuest):?>
            <div class="otstup_area2">
                <p>Ваша анкета не активна! Писать сообщения могут только активные пользователи сайта с статусом Реал.</p>
                <a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>"><b>Подтвердить реальность Вашей анкеты с помощью активации Реал статуса!</b></a>
            </div>
        <?php else:?>
            <div class="otstup_area2">
                <p>Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать сообщение.</p>
            </div>
        <?php endif;?>
    <?php $this->endWidget(); ?>
    <div style="clear:both;"></div>
</div>
<div class="foto_fon_bot"></div>