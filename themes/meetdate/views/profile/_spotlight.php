<?php if (!Yii::app()->user->isGuest && $this->isOwn && Yii::app()->user->model->isReal==1): ?>
			<div class="title">
				<h3 class="left">Галерея лиц</h3>
			</div>
			<div style="height:90px;">
					<div class="spotlight">
						<a href="#" onclick="pickup.showModal('<?=CHtml::normalizeUrl(array('profile/pickup')); ?>'); return false;" class="spl-iwanthere">
							<span><i class="fa fa-share"></i><br>Хочу сюда</span>
						</a>
<?php foreach(User::findSpotlight('10') as $key=>$row): ?>

<?php
if ($row->id):
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(60,75, 'camera_s.png'), '', array('class'=>'', 'width'=>'60', 'height'=>'75')
		), 
		$row->url
	);
else:
	echo CHtml::link(
		CHtml::image(Html::imageUrl('camera_s.png'), '', array('class'=>'', 'width'=>'60', 'height'=>'75')
		), 
		'#', array('onclick'=>'YiiAlert.error(\'Пользователь удален.\', \'404\'); return false;')
	);
endif;
?>

<?php endforeach; ?>
                                        </div>

			</div>
<?php endif; ?>