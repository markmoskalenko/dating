<?php if (count($photos)>0): ?>
					<div class="title">
						<h4 class="left">Мои фотоальбомы</h4>
						<a href="<?php echo CHtml::normalizeUrl(array('photos/index', 'userId'=>$this->user->id)); ?>" class="update-link right">Все</a>
					</div>	
					<div class="subtitle">
						<span class="left"><?=Yii::t('app','{n} альбом|{n} альбома|{n} альбомов', $this->user->counter->albums)?></span>
					</div>
					
					<div class="albums">
<?php foreach($photos as $row): ?>
						<div class="thumb" style="margin-top: 3; margin-bottom: 3px;">
							<a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->user_id, 'albumId'=>$row->id)); ?>">
								<img src="<?php echo $row->photo->filename ? $row->photo->getImage(170,100) : Html::imageUrl('empty_album.png');?>" alt="" />
								<strong><?=$row->name?></strong>
							</a>
						</div>	
<?php endforeach; ?>

					</div>	
<?php endif;?>