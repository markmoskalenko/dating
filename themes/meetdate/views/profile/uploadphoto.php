
						<div class="box-modal" id="uploadPhotoModal">
							<p style="font-weight:bold; color:#246C9E;">Загрузка новой фотографии</p><div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>

        <div class="modal-body">
          

 
<script type="text/javascript">
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : 'server/php/';
    $('#fileupload').fileupload({
        url: '<?=CHtml::normalizeUrl(array('photos/upload', 'albumId'=>$_GET['albumId'])); ?>',
        dataType: 'json',
	formData: function (form) {
		
    return [{name: 'CSRFTOKEN',value: '<?php echo Yii::app()->request->csrfToken; ?>'}, {name: 'SESSION_ID',value: '<?php echo Yii::app()->session->sessionID; ?>'}];
},
    add: function (e, data) {
           /* $('.progress').attr(
                'class',
                'progress progress-striped active'
            );*/
        var goUpload = true;
        var uploadFile = data.files[0];
        if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
           // alert('You must select an image file only');
           // goUpload = false;
        }
        if (uploadFile.size > 2000000) { // 2mb
           // alert('Please upload a smaller image, max size is 2 MB');
           // goUpload = false;
        }
        if (goUpload == true) {
        var jqXHR = data.submit()
            .success(function (result, textStatus, jqXHR) {
		
		location.reload(true);
		
/*$('#uploadModal').modal('hide');
            $('#progress').css(
                'width',
                '0%'
            );
		$('#done').prepend(result.html);*/
		})
            .error(function (jqXHR, textStatus, errorThrown) {
		
		})
            .complete(function (result, textStatus, jqXHR) {
           /* $('.progress').attr(
                'class',
                'progress'
            );*/
		});
        }
	
	
	

    },
        progressall: function (e, data) {
		var progress = Math.ceil((data.loaded / data.total) * 100);
            $('#progress').css(
                'width',
                progress + '%'
            );
	    $('#progress .sr-only').html(progress+'% Complete');
	   //$('#progress').attr("style", "width: "+progress+"%;");
        }
    });
});
</script>

<div>
    
    

    
    
    
    
<span class="btn2 btn-default btn-file" id="UploadPhotos">
                            <i class="fa fa-camera"></i><span>Загрузить с компьютера</span>
                            <input type="file" name="UploadForm[file]" id="fileupload"/>
                        </span>
<span>или Перетащите фотофайлы прямо в окно браузера.</span>
</div>

<br>
    
<div class="progress">
  <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    <span class="sr-only">60% Complete</span>
  </div>
</div>

    
    
<p class="text-info"><strong>Запрещено загружать:</strong></p>
<ul>
    <li>Чужие и детские фотографии;</li>
    <li>Фотографии содержащие рекламу, посторонние надписи, названия сайтов;</li>
    <li>Фотографии порнографического характера (демонстрация половых органов);</li>
    <li>Фотографии оскорбляющие честь и достоинство пользователей сайта;</li>
    <li>Демонстрирующих сцены насилия над людьми или животными;</li>
    <li>Публичное распространение которых запрещено законом.</li>
</ul>
<p class="text-info"><strong>Фотография будет помещена в альбом "Разное", если:</strong></p>
<ul>
    <li>На фото не видно или плохо видно Ваше лицо;</li>
    <li>Фото размытое, некачественное;</li>
    <li>Фотография изменена в графическом редакторе или на специальных сайтах.</li>
</ul>
    <a href="<?=CHtml::normalizeUrl(array('pages/index', 'id'=>'fotorules')); ?>" target="_blank">Правила публикации фотографий</a>

 
        </div>

						</div>
