
<div class="">
<?php foreach($wall as $row):?>

						<div class="cell" id="wall_<?=$row->id?>">
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'), '', array('class'=>'left')
		), 
		$row->user->url
	);
else:
	echo CHtml::image(Html::imageUrl('camera_s.png'), '', array('class'=>'left'));
endif;
?>
							<div class="cont">
								<span class="name"><?php
	//echo CHtml::image(Html::imageUrl($row->user->sex==2 ? 'user_female.png' : 'user_male.png'));
	//echo ' ';
	echo CHtml::link(
		$row->user->name ? $row->user->name : 'DELETED', 
		$row->user->url,
		array('rel'=>'ajax1')
	);
?><?php if ($row->photoId):?> <span style="color:#A0A0A0;">обновил<?php echo $row->user->sex==2 ? 'а' : ''?> фотографию на странице:</span><?php endif;?></span>
								<?php if ($row->photo->id):?>
								<a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->photo->user_id, 'photoId'=>$row->photo->id, 'albumId'=>0)); ?>" id="showModalPhotoBox"><img src="<?php echo ImageHelper::thumb(250, 380, $row->photo->user_id, $row->photo->filename, array('method' => 'adaptiveResize'));?>" alt=""></a>
	
								<?php endif;?>
								<?=Html::message($row->message)?>
							</div>	
							<div class="foot">
								<span class="date left"><?php echo Html::date($row->date); ?></span>
								
									<span class="comments right">
										<a href="#">Комментировать</a>
<?php if (Yii::app()->user->checkAccess('deleteComment', array('author_id'=>$row->user->id, 'own_id'=>$this->user->id))):?>

 |

<a class="delete" title="удалить" rel="tooltip" href="#" onclick="deleteWall('<?=CHtml::normalizeUrl(array('profile/wall', 'delWallId'=>$row->id)); ?>'); return false;">Удалить</a><?php endif; ?>
									</span>
							</div>	
						</div>
						
						
						



<?php endforeach; ?>



<? $this->widget('CLinkPager', array(
        'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination clearfix'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>



</div>