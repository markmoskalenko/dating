<?php if (count($gifts)>0): ?>
					<div class="title">
						<h4 class="left">Подарки</h4>
						<a href="<?php echo CHtml::normalizeUrl(array('gifts/index', 'userId'=>$this->user->id)); ?>" id="showModalGifts" class="hide-link right">все</a>
					</div>	
					<div class="presents">
<?php foreach($gifts as $row): ?>

<a href="<?php echo CHtml::normalizeUrl(array('gifts/index', 'userId'=>$row->userId)); ?>" id="showModalGifts"><img src="<?php echo Html::imageUrl('gifts/gift_'.$row->giftId.'.png');?>" width="55" height="55" alt=""></a>

<?php endforeach; ?>
					</div>
<?php endif;?>