<div class="text_anketa">
    <?php if (count($visitors) > 0) : ?>
        За сегодня Вашу анкету просмотрели:
        <?php foreach ($visitors as $visitor) : ?>
            <a href="<?= Yii::app()->createUrl('profile/index', array('userId' => $visitor->user_id)) ?>">
                <?= $visitor->user->name ?>
            </a>
            <?php if(end($visitors) != $visitor) : ?>,<?php endif; ?>
        <? endforeach; ?>
    <?php else : ?>
        За сегодня Вашу анкету никто не просмотрел! Чтобы исправить поднемите анкету наверх!
    <?endif; ?>
</div>

