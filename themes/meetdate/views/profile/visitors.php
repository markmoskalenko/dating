    <?php if (count($visitors) > 0) : ?>
        За сегодня Вашу анкету просмотрели:
        <?php foreach ($visitors as $visitor) : ?>

                <div class="visitor-info">
                    <div class="visitor-photo">
                        <?php if ($visitor->user->have_photo==1):?>
                            <a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$visitor->user->id, 'albumId'=>0, 'photoId'=>$visitor->user->photoId)); ?>" id="showModalPhotoBox">
                                <img src="<?php echo $visitor->user->have_photo==1 ? ImageHelper::thumb(171, 171, $visitor->user->id, $visitor->user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('photo-activation.jpg');?>" alt="" width="171" height="171" />
                            </a>
                        <?php else:?>
                            <div class="noavatar"><i class="fa fa-camera"></i></div>
                        <?php endif;?>
                    </div>
                    <div class="visitor-name">
                        <a href="<?= Yii::app()->createUrl('profile/index', array('userId' => $visitor->user_id)) ?>">
                            <?= $visitor->user->name ?>
                        </a>
                    </div>
                    <?php if($visitor->user->birthday != '0000-00-00') : ?>
                        <div class="visitor-birthday">
                            <?=$visitor->user->birthday ?>
                        </div>
                    <?php endif; ?>
                </div>

        <? endforeach; ?>
    <?php else : ?>
        За сегодня Вашу анкету никто не просмотрел! Чтобы исправить поднемите анкету наверх!
    <?endif; ?>

