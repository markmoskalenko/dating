<div class="block_person">

    <div><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/foto12.png" alt="" width="618" height="308"></div>
    <div class="foto_b_p">
<!--        <img src="images/site/foto14.png" alt="" width="171" height="171">-->
        <?php if ($this->user->have_photo==1):?>
            <a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$this->user->id, 'albumId'=>0, 'photoId'=>$this->user->photoId)); ?>" id="showModalPhotoBox">
                <img src="<?php echo $this->user->have_photo==1 ? ImageHelper::thumb(171, 171, $this->user->id, $this->user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('photo-activation.jpg');?>" alt="" width="171" height="171" />
            </a>
            <?php if ($this->user->isReal):?><label class="icn-real">&nbsp;</label><?php endif;?>
        <?php else:?>
            <?php if ($this->isOwn):?>
                <a href="#" onclick="$('#uploadPhotoModal').arcticmodal();return false;" class="noavatar"><i class="fa fa-camera"></i><div>Загрузить фотографию</div></a>
            <?php else:?>
                <div class="noavatar"><i class="fa fa-camera"></i></div>
            <?php endif;?>
        <?php endif;?>
    </div>
    <div class="block_right_b_p">
        <div style="overflow:hidden;">
            <div class="name_b_p"><a href="#"><?=$this->user->name?></a></div>
            <div style="clear:both;"></div>
            <div style="float:none; clear:left; height:0;"></div>

        </div>
        <div class="text1_b_p"><span><?php echo $this->user->geo->city;?></span>, <span><?php echo Html::age($this->user->birthday); ?></span>, <span><?php echo Html::zodiak($this->user->birthday);?></span></div>
        <div class="time_b_p">
            <!--            была вчера в 12:45-->
            <?php if ($this->user->isOnline) {
                echo '<label class="right" style="font-size:11px;padding-right:5px;;padding-top:2px;color:#37BF00; font-weight:bold;">';
                if (!$this->user->isMobile) {
                    echo '<i class="fa fa-circle"></i>Online';
                } else {
                    echo '<i class="fa fa-mobile"></i>Mobile';
                }
                echo '</label>';
            }
            else {
                echo '<label class="right" style="font-size:11px;padding-right:5px;;padding-top:2px; font-weight:bold;color:#71A0B4;">';
                if ($this->user->isMobile) {
                    echo '<i class="fa fa-mobile"></i>';
                }
                echo $this->user->sex==1 ? 'заходил ' : 'заходила ';
                echo Html::date($this->user->last_date).'</label>';
            }
            ?>
        </div>

        <div style="clear:both;"></div>


    </div>
    <?php if ($this->isOwn):?>
    <?php elseif (!Yii::app()->user->isGuest):?>
        <div class= "write_b_p"><a id="showModalSendMessage" href="<?php echo CHtml::normalizeUrl(array('messages/send', 'id'=>$this->user->id)); ?>">Написать сообщение</a></div>
        <div class="write_b_p3"><a href="#">Подарить подарок</a></div>
        <div class="write_b_p2"><a href="#">Ещё <span class="wr_b_p4">[+]</span></a></div>
        <?php if (!$this->isFriend):?>

<!--            <div id="friendshipRequest"><a href="--><?php //echo CHtml::normalizeUrl(array('friends/apply', 'id'=>$this->user->id)); ?><!--" class="btn small sendmessage" ><i class="fa fa-plus"></i>Добавить в друзья</a></div>-->
        <?php endif;?>
        <?php if ($this->isFriend): ?>
<!--            <div style="padding:10px;margin-bottom:5px;background:#F7F7F7;color:#626262;text-align:center; font-size:11px;">--><?//=$this->user->name?><!-- у Вас в друзьях</div>-->
        <?php endif;?>
    <?php else:?>
<!--        <div style="padding:10px;margin-bottom:10px;background:#F7F7F7;color:#626262;text-align:center; font-size:11px;">Пожалуйста, <a href="--><?php //echo CHtml::normalizeUrl(array('user/login')); ?><!--"><b>войдите</b></a> на сайт или <a href="--><?php //echo CHtml::normalizeUrl(array('user/register')); ?><!--"><b>зарегистрируйтесь</b></a>, чтобы написать сообщение.</div>-->
    <?php endif;?>

    <div style="clear:both;"></div>
    <div class="palka_pod_foto"></div>

</div>

<div id="tabs3">
    <ul class="tabs-navigation3">
        <li><a href="#tab-1">Моя анкета</a></li>
        <li><a href="#tab-2" >Мои фото</a><span class="rf">10</span></li>
        <li><a href="#tab-3">Мой блог</a><span class="rf">34</span></li>
        <li><a href="#tab-4" >Сообщества</a><span class="rf">5</span></li>
        <li><a href="#tab-5" >Приложения</a></li>
    </ul>
    <div style="clear:left;"></div>
    <div class="tabs-container3">
        <div id="tab-1" onmouseover="document.getElementById('tab-1').style.display='block'" onmouseout="document.getElementById('tab-1').style.display='block'">
            <div class="vmestimost">
                <?php foreach($this->profileFields as $row): ?>
                    <?php if ($this->isOwn OR count($row[field])):?>
                        <div class="characteristic">
                            <div class="zagolovok_ch"><?php echo $row[title]?></div>
                            <div class="knopka_redactirovnie">
                                <?php if ($this->isOwn):?><a href="<?php echo CHtml::normalizeUrl(array('edit/profile', 'section'=>$row[name])); ?>" class="update-link right">Редактировать</a><?php endif;?>
                            </div>
                            <div style="clear:both;"></div>
                            <?php foreach($row[field] as $row): ?>
                                <div class="left_kolonka"><?php echo $row[title]?>:</div>
                                <div class="right_kolonka"><?php echo $row[value]?></div>
                                <div style="clear:both;"></div>
                            <?php endforeach; ?>

                            <div class="palka_niz4"></div>
                        </div>
                    <?php endif;?>
                <?php endforeach; ?>

            </div>


        </div>


        <div id="tab-2" onmouseover="document.getElementById('tab-2').style.display='block'" onmouseout="document.getElementById('tab-2').style.display='block'">

        </div>
        <div id="tab-3" onmouseover="document.getElementById('tab-3').style.display='block'" onmouseout="document.getElementById('tab-3').style.display='block'">

        </div>
        <div id="tab-4" onmouseover="document.getElementById('tab-4').style.display='block'" onmouseout="document.getElementById('tab-4').style.display='block'">

        </div>
        <div id="tab-5" onmouseover="document.getElementById('tab-5').style.display='block'" onmouseout="document.getElementById('tab-5').style.display='block'">

        </div>

    </div>
</div>
