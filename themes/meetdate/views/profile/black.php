<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('profile.js'));  ?>
<!-- рур!-->
			<div class="title">
				<h3 class="left"><?=$this->user->name?></h3>
				
<?php if ($this->user->isOnline) {
	echo '<label class="right" style="font-size:11px;padding-right:5px;;padding-top:2px;color:#37BF00; font-weight:bold;">';
	if (!$this->user->isMobile) {
		echo '<i class="fa fa-circle"></i>Online';
	} else {
		echo '<i class="fa fa-mobile"></i>Mobile';
	}
	echo '</label>';
}
else {
	echo '<label class="right" style="font-size:11px;padding-right:5px;;padding-top:2px; font-weight:bold;color:#71A0B4;">';
	if ($this->user->isMobile) {
		echo '<i class="fa fa-mobile"></i>';
	}
    echo $this->user->sex==1 ? 'заходил ' : 'заходила ';
    echo Html::date($this->user->last_date).'</label>';
}
?>
			</div>	
			<div class="wall">
				<div class="leftcol">
					<div class="face-user">
<?php if ($this->user->have_photo==1):?>
						<img src="<?php echo $this->user->have_photo==1 ? ImageHelper::thumb(180, 240, $this->user->id, $this->user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('photo-activation.jpg');?>" alt="" />
						<label class="icn-real">&nbsp;</label>
<?php else:?>
		<div class="noavatar"><i class="fa fa-camera"></i></div>
<?php endif;?>
					</div>	
					<div class="menu-wall">

	
					</div>

<?php if (!Yii::app()->user->isGuest && !$this->isOwn):?>

					<div class="menu-wall">
		<?php $this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
			'items'=>array(
				
				array('label'=>'<i class="fa fa-times"></i>Убрать из друзей', 'url'=>array('video/index', 'userId'=>$this->user->id), 'icon'=>'trash', 'visible'=>$this->isFriend),
				array('label'=>Blacklist::checkBlackByUser($this->user->id)->id ? '<i class="fa fa-check-circle"></i>Разблокировать' : '<i class="fa fa-ban"></i>Заблокировать', 'url'=>array('settings/blacklist', 'bId'=>$this->user->id), 'itemOptions'=>array('id'=>'AddToBlacklist')),
				array('label'=>'<i class="fa fa-warning"></i>Пожаловаться', 'url'=>array('abuse/index', 'userId'=>$this->user->id), 'icon'=>'warning-sign'),

				
                    array('label'=>'<span class="glyphicon glyphicon-remove"></span> Удалить', 'url'=>'#', 'visible'=>Yii::app()->user->role=='admin'),
                    array('label'=>'<span class="glyphicon glyphicon-ban-circle"></span> Забанить', 'url'=>'#', 'visible'=>Yii::app()->user->role=='admin'),

				
			),
		)); ?>
			
					</div>	
<?php endif;?>
				</div>	
				<div class="rightcol">
					<div class="data-user">
						<ul>
							<li>
								<label>Имя:</label>
								<span><?=$this->user->name?></span>
							</li>		
							<li>		
								<label>Пол:</label>
								<span><?php echo $this->user->sex=='2' ? 'Женский' : 'Мужской';?></span>
							</li>	
							<li>									
								<label>Возраст:</label>
								<span><?php echo Html::age($this->user->birthday).', '.Html::zodiak($this->user->birthday);?></span>
							</li>	 	
							<li>		
								<label>Город:</label>
								<span><?php echo $this->user->geo->country;?>, <?php echo $this->user->geo->city;?></span>
							</li>				
						</ul>	
	<div class="alert alert-danger">
		Пользователь ограничил доступ к своей страницы.
		</div>	
					</div>


					


				</div>	
			</div>	