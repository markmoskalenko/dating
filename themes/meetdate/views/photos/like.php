<div id="getLike" style="width:560px;">
<div class="clearfix userlikes_c"><?=Yii::t('app','{n} оценка|{n} оценки|{n} оценок', $count)?></div>

<div class="userlikes-list">
<?php foreach($like as $num=>$row):?>
				
					<div class="row<?php echo $num%5  == 4 ? ' last' : '';?>">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(100,100, 'camera_m.png'),
			'',
			array('class'=>'ava')
		), 
		$row->url,
		array()
	);
?>
<?php if ($row->isOnline):?>
<span class="f_online">
	<i class="fa fa-circle"></i>
	</span>
<?php endif;?>
<?php
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('rel'=>'ajax1')
	);
?>
					</div>	
<?php endforeach; ?>
</div>

<div class="clearfix">
<?php $this->widget('CLinkPager', array(
        'id'=>'likePages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?></div>
</div>