			<div class="title">
				<h3 class="left">Редакрировать альбом</h3>
			</div>	
			<div class="section-tabs">
<?php $this->widget('Menu', array(
	'htmlOptions'=>array('class'=>'tabs'),
    'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'Обзор фотографий', 'url'=>array('photos/index', 'userId'=>$model->user_id), 'linkOptions'=>array()),
				array('label'=>$model->name ? $model->name : Yii::t('app', 'Photos from'), 'url'=>array('photos/index', 'userId'=>$model->user_id, 'albumId'=>$model->id), 'linkOptions'=>array(), 'visible'=>$model->id),
				array('label'=>'Редактировать альбом', 'url'=>array('photos/editAlbum', 'id'=>$model->id), 'linkOptions'=>array(), 'icon'=>'pencil'),
				array('label'=>'Создать альбом', 'url'=>array('photos/createAlbum'), 'icon'=>'plus', 'linkOptions'=>array()),
			),
)); ?>

				<div class="tab visible clearfix">
					<div style="float:left;width:500px;">
<?php $this->renderPartial('_albumForm', array('model'=>$model)); ?>
					</div>
					<div style="float:right;width:170px;">
						
						<?php

	echo CHtml::image(
			  // $model->photo->getImage('Ширина', 'Высота', 'Показывать картинку если фотографии нету'),
			$model->photo->id ? $model->photo->getImage(170,120) : Html::imageUrl('empty_album.png'), '', array('width'=>'170', 'height'=>'120')
		);
?>

					<div class="menu-wall">
		<?php $this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
			'items'=>array(
				
				array('label'=>'<i class="fa fa-times"></i>Удалить альбом', 'url'=>array('photos/editAlbum', 'id'=>$model->id, 'cmd'=>'delete'), 'linkOptions'=>array('confirm'=>'Удалить альбом и все фотографии в нём?')),

				
			),
		)); ?>
			
					</div>


					</div>
				</div>	
			</div>