<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="alert flash-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

<div class="alert flash-success" style="display:none;">
	Изменения сохранены.
</div>
		<div id="errorSummary" class="alert alert-danger" style="display:none; padding:4px;">
		</div>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
)); ?>
<?php echo CHtml::errorSummary($model);?>

						<div class="inputs">
<?php echo $form->labelEx($model,'descr',array('class'=>'control-label')); ?>
		<?php echo $form->textArea($model, 'descr',array('class'=>'txt-field')); ?>
		<?php echo $form->error($model,'descr'); ?>
						</div>	
						<div class="inputs">
<?php echo $form->labelEx($model,'album_id',array('class'=>'col-lg-3 control-label')); ?>

		<?php echo $form->dropDownList($model, 'album_id', Albums::model()->listData(),array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'album_id'); ?>
						</div>

		
		<?php /*if (!$model->isNewRecord) $this->widget('bootstrap.widgets.TbButton', array(
    'type'=>'danger',
    'buttonType'=>'link',
    'label'=>'Удалить альбом',
    'url'=>array('settings/remove'),
    'icon'=>'remove',
    //'block'=>true,
));*/ ?>

		
						<div class="inputs">
							<input type="submit" value="Сохранить" class="btn standart" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>
<?php $this->endWidget(); ?>