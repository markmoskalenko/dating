<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo.js'));  ?>
<?php $this->pageTitle='Все Альбомы';?>

			<div class="title">
				<h3 class="left">Все Альбомы</h3>
				<a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$_GET['userId'])); ?>" class="right extended-link"><i class="fa fa-reply"></i>К обзору фотографий</a>
			</div>	
			<div class="section-tabs">
				<div class="tab visible">

<div class="photos-list albums-list">
		<?php foreach($albums as $key=>$row): ?>
		<div class="row<?php echo $key%2  == 0 ? ' first' : '';?>">
		<a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->user_id, 'albumId'=>$row->id)); ?>"><img src="<?php echo $row->photo->filename ? ImageHelper::thumb(350, 200, $row->photo->user_id, $row->photo->filename, array('method' => 'adaptiveResize')) : Html::imageUrl('empty_album.png');?>" width="350" height="200" border="0" /></a>
		<span><a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->user_id, 'albumId'=>$row->id)); ?>"><?=$row->name?></a><?php if ($this->isOwn):?><a href="<?=CHtml::normalizeUrl(array('photos/upload', 'albumId'=>$row->id)); ?>" class="right" style="cursor: pointer"><i class="fa fa-upload"></i></a><?php if (!$row->system):?><a href="<?=CHtml::normalizeUrl(array('photos/editAlbum', 'id'=>$row->id)); ?>" class="right" style="cursor: pointer"><i class="fa fa-gear"></i></a><?php endif;?><?php endif;?></span>
		</div>
		<?php endforeach; ?>
</div>

<div class="clearfix">
<?php $this->widget('CLinkPager', array(
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
			</div>	
		</div>