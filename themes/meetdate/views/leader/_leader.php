<?php
    $leader = Leader::model()->find('is_current = 1');
?>


<div class="block_dl9_blocka2">
    <div class="embroidery_top"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/poisk_foto_top.png" alt="" width="231" height="29"></div>
    <div class="lider">Лидер</div>
    <?php if ($leader): ?>
        <div class="img_lider">
            <img src="<?php echo $leader->photo ? ImageHelper::thumb(76, 76, $leader->user_id, $leader->photo->filename, array('method' => 'adaptiveResize')) : Html::imageUrl('photo-activation.jpg');?>" alt="" width="76" height="76" />
        </div>
        <div class="text_lider"><span>Город:</span> <span><?= $leader->user->geo->city ?></span></div>
        <div class="text_lider"><span><?= $leader->user->name ?></span>, <span><?= Html::age($leader->user->birthday) ?></span></div>
        <div class="text_lider"><span>Ставки:</span> <span><?= $leader->rate ?></span></div>
        <div style="clear:both;"></div>
    <?php endif; ?>
    <div class="silka_lider"><a href="#">Стать “Супер-Лидером”</a></div>
    <div class="silka_lider2"><a href="<?=CHtml::normalizeUrl(array('leader/index')); ?>" id="beLeader">Стать “Лидером”</a></div>
    <div class="embroidery_top2"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/poisk_foto_bot.png" alt="" width="231" height="29"></div>
</div>


<?php $this->renderPartial('//leader/_beLeader'); ?>

<script>
    $('#beLeader').click(function(){
        $('#leaderModal').arcticmodal({
            type: 'ajax',
            url: $(this).attr("href"),
            ajax: {
                type: 'GET',
                cache: false,
                //dataType: 'json',
                success: function(data, el, responce) {
                    data.body.html(responce);

                    $('.leader-photo').click(function(){

                        $.ajax({
                            type: 'GET',
                            url: $(this).attr("href"),
                            success: function(responce) {
                                data.body.html(responce);

                                $('#rateForm input[type="submit"]').click(function(){
                                    var form = $('#rateForm');
                                    $.ajax({
                                        type: 'POST',
                                        url: form.attr("action"),
                                        data: form.serialize(),
                                        success: function(responce){
                                            data.body.html(responce);
                                        }
                                    });
                                    return false;
                                });
                            }
                        })


//                        $('#leaderModal').arcticmodal({
//                            type: 'ajax',
//                            url: $(this).attr("href"),
//                            ajax: {
//                                type: 'GET',
//                                cache: false,
//                                //dataType: 'json',
//                                success: function(data, el, responce) {
//                                    data.body.html(responce);
//                                }
//                            }
//                        });
                        return false;
                    });
                }
            }
        });
        return false;
    });
</script>