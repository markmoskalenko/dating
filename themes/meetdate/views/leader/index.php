<div class="block_form1" id="leaderModal">
    <div class="foto_fon_top"></div>
    <div class="otsup_kraya">
        <div class="zagolovok_lider">Стать лидером</div>
        <div class="box-modal_close close arcticmodal-close"></div>
        <div style="clear:both;"></div>
        <ul class="spisok1">
            <li class="first4">1. Параметры</li>
            <li><span><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/strelka3.png" alt="" width="14" height="5"></span></li>
            <li>2. Ставка</li>
        </ul>
        <div style="clear:both;"></div>

        <?php if (count($photos)): ?>
            <h2>Выберите фотографию</h2>
            <?php foreach($photos as $photo): ?>
                <a href="<?=CHtml::normalizeUrl(array('leader/rate', 'id'=>$photo->id)); ?>" class="leader-photo" data-id="<?= $photo->id ?>">
                    <img src="<?php echo ImageHelper::thumb(250, 250, $photo->user_id, $photo->filename, array('method' => 'adaptiveResize')); ?>" alt=""/>
                </a>
            <?php endforeach; ?>
        <?php else: ?>
            <p>К сожалению у Вас нет ни одной фотографии подтвержденной модератором</p>
            <p>Вы можите пройти по ссылке для загрузки фотографии</p>
            <a href="<?=CHtml::normalizeUrl(array('photos/upload')); ?>">Загрузить</a>
        <?php endif; ?>

        <div style="clear:both;"></div>
    </div>
    <div class="foto_fon_bot"></div>
</div>