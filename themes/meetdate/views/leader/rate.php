<div class="block_form1" id="leaderModal">
    <div class="foto_fon_top"></div>
    <div class="otsup_kraya">
        <div class="zagolovok_lider">Стать лидером</div>
        <div class="box-modal_close close arcticmodal-close"></div>
        <div style="clear:both;"></div>
        <ul class="spisok1">
            <li>1. Параметры</li>
            <li><span><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/strelka3.png" alt="" width="14" height="5"></span></li>
            <li class="first4">2. Ставка</li>
        </ul>
        <div style="clear:both;"></div>

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'rateForm',
            'htmlOptions'=>array(),
            'enableClientValidation'=>true,
            'enableAjaxValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
                //'validateOnChange'=>false,
                //'afterValidate'=>'js:afterValidateSettingsForm',
            ),
        )); ?>

        <div class="alignment_radio3">
            <input type="radio" id="myId33" value="1" name="Leader[rate]" checked>
            <label for="myId33">
                <span class="pseudo-checkbox"></span>
                <span class="text_radio">1 ставка</span>
            </label>
        </div>

        <div class="alignment_radio3">
            <input type="radio" id="myId44" value="3" name="Leader[rate]">
            <label for="myId44">
                <span class="pseudo-checkbox"></span>
                <span class="text_radio">3 ставки</span>
            </label>
        </div>

        <div class="alignment_radio3">
            <input type="radio" id="myId55" value="5" name="Leader[rate]">
            <label for="myId55">
                <span class="pseudo-checkbox"></span>
                <span class="text_radio">5 ставок</span>
            </label>
        </div>
        <div class="alignment_radio3">
            <input type="radio" id="myId66" value="10" name="Leader[rate]">
            <label for="myId66">
                <span class="pseudo-checkbox"></span>
                <span class="text_radio">10 ставок</span>
            </label>
        </div>
        <div style="clear:both;"></div>


        <?php if($leaders): ?>
            <?php $count = count($leaders); ?>
            <?php foreach($leaders as $leader): ?>
                <div class="block_stavki3">
                    <div class="qw">
                        <div class="bl_number3"><?= $leader->rate ?></div>
                        <div class="bl_img_pl"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/pl<?= $count ?>.png" alt="" width="50" height="<?php echo ($count-1)*10+9; ?>"></div>
                    </div>
                    <div>
                        <img src="<?php echo ImageHelper::thumb(87, 87, $leader->user_id, $leader->photo->filename, array('method' => 'adaptiveResize'));?>" alt="" width="87" height="87">
                    </div>
                    <div class="text_name3"><a href="<?=CHtml::normalizeUrl($leader->user->url); ?>"><?= $leader->user->name ?></a></div>
                </div>
                <?php $count--; ?>
            <?php endforeach; ?>
        <?php endif; ?>
<!--        <div class="block_stavki3">-->
<!--            <div class="qw">-->
<!--                <div class="bl_number3">10</div>-->
<!--                <div class="bl_img_pl"><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/pl5.png" alt="" width="50" height="49"></div>-->
<!--            </div>-->
<!--            <div><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/img1.png" alt="" width="87" height="87"></div>-->
<!--            <div class="text_name3"><a href="#">Иван</a></div>-->
<!--        </div>-->
<!---->
<!--        <div class="block_stavki3">-->
<!--            <div class="qw">-->
<!--                <div class="bl_number3">5</div>-->
<!--                <div class="bl_img_pl"><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/pl4.png" alt="" width="50" height="39"></div>-->
<!--            </div>-->
<!--            <div><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/img2.png" alt="" width="87" height="87"></div>-->
<!--            <div class="text_name3"><a href="#">Петруха</a></div>-->
<!--        </div>-->
<!---->
<!--        <div class="block_stavki3">-->
<!--            <div class="qw">-->
<!--                <div class="bl_number3">3</div>-->
<!--                <div class="bl_img_pl"><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/pl3.png" alt="" width="50" height="29"></div>-->
<!--            </div>-->
<!--            <div><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/img3.png" alt="" width="87" height="87"></div>-->
<!--            <div class="text_name3"><a href="#">Гриша</a></div>-->
<!--        </div>-->
<!---->
<!--        <div class="block_stavki3">-->
<!--            <div class="qw">-->
<!--                <div class="bl_number3">1</div>-->
<!--                <div class="bl_img_pl"><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/pl2.png" alt="" width="50" height="19"></div>-->
<!--            </div>-->
<!--            <div><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/img3.png" alt="" width="87" height="87"></div>-->
<!--            <div class="text_name3"><a href="#">Жорик</a></div>-->
<!--        </div>-->
<!--        <div class="block_stavki3">-->
<!--            <div class="qw">-->
<!--                <div class="bl_number3">0</div>-->
<!--                <div class="bl_img_pl"><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/pl1.png" alt="" width="50" height="9"></div>-->
<!--            </div>-->
<!--            <div><img src="--><?php //echo Yii::app()->theme->baseUrl ?><!--/images/img4.png" alt="" width="87" height="87"></div>-->
<!--            <div class="text_name3"><a href="#">Колян</a></div>-->
<!--        </div>-->

        <div style="clear:both;"></div>
        <input type="submit" value="Сделать ставку" class="bet2">
        <?php $this->endWidget(); ?>

        <div style="clear:both;"></div>
    </div>
    <div class="foto_fon_bot"></div>
</div>