<!--спам протекшин-->
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'spamProtectionForm',
    //'htmlOptions'=>array('class'=>'search-partners'),
    //'type'=>'horizontal',
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,


)); ?>
	<div id="errorSummary" class="alert alert-error" style="display:none;">
		</div>
<?php echo $form->textFieldRow($model, 'verifyCode', array('hint'=>'')); ?>
<div class="control-group ">
	<label class="control-label"></label>
	<div class="controls"><?php $this->widget('CCaptcha', array('captchaAction' => 'site/captcha', 'clickableImage'=>true, 'showRefreshButton'=>false, 'imageOptions'=>array('style'=>'cursor:pointer;', 'id'=>'captcha'))); ?></div>
</div>

<div class="form-actions">
<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'success','buttonType'=>'submit', 'label'=>'Подтвердить', 'htmlOptions'=>array('onclick'=>'spamProtection.enter($("#spamProtectionForm")); return false;'))); ?>
</div>
<?php $this->endWidget(); ?>