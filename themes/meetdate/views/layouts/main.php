<?php
Yii::app()->clientScript->registerCssFile(Html::cssUrl('style3.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('dd.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('../js/arcticmodal/jquery.arcticmodal-0.3.css'));

//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery-1.5.min.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.jcarousel.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.dd.min.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('arcticmodal/jquery.arcticmodal-0.3.min.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.blockUI.js'));
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.ui.widget.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.iframe-transport.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.fileupload.js'));
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('script.js'));
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.form.js'));
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/socket.io.js'));
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $this->pageTitle ? $this->pageTitle : 'Взрослая социальная сеть, знакомства для взрослых'; ?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <script type="text/javascript">
        //Простое увеличение картинки от http://megaweb.su/ блок растягивается
        $(function(){
            $('.zoom3').hover(function(){
                    $(this).stop().animate({width:"134px",height:"134px",left:"0px",top:"-29px"}, 400);
                },
                function(){
                    $(this).stop().animate({width:"97px",height:"97px",left:"0",top:"0"}, 400);
                });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            var tabContainers = $('.tabs-container3 > div');
            tabContainers.hide().filter(':first').show();

            $('ul.tabs-navigation3 li a').click(function(){
                tabContainers.hide();
                tabContainers.filter(this.hash).show();
                $('ul.tabs-navigation3 a').parent('li').removeClass('selected');
                $(this).parent('li').addClass('selected');
                return false;
            }).filter(':first').click();

        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            // Initialise the first and second carousel by class selector.
            // Note that they use both the same configuration options (none in this case).
            jQuery('.d-carousel3 .carousel3').jcarousel({
                scroll: 1
            });
        });
    </script>
<!--    <script src="js/jquery.dd.min.js" type="text/javascript"></script>-->
    <script type="text/javascript">

        $(document).ready(function(e) {
            //no use
            try {
                var pages = $("#pages").msDropdown({on:{change:function(data, ui) {
                    var val = data.value;
                    if(val!="")
                        window.location = val;
                }}}).data("dd");

                var pagename = document.location.pathname.toString();
                pagename = pagename.split("/");
                pages.setIndexByValue(pagename[pagename.length-1]);
                $("#ver").html(msBeautify.version.msDropdown);
            } catch(e) {
                //console.log(e);
            }

            $("#ver").html(msBeautify.version.msDropdown);

            //convert
            $("select").msDropdown({roundedBorder:false});
//            createByJson();
            $("#tech").data("dd");
        });
        function showValue(h) {
            console.log(h.name, h.value);
        }
        $("#tech").change(function() {
            console.log("by jquery: ", this.value);
        })
        //
    </script>

</head>
<body>

<div id="wreaper2">
    <div class="header">
        <div class="logo4"><a href="<?=CHtml::normalizeUrl(array('site/index')); ?>"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/logo.png" alt="" width="162" height="42"></a></div>
        <div class="otstyp_palka"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/palka.png" alt="" width="1" height="31"></div>
        <div class="silki">
            <a href="<?=CHtml::normalizeUrl(array('search/index')); ?>"><span class="img_silki"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/poisk.png" alt="" width="16" height="17"></span><span class="text_silki">Поиск</span></a>
        </div>
        <div class="silki">
            <a href="#"><span class="img_silki"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/message.png" alt="" width="18" height="16"></span><span class="text_silki">Общение</span></a>
        </div>
        <div class="silki">
            <a href="#"><span class="img_silki2"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/settings.png" alt="" width="19" height="19"></span><span class="text_silki">Приложения</span></a>
        </div>
        <div class="otstyp_palka2"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/palka.png" alt="" width="1" height="31"></div>
        <div class="authorization"><a href="#">Привет, <span class="color_auth"><?php echo Yii::app()->user->model->name; ?></span><span class="color_auth">!</span><span class="otstup_auth"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/punkt.png" alt="" width="11" height="9"></span></a></div>
    </div>
    <div style="clear:both;"></div>

    <?php $this->renderPartial('//top/_top3'); ?>
    <div style="clear:both;"></div>

    <?php $this->renderPartial('/layouts/user_menu'); ?>
    <div style="clear:both;"></div>

    <?php $this->renderPartial('/layouts/sidebar_left'); ?>

    <?php $this->renderPartial('/layouts/sidebar_right'); ?>

    <?php echo $content; ?>

    <div style="clear:both;"></div>
    <div id="footer">
        <div class="block_left">
            <div class="attendance"><span>9 460 309</span> человек уже здесь</div>
            <div class="attendance"><span>13 070</span> сейчас на сайте</div>
            <div class="attendance">Copyright 2014, <span><a href="#">LovePlanet.ru</a></span></div>
        </div>
        <ul class="foot_menu">
            <li class="first"><a href="#">Партнёрская программа</a></li>
            <li><a href="#">Компания</a></li>
            <li><a href="#">Реклама на сайте</a></li>
            <li><a href="#">Разработчикам приложений</a></li>
        </ul>
        <div class="counter"><span><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/site/stata.png" alt="" width="88" height="31"></span></div>
    </div>




</div>
</body>
</html>