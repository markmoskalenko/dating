<?php
    Yii::app()->clientScript->registerCssFile(Html::cssUrl('style.css'));
    Yii::app()->clientScript->registerCssFile(Html::cssUrl('rcarousel.css'));
    Yii::app()->clientScript->registerCssFile(Html::cssUrl('additional.css'));

    Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery-1.5.min.js'));
    Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.jcarousel.js'));
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $this->pageTitle ? $this->pageTitle : 'Взрослая социальная сеть, знакомства для взрослых'; ?></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

    <script type="text/javascript">
        //Простое увеличение картинки от http://megaweb.su/ блок растягивается
        $(function(){
            $('.zoom').hover(function(){
                    $(this).stop().animate({width:"134px",height:"134px",left:"0px",top:"-29px"}, 400);
                },
                function(){
                    $(this).stop().animate({width:"97px",height:"97px",left:"0",top:"0"}, 400);
                });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            var tabContainers = $('.tabs-container > div');
            tabContainers.hide().filter(':first').show();

            $('ul.tabs-navigation li a').click(function(){
                tabContainers.hide();
                tabContainers.filter(this.hash).show();
                $('ul.tabs-navigation a').parent('li').removeClass('selected');
                $(this).parent('li').addClass('selected');
                return false;
            }).filter(':first').click();

        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            // Initialise the first and second carousel by class selector.
            // Note that they use both the same configuration options (none in this case).
            jQuery('.d-carousel .carousel').jcarousel({
                scroll: 1
            });
        });
    </script>

</head>
<body>

<div id="wreaper">
    <div id="wreaper2">
        <div class="header">
            <?php $this->renderPartial('/layouts/menu'); ?>


        </div>

        <div style="clear:both;"></div>

        <?php echo $content; ?>

        <div style="clear:both;"></div>



        <?php $this->renderPartial('//top/_tops'); ?>


        <div style="clear:both;"></div>


        <div id="footer">
            <div class="block_left">
                <div class="attendance"><span>9 460 309</span> человек уже здесь</div>
                <div class="attendance"><span>13 070</span> сейчас на сайте</div>
                <div class="attendance">Copyright 2014, <span><a href="#">LovePlanet.ru</a></span></div>
            </div>
            <ul class="foot_menu">
                <li class="first"><a href="#">Партнёрская программа</a></li>
                <li><a href="#">Компания</a></li>
                <li><a href="#">Реклама на сайте</a></li>
                <li><a href="#">Разработчикам приложений</a></li>
            </ul>
            <div class="counter"><span><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/stata.png" alt="" width="88" height="31"></span></div>
        </div>

    </div>
</div>
</body>
</html>
