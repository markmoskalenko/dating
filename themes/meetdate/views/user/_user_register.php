<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=> array('/register'),
    'id'=>'registerForm',
    'errorMessageCssClass'=> 'error',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnType'=>true,
        'validateOnChange'=>true,
        'beforeValidate'=>'js:function(){
                            // $("#mydialog").click();
                            //$("#mydialog").dialog("open");
                            //$("body").scrollTo( 0, 310 );
                                    $("#registerForm input[type=image]").attr("disabled", "true");
                                    //$("#registerForm input[type=image]").fadeTo("speed", 0.7);
                            return true;
                        }',
        'afterValidate'=>'js:function(form, data, hasError){

               // setTimeout(function(){
               //$("#registerForm input[type=image]").fadeTo("speed", 1.0);
                $("#registerForm input[type=image]").removeAttr("disabled");

                //}, 1000);
                return true;
             }',
    ),

)); ?>
<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownList($model, 'sex', array('1'=>'Парень', '2'=>'Девушка'),array('class'=>'form1')); ?>
<?php //echo $form->error($model,'sex'); ?>

<?php echo $form->textField($model, 'name', array('placeholder'=>'Введите Ваше имя', 'class'=>'form1')); ?>
<?php //echo $form->error($model,'name', array()); ?>

<?php echo $form->textField($model, 'email', array('placeholder'=>'Введите Ваш email', 'class'=>'form1')); ?>
<?php //echo $form->error($model,'email', array()); ?>

<?php echo $form->textField($model, 'password', array('placeholder'=>'Введите Ваш пароль', 'class'=>'form1')); ?>
<?php //echo $form->error($model,'password', array()); ?>

    <div class="chex_right">
        <?php echo $form->checkBox($model, 'agree_rules', array('id'=>"myId1", 'class'=>'form1')); ?>
<!--        <input type="checkbox" id="myId1" name="agree_rules" checked>-->
        <label for="myId1">
            <span class="pseudo-checkbox"></span>
            <span class="label-text">Я соглашаюсь с <span class="pravila_saita"><a href="#">правилами сайта</a></span></span>
        </label>
    </div>
    <div style="clear:both;"></div>
    <div class="palka_bot"></div>

    <input type="submit" value="Регистрация" class="register">
<?php $this->endWidget(); ?>