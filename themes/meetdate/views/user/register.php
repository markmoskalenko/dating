<div id="tabs">
    <ul class="tabs-navigation">
        <li class="tabb12"><a href="#tab-1">Регистрация</a></li>
        <li><a href="#tab-2" >Вход на сайт</a></li>
    </ul>
    <div style="clear:left;"></div>
    <div class="tabs-container">
        <div id="tab-1" onmouseover="document.getElementById('tab-1').style.display='block'" onmouseout="document.getElementById('tab-1').style.display='block'">

            <div class="contents_taba">
                <div class="palka_center"></div>
                    <?php $this->renderPartial('_user_register', array('model'=>$model)); ?>
                <div class="img_bot"></div>

            </div>

        </div>

        <div id="tab-2" onmouseover="document.getElementById('tab-2').style.display='block'" onmouseout="document.getElementById('tab-2').style.display='block'">
            <div class="contents_taba2">
                <?php $this->renderPartial('_user_login', array('model'=>$login)); ?>
            </div>

        </div>

    </div>
</div>
