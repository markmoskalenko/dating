jQuery(function($) {
						
							$("#showModalSendMessage").live('click', function() {
							
							$.arcticmodal({
                            //$('#leaderModal').arcticmodal({
								type: 'ajax',
								url: $(this).attr("href"),
								ajax: {
									type: 'GET',
									cache: false,
									//dataType: 'json',
									success: function(data, el, responce) {
										
										var h = $('<div class="block_form1">' +
                                                responce+
												//'<div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>' +
												//'<p><b /></p><p />' +
												'</div>');
										//$('B', h).html('<span id="title" style="font-weight:bold; color:#246C9E;"><i class="fa fa-envelope"></i>Новое сообщение</span>');
										//$('div:last', h).html(responce);
										data.body.html(h);
									}
								},
								beforeOpen: function(data, el) {
									$('.navbar').attr('style','margin-right:17px;');
								},
								afterClose: function(data, el) {
									$('.navbar').attr('style','');
								},
								openEffect: {
									type: 'none'
								},
								closeEffect: {
									type: 'none'
								}
							});
							return false;
						});
});

function sendMessage(obj) {
	//console.log(obj.attr('action'));
	$.ajax({
		type: "POST",
		url: obj.attr('action'),
		data: obj.serialize(),
		cache: false,
		dataType: 'json',
		beforeSend: function(){
		   $("#messageForm textarea").attr("value", "");
		},
		success: function(responce){
			$("#messageForm #errorSummary").hide().empty();
			
			if (responce.ok) {
               $.arcticmodal('close');
               $.blockUI({
                    message: "<b>Сообщение отправлено.</b>",
                    fadeIn: 100,
                    centerY: true,
                    css: {
                        textAlign:      'center',
                        left: '40%',
                        width: '400px',
                        border: 'none',
                        padding: '10px',
                        backgroundColor: '#000',
                        opacity: .6,
                        color: '#fff' ,
                        'border-radius': '6px',
                        '-moz-border-radius':'6px',
                        '-khtml-border-radius': '6px'
                    },
                    showOverlay: false

                    /*overlayCSS:  {
                        backgroundColor: '#000',
                        opacity:          .3
                    }*/

               });
               setTimeout("$.unblockUI()", 2000);
                return;
           }

    $.each(responce, function(key, value) {

      $("#messageForm #errorSummary").prepend('<div>'+value+'</div>');

    });
    $("#messageForm #errorSummary").fadeTo('speed', 1.0);

			
			
		}
	});
	

}