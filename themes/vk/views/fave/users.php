<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));  ?>
<?php
$this->pageTitle='Мои закладки';
$this->breadcrumbs=array(
	'Мои закладки'
);
?>
			<div class="title">
				<h3 class="left">Мои закладки</h3>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>

				<div class="tab visible">
<div class="friends-list">
<?php foreach($users as $num=>$row):?>
				
				
					<div class="row<?php echo $num%4  == 0 ? ' first' : '';?>">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(150,120),
			'',
			array('class'=>'ava')
		), 
		$row->url,
		array()
	);
?>
<?php if ($row->isOnline):?>
<span class="f_online">
	<?php if (!$row->isMobile):?><i class="fa fa-circle"></i>Online<?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?>
	</span>
<?php endif;?>
<span class="ttl"><i class="fa fa-<?=$row->sex==2 ? 'fe' : ''?>male"></i><?php
echo '';
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('rel'=>'ajax1')
	);
?></span>


		<?php $this->widget('Menu',array(
			//'type'=>'list',
			'htmlOptions'=>array('class'=>'friend-nav'),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			'items'=>array(
				array('label'=>'<i class="fa fa-envelope"></i>Написать сообщение', 'url'=>array('messages/send', 'id'=>$row->id), 'linkOptions'=>array('id'=>'showModalSendMessage')),
				array('label'=>'<i class="fa fa-camera"></i>Смотеть фотографии', 'url'=>array('photos/index', 'userId'=>$row->id, 'albumId'=>0, 'photoId'=>$row->photoId), 'linkOptions'=>array('id'=>'showModalPhotoBox')),
			),
		)); ?>
					</div>	

				

				


<?php endforeach; ?>
</div>


<div class="pagination">
<?php $this->widget('bootstrap.widgets.TbPager', array(
        'id'=>'likePages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	//'htmlOptions'=>array('class'=>'pages'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'>',
	'prevPageLabel'=>'<',
        'lastPageLabel'=>'>>',
	'firstPageLabel'=>'<<',
        ));
?>

</div>
			</div>	
		</div>
