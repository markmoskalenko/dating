<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>
<?php
$this->pageTitle='Мои закладки';
$this->breadcrumbs=array(
	'Мои закладки'
);
?>
			<div class="title">
				<h3 class="left">Мои закладки</h3>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>

				<div class="tab visible">

<div class="photos-list" id="fPhotos">
<?php $this->renderPartial('_photos', array('photos'=>$photos)); ?>
</div>
<div class="clear: both; float: none;"></div>

<script type="text/javascript">
 $(document).ready(function(){
  photoShow.pageStart=<?=$_GET['page'] ? $_GET['page'] : 1?>;
 });
</script>
<div class="clearfix">
<?php $this->widget('CLinkPager', array(
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
			</div>	
		</div>