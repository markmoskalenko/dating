			<div class="title">
				<h3 class="left">Альбомы</h3>
			</div>	
			<div class="section-tabs">
<?php $this->widget('Menu', array(
	'htmlOptions'=>array('class'=>'tabs'),
    'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'Обзор фотографий', 'url'=>array('photos/index', 'userId'=>Yii::app()->user->id), 'linkOptions'=>array()),
				array('label'=>'Создать альбом', 'url'=>array('photos/createAlbum'), 'icon'=>'plus', 'linkOptions'=>array()),
			),
)); ?>

				<div class="tab visible">
<?php $this->renderPartial('_albumForm', array('model'=>$model)); ?>

				</div>	
			</div>