<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>
<?php $this->pageTitle='Мои Фотографии';?>
			<div class="title">
				<h3 class="left">Мои Фотографии</h3>
				<a href="<?=CHtml::normalizeUrl(array($this->user->url)); ?>" class="right extended-link"><i class="fa fa-reply"></i>К странице <?php echo $this->user->name?></a>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu'); ?>

				<div class="tab visible">

<?php if ($this->isOwn/* && $_GET['albumId']!=0*/):?>
<div style="margin-bottom:15px;">
<a href="<?=CHtml::normalizeUrl(array('photos/upload', 'albumId'=>Yii::app()->request->getQuery('albumId', 0))); ?>" class="btn2 btn-default btn-lg btn-block"><i class="fa fa-camera"></i>Добавить новые фотографии</a>
</div>
<?php endif;?>

<?php if (!isset($_GET['albumId']) && !isset($_GET['page']) && $this->albums):?>

<div style="margin-bottom:15px;margin-top:0px;font-weight:bold;">Альбомы</div>
<div class="photos-list albums-list">
		<?php foreach($this->albums as $key=>$row): ?>
		<div class="row<?php echo $key%2  == 0 ? ' first' : '';?>">
		<a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->user_id, 'albumId'=>$row->id)); ?>"><img src="<?php echo $row->photo->filename ? ImageHelper::thumb(350, 200, $row->photo->user_id, $row->photo->filename, array('method' => 'adaptiveResize')) : Html::imageUrl('empty_album.png');?>" width="350" height="200" border="0" /></a>
		<span><a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->user_id, 'albumId'=>$row->id)); ?>"><?=$row->name?></a><?php if ($this->isOwn):?><a href="<?=CHtml::normalizeUrl(array('photos/upload', 'albumId'=>$row->id)); ?>" class="right" style="cursor: pointer"><i class="fa fa-upload"></i></a><?php if (!$row->system):?><a href="<?=CHtml::normalizeUrl(array('photos/editAlbum', 'id'=>$row->id)); ?>" class="right" style="cursor: pointer"><i class="fa fa-gear"></i></a><?php endif;?><?php endif;?></span>
		</div>
		<?php endforeach; ?>
</div>


    <a href="<?=CHtml::normalizeUrl(array('photos/allAlbums', 'userId'=>$_GET['userId'])); ?>" class="right">Показать все альбомы</a>

    <div style="margin-bottom:15px;"></div>
<?php endif;?>


<div style="margin-bottom:15px;font-weight:bold;">Фотографии</div>
<div class="photos-list">
		<?php foreach($photos as $key=>$row): ?>
		<div id="i<?=$row->id; ?>" class="row<?php echo $key%4  == 0 ? ' first' : '';?>">
		<a href="<?=$row->normalizeUrl; ?>" id="showModalPhotoBox"><img src="<?php echo ImageHelper::thumb(165, 120, $row->user_id, $row->filename, array('method' => 'adaptiveResize'));?>" border="0" /></a>
		
		</div>
		<?php endforeach; ?>
</div>
<script>
jQuery(function($) {
$("#showModalPhotoBox[href='<?=Yii::app()->getRequest()->getUrl()?>']").click();
});
</script>

<div class="clearfix">
<?php $this->widget('CLinkPager', array(
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
			</div>	
		</div>