
<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="alert alert-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>




<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
            'beforeValidate'=>'js:function(){

                return true;
             }',
           'afterValidate' => 'js: function(form, data, hasError) {
if (!hasError) {
        $.ajax({
          type: "POST",
          url: $("#registerForm").attr("action"),
          data: $("#registerForm").serialize(),
	  dataType: "json",
	beforeSend: function(){
	$("#registerForm .btn").attr("disabled","disabled").fadeTo(1, 0.4);
			$("#loading_stat").show();
		},
          success: function(data){
	  if (data.redirect) {
	  location= data.redirect;
	  
	  return;
	  }
	  
	  
	  $("#loading_stat").hide();
	  $(".flash-success").fadeTo(\'speed\', 1.0);
	  

	  $("#registerForm .btn").removeAttr("disabled").fadeTo("speed", 1.0);
	  setTimeout("$(\'.flash-success\').fadeTo(\'speed\', 0.4);", 1500);

	  }

        });
        return false;
        }}'
            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),

)); ?>
<?php echo CHtml::errorSummary($model);?>
<div class="alert alert-success flash-success" style="display:none;">
	Изменения сохранены.
</div>
						<div class="inputs">
<?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>
		<?php echo $form->textField($model, 'name',array('class'=>'txt-field')); ?>
		<?php echo $form->error($model,'name'); ?>
						</div>	
						<div class="inputs">
<?php echo $form->labelEx($model,'descr',array('class'=>'control-label')); ?>
		<?php echo $form->textArea($model, 'descr',array('class'=>'textarea', 'style'=>'width:295px;')); ?>
		<?php echo $form->error($model,'descr'); ?>
						</div>	
						<div class="inputs">
<?php echo $form->labelEx($model,'access_to_view',array('class'=>'col-lg-3 control-label')); ?>

		<?php echo $form->dropDownList($model, 'access_to_view', array(
			'all'=>'Все пользователи',
			'friends'=>'Только друзья',
		),array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'access_to_view'); ?>
						</div>	
						<div class="inputs">
<?php echo $form->labelEx($model,'access_to_comment',array('class'=>'col-lg-3 control-label')); ?>
		<?php echo $form->dropDownList($model, 'access_to_comment', array(
			'all'=>'Все пользователи',
			'friends'=>'Только друзья',
		),array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'access_to_comment'); ?>
						</div>	

						<div class="inputs">
<?php echo $form->labelEx($model,'censor',array('class'=>'col-lg-3 control-label', 'label'=>'')); ?>
<?php echo $form->checkBox($model, 'censor',array('id'=>'censor', 'disabled'=>!$model->isNewRecord)); ?> <label for="censor">Материалы для взрослых</label>
		<?php echo $form->error($model,'censor'); ?>
						</div>	
		<?php /*if (!$model->isNewRecord) $this->widget('bootstrap.widgets.TbButton', array(
    'type'=>'danger',
    'buttonType'=>'link',
    'label'=>'Удалить альбом',
    'url'=>array('settings/remove'),
    'icon'=>'remove',
    //'block'=>true,
));*/ ?>

						<div class="inputs">
							<input type="submit" value="Сохранить" class="btn standart" /> <span id="loading_stat" style="display:none;"><i class="fa fa-cog fa-spin" style="margin-top:6px;font-size:20px;color:#246C9E;"></i></span>
						</div>

<?php $this->endWidget(); ?>