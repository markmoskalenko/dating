<div class="clearfix" style="width:805px;" align="left">
    <div style="float:left; width:545px;">

					<div class="title">
						<h4 class="left">Ваш комментарий</h4>
					</div>	
    
<?php if (Yii::app()->user->id==$photo->user_id OR Yii::app()->user->model->isReal):?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'commentsForm',
    'htmlOptions'=>array('class'=>'form-horizontal'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
    'action'=>CHtml::normalizeUrl(array('photos/addComment', 'photoId'=>$photo->id))
)); ?>


					<div class="write-msg">
<?php echo $form->textArea($commentsForm,'message',array('style'=>'margin-bottom:3px;')); ?>
<?php echo $form->error($commentsForm,'message',array('class'=>'err')); ?>


						<div class="foot">
								<div id="errorSummary" class="alert alert-danger left" style="display:none; padding:4px;">
		</div>
							<input type="submit" value="ОТПРАВИТЬ" class="btn small right" onclick="photoComments.add($('#commentsForm')); return false;" />
							<span id="loading_add_comment" class="right" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>	
					</div>	


<?php $this->endWidget(); ?>



<?php elseif (Yii::app()->user->id!=$photo->user_id && !Yii::app()->user->isGuest):?>
					<div class="write-msg" style="background: #F3F9FC; padding:12px;">
						Писать комментарии могут только Реальные пользователи сайта. <a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>"><b>Подтвердить реальность</b></a>
					</div>
<?php else:?>
					<div class="write-msg" style="background: #F3F9FC;">
						Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать комментарий к фотографии.
					</div>
<?php endif;?>
<div class="wall-msgs" id="commentsRows">
<?php echo $this->getComments($photo->id);?>
</div>

    </div>
    <div style="float:left; margin-left:25px;">
		<div>
<?php if(Yii::app()->user->id==$photo->user_id && $photo->album_id==0):?>
<?php if($photo->approve):?>
		    <div class="" style="margin-bottom:10px;color:#247738;">Фото одобрено модератором <span class="glyphicon glyphicon-ok-sign" style="font-size:15px;"></span> </div>
<?php else:?>
		    <div class="" style="margin-bottom:10px;color:#B25900;">Фото еще не проверено модератором <i class="fa fa-clock-o"></i> </div>
<?php endif;?>
<?php endif;?>

		
</div>
		
					<div class="menu-wall0" style="margin-bottom:15px;">
						<ul class="friend-nav">
							<li>Отправитель:
							<br>
<?php
	echo CHtml::link(
		$photo->user->name, 
		$photo->user->url
	);
?>
							</li>
							
						</ul>
						<ul class="friend-nav">
							<li>Альбом:
							<br>
<?php
	echo CHtml::link(
		$photo->album->name ? $photo->album->name : 'Фотографии со страницы', 
		CHtml::normalizeUrl(array('photos/index', 'userId'=>$photo->user_id, 'albumId'=>$photo->album_id))
	);
?>
							</li>
							
						</ul>
                                                
					</div>
		
		
		<?php $this->widget('zii.widgets.CMenu',array(
			//'type'=>'list',
			'htmlOptions'=>array('class'=>'friend-nav'),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			'items'=>array(
				array('label'=>'<i class="fa fa-exclamation-triangle"></i>Пожаловаться', 'url'=>array('photos/abuse', 'id'=>$photo->id), 'visible'=>Yii::app()->user->id!=$photo->user_id),
				array('label'=>'<i class="fa fa-mail-reply-all"></i>Сделать аватаркой', 'url'=>array('photos/index', 'setAvatar'=>$photo->id), 'visible'=>Yii::app()->user->id==$photo->user_id && $photo->album_id==0, 'linkOptions'=>array('id'=>'setAvatar')),
				array('label'=>'<i class="fa fa-edit"></i>Редактировать', 'url'=>array('photos/editPhoto', 'id'=>$photo->id), 'visible'=>Yii::app()->user->id==$photo->user_id && $photo->album_id!=0, 'linkOptions'=>array('id'=>'editPhoto')),
				array('label'=>'<i class="fa fa-times"></i>Удалить', 'url'=>array('photos/index', 'delPhoto'=>$photo->id), 'visible'=>Yii::app()->user->id==$photo->user_id, 'linkOptions'=>array('id'=>'deletePhoto')),
				array('label'=>'<i class="fa fa-upload"></i>Загрузить оригинал на диск', 'url'=>$photo->imageSource, 'linkOptions'=>array('target'=>'_blank')),

			),
		)); ?>
    </div>
</div>