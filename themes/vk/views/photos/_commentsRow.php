<div class="recording-wall">
<?php foreach($comments as $row):?>

						<div class="cell" id="comment_<?=$row->id?>">
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'), '', array('class'=>'left')
		), 
		$row->user->url
	);
else:
	echo CHtml::image(Html::imageUrl('camera_s.png'), '', array('class'=>'left'));
endif;
?>
							<div class="cont">
								<span class="name"><?php
	//echo CHtml::image(Html::imageUrl($row->user->sex==2 ? 'user_female.png' : 'user_male.png'));
	//echo ' ';
	echo CHtml::link(
		$row->user->name ? $row->user->name : 'DELETED', 
		$row->user->url,
		array('rel'=>'ajax1')
	);
?></span>
								<?=Html::message($row->message)?>
							</div>	
							<div class="foot">
								<span class="date left"><?php echo Html::date($row->date); ?></span>
								
									<span class="comments right">
<?php if (Yii::app()->user->checkAccess('deleteComment', array('author_id'=>$row->user->id, 'own_id'=>$this->user->id))):?>

<a class="delete" title="удалить" rel="tooltip" href="#" onclick="photoComments.deleteComment('<?=CHtml::normalizeUrl(array('photos/index', 'delCommId'=>$row->id)); ?>'); return false;"><i class="fa fa-times"></i></a><?php endif; ?>
									</span>
							</div>	
						</div>
						
						
						



<?php endforeach; ?>



<? $this->widget('CLinkPager', array(
        'id'=>'commentsPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>



</div>