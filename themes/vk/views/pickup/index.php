<?php
$this->pageTitle =Yii::t(Yii::app()->language, 'Поднять страницу «наверх»');
$this->breadcrumbs=array(
	'Поднять страницу «наверх»'
);
?>

<div class="well alert-success">
	
<p>В поиске ваша страница находится на <?php echo Yii::app()->user->position; ?> месте</p>

<p>Поднятие «наверх» повысит привлекательность вашей страницы в 5 раз!</p>

</div>