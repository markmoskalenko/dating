<div class="title">
    <h3 class="left">Подтверждение адреса электронной почты</h3>
</div>
<!--<div class="subtitle">-->
<!--    <span>Введите свой E-mail указанный вами при регистрации, и вам будет выслан пароль.</span>-->
<!--</div>-->
<?php if ($model->email_confirm): ?>
    <p class="reg-form" style="text-align: center;padding: 20px 0">
        Ваш адрес электронной почты <?= $model->email ?> успешно подтвержден.
    </p>
<?php else: ?>
    <p class="reg-form" style="text-align: center;padding: 20px 0">
        При подтверждении адреса электронной почты <?= $model->email ?> произошла ошибка.
    </p>
<?php endif; ?>