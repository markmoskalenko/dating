			<div class="title">
				<h3 class="left">Вход на сайт</h3>
				<a href="<?=CHtml::normalizeUrl(array('user/register')); ?>" class="right extended-link">Регистрация</a>
			</div>	
			<div class="subtitle">
				<span>Добро пожаловать на сайт знакомств для взрослых! Для входа на свою страницу используйте форму авторизации ниже.</span>
			</div>	
		<div class="free-reg">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'auhtForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    //'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            //'validateOnChange'=>false,
            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),
)); ?>
<?php //echo CHtml::errorSummary($model);?>
				<div class="inputs">
					<?php echo $form->labelEx($model,'username', array('label' => 'Логин или Email', 'class'=>'control-label')); ?>
					<?php echo $form->textField($model,'username',array('class'=>'txt-field left')); ?>
					
				<?php echo $form->error($model,'username', array('style'=>'margin-left:212px;')); ?>
				</div>
				
				
				<div class="inputs">
					<?php echo $form->labelEx($model,'password', array('label' => 'Пароль', 'class'=>'control-label')); ?>
					<?php echo $form->passwordField($model,'password',array('class'=>'txt-field left')); ?>
					
				<?php echo $form->error($model,'password', array('style'=>'margin-left:212px;')); ?>
				</div>
				<div class="inputs">
					<input type="submit" value="Войти" class="btn standart" />
				</div>	
<?php $this->endWidget(); ?>
		</div>