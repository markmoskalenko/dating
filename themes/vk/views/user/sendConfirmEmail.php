<div class="title">
    <h3 class="left">Отправка сообщения подтверждения адреса электронной почты</h3>
</div>
<?php if ($result): ?>
    <p class="reg-form" style="text-align: center;padding: 20px 0">
        На Ваш адрес электронной почты <?= $model->email ?> отправлена ссылка подтверждения.
    </p>
<?php else: ?>
    <p class="reg-form" style="text-align: center;padding: 20px 0">
        Произошла ошибка при отправке сообщения.
    </p>
<?php endif; ?>