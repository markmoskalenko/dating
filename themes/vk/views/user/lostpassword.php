<div class="title">
    <h3 class="left">Восстановление пароля</h3>
</div>
<div class="subtitle">
    <span>Введите свой E-mail указанный вами при регистрации, и вам будет выслан пароль.</span>
</div>


<?php if ($email = Yii::app()->user->getFlash('sentemail')): ?>

    <p class="reg-form" style="text-align: center;padding: 20px 0">
        На Ваш email (<?php echo $email; ?>) отправлено письмо с паролем.<br><br>
        Если письмо не пришло то посмотрите в папке спам, иногда туда попадают письма с сайтов.
    </p>

<?php else: ?>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'lostpasswordForm',
        'htmlOptions' => array('class' => 'reg-form'),
        //'enableClientValidation'=>true,
        // 'enableAjaxValidation'=>true,
        'clientOptions' => array(//'validateOnSubmit'=>true,
        ),
    )); ?>
    <div class="registration-user-full">
        <div class="inputs">
            <?php echo $form->labelEx($model, 'email', array('label' => 'Ваш E-mail', 'class' => 'control-label')); ?>
            <?php echo $form->textField($model, 'email', array('class' => 'txt-field')); ?>

            <?php echo $form->error($model, 'email', array('style' => 'margin-left:212px;')); ?>
        </div>

        <div class="inputs">
            <div class="cupcha left">
                <?php echo $form->labelEx($model, 'verifyCode', array('label' => 'Введите код', 'class' => 'control-label')); ?>
                <div class="cupcha-box txt-right">
                    <?php $this->widget('CCaptcha', array('captchaAction' => 'user/captcha', 'clickableImage' => true, 'showRefreshButton' => false, 'imageOptions' => array('style' => 'cursor:pointer;border-radius:4px;', 'id' => 'captcha'))); ?>
                </div>
            </div>
            <?php echo $form->textField($model, 'verifyCode', array('class' => 'txt-field left', 'style' => 'width:100px;')); ?>
            <?php echo $form->error($model, 'verifyCode', array('style' => 'margin-left:212px;')); ?>
        </div>

        <div class="inputs">
            <input type="submit" value="Восстановить" class="btn standart"/>
        </div>

    </div>
    <?php $this->endWidget(); ?>

<?php endif; ?>
