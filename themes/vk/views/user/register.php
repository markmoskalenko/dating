			<div class="title">
				<h3 class="left">Создание новой анкеты</h3>
				<a href="<?=CHtml::normalizeUrl(array('user/login')); ?>" class="right extended-link">Войти если уже зарегистрирован</a>
			</div>	
			<div class="subtitle">
				<span>Регистрация осуществляется абсолютно БЕСПЛАТНО!</span>
			</div>	

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>true,
            'validateOnChange'=>true,
            'beforeValidate'=>'js:function(){
	   // $("#mydialog").click();
	  //$("#mydialog").dialog("open");
			//$("body").scrollTo( 0, 310 );
                                $("#registerForm input[type=image]").attr("disabled", "true");
                                //$("#registerForm input[type=image]").fadeTo("speed", 0.7);
                return true;
             }',
            'afterValidate'=>'js:function(form, data, hasError){
            
		 
	   
               // setTimeout(function(){
               //$("#registerForm input[type=image]").fadeTo("speed", 1.0);
                $("#registerForm input[type=image]").removeAttr("disabled");
                
                //}, 1000);
                return true;
             }',
            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),

)); ?>
<?php //echo CHtml::errorSummary($model);?>

						<div class="inputs">
							<?php echo $form->labelEx($model,'sex',array('label'=>'Ваш пол', 'class'=>'control-label')); ?>
							<?php echo $form->dropDownList($model, 'sex', array('1'=>'Парень', '2'=>'Девушка'),array('class'=>'')); ?>
							<?php echo $form->error($model,'sex'); ?>
						</div>	

				<div class="inputs">
 <?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>

<?php echo $form->textField($model, 'name', array('class'=>'txt-field')); ?>
<?php echo $form->error($model,'name', array('style'=>'margin-left:212px;')); ?>
				</div>	

				<div class="inputs">
 <?php echo $form->labelEx($model,'email',array('class'=>'control-label')); ?>

<?php echo $form->textField($model, 'email', array('class'=>'txt-field')); ?>
<?php echo $form->error($model,'email', array('style'=>'margin-left:212px;')); ?>
				</div>	

				<div class="inputs">
 <?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>

<?php echo $form->textField($model, 'password', array('class'=>'txt-field')); ?>
<?php echo $form->error($model,'password', array('style'=>'margin-left:212px;')); ?>
				</div>	

				<div class="inputs">
 <?php echo $form->labelEx($model,'confirm_password',array('class'=>'control-label')); ?>

<?php echo $form->textField($model, 'confirm_password', array('class'=>'txt-field')); ?>
<?php echo $form->error($model,'confirm_password', array('style'=>'margin-left:212px;')); ?>
				</div>	

				<div class="inputs">


 					<div class="cupcha left">
						 <?php echo $form->labelEx($model,'verifyCode',array('label'=>'Введите код', 'class'=>'control-label')); ?>
						<div class="cupcha-box txt-right">
							<?php $this->widget('CCaptcha', array('captchaAction' => 'user/captcha', 'clickableImage'=>true, 'showRefreshButton'=>false, 'imageOptions'=>array('style'=>'cursor:pointer;', 'id'=>'captcha'))); ?>

						</div>	
					</div>
 
 
<?php echo $form->textField($model, 'verifyCode', array('class'=>'txt-field', 'style'=>'width:100px;')); ?>
<?php echo $form->error($model,'verifyCode', array('style'=>'margin-left:212px;')); ?>
				</div>	


				
				<div class="inputs">
					<input type="submit" value="Зарегистрироваться" class="btn standart" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
				</div>	
<?php $this->endWidget(); ?>