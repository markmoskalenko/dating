<div class="title">
    <h3 class="left">Восстановление пароля</h3>
</div>



<?php if ($message = Yii::app()->user->getFlash('success')): ?>

    <p class="reg-form" style="text-align: center;padding: 20px 0">
        <?php echo $message; ?>
    </p>

<?php else: ?>

    <?php if (Yii::app()->user->getFlash('error')) { ?>
        <p><?php echo Yii::app()->user->getFlash('error'); ?></p>
    <?php }; ?>

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'changePasswordForm',
        'htmlOptions'=>array('class'=>'reg-form'),
    )); ?>
    <div class="registration-user-full">
        <?php echo $form->hiddenField($model,'user_id'); ?>
        <div class="inputs">
            <?php echo $form->labelEx($model,'password', array('class'=>'control-label')); ?>
            <?php echo $form->passwordField($model,'password',array('class'=>'txt-field')); ?>

            <?php echo $form->error($model,'password', array('style'=>'margin-left:212px;')); ?>
        </div>

        <div class="inputs">
            <?php echo $form->labelEx($model,'confirm_password', array('class'=>'control-label')); ?>
            <?php echo $form->passwordField($model,'confirm_password',array('class'=>'txt-field')); ?>

            <?php echo $form->error($model,'confirm_password', array('style'=>'margin-left:212px;')); ?>
        </div>

        <div class="inputs">
            <input type="submit" value="Восстановить" class="btn standart" />
        </div>

    </div>
    <?php $this->endWidget(); ?>

<?php endif; ?>