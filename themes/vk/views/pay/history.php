<?php
$this->pageTitle =Yii::t(Yii::app()->language, 'История операций');
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language, 'История операций')
);
?>

			<div class="title">
				<h3 class="left"><?php echo Yii::t(Yii::app()->language, 'История операций');?></h3>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu'); ?>

				<div class="tab visible">
<table class="table table-striped table-hover" cellspacing="1" cellpadding="0"> 
<thead>
  <tr>
    <th>Дата оплаты</th>
    <th>Сумма</th>
    <th>Операция</th>

  </tr>
</thead>

<tbody>
		<?php foreach($history as $row): ?>
		

  <tr class="light">
    <td><?=Html::date($row->date);?></td>
    <td><?=Yii::t('app','{n} монета|{n} монеты|{n} монет', $row->coins);?></td>
    <td><?=$row->comment_payment;?></td>

  </tr>
		
		<?php endforeach; ?>
</tbody>
</table>

			</div>	
		</div>