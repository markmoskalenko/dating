<?php
$lpx=array('0','40','80', '120');
Yii::app()->clientScript->registerCss('logo', 'a.logo { width:226px; height:38px; background-image:url("'.Html::imageUrl('logos.png').'"); background-position: 0px -'.$lpx[0].'px!important;}');
Yii::app()->clientScript->registerCssFile(Html::cssUrl('style.css?v6'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('select2-bootstrap.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('jquery.arcticmodal-0.3.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('themes/simple.css'));
Yii::app()->clientScript->registerCoreScript('jquery');
//Yii::app()->clientScript->registerCoreScript('form');
Yii::app()->clientScript->registerCoreScript('cookie');
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('main.js?v1'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.scrollTo-min.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.blockUI.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.form.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.arcticmodal-0.3.min.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.noty.packaged.min.js'));
if (Yii::app()->params['noty'] && !Yii::app()->user->isGuest) Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/socket.io.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.base64.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/ion.sound.js'));
if (Yii::app()->params['noty'] && !Yii::app()->user->isGuest) Yii::app()->clientScript->registerScriptFile(Html::jsUrl('YCNoty.js'));
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.lazyload.js'));
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.color.js'));
//Yii::app()->clientScript->registerCoreScript('jquery.ui');
//Yii::app()->bootstrap->register();
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/cusel-min-2.4.1.js'));
//Yii::app()->bootstrap->registerCoreScripts();
//Yii::app()->bootstrap->registerCoreCss();

Yii::app()->clientScript->registerCssFile(Html::cssUrl('noty.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('font-awesome.css'));
//Yii::app()->clientScript->registerCssFile(Html::cssUrl('bootstrap.css'));
//Yii::app()->clientScript->registerCssFile(Html::cssUrl('bootstrap-theme.css'));
//Yii::app()->clientScript->registerCssFile(Html::cssUrl('theme.css'));
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('bootstrap.min.js'));
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('bootstrap/js/holder.js'));

?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title><?php echo $this->pageTitle ? $this->pageTitle : 'Взрослая социальная сеть, знакомства для взрослых'; ?></title>
<script type='text/javascript'>if (window.parent.frames.length > 0) { window.top.location.href = 'http://<?=Yii::app()->request->domain?><?=Yii::app()->request->requestUri?>'; }</script>
</head>
<body>
<script type="text/javascript">
 function imageUrl(url){
  return '<?=Html::imageUrl('')?>'+url;
 }
 function isGuest(){
  return <?=Yii::app()->user->isGuest ? Yii::app()->user->isGuest : 0?>;
 }
 function userBalance(){
  return '<?=Yii::app()->user->balance?>';
 }
jQuery(function($) {
        $.ionSound({
            sounds: [
                "new_message",
                "noty"
            ],
            path: "<?=Yii::app()->baseUrl.'/themes/'.Yii::app()->theme->name?>/sounds/",
            multiPlay: true,
            volume: "1.0"
        });
<?php if(!Yii::app()->user->isGuest):?>
<?php if(Yii::app()->params['noty']):?>
	YCNoty.init('<?php echo md5(Yii::app()->user->id.'dsbetngaedgc')?>', '<?php echo base64_encode(Yii::app()->params['noty_hostClient'])?>');
<?php endif;?>
<?php if (Yii::app()->user->newMessagesCounter):?>
	$("#newMessagesCounter").show();
	$("#newMessagesCounter label").text('<?php echo Yii::app()->user->newMessagesCounter?>');
<?php endif;?>
<?php if (Yii::app()->user->requestFriendsCounter):?>
	$("#requestFriendsCounter").show();
	$("#requestFriendsCounter label").text('<?php echo Yii::app()->user->requestFriendsCounter?>');
<?php endif;?>
<?php if (Yii::app()->user->model->notifications):?>
	$("#notificationsCounter").show();
	$("#notificationsCounter label").text('<?php echo Yii::app()->user->model->notifications?>');
<?php endif;?>
<?php endif;?>
});
</script>
<div id="wrapper">
	<div id="header">
		<a href="<?=CHtml::normalizeUrl(array('site/index')); ?>" class="logo"></a>
		<?php $this->widget('Menu',array(
			'id'=>'nav',
			//'htmlOptions'=>array('class'=>'main-nav'),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
			'items'=>array(
				array('label'=>'<i class="fa fa-search"></i>Поиск', 'url'=>array('search/index'), 'linkOptions'=>array('class'=>'icn-dating')),
				array('label'=>'<i class="fa fa-star"></i>Топ-100', 'url'=>array('top/index'), 'linkOptions'=>array('class'=>'icn-top')),
				array('label'=>'<i class="fa fa-camera"></i>Фотографии', 'url'=>array('top/photos'), 'linkOptions'=>array('class'=>'icn-app')),
				array('label'=>'<i class="fa fa-gamepad"></i>Игры', 'url'=>array('apps/index'), 'linkOptions'=>array('class'=>'icn-app')),
				array('label'=>'<i class="fa fa-video-camera"></i>Видео чат', 'url'=>array('apps/play', 'id'=>'177'), 'linkOptions'=>array('class'=>'icn-app')),
				//array('label'=>'Видео', 'url'=>array('video/index'), 'linkOptions'=>array('class'=>'icn-travel')),
				//array('label'=>'Группы', 'url'=>array('groups/index'), 'itemOptions'=>array('class'=>'icn-groups-but')),
				//array('label'=>'Дневники', 'url'=>array('journals/index'), 'itemOptions'=>array('class'=>'icn-diaries-but')),
				array('label'=>'<i class="fa fa-user"></i>Создать анкету', 'url'=>array('user/register'), 'visible'=>Yii::app()->user->isGuest),
			),
		)); ?>
		<!--form action="#" class="form-search">
			<input type="text" class="search-field" value="Поиск" id="showModalRegisterBox" />
			<input type="submit" class="search-btn" value="" id="showModalRegisterBox" />
		</form-->	
	</div>
	<div id="content" class="clearfix">
	

<?php echo $content; ?>
	</div>
</div>	
<div id="footer">
	<div id="footer-wrapper">
		<a href="<?=CHtml::normalizeUrl(array('support/index')); ?>" class="support-link left">Служба поддержки</a>
		<div class="counter">
			<?php echo date("Y")?> &copy; Знакомства <?php echo ucfirst(Yii::app()->request->domain)?> | <a href="http://vl2v.biz/" target="_blank"><i class="fa fa-android"></i><i class="fa fa-apple"></i><i class="fa fa-windows"></i>Мобильный сайт</a>
			<div class="right" style="font-size:11px;">
<?php
      $memory = round(Yii::getLogger()->memoryUsage/1024/1024, 3);
      $time = round(Yii::getLogger()->executionTime, 3);
     // echo 'Memory: '.$memory.' МБ ';
      echo ''.$time.'с ';
      $dbStats = Yii::app()->db->getStats();
      echo 'DB: '.$dbStats[0].'('.round($dbStats[1], 5).'с)';
?>
			</div>
		</div>	
	</div>	
</div>
<?php //$this->widget('vc.widgets.RulesFooterWidget'); ?>
<script type="text/javascript">
 $(document).ready(function(){
  var time_zone = (new Date().getTimezoneOffset()/60)*(1);
  var dateoffset = time_zone*60*60*1000;

    $.cookie('time_zone', time_zone, {path: "/"});

    $("body").ajaxError(function(x,e, settings, exception) {
         YiiAlert.error(e.responseText, e.status);
    });
 });
</script>
</body>
</html>