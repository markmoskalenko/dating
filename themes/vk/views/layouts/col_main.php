<?php $this->beginContent('/layouts/main'); ?>
		<div id="leftcol">
<?php if (Yii::app()->user->isGuest): ?>
			<div class="registration">
				<h4><a href="<?=CHtml::normalizeUrl(array('user/register')); ?>" id="showModalRegisterBox">Регистрация на сайте</a></h4>
<?php $login=$this->beginWidget('LoginActiveForm', array(
    'action'=>CHtml::normalizeUrl(array('user/login')),
    'id'=>'auth6',
    'htmlOptions'=>array('class'=>'enter-site'),
)); ?>
				<div class="inputs">
                                        <?php echo $login->textField('username',array('class'=>'field', 'placeholder'=>'E-mail или логин')); ?>
				</div>	
				<div class="inputs">
					<?php echo $login->passwordField('password',array('class'=>'field', 'placeholder'=>'Пароль')); ?>
				</div>	
				<div class="inputs">
					<div class="checked left">
						<input type="checkbox" value="" id="remind" />
						<label for="remind">запомнить</label>
					</div>	
					<a href="<?=CHtml::normalizeUrl(array('user/lostpassword')); ?>" class="remind-link right">Забыли пароль?</a>
				</div>
				<div class="inputs">
					<input type="submit" value="ВОЙТИ" class="btn standart" />
				</div>
<?php $this->endWidget(); ?>

			</div>	
<?php else: ?>

			<div class="vertical-nav">
		<?php $this->widget('Menu',array(
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
                        'htmlOptions'=>array('class'=>'nav nav-pills nav-stacked'),
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
                'items'=>array(
			//array('label'=>'LIST HEADER'),
				array('label'=>'<i class="fa fa-home"></i>Моя Страница'.CHtml::link('Ред.',array('edit/index'), array('class'=>'right')), 'url'=>Yii::app()->user->model->url, 'icon'=>'home'),
				array('label'=>'<i class="fa fa-envelope"></i>Мои Cообщения <span class="count right" id="newMessagesCounter" style="display:none;">+<label>0</label></span>', 'url'=>array('messages/index'), 'icon'=>'envelope'),
				array('label'=>'<i class="fa fa-user"></i>Мои Друзья <span class="count right" id="requestFriendsCounter" style="display:none;">+<label>0</label></span>', 'url'=>array('friends/index', 'section'=>'requests')),
				array('label'=>'<i class="fa fa-camera"></i>Мои Фотографии', 'url'=>array('photos/index', 'userId'=>Yii::app()->user->id), 'itemOptions'=>array(), 'icon'=>'camera'),
				array('label'=>'<i class="fa fa-bell"></i>Уведомления<span class="count right" id="notificationsCounter" style="display:none;">+<label>0</label></span>', 'url'=>array('notifications/index'), 'itemOptions'=>array(), 'icon'=>'bell'),
				//array('label'=>'Мои Группы', 'url'=>array('groups/index', 'userId'=>Yii::app()->user->id), 'itemOptions'=>array()),
				//array('label'=>'<i class="fa fa-eye"></i>Мои Гости', 'url'=>array('guests/index'), 'icon'=>'eye-open'),
				array('label'=>'<i class="fa fa-star"></i>Мои Закладки', 'url'=>array('fave/index'), 'icon'=>'star-empty'),
				array('label'=>'<i class="fa fa-money"></i>Мой счёт <span class="count right">'.Yii::app()->user->balance.'</span>', 'url'=>array('pay/index')),
				array('label'=>'<i class="fa fa-youtube-play"></i>Мои Приложения', 'url'=>array('apps/index')),
				array('label'=>'<i class="fa fa-gear"></i>Мои Настройки', 'url'=>array('settings/index')),
				array('label'=>'<i class="fa fa-share"></i>Выйти', 'url'=>array('site/logout'), 'visible'=>!Yii::app()->user->isGuest, 'icon'=>'off white'),
				/*array('label'=>'Мои Настройки', 'url'=>array('settings/index'), 'items'=>array(
                    array('label'=>'ТОП 100 Девушки', 'url'=>'#'),
                    array('label'=>'ТОП 100 Парни', 'url'=>'#'),
                )),*/
				//array('label'=>'Получить Real-статус', 'url'=>array('real/index'), 'itemOptions'=>array('class'=>'fav'), 'visible'=>!Yii::app()->user->isGuest),
			),
		)); ?>
			</div>	
<?php endif; ?>

<?php /*if (!Yii::app()->user->isGuest && Yii::app()->user->model->isReal==1): ?>
			<div>
				<div class="title">
					<h4 class="left">Галерея лиц</h4>
				</div>
				
					<div class="block-spotlight">
<?php foreach(User::findSpotlight() as $key=>$row): ?>

<?php
if ($row->id):
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(60,75, 'camera_s.png'), '', array('class'=>'', 'width'=>'60', 'height'=>'75')
		).'<span style="font-size:11px;">'.Html::fio2Name($row->name, 10, '<br>').'</span>', 
		$row->url
	);
else:
	echo CHtml::link(
		CHtml::image(Html::imageUrl('camera_s.png'), '', array('class'=>'', 'width'=>'60', 'height'=>'75')
		).'DELETED', 
		'#', array('onclick'=>'YiiAlert.error(\'Пользователь удален.\', \'404\'); return false;')
	);
endif;
?>

<?php endforeach; ?>
                                        </div>
			</div>
<?php endif; */?>
<?php if (Yii::app()->user->model->isReal):?>
<?php $this->renderPartial('/layouts/_adv_block'); ?>
<?php endif;?>

<?php //if (Yii::app()->vc->optionsDomain->typePaymentRU!='mt'):?>
			<div class="chat">
				<div class="title">
					<h4 class="left">Чат</h4>
				</div>
				
				<div id="chat_block">
				<?php $this->renderPartial('/chat/_chatBlock'); ?>
				</div>
				
				<div class="footer">
					<a href="#" class="btn standart" onclick="$('#sendChatModal').arcticmodal();return false;">НАПИСАТЬ СЮДА</a>
				</div>

					<div class="g-hidden" style="display:none;">
						<div class="box-modal" id="sendChatModal" style="background: #F3F9FC;">
							<p style="font-weight:bold; color:#246C9E;"><i class="fa fa-pencil"></i>Написать сообщение в чат</p><div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>
							
							
 <?php if (Yii::app()->user->model->isReal):?>
 
 <?php if (Yii::app()->user->getTransactionsTypes('chat')>=Yii::app()->user->balance):?>
 
					<div class="about-service" style="width:500px;background: #fff; padding:12px; font-size:12px;">
						<p>Стоимость сообщения: <?php echo Yii::app()->user->getTransactionsTypes('chat');?></p>
						<p>У Вас не достаточно на счету монет, пожалуйста <a href="<?=CHtml::normalizeUrl(array('pay/index')); ?>"><b>пополните счет</b></a>.</p>

					</div>
<?php else:?>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'chatForm',
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
    'action'=>CHtml::normalizeUrl(array('chat/index'))
)); ?>
					<div class="write-msg" style="background: #fff;">	<div id="errorSummary" class="alert alert-danger" style="display:none;">
		</div>
						<?php echo $form->textArea(new Chat,'message',array('class'=>'', 'style'=>'')); ?>
						<div class="foot">

							<input type="submit" value="ОТПРАВИТЬ" class="btn small right" />
						</div>	
					</div>
<?php $this->endWidget(); ?>
<?php endif;?>

<?php elseif (!Yii::app()->user->isGuest):?>
				<div>
					<div class="about-service" style="background: #fff; padding:12px;">
						<p>Ваша анкета не подтверждена реальностью! Писать сообщения могут только Реальные пользователи сайта.</p>
						<a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>"><b>Подтвердить реальность</b></a>
					</div>
				</div>
<?php else:?>
				<div>
					<div class="about-service" style="background: #fff; padding:12px;">
						<p>Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать сообщение.</p>
					</div>
				</div>
<?php endif;?>

						</div>
					</div>	
							
			</div>
<?php //endif; ?>
<?/*
			<div class="leader">
				<div class="title">
					<h4 class="left">Лидер сайта</h4>
				</div>
				

			</div>	

			<div class="statistics">
				<div class="title">
					<h4 class="left">Статистика</h4>
				</div>	
				<ul>
					<li>
						<span class="name">Сейчас на сайте:</span>
						<span class="value" id="online_counter">29345</span>
					</li>	
					<li class="odd">
						<span class="name">Новых анкет:</span>
						<span class="value" id="counter_new_user">10034</span>
					</li>	
					<li>
						<span class="name">Всего анкет:</span>
						<span class="value" id="counter_allusers">7453323</span>
					</li>	
					<li class="odd">
						<span class="name">Фотографий:</span>
						<span class="value" id="counter_foto">17166680</span>
					</li>	
					<li>
						<span class="name">Видео-роликов:</span>
						<span class="value" id="counter_video">1217821</span>
					</li>	
					<li class="odd">
						<span class="name">Веб-камеры:</span>
						<span class="value" id="onlinecams_counter">1217821</span>
					</li>	
				</ul>	
			</div>
*/?>
		</div>
		<div id="maincol">
            <?php if(Yii::app()->session['coins']): ?>
                <div class="alert alert-success" id="warning" style="margin-bottom:0px;font-size:12px;">
                    <i class="fa fa-exclamation-triangle" style="font-size:15px;"></i>
                    <?= Yii::app()->session['coins'] ?>
                </div>
                <?php unset(Yii::app()->session['coins']); ?>
            <?php endif; ?>
            <?php if (!Yii::app()->user->isGuest && !Yii::app()->user->getEmailConfirm()): ?>
                <?php $oUserA = UserAuthorization::model()->find('user_id = :user', array('user'=>Yii::app()->user->id)) ?>
                <div class="alert alert-danger" id="warning" style="margin-bottom:0px;font-size:12px;">
                    <i class="fa fa-exclamation-triangle" style="font-size:15px;"></i>
                    Вам необходимо подтвердить адрес электронной почты. <be />
                    Послать ссылку для подтверджения на адрес <?= $oUserA->email ?> (<a href="/settings">изменить почту</a>).
                    <br>
                    <br>
                    <a href="<?= CHtml::normalizeUrl(array('user/sendConfirmEmail', 'userId' => Yii::app()->user->id)) ?>" class="btn standart">Отправить</a>
                </div>
            <?php endif; ?>
<?php echo $content; ?>

		</div>
		
		
<?php $this->endContent(); ?>