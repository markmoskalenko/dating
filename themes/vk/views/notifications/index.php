<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('gifts.js'));  ?>
<?php
$this->pageTitle='Уведомления';
$this->breadcrumbs=array(
	'Уведомления'
);
?>

			<div class="title">
				<h3 class="left">Уведомления</h3>
			</div>	
			<div class="section-tabs">
<?php if (!$notifications): ?>

<div style="margin:150px;" align="center">Здесь будут отображаться уведомления об новых комментариях, лайках, заявки в друзей и др.
</div>

<?php else: ?>	
				<div class="tab visible">
					<div class="recording-wall">
<?php foreach($notifications as $num=>$row): ?>

						<div class="cell">
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'), '', array('class'=>'left')
		), 
		$row->user->url
	);
	elseif ($row->from_userId=='0'):
	echo CHtml::image( 
			Html::imageUrl('gear_s.png'), '', array('class'=>'left')
		);
endif;
?> 
							<div class="cont">
								<span class="name"><?php
if ($row->user->id):
	//echo CHtml::image(Html::imageUrl($row->user->sex==2 ? 'user_female.png' : 'user_male.png'));
	//echo ' ';
	echo CHtml::link(
		$row->user->name, 
		$row->user->url,
		array('rel'=>'ajax1')
	);
	elseif (in_array($row->type, array(Notifications::PHOTO_APPROVED,Notifications::PHOTO_MOVED,Notifications::PHOTO_REMOVED))):
		echo '<span style="color:#246C9E;">Служба фотографий</span>';
	elseif ($row->from_userId=='0'):
		echo '<span style="color:#246C9E;">Администрация</span>';
	elseif ($row->from_userId!='0'):
		echo 'DELETED';
?>
<?php 
	endif;
?> 
<span style="font-size:11px; color:#959595;">
<?php if ($row->type=='1'):?><span><i class="fa fa-check-circle" style="color:#65BF63; font-size:15px;"></i></span><? endif;?>
<?php if ($row->type=='2'):?><span><i class="fa fa-share" style="color:#95B4C4; font-size:15px;"></i></span><? endif;?>
<?php if ($row->type=='3'):?><span><i class="fa fa-times-circle" style="color:#FF0000; font-size:15px;"></i></span><? endif;?>
<?php if ($row->type=='4'):?><span><i class="fa fa-gittip" style="color:#E4007D; font-size:15px;"></i>оценил<?=$row->user->sex==2 ? 'а' : ''?> вашу фотографию</span><? endif;?>
<?php if ($row->type=='5'):?><span><i class="fa fa-comment" style="color:#65BF63; font-size:15px;"></i>прокомментировал<?=$row->user->sex==2 ? 'а' : ''?> вашу фотографию</span><? endif;?>
<?php if ($row->type=='6'):?><span><i class="fa fa-gittip" style="color:#E4007D; font-size:15px;"></i>вы понравились</span><? endif;?>
<?php if ($row->type=='7'):?><span><i class="fa fa-plus-circle" style="color:#65BF63; font-size:15px;"></i>Принял<?=$row->user->sex==2 ? 'а' : ''?> вашу заявку в друзья</span><? endif;?>
<?php if ($row->type=='8'):?><span><i class="fa fa-minus-circle" style="color:#FF0000; font-size:15px;"></i>Отклонил<?=$row->user->sex==2 ? 'а' : ''?> вашу заявку в друзья</span><? endif;?>
<?php if ($row->type=='9'):?><span><i class="fa fa-times-circle" style="color:#FF0000; font-size:15px;"></i>Удалил<?=$row->user->sex==2 ? 'а' : ''?> вас из друзей</span><? endif;?>
<?php if ($row->type=='10'):?><span><i class="fa fa-gift" style="color:#E4007D; font-size:15px;"></i>подарил<?=$row->user->sex==2 ? 'а' : ''?> вам подарок</span><? endif;?>

</span>

</span>
<?php if ($row->giftId):?><a href="<?php echo CHtml::normalizeUrl(array('gifts/index', 'userId'=>$row->userId)); ?>" id="showModalGifts"><img src="<?php echo Html::imageUrl('gifts/gift_'.$row->giftId.'.png');?>" width="90" height="90" alt=""></a><? endif;?>
<?php if ($row->photoCommentId):?><p><?=$row->photoComment->message?></p><? endif;?>
<?php if ($row->text):?><p><?=$row->text?></p><? endif;?>
<?php 
	if ($row->photo->id):
?>
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->photo->getImage(100,100)
		), 
		CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->photo->user_id, 'photoId'=>$row->photo->id)),
		array('id'=>'showModalPhotoBox')
	);
?> 
<?php 
	endif;
?> 
							</div>	
							<div class="foot">
								<span class="date left"><i class="fa fa-clock-o"></i><?=$row->date?></span>
								
									<span class="comments right">

									</span>
							</div>	
						</div>







<?php endforeach;?>

					</div>
					
<div class="clearfix">
<?php $this->widget('CLinkPager', array(
        'id'=>'notifPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?></div>



			</div>
<?php endif; ?>	
		</div>