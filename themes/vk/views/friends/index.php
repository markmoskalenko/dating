<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('gifts.js'));  ?>
			<div class="title">
				<h3 class="left">Мои друзья</h3>
				<?php if ($user->name):?><a href="<?=CHtml::normalizeUrl(array($user->url)); ?>" class="right extended-link"><i class="fa fa-reply"></i>К странице <?php echo $user->name?></a><?php endif;?>
			</div>	
			<div class="section-tabs">
<?php $this->widget('Menu', array(
	'htmlOptions'=>array('class'=>'tabs'),
    'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'Все друзья', 'url'=>array('friends/index', 'userId'=>$_GET['userId']), 'linkOptions'=>array('rel'=>'ajax1')),
				array('label'=>'Друзья онлайн', 'url'=>array('friends/index', 'section'=>'online', 'userId'=>$_GET['userId']), 'linkOptions'=>array()),
				array('label'=>'Заявки в друзья <span>('.Yii::app()->user->requestFriendsCounter.')</span>', 'url'=>array('friends/index', 'section'=>'requests'), 'linkOptions'=>array(), 'visible'=>Yii::app()->user->requestFriendsCounter && $this->isOwn),
				array('label'=>'Отправленные заявки <span>('.$this->sentFriendsCounter.')</span>', 'url'=>array('friends/index', 'section'=>'sent'), 'linkOptions'=>array(), 'visible'=>$this->sentFriendsCounter && $this->isOwn),
			),
)); ?>

				<div class="tab visible">
<div class="friends-list">
    <div class="">
       <?= Yii::app()->user->getFlash('accept_message');?>
    </div>
<?php foreach($friends as $num=>$row):?>
				
					<div class="row<?php echo $num%4  == 0 ? ' first' : '';?>">
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(150,120, 'camera_c.png'),
			'',
			array('class'=>'ava')
		), 
		$row->user->url,
		array()
	);
else:
	echo CHtml::image( 
			Html::imageUrl('camera_c.png'),
			'',
			array('class'=>'ava')
		);
endif;
?>
<?php if ($row->user->isOnline):?>
<span class="f_online">
	<?php if (!$row->user->isMobile):?><i class="fa fa-circle"></i>Online<?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?>
	</span>
<?php endif;?>
<span class="ttl"><i class="fa fa-<?=$row->user->sex==2 ? 'fe' : ''?>male"></i><?php
echo '';
	echo CHtml::link(
		$row->user->name ? $row->user->name : DELETED, 
		$row->user->url,
		array('rel'=>'ajax1')
	);
?></span>


		<?php $this->widget('Menu',array(
			//'type'=>'list',
			'htmlOptions'=>array('class'=>'friend-nav'),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			'items'=>array(
				array('label'=>'<i class="fa fa-envelope"></i>Написать сообщение', 'url'=>array('messages/send', 'id'=>$row->user->id), 'linkOptions'=>array('id'=>'showModalSendMessage'), 'visible'=>Yii::app()->user->id!=$row->user->id),
				array('label'=>'<i class="fa fa-camera"></i>Смотреть фотографии', 'url'=>array('photos/index', 'userId'=>$row->user->id, 'albumId'=>0, 'photoId'=>$row->user->photoId), 'linkOptions'=>array('id'=>'showModalPhotoBox')),
				array('label'=>'<i class="fa fa-gift"></i>Отправить подарок', 'url'=>array('gifts/donate', 'id'=>$row->user->id), 'icon'=>'gift', 'itemOptions'=>array('id'=>'donateGifts'), 'visible'=>Yii::app()->user->id!=$row->user->id),
				array('label'=>'<i class="fa fa-times-circle"></i>Отменить', 'url'=>array('friends/index', 'section'=>'sent', 'cmd'=>'cancel', 'id'=>$row->user->id), 'icon'=>'minus-sign', 'visible'=>$_GET['section']=='sent', 'linkOptions'=>array('confirm'=>'Отменить заявку?')),
				array('label'=>'<i class="fa fa-check"></i>Принять', 'url'=>array('friends/index', 'section'=>'requests', 'cmd'=>'accept', 'id'=>$row->user->id), 'icon'=>'plus', 'visible'=>$_GET['section']=='requests'),
				array('label'=>'<i class="fa fa-times-circle"></i>Отклонить', 'url'=>array('friends/index', 'section'=>'requests', 'cmd'=>'decline', 'id'=>$row->user->id), 'icon'=>'minus-sign', 'visible'=>$_GET['section']=='requests', 'linkOptions'=>array('confirm'=>'Отклонить заявку?')),
				array('label'=>'<i class="fa fa-times-circle"></i>Убрать из друзей', 'url'=>array('friends/index', 'cmd'=>'delete', 'id'=>$row->friend_userId), 'icon'=>'remove', 'visible'=>($_GET['section']=='' OR $_GET['section']=='online') && $this->isOwn, 'linkOptions'=>array('confirm'=>'Убрать из друзей?')),
			),
		)); ?>
					</div>	

				

<?php endforeach; ?>
</div>


			<div class="clearfix" style="margin-top:0px;">
<?php $this->widget('CLinkPager', array(
        'id'=>'friendsPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
			</div>	
		</div>
