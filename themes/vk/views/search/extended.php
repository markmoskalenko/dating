<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo.js'));  ?>

<?php $this->pageTitle='Расширенный поиск';?>
<?php
$this->breadcrumbs=array('Расширенный поиск');
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'searchForm',
    'type'=>'horizontal',
    'method'=>'GET',
    'htmlOptions'=>array('class'=>''),
    'action'=>CHtml::normalizeUrl(array('search/extended'))
)); ?>

<fieldset>
 
    <legend>Знакомства</legend>

 <div class="control-group">
<label class="control-label">Цель знакомства</label>
<div class="controls">
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'exC[tg]',
  'value'=>$_GET['exC']['tg'],
  'data'=>ProfileField::getListData(3),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:93%','multiple'=>'multiple',
  ),
  'options'=>array(
    'placeholder'=>'выбрать',
    'allowClear'=>true,
    //'width'=>'element',
  ),
)); ?>

</div>
</div>

 <div class="control-group">
<label class="control-label">Знак зодиака</label>
<div class="controls">
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'exC[zodiac]',
  'value'=>$_GET['exC']['zodiac'],
  'data'=>array('1'=>'Овен', '2'=>'Телец', '3'=>'Близнецы', '4'=>'Рак', '5'=>'Лев', '6'=>'Дева', '7'=>'Весы', '8'=>'Скорпион', '9'=>'Стрелец', '10'=>'Козерог', '11'=>'Водолей', '12'=>'Рыбы'),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:93%','multiple'=>'multiple',
  ),
  'options'=>array(
    'placeholder'=>'выбрать',
    'allowClear'=>true,
    //'width'=>'element',
  ),
)); ?>

</div>
</div>

 <div class="control-group">
<label class="control-label">Есть ли дети</label>
<div class="controls">
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'exC[children]',
  'value'=>$_GET['exC']['children'],
  'data'=>array('1'=>'Нету', '2'=>'Детей нет, но хочет', '3'=>'Дети есть, живут вместе', '4'=>'Дети есть, живут порознь'),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:93%','multiple'=>'multiple',
  ),
  'options'=>array(
    'placeholder'=>'выбрать',
    'allowClear'=>true,
    //'width'=>'element',
  ),
)); ?>

</div>
</div>


    <legend>Сексуальные предпочтения</legend>

 <div class="control-group">
<label class="control-label">Как часто вы бы хотели заниматься сексом</label>
<div class="controls">
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'exC[sextime]',
  'value'=>$_GET['exC']['sextime'],
  'data'=>ProfileField::getListData(26),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:93%','multiple'=>'multiple',
  ),
  'options'=>array(
    'placeholder'=>'выбрать',
    'allowClear'=>true,
    //'width'=>'element',
  ),
)); ?>

</div>
</div>

<div class="form-actions">
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'success', 'icon'=>'search white', 'label'=>'Искать')); ?>
<span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
</div>

 
</fieldset>
<?php $this->endWidget(); ?>