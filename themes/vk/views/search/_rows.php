
			<div class="title">
				<h3 class="left"><?php if ($count):?><?=Yii::t('app','{n} person found|Found {n} people|Found {n} people', $count)?><? else:?><?=Yii::t('app','Your request returned no results')?>.<? endif;?></h3>
			</div>	
			<div class="participants">
				
<?php foreach($profiles as $num=>$row):?>
<?php //echo ($num%6 == 0) ? '<div class="row">' : ''; ?>
				
				
				<div class="user">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(104,138, 'camera_a.png'),
			'',
			array('class'=>'img-responsive')
		), 
		$row->url,
		array()
	);
?>
					<span class="name"><?php
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('class'=>'name-user')
	);
?></span>
					<span class="city"><?php echo $row->geo->city?></span>
					<span class="age"><?php echo Html::age($row->birthday)?></span>
                        <?php if($row->isOnline && !$row->isMobile):?><label class="online">&nbsp;</label>
			<?php elseif($row->isOnline && $row->isMobile):?><label class="mobile">&nbsp;</label><?php endif;?>
						<a href="<?php echo CHtml::normalizeUrl(array('messages/send', 'id'=>$row->id)); ?>" class="add-friend" id="showModalSendMessage" <?=Yii::app()->user->id==$row->id ? ' disabled="disabled"' : ''?>><i class="fa fa-envelope"></i>Сообщение</a>

					<?php if($row->isReal):?><label class="icn-real">&nbsp;</label><?php endif;?>
				</div>	
				
				
				
				


				
<?php //echo ($num%6 == 5) ? '</div>' : ''; ?>
<?php endforeach; ?>




			</div>
			
			<div class="clearfix" style="margin-top:-20px;">
<?php $this->widget('CLinkPager', array(
        'id'=>'searchPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>