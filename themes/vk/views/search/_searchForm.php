			<div class="title">
				<h3 class="left">Поиск<?php /*if (Yii::app()->vc->optionsDomain->typePaymentRU!='mt'):?> секс партнера<?php endif;*/ ?></h3>
				<!--a href="#" class="right extended-link">Расширенный поиск</a-->
			</div>	
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'searchForm',
    'method'=>'GET',
    'htmlOptions'=>array('class'=>'search-partner'),
    'action'=>CHtml::normalizeUrl(array('search/index'))
)); ?>
				<div class="inputs">
					<label class="inline">
						Я
					</label>	
					<?php echo CHtml::dropDownList('c[looking]', $_GET['c']['looking'] ? $_GET['c']['looking'] : Yii::app()->user->sex , array('2'=>'Девушка', '1'=>'Парень'), array('class'=>'short left', 'empty'=>'---'));?>	
					<label class="inline">
						Ищу
					</label>
					<?php echo CHtml::dropDownList('c[sex]', $_GET['c']['sex'], array('2'=>'Девушку', '1'=>'Парня'), array('class'=>'short left', 'empty'=>'---'));?>	
					<label class="inline">
						В возрасте
					</label>
					<?php echo CHtml::dropDownList('c[age_from]', $_GET['c']['age_from'], User::getRangedown(18,75), array('class'=>'short left', 'style'=>'width:50px;', 'empty'=>'---'));?>
					<label class="inline">
						до
					</label>
					<?php echo CHtml::dropDownList('c[age_to]', $_GET['c']['age_to'], User::getRangedown(18,75), array('class'=>'short left', 'style'=>'width:50px;', 'empty'=>'---'));?>
				</div>	
				<div class="inputs special-inputs">
					<span>Отобрать только:</span>
					
					<?php echo CHtml::checkBox('c[photo]', $_GET['c']['photo'], array('id'=>'withPhoto')); ?>
					<label for="withPhoto">с фото</label>

					<?php echo CHtml::checkBox('c[new]', $_GET['c']['new'], array('id'=>'newFaces')); ?>
					<label for="newFaces">новые лица</label>

					<?php echo CHtml::checkBox('c[online]', $_GET['c']['online'], array('id'=>'nowOnly')); ?>
					<label for="nowOnly">сейчас на сайте</label>

					<input type="checkbox" value="" id="nearYou">
					<label for="nearYou">рядом с вами</label>
	
					<?php echo CHtml::checkBox('c[real]', $_GET['c']['real'], array('id'=>'realStatus')); ?>
					<label for="realStatus">с Real - статусом</label>
				</div>	
				<div class="inputs">
					<div class="col">
						<label class="inline">
							Страна:
						</label>	
						<?php echo CHtml::dropDownList('c[country_id]', $_GET['c']['country_id'], CHtml::listData(GEOCountry::model()->findAll(), 'id_country', 'name'), array('class'=>'', 'empty'=>'Выберите страну'));?>
					</div>	
					<div class="col">
						<label class="inline">
							Регион:
						</label>	
						<?php echo CHtml::dropDownList('c[region_id]', $_GET['c']['region_id'], CHtml::listData(GEORegion::model()->findAll('id_country=:id_country', array('id_country'=>$_GET['c']['country_id'])), 'id_region', 'name'), array('class'=>'', 'empty'=>'Выберите регион'));?>
					</div>
					<div class="col">
						<label class="inline">
							Город:
						</label>	
						<?php echo CHtml::dropDownList('c[city_id]', $_GET['c']['city_id'], CHtml::listData(GEOCity::model()->findAll('id_region=:id_region AND id_country=:id_country', array('id_region'=>$_GET['c']['region_id'], 'id_country'=>$_GET['c']['country_id'])), 'id_city', 'name'), array('class'=>'', 'empty'=>'Выберите город'));?>
					</div>
				</div>	
				<input type="submit" value="ИСКАТЬ" class="btn standart" />

<?php $this->endWidget(); ?>

<?php

		$cs=Yii::app()->getClientScript();
		$jsonurl=CHtml::normalizeUrl(array('site/geo'));
		$script=<<<END
$("select#c_country_id").change(function() {

        id=$(this).attr('value');
	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "country_id="+id,
		cache: false,
		beforeSend: function(){
			$("#loading_result").show();
		},
		success: function(data) {
			$("#loading_result").hide();
                        var setoptions = '<option value="" selected="selected">Выберите регион</option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_region + '">' + data[i].name + '</option>';
				}
			}
			
			$("select#c_region_id").html(setoptions);
			/*$("select#region_id").select2({
				placeholder: "Выберите регион",
				allowClear: true
			});*/
			$("select#c_region_id").change();
                }  
	});
});

$("select#c_region_id").change(function() {
        id=$(this).attr('value');

	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "region_id="+id,
		cache: false,
		beforeSend: function(){
                       $("#loading_result").show();
		},
		success: function(data) {
			$("#loading_result").hide();
			var setoptions = '<option value="" selected="selected">Выберите город</option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_city + '">' + data[i].name + '</option>';
				}
			}
                        $("select#c_city_id").html(setoptions); 
			
			/*$("select#city_id").select2({
				placeholder: "Выберите город",
				allowClear: true
			});*/
                }  
	});
});

END;
		$cs->registerScript(__CLASS__.'#geo1', $script);
?>