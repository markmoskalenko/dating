<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));  ?>
<?php $script=<<<JS
$("#searchForm").change(function() {
	$("#searchForm").submit();
});
    
$("#searchForm").submit(function() {
        
$.ajax({
			type: "GET",
			url: $(this).attr('action'),
			data: $(this).serialize()+'&_a=getQuickSearch',
			cache: false,
			beforeSend: function(){
			   $("#loading_result").show();
			},
			success: function(html){
				$("#getQuickSearch").html(html);
				$("#loading_result").hide();
			}
		});
	return false;
});
$("#searchPages a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		data: '&_a=getQuickSearch',
		cache: false,
		beforeSend: function(){
		   $('#getQuickSearch').fadeTo(0, 0.5);
		},
		success: function(html){
			$("body").scrollTo( "#getQuickSearch", 100 );
			$("#getQuickSearch").html(html);
			$('#getQuickSearch').fadeTo(0, 1.0);
		}
	});
	return false;
});
JS;
Yii::app()->clientScript->registerScript('statistic', $script, CClientScript::POS_READY);?>
<?php $this->pageTitle='Поиск';?>
<?php
$this->breadcrumbs=array('Поиск');
?>

<?php $this->renderPartial('_searchForm', array()); ?>

<?php if (!Yii::app()->user->isGuest):?>
					<div class="search-about-service">
						<p>В поиске ваша страница находится на <strong id="position"><?php echo Yii::app()->user->position; ?></strong> месте.<br>Первый в поиске – это уникальная возможность привлечь к себе внимание и завести новые знакомства. </p>
						<a href="#" onclick="pickup.showModal('<?=CHtml::normalizeUrl(array('profile/pickup')); ?>'); return false;"><strong><i class="fa fa-level-up"></i>Стать первым</strong></a>
					</div>			
<?php endif;?>
					
<div id="getQuickSearch">
<?php echo $this->getQuickSearch(); ?>
</div>