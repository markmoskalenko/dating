<?php if (count($friends)>0): ?>

					<div class="title">
						<h4 class="left">Друзья<?php if ($online) echo ' online'?></h4>
						<a href="<?php echo CHtml::normalizeUrl(array('friends/index', 'userId'=>$this->user->id)); ?>" class="update-link right">Все</a>
					</div>	
					<div class="subtitle">
						<span class="left"><?=Yii::t('app','{n} друг|{n} друга|{n} друзей', $count)?></span>
					</div>


					<div class="friends">
<?php foreach($friends as $key=>$row): ?>

<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'), '', array('class'=>'')
		).'<span style="font-size:11px;">'.Html::fio2Name($row->user->name, 10, '<br>').'</span>', 
		$row->user->url
	);
else:
	echo CHtml::link(
		CHtml::image(Html::imageUrl('camera_s.png'), '', array('class'=>'')
		).'DELETED', 
		'#', array('onclick'=>'YiiAlert.error(\'Пользователь удален.\', \'404\'); return false;')
	);
endif;
?>

<?php endforeach; ?>
                                        </div>
<?php endif;?>
 