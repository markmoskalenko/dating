<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('upload.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('profile.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('gifts.js'));  ?>
<?php
//Yii::app()->clientScript->registerCssFile(Html::cssUrl('profile.css'));
//if ($this->user->theme) Yii::app()->clientScript->registerCssFile(Html::cssUrl('themes/'.$this->user->theme.'/style.css'));
//$this->breadcrumbs=array($this->user->name);
?>
<!-- рур!-->
<?php $this->renderPartial('_spotlight'); ?>
			<div class="title">
				<h3 class="left"><?=$this->user->name?></h3>
				
<?php if ($this->user->isOnline) {
	echo '<label class="right" style="font-size:11px;padding-right:5px;;padding-top:2px;color:#37BF00; font-weight:bold;">';
	if (!$this->user->isMobile) {
		echo '<i class="fa fa-circle"></i>Online';
	} else {
		echo '<i class="fa fa-mobile"></i>Mobile';
	}
	echo '</label>';
}
else {
	echo '<label class="right" style="font-size:11px;padding-right:5px;;padding-top:2px; font-weight:bold;color:#71A0B4;">';
	if ($this->user->isMobile) {
		echo '<i class="fa fa-mobile"></i>';
	}
    echo $this->user->sex==1 ? 'заходил ' : 'заходила ';
    echo Html::date($this->user->last_date).'</label>';
}
?>
			</div>	
			<div class="wall">
<?php if ($this->isOwn && !$this->user->isReal):?>
				<div>
					<div class="about-service">
						<p>Ваша анкета не подтверждена реальностью! Вы пользуетесь ограниченной версией сайта. Большинство функций будет доступно после <a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>">активации анкеты</a>.</p>
						<a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>">Получить Real - статус<?php if (Yii::app()->vc->optionsDomain->typePaymentRU=='mt'):?> - Отправка личных сообщений, комментировать фотографии, участие в играх, 5 монет на счёт сразу после подписки плюс по одной монете каждый день<?php endif; ?></a>
					</div>
				</div>
<?php endif;?>
				
				<div class="leftcol">
					<div class="face-user">
<?php if ($this->user->have_photo==1):?>
						<a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$this->user->id, 'albumId'=>0, 'photoId'=>$this->user->photoId)); ?>" id="showModalPhotoBox"><img src="<?php echo $this->user->have_photo==1 ? ImageHelper::thumb(180, 240, $this->user->id, $this->user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('photo-activation.jpg');?>" alt="" /></a>
						<?php if ($this->user->isReal):?><label class="icn-real">&nbsp;</label><?php endif;?>
<?php else:?>
	<?php if ($this->isOwn):?>
		<a href="#" onclick="$('#uploadPhotoModal').arcticmodal();return false;" class="noavatar"><i class="fa fa-camera"></i><div>Загрузить фотографию</div></a>
	<?php else:?>
		<div class="noavatar"><i class="fa fa-camera"></i></div>
	<?php endif;?>
<?php endif;?>
					</div>	
					<div class="menu-wall">
<?php if ($this->isOwn):?>
						<ul>
							<li><a href="#" onclick="$('#uploadPhotoModal').arcticmodal();return false;"><strong><i class="fa fa-camera"></i>Загрузить фотографию</strong></a></li>
							<li><a href="<?=CHtml::normalizeUrl(array('edit/index')); ?>"><strong><i class="fa fa-pencil-square-o"></i>Редактировать страницу</strong></a></li>
							<li><a href="<?=CHtml::normalizeUrl(array('profile/like', 'userId'=>$this->user->id)); ?>" id="showModalLikes">Вы понравились<label class="count right"><b><i class="fa fa-heart"></i><?=$this->user->likes?></b></label></a></li>
						</ul>
<?php $this->renderPartial('_uploadPhoto'); ?>
<?php elseif (!Yii::app()->user->isGuest):?>
					<div class="menu-wall">
						<a href="<?php echo CHtml::normalizeUrl(array('messages/send', 'id'=>$this->user->id)); ?>" class="btn small sendmessage" id="showModalSendMessage"><i class="fa fa-envelope"></i>Отправить сообщение</a>

<?php if (!$this->isFriend):?>

					<div id="friendshipRequest"><a href="<?php echo CHtml::normalizeUrl(array('friends/apply', 'id'=>$this->user->id)); ?>" class="btn small sendmessage" ><i class="fa fa-plus"></i>Добавить в друзья</a></div>
<?php endif;?>
<?php if ($this->isFriend): ?>
<div style="padding:10px;margin-bottom:5px;background:#F7F7F7;color:#626262;text-align:center; font-size:11px;"><?=$this->user->name?> у Вас в друзьях</div>
<?php endif;?>
			
		<?php $this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
                        'htmlOptions'=>array('class'=>'nav nav-pills nav-stacked', 'style'=>'margin-top:5px;margin-bottom:5px;'),
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
			'items'=>array(
				array('label'=>'<i class="fa fa-gift right"></i>Отправить подарок', 'url'=>array('gifts/donate', 'id'=>$this->user->id), 'icon'=>'gift', 'itemOptions'=>array('id'=>'donateGifts')),
				array('label'=>$this->user->sex==2 ? '<i class="fa fa-heart right"></i>Понравилась' : '<i class="fa fa-heart right"></i>Понравился', 'url'=>array('profile/addlike', 'userId'=>$this->user->id), 'icon'=>'heart', 'itemOptions'=>array('id'=>'addlike'), 'visible'=>!$this->isLike),
				array('label'=>'<label class="count right"><i class="fa fa-camera"></i><b>'.$this->user->counter->photos.'</b></label>Фотографии со мной', 'url'=>array('photos/index', 'userId'=>$this->user->id), 'icon'=>'camera'),
			)
		)); ?>	
					</div>
<?php else:?>
<div style="padding:10px;margin-bottom:10px;background:#F7F7F7;color:#626262;text-align:center; font-size:11px;">Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать сообщение.</div>
<?php endif;?>	
						
	
					</div>	
<?php $this->getGifts('3'); ?>

<?php $this->getFriends('6'); ?>

<?php $this->getFriends('6', true); ?>

<?php $this->getAlbums('2'); ?>

<?php if (!Yii::app()->user->isGuest && !$this->isOwn):?>

					<div class="menu-wall">
		<?php $this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
			'items'=>array(
				
				array('label'=>'<i class="fa fa-times"></i>Убрать из друзей', 'url'=>array('friends/index', 'cmd'=>'delete', 'id'=>$this->user->id), 'icon'=>'trash', 'visible'=>$this->isFriend),
				array('label'=>Blacklist::checkBlackByUser($this->user->id)->id ? '<i class="fa fa-check-circle"></i>Разблокировать' : '<i class="fa fa-ban"></i>Заблокировать', 'url'=>array('settings/blacklist', 'bId'=>$this->user->id), 'itemOptions'=>array('id'=>'AddToBlacklist')),
				array('label'=>'<i class="fa fa-warning"></i>Пожаловаться', 'url'=>array('abuse/index', 'userId'=>$this->user->id), 'icon'=>'warning-sign'),

				
                    array('label'=>'<span class="glyphicon glyphicon-remove"></span> Удалить', 'url'=>'#', 'visible'=>Yii::app()->user->role=='admin'),
                    array('label'=>'<span class="glyphicon glyphicon-ban-circle"></span> Забанить', 'url'=>'#', 'visible'=>Yii::app()->user->role=='admin'),

				
			),
		)); ?>
			
					</div>	
<?php endif;?>
				</div>	
				<div class="rightcol">
					
					<div class="data-user">

<?php if ($this->isOwn):?>
						
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'statusForm',
    'htmlOptions'=>array('style'=>'display:none;position:absolute;z-index:3'),
)); ?>
					<div class="write-msg" style="box-shadow: 0px 0px 3px #ccc, 0 10px 15px #fff inset;">
						<?php echo $form->textArea($this->user->profile,'status',array('placeholder'=>'Введите ваш статус', 'style'=>'resize:none; height:30px;')); ?>
						<div class="foot">

							<input type="submit" value="Сохранить" onclick="updateStatus($('#statusForm')); return false;" class="btn small right" />
						</div>	
					</div>
<?php $this->endWidget(); ?>
						
						

						<div style="margin-bottom:10px;margin-top:0px;font-size:11px;">
							<a href="#" id="change_status"<?php echo $this->user->profile->status ? ' style="color:#000;"' : '';?>><?php echo $this->user->profile->status ? $this->user->profile->status : 'изменить статус';?></a>
						</div>
<?php elseif ($this->user->profile->status):?>
						<div style="margin-bottom:10px;margin-top:0px;font-size:11px;">
							<?=$this->user->profile->status?>
						</div>
<?php endif;?>
						<ul>
							<li>
								<label>Имя:</label>
								<span><?=$this->user->name?></span>
							</li>		
							<li>		
								<label>Пол:</label>
								<span><?php echo $this->user->sex=='2' ? 'Женский' : 'Мужской';?></span>
							</li>	
							<?php if (Html::age($this->user->birthday)):?>
							<li>									
								<label>Возраст:</label>
								<span><?php echo Html::age($this->user->birthday).', '.Html::zodiak($this->user->birthday);?></span>
							</li>
							<?php endif;?>
							<?php if ($this->user->geo->country):?>
							<li>		
								<label>Город:</label>
								<span><?php echo $this->user->geo->country;?>, <?php echo $this->user->geo->city;?></span>
							</li>
							<?php endif;?>
						</ul>	
						<a href="#" id="showFull_information">Показать подробную информацию</a>		
					</div>
<div class="data-user" id="full_information" style="display:none;margin-top:15px;">
						
<?php foreach($this->profileFields as $row): ?>
<?php if ($this->isOwn OR count($row[field])):?>

					<div class="subtitle">
						<span class="left"><?php echo $row[title]?></span>
						<?php if ($this->isOwn):?><a href="<?php echo CHtml::normalizeUrl(array('edit/profile', 'section'=>$row[name])); ?>" class="update-link right">Редактировать</a><?php endif;?>
					</div>	
						<ul>
	<?php foreach($row[field] as $row): ?>
							<li>
			<label><?php echo $row[title]?></label>
			<span><?php echo $row[value]?></span>
							</li>
	<?php endforeach; ?>
	
						</ul>
<?php endif;?>
<?php endforeach; ?>
</div>
<?php $this->getPhotos('8'); ?>	
					
					<div class="title" id="wall_widget">
						<h4 class="left">Стена</h4>
					</div>
<?php if (/*$this->isOwn OR */Yii::app()->user->model->isReal):?>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'wallForm',
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
    'action'=>$this->user->url,
)); ?>
					<div class="write-msg">
						<?php echo $form->textArea($wallForm,'message',array('class'=>'', 'style'=>'')); ?>
						<div class="foot">
	<div id="errorSummary" class="alert alert-danger" style="display:none;">
		</div>
							<input type="submit" value="ОТПРАВИТЬ" class="btn small right" />
						</div>	
					</div>
<?php $this->endWidget(); ?>
<?php elseif (/*!$this->isOwn && */!Yii::app()->user->isGuest):?>
					<div class="about-service" style="background: #F3F9FC; padding:12px; font-size:11px;">
						<p>Ваша анкета не активна! Писать сообщения на стене могут только активные пользователи сайта с статусом Реал.</p>
						<a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>"><b>Подтвердить реальность Вашей анкеты с помощью активации Реал статуса!</b></a>
					</div>
<?php else:?>
					<div class="about-service" style="background: #F3F9FC; padding:12px; font-size:11px;">
						Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать на стене сообщение.
					</div>
<?php endif;?>
					<div class="recording-wall" id="content_wall">
<?php $this->renderPartial('wall'); ?>

					</div>	

				</div>	
			</div>
			
<?php if (!$this->user->have_photo && $this->isOwn):?>
<script type="text/javascript">
jQuery(function($) {
$('#no_photo').arcticmodal();
});
</script>
					<div class="g-hidden" style="display:none;">
						<div class="box-modal" id="no_photo">
							<p style="font-weight:bold; color:#246C9E;">Уважаем<?php echo $this->user->sex==1 ? 'ый' : 'ая'?> <?=$this->user->name?>!</p><div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>

        <div class="modal-body">
<div style="margin-bottom:7px;text-align:center;">Пользователи не видят вас в результатах поиска.</div>

<div style="margin-bottom:7px;text-align:center; font-weight:bold;">В вашей анкете нет ни одной фотографии.</div>

<div style="margin-bottom:7px;text-align:center; font-weight:bold;">Чтобы начать знакомиться и просматривать анкеты необходимо загрузить хотябы одну фотографию.</div>
<div style="text-align:center;">
<a href="#" class="btn standart" onclick=" $.arcticmodal('close');$('#uploadPhotoModal').arcticmodal();return false;">Загрузить фото</a>
</div>

        </div>

						</div>
					</div>
<?php endif;?>