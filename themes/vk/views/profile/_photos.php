<?php if (count($photos)>0): ?>

					<div class="title">
						<h4 class="left"><?=Yii::t('app','{n} фотография|{n} фотографии|{n} фотографий', $this->user->counter->photos)?></h4>
						<a href="<?php echo CHtml::normalizeUrl(array('photos/index', 'userId'=>$this->user->id)); ?>" class="right addphotos-link">Все</a>
					</div>

					<div class="photos">

		<?php foreach($photos as $row): ?>
		
    <a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->user_id, 'photoId'=>$row->id)); ?>" id="showModalPhotoBox"><img src="<?php echo ImageHelper::thumb(61, 72, $row->user_id, $row->filename, array('method' => 'adaptiveResize'));?>" alt=""></a>
		
		
		<?php endforeach; ?>

					</div>
<?php endif;?>