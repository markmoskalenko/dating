			<div class="title">
				<h3 class="left">Популярные приложения </h3>
			</div>	
				
				
			<div class="apps clearfix">
<?php foreach($apps as $row):?>
			
			<div class="app-item">
			<a href="<?=CHtml::normalizeUrl(array('apps/play', 'id'=>$row['id'])); ?>"><img src="<?php echo $row['logo'];?>" alt="<?php echo $row['description'];?>" title="<?php echo $row['description'];?>"></a>
			<div class="app-name"><a href="<?=CHtml::normalizeUrl(array('apps/play', 'id'=>$row['id'])); ?>"><?php echo $row['title'];?></a></div>
			</div>
			
			
<?php endforeach;?>
			</div>