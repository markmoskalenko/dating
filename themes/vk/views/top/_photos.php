<div class="photosrating-list clearfix">
		<?php foreach($photos as $key=>$row): ?>
		<div id="i<?=$row->id; ?>" class="row<?php echo $key%4  == 0 ? ' first' : '';?>">
		<a href="<?php echo CHtml::normalizeUrl(array('top/photos', 'userId'=>$row->user_id, 'photoId'=>$row->id)); ?>" id="showModalPhotoBox" data-params="top=likes" data-key="<?=$key?>"><img src="<?php echo ImageHelper::thumb(220, 270, $row->user_id, $row->filename, array('method' => 'adaptiveResize'));?>" border="0" width="220" height="270" class="photorating-img" /></a>
		<span class="photosrating-likes"><label class="count right"><a href="<?php echo CHtml::normalizeUrl(array('photos/like', 'id'=>$row->id))?>" id="showModalLikePhoto"><i class="fa fa-heart"></i><?=$row->likes?></a></label></span>
		
<div class="photosrating-user clearfix">
		<div class="left photosrating-ava">
	<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(50,50, 'camera_s.png'),
			'',
			array('class'=>'img-responsive', 'width'=>'36', 'height'=>'36')
		), 
		$row->user->url,
		array()
	);
?>
</div>
<div class="left photosrating-name">
					<?php
	echo CHtml::link(
		$row->user->name, 
		$row->user->url,
		array('class'=>'name-user')
	);
?>

<div><?php echo Html::age($row->user->birthday)?>, <?php echo $row->user->geo->city?></div>

</div>	
</div>



</div>
		<?php endforeach; ?>
</div>
<script>
jQuery(function($) {
$("#showModalPhotoBox[href='<?=Yii::app()->getRequest()->getUrl()?>']").click();
});
</script>
<script type="text/javascript">
 $(document).ready(function(){
  photoShow.pageStart=<?=$_GET['page'] ? $_GET['page'] : 1?>;
 });
</script>
<div class="clearfix">
<?php $this->widget('CLinkPager', array(
        'id'=>'photoPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?></div>