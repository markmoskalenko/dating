<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>
<?php $this->pageTitle='Фото';?>
			<div class="title">
				<h3 class="left">Фотографии</h3>
			</div>	
			<div class="section-tabs">
<?php //$this->renderPartial('_menu'); ?>

				<div class="tab visible">
<div id="content_photos">

<?php $this->renderPartial('_photos', array('photos'=>$photos, 'pages'=>$pages)); ?>
</div>

<script type="text/javascript">
 $(document).ready(function(){
$("#photoPages a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		data: '&_a=photo',
		cache: false,
		beforeSend: function(){
		   $('#content_photos').fadeTo(0, 0.5);
		},
		success: function(html){
			$("body").scrollTo( "body", 100 );
			$("#content_photos").html(html);
			$('#content_photos').fadeTo(0, 1.0);
		}
	});
	return false;
});

 });
</script>
			</div>	
		</div>