<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));  ?>
			<div class="title">
				<h3 class="left">Топ 100 участников</h3>
			</div>	
			<div class="participants">
				
<div style="margin-bottom:15px; margin-left:17px;margin-right:10px; ">
<a href="#" onclick="pickup.showModal('<?=CHtml::normalizeUrl(array('profile/pickup')); ?>'); return false;" class="btn2 btn-default btn-lg btn-block"><i class="fa fa-arrow-up"></i>Стать первым</a>
</div>
				
				
<?php foreach($profiles as $num=>$row): ?>
				<div class="user">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(104,138, 'camera_a.png'),
			'',
			array('class'=>'img-responsive')
		), 
		$row->url,
		array()
	);
?>
					<span class="name"><?php
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('class'=>'name-user')
	);
?></span>
					<span class="city"><?php echo $row->geo->city?></span>
					<span class="age"><?php echo Html::age($row->birthday)?></span>
                        <?php if($row->isOnline && !$row->isMobile):?><label class="online">&nbsp;</label>
			<?php elseif($row->isOnline && $row->isMobile):?><label class="mobile">&nbsp;</label><?php endif;?>
						<a href="<?php echo CHtml::normalizeUrl(array('messages/send', 'id'=>$row->id)); ?>" class="add-friend" id="showModalSendMessage" <?=Yii::app()->user->id==$row->id ? ' disabled="disabled"' : ''?>><i class="fa fa-envelope"></i>Сообщение</a>
					<?php if($row->isReal):?><label class="icn-real">&nbsp;</label><?php endif;?>
				</div>	
<?php endforeach;?>
			</div>
<div class="clearfix">
<?php $this->widget('CLinkPager', array(
        'id'=>'photoPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?></div>