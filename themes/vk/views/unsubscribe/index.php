			<div class="title">
				<h3 class="left">Управление подпиской на Контент</h3>
			</div>	

<?php if(Yii::app()->user->hasFlash('ok')): ?>

<div class="flash-success">
	Подписка остановлена.
	
</div>

<?php else: ?>



			<div class="registration-user-full">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'supportForm',
    'htmlOptions'=>array('class'=>'reg-form'),

)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>

								<div class="inputs" style="padding:5px;">
            <div style="font-size: 12px;line-height: 12px;">
                <p style="font-size: 12px;">Для остановки подписки отправьте СМС сообщение:</p>
                <p style="font-size: 12px;">Для абонентов Билайн сообщение STOP на номер 7355</p>
                <p style="font-size: 12px;">Для абонентов Мегафон сообщение СТОП 4881 на номер 5051</p>
                <p style="font-size: 12px;">Для абонентов МТС сообщение СТОП на номер 770585</p>
            </div>
							</div>
	
	
							<div class="inputs">
								<?php echo $form->labelEx($model,'phone', array('class'=>'control-label')); ?>
								
										<?php echo $form->textField($model,'phone',array('class'=>'txt-field', 'placeholder'=>'Пример: +79056543201')); ?>
									
									<?php echo $form->error($model,'phone'); ?>
								
							</div>
							<div class="inputs">
								<?php echo $form->labelEx($model,'email', array('class'=>'control-label')); ?>
								
										<?php echo $form->textField($model,'email',array('class'=>'txt-field')); ?>
									
									<?php echo $form->error($model,'email'); ?>
								
							</div>

							<div class="inputs">
								<?php echo $form->labelEx($model,'message', array('class'=>'control-label')); ?>
								
										<?php echo $form->textArea($model,'message',array('class'=>'txt-field', 'style'=>'height:70px;')); ?>
									
									<?php echo $form->error($model,'message'); ?>
								
							</div>
							
				<div class="inputs">
					<input type="submit" value="Отписаться" class="btn standart" />
				</div>	
					
					
					
					
					
					

<?php $this->endWidget(); ?>


			</div>


<?php endif; ?>
