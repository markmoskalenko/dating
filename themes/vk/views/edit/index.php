<?php if (!Yii::app()->user->model->not_new): ?>
    <?php User::notNew(Yii::app()->user->id); ?>
    <?php $oUser = User::model()->find('id = :user', array('user'=>Yii::app()->user->id)) ?>
    <?php $oUserA = UserAuthorization::model()->find('user_id = :user', array('user'=>Yii::app()->user->id)) ?>
    <?php if (!$oUserA->email_confirm): ?>
        <div class="alert alert-danger" id="warning" style="margin-bottom:0px;font-size:12px;">
            <i class="fa fa-exclamation-triangle" style="font-size:15px;"></i>
            <strong>Дорогой, <?= $oUser->name ?>!</strong>
            <p>Для использования сайта в полном объеме, включая чат, а также Вашей безопасности, необходимо подтвердить адрес электронной почты. На данный момент вы указали адрес: <?= $oUserA->email ?></p>
            <p>После отправки проверьте почту в папке "Входящие" или "Спам" и кликните по ссылке для подтверждения.</p>
        </div>
    <?php endif; ?>
<?php endif; ?>
<?php if(!Yii::app()->user->model->name OR Yii::app()->user->model->birthday=='0000-00-00'):?>
    <div class="alert alert-danger" id="warning" style="margin-bottom:0px;font-size:12px;">
    <i class="fa fa-exclamation-triangle" style="font-size:15px;"></i>Чтобы продолжить работать  с сайтом укажите о себе основную информацию.
    </div>
<?php endif;?>
			<div class="title">
				<h3 class="left">Редактировать профиль</h3>
			</div>	
			<div class="section-tabs">
<?php $this->widget('Menu', array(
    'htmlOptions'=>array('class'=>'tabs'),
    'items'=>$this->menu,
)); ?>


				<div class="tab visible">
 <?php if(Yii::app()->user->hasFlash('edit')): ?>
<script type="text/javascript">
$().ready(function() {
 setTimeout("$('.flash-success').fadeTo('speed', 0.4);", 1500);
});
</script>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

    <div class="alert alert-success" style="display:none;" id="flash-success">
    Изменения сохранены.
    </div>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('CActiveForm', array(
    'id'=>'editForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
           'afterValidate' => 'js: function(form, data, hasError) {
if (!hasError) {
        $.ajax({
          type: "POST",
          url: $("#editForm").attr("action"),
          data: $("#editForm").serialize(),
	beforeSend: function(){
			$("#loading_stat").show();
		},
          success: function(data){
	  $("#loading_stat").hide();
	  $("#flash-success").fadeTo(\'speed\', 1.0);
	   $("body").scrollTo( 0, 310 );
	  setTimeout("$(\'#flash-success\').fadeTo(\'speed\', 0.4);", 1500);

	  }

        });
        return false;
        }}'
    ),
)); ?>

						<div class="inputs">
							<?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>
							<?php echo $form->textField($model, 'name', array('class'=>'txt-field')); ?>
							<?php echo $form->error($model,'name'); ?>
						</div>	

						<div class="inputs">
							<?php echo $form->labelEx($model,'sex',array('label'=>'Ваш пол:', 'class'=>'control-label')); ?>
							<?php echo $form->dropDownList($model, 'sex', array('1'=>'Парень', '2'=>'Девушка'),array('class'=>'')); ?>
							<?php echo $form->error($model,'sex'); ?>
						</div>	
						<div class="inputs">
<?php echo $form->labelEx($model,'birthday',array('label'=>'Дата рождения:', 'class'=>'control-label')); ?>

	
<?php $this->widget('EHtmlDateSelect',
                        array(
                              'time'=>$model->birthday,
                              'field_array'=>'User[ItemsBirthday]',
                              'prefix'=>'',
                              'field_order'=>'DMY',
                              'start_year'=>1940,
                              'end_year'=>date("Y")-14,
			      'day_extra'=>'style="width:80px;"',
			      'month_extra'=>'style="width:130px;"',
			      'year_extra'=>'style="width:90px;"',
			      'day_empty'=>'День',
			      'month_empty'=>'Месяц',
			      'year_empty'=>'Год',
                             )
                       );
	
	 echo $form->hiddenField($model,'birthday');
	?>

									<?php echo $form->error($model,'birthday'); ?>

						</div>	

<?php// echo $form->dropDownListRow($model, 'country_id', CHtml::listData(GeoCountry::model()->findAll(), 'id_country', 'name')); ?>

<?php

		$cs=Yii::app()->getClientScript();
		$jsonurl=CHtml::normalizeUrl(array('site/geo'));
		$script=<<<END
$("#User_country_id").change(function() {
        id=$("#User_country_id").attr('value');
	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "country_id="+id,
		cache: false,
		beforeSend: function(){
			$("#loading_region").show();
		},
		success: function(data) {
			$("#loading_region").hide();
                        var setoptions = '<option value="" selected="selected">Выберите регион</option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_region + '">' + data[i].name + '</option>';
				}
			}
                        $("#User_region_id").html(setoptions);
			
			/*$("#User_region_id").select2({
				placeholder: "Выберите регион"
			});*/
			
			$("#User_region_id").change();
                }  
	});
});

$("#User_region_id").change(function() {
        id=$("#User_region_id").attr('value');

	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "region_id="+id,
		cache: false,
		beforeSend: function(){
                       $("#loading_city").show();
		},
		success: function(data) {
			$("#loading_city").hide();
			var setoptions = '<option value="" selected="selected">Выберите город</option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_city + '">' + data[i].name + '</option>';
				}
			}
                        $("#User_city_id").html(setoptions); 
			
				/*$("#User_city_id").select2({
					placeholder: "Выберите город"
				});*/
                }  
	});
});

END;
		$cs->registerScript(__CLASS__.'#geo1', $script);
?>
						<div class="inputs">
<?php echo $form->labelEx($model,'country_id',array('label'=>'Страна:', 'class'=>'control-label')); ?>

<?php echo $form->dropDownList($model, 'country_id', CHtml::listData(GEOCountry::model()->findAll(), 'id_country', 'name'), array('class'=>'', 'empty'=>'Выберите страну'));?>


<?php echo $form->error($model,'country_id'); ?>

						</div>

						<div class="inputs">
<?php echo $form->labelEx($model,'region_id',array('label'=>'Регион:', 'class'=>'control-label')); ?>

<?php echo $form->dropDownList($model, 'region_id', CHtml::listData(GEORegion::model()->findAll('id_country=:id_country', array('id_country'=>$model->country_id)), 'id_region', 'name'), array('empty'=>'Выберите регион'));?>


<span id="loading_region" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
<?php echo $form->error($model,'region_id'); ?>

						</div>

						<div class="inputs">
<?php echo $form->labelEx($model,'city_id',array('label'=>'Город:', 'class'=>'col-lg-3 control-label')); ?>

<?php echo $form->dropDownList($model, 'city_id', CHtml::listData(GEOCity::model()->findAll('id_region=:id_region AND id_country=:id_country', array('id_region'=>$model->region_id, 'id_country'=>$model->country_id)), 'id_city', 'name'), array('class'=>'', 'empty'=>'Выберите город'));?>


<span id="loading_city" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
<?php echo $form->error($model,'city_id'); ?>
						</div>
 


						<div class="inputs">
							<input type="submit" value="Сохранить" class="btn standart" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>
 


<?php $this->endWidget(); ?>
			</div>	
		</div>