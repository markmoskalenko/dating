		<div class="content registration">
			<h1><?php $this->widget('zii.widgets.CBreadcrumbs', array(
		//'homeLink'=>CHtml::link(Yii::t('zii','Home'),Yii::app()->homeUrl),
		'homeLink'=>false,
		'links'=>$this->breadcrumbs,
		//'separator'=>' | ',
		'htmlOptions'=>array('class'=>'title_2'),
)); ?><!-- breadcrumbs --></h1>
			
		<div class="profile-header">
		<?php $this->widget('Menu',array(
			'htmlOptions'=>array('class'=>'profile-edit'),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			//'itemTemplate'=>' {menu}',
			'items'=>$this->menu,
		)); ?>
		</div>

<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

<?php $form=$this->beginWidget('ProfileActiveForm', array(
    'id'=>'editForm',
    'htmlOptions'=>array('class'=>'simp_com1 register1'),
)); ?>

<?php foreach($forms as $row): ?>
			<dl class="purpose2 mark">
				<dt><?php echo $form->labelEx($model,$row->name); ?></dt>
				<dd>
					<?php echo $form->{$row->presentation}($model,$row->name); ?>
					<?php echo $form->error($model,$row->name,array('class'=>'err')); ?>
				</dd>
			</dl>
<?php endforeach; ?>
			<div class="submit"><?=Html::submitButton('СОХРАНИТЬ')?></div>
<?php $this->endWidget(); ?>
		</div><!--content-->