			<div class="title">
				<h3 class="left">Редактировать профиль</h3>
			</div>	
			<div class="section-tabs">
<?php $this->widget('Menu', array(
    'htmlOptions'=>array('class'=>'tabs'),
    'items'=>$this->menu,
)); ?>
				<div class="tab visible">
<?php if(Yii::app()->user->hasFlash('edit')): ?>
<script type="text/javascript">
$().ready(function() {
 setTimeout("$('.flash-success').fadeTo('speed', 0.4);", 1500);
});
</script>
<div class="alert alert-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

    <div class="alert alert-success" style="display:none;" id="flash-success">
    Изменения сохранены.
    </div>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('CActiveForm', array(
    'id'=>'editForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
           'afterValidate' => 'js: function(form, data, hasError) {
if (!hasError) {
        $.ajax({
          type: "POST",
          url: $("#editForm").attr("action"),
          data: $("#editForm").serialize(),
	beforeSend: function(){
			$("#loading_stat").show();
		},
          success: function(data){
	  $("#loading_stat").hide();
	  $("#flash-success").fadeTo(\'speed\', 1.0);
	   $("body").scrollTo( 0, 310 );
	  setTimeout("$(\'#flash-success\').fadeTo(\'speed\', 0.4);", 1500);

	  }

        });
        return false;
        }}'
    ),
)); ?>
<?php foreach($forms as $row): ?>
   <div class="inputs">
 <?php echo $form->labelEx($model,$row->name,array('class'=>'control-label')); ?>
    <div class="checkboxes">
<?php if (is_array($field_values[$row->name]))
    echo $form->{$row->presentation}($model, $row->name, $field_values[$row->name], array('class'=>''));
else {

    $htmlOptions=array();
    
    if ($row->hint) $htmlOptions['hint']=$row->hint;
    if ($row->width) $htmlOptions['style']='width:'.$row->width.'px;';
    if (in_array($row->presentation, array('textField'))) $htmlOptions['class']='txt-field';
    if (in_array($row->presentation, array('textArea'))): $htmlOptions['class']='textarea'; $htmlOptions['style']='width:400px;'; endif;
    echo $form->{$row->presentation}($model, $row->name, $htmlOptions);
    if ($row->append) echo ' <span class="hint" style="margin-left:5px;float:left;">'.$row->append.'</span>';
}
    ?>
    </div>
    
<?php echo $form->error($model,$row->name); ?>
    </div>
<?php endforeach; ?>

						<div class="inputs">
							<input type="submit" value="Сохранить" class="btn standart" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>
<?php $this->endWidget(); ?>

			</div>	
		</div>