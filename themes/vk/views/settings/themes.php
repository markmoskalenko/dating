Выберите фон страницы:

<ul class="thumbnails">
	

		<?php foreach($themes as $row): ?>
		<li class="span2">
    <a href="<?=CHtml::normalizeUrl(array('settings/themes', 'set'=>$row->id)); ?>" class="thumbnail" rel="tooltip" data-title="<?php echo $row->name;?>">
        <img src="<?php echo Html::imageUrl('themes/'.$row->name.'/preview.jpg');?>" alt="" class="img-rounded">
    </a>
		</li>
		
		<?php endforeach; ?>

		

            </ul>

<a href="<?=CHtml::normalizeUrl(array('settings/themes', 'set'=>'delete')); ?>">Убрать фон</a>