			<div class="title">
				<h3 class="left">Настройки</h3>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>


				<div class="tab visible">

<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="alert flash-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

<div class="alert flash-success" style="display:none;">
	Настройки изменены.
</div>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
            'beforeValidate'=>'js:function(){

                return true;
             }',
           'afterValidate' => 'js: function(form, data, hasError) {
if (!hasError) {
        $.ajax({
          type: "POST",
          url: $("#registerForm").attr("action"),
          data: $("#registerForm").serialize(),
	beforeSend: function(){
			$("#loading_stat").show();
		},
          success: function(data){
	  $("#loading_stat").hide();
	  $(".flash-success").fadeTo(\'speed\', 1.0);
	  
	  setTimeout("$(\'.flash-success\').fadeTo(\'speed\', 0.4);", 1500);

	  }

        });
        return false;
        }}'
            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),

)); ?>
<?php echo CHtml::errorSummary($model);?>

<div>

					
	<div style="margin-bottom:10px;margin-top:10px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;">Сообщения</div>
	
	<p><?php echo $form->checkBox($model, 'messages_only_from_real',array('class'=>'')); ?> <label for="UserSettings_messages_only_from_real">Получать сообщения только от Real-пользователей</label></p>


</div>



						<div class="inputs">
							<input type="submit" value="Сохранить" class="btn standart" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>


<?php $this->endWidget(); ?>

			</div>	
		</div>