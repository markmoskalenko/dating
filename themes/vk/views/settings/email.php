			<div class="title">
				<h3 class="left">Настройки</h3>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>


				<div class="tab visible">

<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="alert flash-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

<div class="alert flash-success" style="display:none;">
	Email изменен.
</div>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
            'beforeValidate'=>'js:function(){

                return true;
             }',
           'afterValidate' => 'js: function(form, data, hasError) {
if (!hasError) {
        $.ajax({
          type: "POST",
          url: $("#registerForm").attr("action"),
          data: $("#registerForm").serialize(),
	beforeSend: function(){
			$("#loading_stat").show();
		},
          success: function(data){
	  $("#loading_stat").hide();
	  $(".flash-success").fadeTo(\'speed\', 1.0);
	  
	  setTimeout("$(\'.flash-success\').fadeTo(\'speed\', 0.4);", 1500);

	  }

        });
        return false;
        }}'
            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),

)); ?>
<?php echo CHtml::errorSummary($model);?>

 <div class="inputs">
<?php echo $form->labelEx($model,'email',array('class'=>'col-lg-3 control-label')); ?>

		<?php echo $form->textField($model, 'email',array('class'=>'txt-field')); ?>
		<?php echo $form->error($model,'email'); ?>

</div>

						<div class="inputs">
							<input type="submit" value="Сохранить" class="btn standart" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>


<?php $this->endWidget(); ?>

			</div>	
		</div>