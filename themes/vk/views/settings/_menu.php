<?php $this->widget('Menu', array(
    'htmlOptions'=>array('class'=>'tabs'),
    'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'Общее', 'url'=>array('settings/index'), 'linkOptions'=>array()),
				array('label'=>'Приватность', 'url'=>array('settings/privacy'), 'linkOptions'=>array()),
				array('label'=>'Оповещения', 'url'=>array('settings/notify'), 'linkOptions'=>array()),
				array('label'=>'Чёрный список', 'url'=>array('settings/blacklist'), 'linkOptions'=>array()),
				array('label'=>'Баланс', 'url'=>array('pay/index'), 'linkOptions'=>array()),
			),
)); ?>