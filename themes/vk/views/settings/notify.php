			<div class="title">
				<h3 class="left">Настройки</h3>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>


				<div class="tab visible">

<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="alert flash-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

<div class="alert flash-success" style="display:none;">
	Настройки изменены.
</div>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
            'beforeValidate'=>'js:function(){

                return true;
             }',
           'afterValidate' => 'js: function(form, data, hasError) {
if (!hasError) {
        $.ajax({
          type: "POST",
          url: $("#registerForm").attr("action"),
          data: $("#registerForm").serialize(),
	beforeSend: function(){
			$("#loading_stat").show();
		},
          success: function(data){
	  $("#loading_stat").hide();
	  $(".flash-success").fadeTo(\'speed\', 1.0);
	  
	  setTimeout("$(\'.flash-success\').fadeTo(\'speed\', 0.4);", 1500);

	  }

        });
        return false;
        }}'
            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),

)); ?>
<?php echo CHtml::errorSummary($model);?>

<div>

					
	<div style="color:#246C9E;margin-bottom:10px;margin-top:10px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;">Оповещения по электронной почте</div>
<?php if (Yii::app()->user->email):?>
	
	<p>E-Mail для оповещений: <?php echo Yii::app()->user->email?> (<a href="<?=CHtml::normalizeUrl(array('settings/email')); ?>">Изменить</a>)</p>
	
	<p><?php echo $form->checkBox($model, 'email_notify_friends',array('class'=>'')); ?> <label for="UserSettings_email_notify_friends">Заявки в друзья</label></p>
	<p><?php echo $form->checkBox($model, 'email_notify_messages',array('class'=>'')); ?> <label for="UserSettings_email_notify_messages">Личные сообщения</label></p>
	<p><?php echo $form->checkBox($model, 'email_notify',array('class'=>'')); ?> <label for="UserSettings_email_notify">Уведомления</label></p>
<?php else:?>

    <div class="alert alert-danger" id="warning" style="margin-bottom:15px;font-size:12px;">
    <i class="fa fa-exclamation-triangle" style="font-size:15px;"></i>Укажите свой E-mail адрес для оповещения о новых сообщениях, заявках в друзья, комментариях и уведомлениях. <a href="<?=CHtml::normalizeUrl(array('settings/email')); ?>">Ввести E-mail адрес</a>
    </div>
<?php endif;?>


	<div style="color:#246C9E;margin-bottom:10px;margin-top:10px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;">Оповещения через SMS</div>
<?php if (Yii::app()->user->phone):?>
	
	<p><strong>SMS</strong>-оповещения будут приходить на номер <strong><?php echo Yii::app()->user->phone?></strong><br>Изменить номер, к которому привязана страница, Вы можете <a href="<?=CHtml::normalizeUrl(array('settings/phone')); ?>">здесь</a></p>
	
	
<?php else:?>

    <div class="alert alert-danger" id="warning" style="margin-bottom:15px;font-size:12px;">
    <i class="fa fa-exclamation-triangle" style="font-size:15px;"></i>Необходимо привязать вашу страницу к номеру телефона для SMS-оповещения о новых сообщениях. <a href="<?=CHtml::normalizeUrl(array('settings/phone')); ?>">Ввести номер телефона</a>
    </div>
<?php endif;?>
<p><?php echo $form->checkBox($model, 'sms_notify',array('class'=>'')); ?> <label for="UserSettings_sms_notify">Получать Личные сообщения через SMS</label></p>

	<div style="color:#246C9E;margin-bottom:10px;margin-top:10px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;">Моментальные оповещения на сайте</div>
	<p>На сайте используется моментальное оповещение о новых событиях, личных сообщениях, заявки в друзья, новые комментарии.</p>

	<p><?php echo $form->checkBox($model, 'sound_notify',array('class'=>'')); ?> <label for="UserSettings_sound_notify">Включить звуковые оповещения</label></p>

</div>



						<div class="inputs">
							<input type="submit" value="Сохранить" class="btn standart" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>


<?php $this->endWidget(); ?>

			</div>	
		</div>