			<div class="title">
				<h3 class="left">Настройки</h3>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>


				<div class="tab visible">
	
<div>

					
	<div style="margin-bottom:10px;margin-top:10px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;color:#246C9E;"><i class="fa fa-user"></i>Логин и адрес страницы</div>
	
	<?php if (Yii::app()->user->model->login):?><p>Текущий логин: <?php echo Yii::app()->user->model->login?></p><?php endif;?>
	<p>Адрес Вашей страницы: <span class="text-info"><?=Yii::app()->request->hostInfo ?><?php echo Yii::app()->user->model->url?></span></p>
<!--	<a href="--><?//=CHtml::normalizeUrl(array('settings/login')); ?><!--" class="btn2 btn-default"><i class="fa fa-cog"></i>Изменить</a>-->

</div>

<br>

                    <?php if (!Yii::app()->user->isGuest && !Yii::app()->user->getEmailConfirm()): ?>

                    <div>
	<div style="margin-bottom:10px;margin-top:10px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;color:#246C9E;"><i class="fa fa-envelope"></i>Адрес Вашей электронной почты</div>
					
	<p>Текущий адрес: <?php echo Yii::app()->user->email?></p>
	<p class="text-warning">Адрес для входа на сайт, уведомлений и восстановления пароля в случае утери.</p>
	<a href="<?=CHtml::normalizeUrl(array('settings/email')); ?>" class="btn2 btn-default"><i class="fa fa-cog"></i>Изменить</a>

</div>
<br>
                    <?php endif;?>

                    <div>
	<div style="margin-bottom:10px;margin-top:10px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;color:#246C9E;"><i class="fa fa-lock"></i>Пароль от сайта</div>
					
	<a href="<?=CHtml::normalizeUrl(array('settings/password')); ?>" class="btn2 btn-default"><i class="fa fa-cog"></i>Сменить пароль</a>

</div>

<br>

<div>

	<div style="margin-bottom:10px;margin-top:15px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;color:#246C9E;"><i class="fa fa-tablet"></i>Номер Вашего телефона</div>
	
<?php if (Yii::app()->user->phone):?>
	<p>Текущий номер: <?php echo Yii::app()->user->phone?></p>
	<?php if (Yii::app()->user->model->isReal=='1'):?><a href="<?=CHtml::normalizeUrl(array('settings/phone')); ?>" class="btn2 btn-default"><i class="fa fa-cog"></i>Изменить</a><?php endif;?>
<?php else:?>
	<a href="<?=CHtml::normalizeUrl(array('settings/phone')); ?>" class="btn2 btn-default"><i class="fa fa-cog"></i>Указать</a>
<?php endif;?>

</div>

<br>
	
<div>

	<div style="margin-bottom:10px;margin-top:10px;font-weight:bold; padding-bottom:5px;border-bottom: 1px solid #bcdceb;color:#246C9E;">Удаление анкеты</div>
	
    <a href="<?=CHtml::normalizeUrl(array('settings/remove')); ?>" class="btn2 btn-default"><i class="fa fa-times"></i>Удалить страницу</a>

</div>

			</div>	
		</div>