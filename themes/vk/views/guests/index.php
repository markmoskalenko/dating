<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));  ?>
			<div class="title">
				<h3 class="left">Кто смотрел мою страницу</h3>
			</div>	
			<div class="participants">
<?php /*if (!$profiles): ?>

<div style="margin:150px;" align="center">Вашу анкету еще никто не сомтрел
</div>

<?php else:*/ ?>	
				
				
<?php foreach($profiles as $num=>$row): ?>
				<div class="user">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(104,138, 'camera_a.png'),
			'',
			array('class'=>'img-responsive')
		), 
		$row->url,
		array()
	);
?>
					<span class="name"><?php
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('class'=>'name-user')
	);
?></span>
					<span class="city"><?php echo $row->geo->city?></span>
					<span class="age"><?php echo Html::age($row->birthday)?></span>

					<?php if($row->isOnline):?><label class="online">&nbsp;</label><?php endif;?>
						<a href="<?php echo CHtml::normalizeUrl(array('messages/send', 'id'=>$row->id)); ?>" class="add-friend" id="showModalSendMessage" <?=Yii::app()->user->id==$row->id ? ' disabled="disabled"' : ''?>><i class="fa fa-envelope"></i>Сообщение</a>
				</div>	
<?php endforeach;?>
<?php //endif; ?>	
			</div>
