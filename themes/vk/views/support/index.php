			<div class="title">
				<h3 class="left">Служба поддержки пользователей</h3>
			</div>	
			<div class="subtitle">
				<span>Благодарим Вас за обращение к нам. Мы постараемся ответить вам как можно скорее.</span>
			</div>
			
			
			

<?php if(Yii::app()->user->hasFlash('contact')): ?>
<div class="reg-form registration-user-full">
<div class="flash-success">
	Сообщение доставлено в службу поддержки, в ближайшее время Вам ответят.
	
</div>
</div>
<?php else: ?>




			<div class="registration-user-full">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'supportForm',
    'htmlOptions'=>array('class'=>'reg-form'),

)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>

							<div class="inputs">
								<?php echo $form->labelEx($model,'name', array('label' => 'Ваше имя', 'class'=>'control-label')); ?>
								
										<?php echo $form->textField($model,'name',array('class'=>'txt-field')); ?>
									
									<?php echo $form->error($model,'name',array('class'=>'err')); ?>
								
							</div>
							<div class="inputs">
								<?php echo $form->labelEx($model,'email', array('label' => 'Ваш E-mail', 'class'=>'control-label')); ?>
								
										<?php echo $form->textField($model,'email',array('class'=>'txt-field')); ?>
									
									<?php echo $form->error($model,'email',array('class'=>'err')); ?>
								
								<div class="hint"></div>
							</div>
							<div class="inputs">
								<?php echo $form->labelEx($model,'subject', array('label' => 'Тема сообщения', 'class'=>'control-label')); ?>
								
										<?php echo $form->textField($model,'subject',array('class'=>'txt-field')); ?>
									
									<?php echo $form->error($model,'subject',array('class'=>'err')); ?>
								
								<div class="hint"></div>
							</div>
							<div class="inputs">
								<?php echo $form->labelEx($model,'body', array('label' => 'Сообщение', 'class'=>'control-label')); ?>
								
										<?php echo $form->textArea($model,'body',array('class'=>'textarea', 'style'=>'width:295px;height:130px;')); ?>

									<?php echo $form->error($model,'body',array('class'=>'err')); ?>
								
								<div class="hint"></div>
							</div>


				<div class="inputs">
					<div class="cupcha left">
						<?php echo $form->labelEx($model,'verifyCode', array('label' => 'Введите код', 'class'=>'control-label')); ?>
						<div class="cupcha-box txt-right">
							<?php $this->widget('CCaptcha', array('captchaAction' => 'support/captcha', 'clickableImage'=>true, 'showRefreshButton'=>false, 'imageOptions'=>array('style'=>'cursor:pointer;border-radius:4px;', 'id'=>'captcha'))); ?>
						</div>	
					</div>	
					<?php echo $form->textField($model,'verifyCode',array('class'=>'txt-field left', 'style'=>'width:100px;')); ?>
				</div>
				<?php echo $form->error($model,'verifyCode',array('class'=>'err')); ?>

				<div class="inputs">
					<input type="submit" value="Отправить" class="btn standart" />
				</div>	
					
					
					
					
					
					

<?php $this->endWidget(); ?>


			</div>


<?php endif; ?>
