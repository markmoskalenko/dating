<?php $this->pageTitle='Создать альбом';?>
			<div class="block-with-title">
				<div class="title">
					Создать альбом
				</div>
				<div class="content-block">
					
<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>
<?php $form=$this->beginWidget('CActiveForm', array(
    //'id'=>'lostpasswordForm',
    'htmlOptions'=>array('class'=>'search-partners'),
    //'enableClientValidation'=>true,
   // 'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            //'validateOnSubmit'=>true,
    ),
)); ?>
				<div class="line">
					<div class="line">
						<div class="lineForm">
			
						</div>							
					</div>	
						<div class="box-line">		
							<div class="line">
								<div class="lineForm">
									<?php echo $form->labelEx($model,'name', array('label' => 'Название:')); ?>
									<?php echo $form->textField($model,'name',array('class'=>'txt')); ?>
									<span class="hint"></span>
									<?php echo $form->error($model,'name',array('class'=>'err')); ?>									
								</div>		
							</div>
							<div class="line">
								<div class="lineForm">
									<?php echo $form->labelEx($model,'descr', array('label' => 'Описание:')); ?>
									<?php echo $form->textField($model,'descr',array('class'=>'txt')); ?>
									<span class="hint"></span>
									<?php echo $form->error($model,'descr',array('class'=>'err')); ?>									
								</div>		
							</div>
							<div class="line">
								<div class="lineForm">
									<?php echo $form->labelEx($model,'access_to_view', array('label' => 'Кто может просматривать этот альбом?:')); ?>
									<?php echo $form->dropDownList($model,'access_to_view', array(
			'all'=>'Все пользователи',
			'friends'=>'Только друзья',
		),array('class'=>'sel260')); ?>
									<span class="hint"></span>
									<?php echo $form->error($model,'access_to_view',array('class'=>'err')); ?>									
								</div>		
							</div>
							<div class="line">
								<div class="lineForm">
									<?php echo $form->labelEx($model,'access_to_comment', array('label' => 'Кто может комментировать фотографии?:')); ?>
									<?php echo $form->dropDownList($model,'access_to_comment', array(
			'all'=>'Все пользователи',
			'friends'=>'Только друзья',
		),array('class'=>'sel260')); ?>
									<span class="hint"></span>
									<?php echo $form->error($model,'access_to_comment',array('class'=>'err')); ?>									
								</div>		
							</div>
						</div>	
				
				</div>
						<div class="box-line last">
							<div class="line">
								<?=Html::submitButton('Создать альбом', array('class'=>'button-search'))?>
							</div>	
						</div>	


<?php $this->endWidget(); ?>
				</div>
			</div>
