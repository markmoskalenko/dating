<?php $this->pageTitle='Сообщения';?>
<?php 		$this->breadcrumbs=array(
			'Диалог с пользователем',
		);
?>
			<div class="title">
				<h3 class="left">Диалог с пользователем</h3>
			</div>		

			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>

				<div class="tab visible" style="background: #DDEDF5;">
<?php if (Yii::app()->user->model->isReal):?>
<?php if(Yii::app()->user->hasFlash('sent')): ?>
<div class="alert flash-success">
	Сообщение отправлено.
</div>
<?php endif; ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'well1'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>false,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
            'beforeValidate'=>'js:function(){

                return true;
             }',

            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),

)); ?>
<?php echo CHtml::errorSummary($model);?>



<?php echo $form->textArea($model, 'message', array('style'=>'padding: 5px;width:715px;margin-bottom:5px;border: 1px solid #bcdceb;background: #fff;height:60px;resize: vertical;')); ?>
<input type="submit" value="Отправить" class="btn small" />

<?php $this->endWidget(); ?>	
<?php else:?>
				<div>
					<div class="about-service" style="background: #fff; padding:12px;">
						<p>Ваша анкета не подтверждена реальностью! Писать сообщения могут только Реальные пользователи сайта.</p>
						<a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>"><b>Подтвердить реальность</b></a>
					</div>
				</div>

<?php endif;?>

<script type="text/javascript">
function checkAllCheckboxes(object, prefix){
	
  var re = new RegExp('^' + prefix);
  var checkboxes = document.getElementsByTagName('input');
  var total = checkboxes.length;
  for(i = 0; i < total; i++){
    if(checkboxes.item(i).id.match(re)){

        checkboxes.item(i).checked = true;

    }
  }

		$("#control_panel").show();


}
$(document).ready(function(){
	$('#mailForm').ajaxForm({
		dataType: 'json',
		url: $(this).attr("action"),
		type: "POST",
		cache: false,
		beforeSend: function(){

		},
		success: function(data) {
			

			
			
			
			


		}
	});
	

	
$("#mailForm").submit(function() {
        
$('input[type=checkbox]').each(function () {
           if (this.checked) {
               console.log($(this).val());
	       $('#msg'+$(this).val()).hide();

           }
});

});
    
	$("#mailForm").change(function() {
		
	var boxes = $(":checkbox:checked");
	if (boxes.length==1) {
		$("#control_panel").show();
	}
	if (boxes.length==0) {
		$("#control_panel").hide();
	}
	
		
	});

});
</script>
				</div>
				<div class="tab visible">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'mailForm',
    //'htmlOptions'=>array('class'=>'search-partners'),
)); ?>
					<div class="messages-box clearfix">
						<div class="control-bttns">

							<ul class="marking left" style="height:22px;">
								<li>Выделить:</li>
								<li><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">все,</a></li> 
								<li><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">прочитанные,</a></li> 
								<li><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">новые</a></li>
							</ul>	
							<ul class="bttns right" style="display:none;" id="control_panel">
								<li><input type="submit" value="Удалить" name="action" class="btn small" /></li>
								<li><a href="#" class="btn wide">ОТМЕТИТЬ КАК ПРОЧИТАННЫЕ</a></li>
							</ul>	
						</div>
						<div class="head">
							<span><?=$count?> сообщений с <a href="<?=$user->url?>"><?=$user->name?></a></span>
						</div>
<?php foreach($messages as $row):?>
						<div class="message" id="msg<?php echo $row->id;?>">
							<div class="checked">
								<input id="to_message_<?php echo $row->id;?>" type="checkbox" name="to_message[]" value="<?php echo $row->id;?>" class="checkbox">
							</div>	
							<a href="<?=$row->dialogueUser->url; ?>"><img src="<?php echo $row->dialogueUser->getImage(55,55, 'camera_s.png');?>" alt="" class="left" /></a>
							<div class="user-params left">
								<span class="name"><a href="<?=$row->dialogueUser->url; ?>"><?=$row->dialogueUser->id ? $row->dialogueUser->name : 'DELETED'?></a></span>
<?php if ($row->dialogueUser->isOnline):?>
								<label class="status on-line"><?php if (!$row->dialogueUser->isMobile):?><i class="fa fa-circle"></i>Online<?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?></label>
<?php endif;?>
								<span class="date"><?php echo Html::date($row->time);?></span>
							</div>	
							<div class="cont right">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="text-msg"><?=$row->message ?></td>
									</tr>	
								</table>	
							</div>	
						</div>
<?php endforeach;?>


					</div>


<div class="form-actions1" style="display:none;">
<?php $this->widget('bootstrap.widgets.TbButton', array('type'=>'danger', 'buttonType'=>'submit', 'label'=>'Удалить', 'icon'=>'remove')); ?>
</div>

			</div>	
<div class="title" style="margin-top:-20px;">
<? $this->widget('CLinkPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>

<?php $this->endWidget(); ?>
		</div>