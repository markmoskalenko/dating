<?php $this->pageTitle='Написать сообщение';?>
<?php 		$this->breadcrumbs=array(
			'Написать сообщение',
		);
?>
					
<?php if(Yii::app()->user->hasFlash('sent')): ?>
<div class="alert flash-success">
	Сообщение отправлено.
</div>
<?php endif; ?>



<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'messageForm',
    'htmlOptions'=>array('class'=>'well1'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
)); ?>
	<div id="errorSummary" class="alert alert-danger" style="display:none;">
		</div>
<?php echo CHtml::errorSummary($model);?>
<div class="clearfix">


    <div class="left" style="width:65px;">
			
			<a href="<?=$user->url; ?>"><img src="<?php echo $user->have_photo==1 ? ImageHelper::thumb(55, 55, $user->id, $user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('camera_s.png');?>" alt="" class="" style="margin-bottom:5px;" /></a>
	

    </div>
    <div class="left">


<div style="margin-bottom:5px;">Кому: <a href="<?=$user->url; ?>"><?php echo $user->name; ?></a></div>

<?php if (Yii::app()->user->model->isReal):?>
<?php echo $form->textArea($model, 'message', array('class'=>'', 'style'=>'width:500px; height:120px;padding: 5px;margin-bottom:5px;border: 1px solid #bcdceb;background: #fff;resize: vertical;')); ?>

<div style="margin-top:5px; font-size:11px;">
		<a href="<?=CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$user->id)); ?>" class="left">Перейти в диалог</a>	
<input type="submit" value="ОТПРАВИТЬ" class="btn small right" onclick="sendMessage($('#messageForm')); return false;" />

<?php elseif (!Yii::app()->user->isGuest):?>
				<div>
					<div class="about-service" style="background: #fff; padding:12px;">
						<p>Ваша анкета не активна! Писать сообщения могут только активные пользователи сайта с статусом Реал.</p>
						<a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>"><b>Подтвердить реальность Вашей анкеты с помощью активации Реал статуса!</b></a>
					</div>
				</div>
<?php else:?>
				<div>
					<div class="about-service" style="background: #fff; padding:12px;">
						<p>Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать сообщение.</p>
					</div>
				</div>
<?php endif;?>

</div>
</div>
    
    </div>
<?php $this->endWidget(); ?>