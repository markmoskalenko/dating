<?php $this->pageTitle='Мои Cообщения';?>
<?php
$this->breadcrumbs=array('Сообщения');
?>

			<div class="title">
				<h3 class="left">Мои Cообщения</h3>
			</div>

			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>

<?php if (!$messages): ?>

<div style="margin:150px;" align="center">Здесь будет выводиться список Ваших сообщений.

<br><br><br>
Пожалуйста, выберите <a href="<?=CHtml::normalizeUrl(array('search/index')); ?>"><b>пользователя</b></a>, чтобы начать общение.
</div>

<?php else: ?>	
			
			

				<div class="tab visible">
<script type="text/javascript">
function checkAllCheckboxes(object, prefix){
	
  var re = new RegExp('^' + prefix);
  var checkboxes = document.getElementsByTagName('input');
  var total = checkboxes.length;
  for(i = 0; i < total; i++){
    if(checkboxes.item(i).id.match(re)){
      /*if(object.checked){*/
        checkboxes.item(i).checked = true;
      /*}
      else{
        checkboxes.item(i).checked = false;
      }*/
    }
  }

		$("#control_panel").show();


}
$(document).ready(function(){
	$('#mailForm').ajaxForm({
		dataType: 'json',
		url: $(this).attr("action"),
		type: "POST",
		cache: false,
		beforeSend: function(){

		},
		success: function(data) {
			

			
			
			
			


		}
	});
	

	
$("#mailForm").submit(function() {
       $("#mailForm").change();
$('input[type=checkbox]').each(function () {
           if (this.checked) {
               console.log($(this).val());
	       $('#msg'+$(this).val()).hide();

           }
});

});
    
	$("#mailForm").change(function() {
		
	var boxes = $(":checkbox:checked");
	if (boxes.length==1) {
		$("#control_panel").show();
	}
	if (boxes.length==0) {
		$("#control_panel").hide();
	}
	
		
	});

});
</script>



<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'mailForm',
    //'htmlOptions'=>array('class'=>'search-partners'),
)); ?>
					<div class="messages-box clearfix">
						<div class="control-bttns">

							<ul class="marking left" style="height:22px;">
								<li>Выделить:</li>
								<li><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">все,</a></li> 
								<li><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">прочитанные,</a></li> 
								<li><a href="#" onclick="checkAllCheckboxes(this, 'to_message_'); return false;">новые</a></li>
							</ul>	
							<ul class="bttns right" style="display:none;" id="control_panel">
								<li><input type="submit" value="Удалить" name="action" class="btn small" /></li>
								<li><a href="#" class="btn wide">ОТМЕТИТЬ КАК ПРОЧИТАННЫЕ</a></li>
							</ul>	
						</div>
						<div class="head">
							<span><?=$_GET['folder']=='outbox' ? 'Вы отправили '.Yii::t('app','{n} сообщение|{n} сообщения|{n} сообщений', $count) : 'Вы получили '.Yii::t('app','{n} сообщение|{n} сообщения|{n} сообщений', $count)?></span>
						</div>
<?php foreach($messages as $row):?>
						<div class="message<?php if($row->unread) echo ' unread';?>" id="msg<?php echo $row->id;?>">
							<div class="checked">
								<input id="to_message_<?php echo $row->id;?>" type="checkbox" name="to_message[]" value="<?php echo $row->id;?>" class="checkbox">
							</div>
							
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'), '', array('class'=>'left')
		), 
		$row->user->url
	);
else:
	echo CHtml::link(
		CHtml::image(Html::imageUrl('camera_s.png'), '', array('class'=>'left')
		), 
		'#', array('onclick'=>'YiiAlert.error(\'Пользователь удален.\', \'404\'); return false;')
	);
endif;
?>
							
							<div class="user-params left">
								<span class="name"><a href="<?=$row->user->url; ?>"><?=$row->user->id ? $row->user->name : 'DELETED'?></a></span>
<?php if ($row->user->isOnline):?>
								<label class="status on-line"><?php if (!$row->user->isMobile):?><i class="fa fa-circle"></i>Online<?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?></label>
<?php endif;?>
								<span class="date"><?php echo Html::date($row->time);?></span>
							</div>	
							<div class="cont right" onclick="location='<?=CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$row->user->id)); ?>';" style="cursor:pointer;">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td class="text-msg"><?=$row->message ?></td>
									</tr>	
								</table>	
							</div>	
						</div>
<?php endforeach;?>
					</div>
<?php $this->endWidget(); ?>

			</div>
<div class="title" style="margin-top:-20px;">
<? $this->widget('CLinkPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>


<?php endif; ?>	
		</div>