
pickup = {
	showModal: function(url)
	{
		$.arcticmodal({
			type: 'ajax',
			url: url,
			ajax: {
				type: 'GET',
				cache: false,
				//dataType: 'json',
				success: function(data, el, responce) {
					var h = $('<div class="box-modal box-modal-pickup" id="showModalPickup">' +
						'<div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>' +
						'<p><b /></p><p />' +
						'</div>');
					$('B', h).html('<span id="title">Поднять свою анкету на самый верх!</span>');
					$('P:last', h).html(responce);
					data.body.html(h);
				}
			},
			beforeOpen: function(data, el) {
				
			},
			afterClose: function(data, el) {
				
			},
			openEffect: {
				type: 'none'
			},
			closeEffect: {
				type: 'none'
			}
		});
		return false;
	},
        
	go: function(url)
	{
                $.ajax({
                        type: "GET",
                        url: url,
                        data: "go=1",
                        cache: false,
                        dataType: 'json',
                        beforeSend: function(){
		   
                        },
                        success: function(data){
                                if (data.status=='1') {
                                        $("#showModalPickup #pickup").html('<div class="pku-a1">Ваша страница поднята и появится в поиске в течение ~5 минут.</div>');
                                } else if (data.status=='0') {
                                        $("#showModalPickup #pickup").html('Пополните счет');
                                }
                        }
                });
                return false;
	}
}

function deleteChat(url) {
        //$("#commentsPages .active a").attr('href')
	$.ajax({
		type: "GET",
		url: url,
		cache: false,
		dataType: 'json',
		beforeSend: function(){
		   
		},
		success: function(responce){
			
			$("#chat_"+responce.id).html('<div class="alert alert-info">Запись на удалена.</div>');
		}
	});
}

jQuery(function($) {

$("#chatForm").ajaxForm({
		dataType: 'json',
		type: "POST",
		cache: false,
		beforeSend: function(){
		   $("#chatForm textarea").attr("value", "");
		},
		success: function(data){
		
		$("#chatForm #errorSummary").hide().empty();
		
		if (data.html) {
			 $.arcticmodal('close');
		$("#chat_block").fadeTo('speed', 1.0);
			$("#chat_block").html(data.html);
			return;
		}
			
$.each(data, function(key, value) { 

  $("#chatForm #errorSummary").prepend('<div>'+value+'</div>');

});
$("#chatForm #errorSummary").fadeTo('speed', 1.0);
		}
		});

});

spamProtection = {
	obj2: 0,
	
	init: function()
	{
		
	},
	
	captcha: function(url, obj2)
	{
		spamProtection.obj2=obj2;
							$.arcticmodal({
								type: 'ajax',
								url: url,
								ajax: {
									type: 'GET',
									cache: false,
									//dataType: 'json',
									success: function(data, el, responce) {
										
										var h = $('<div class="box-modal" id="exampleModal">' +
												'<div class="box-modal_close arcticmodal-close">закрыть</div>' +
												'<p><b /></p><p />' +
												'</div>');
										$('B', h).html('<span id="title">Защита от спама</span>');
										$('P:last', h).html(responce);
										data.body.html(h);
									}
								},
								beforeOpen: function(data, el) {
									$('.navbar').attr('style','margin-right:17px;');
								},
								afterClose: function(data, el) {
									$('.navbar').attr('style','');
								},
								openEffect: {
									type: 'none'
								},
								closeEffect: {
									type: 'none'
								}
							});
	},
	
	enter: function(obj)
	{
	$.ajax({
		type: "POST",
		url: obj.attr('action'),
		data: obj.serialize(),
		cache: false,
		dataType: 'json',
		beforeSend: function(){
                   $("button", obj).button('loading');
		},
		success: function(responce){
			$("#errorSummary", obj).hide().empty();
			$("button", obj).button('reset');
			
			if (responce.ok) {
				spamProtection.obj2;
				$('#exampleModal').arcticmodal('close');
				return;
			}
                        
$("#captcha").click();

$.each(responce, function(key, value) { 

  $("#errorSummary", obj).prepend('<div>'+value+'</div>');

});
$("#errorSummary", obj).fadeTo('speed', 1.0);

			
			
		}
	});
	}
}

YiiAlert = {
	error: function(text, code)
	{
		if (text) {
			text="<b>Ошибка "+code+"</b>: "+text;
		} else {
			text="Неизвестная ошибка.";
		}
		
	        $.blockUI({ 
            message: text, 
            fadeIn: 100, 
            centerY: true,
            css: {
		textAlign:      'left',
                left: '', 
                //right: '0',
		 top: '0', 
                border: 'none', 
                padding: '10px',
                backgroundColor: '#FA895A',
		fontSize: '11px',
                opacity: .8, 
                color: '#000' ,
'border-radius': '0 0 6px 0',
'-moz-border-radius':'0 0 6px 0',
'-khtml-border-radius': '0 0 6px 0'
            },
	    showOverlay: false
	   
			/*overlayCSS:  {
				backgroundColor: '#000',
				opacity:          .3
			}*/
        
        }); 

	setTimeout("$.unblockUI()", 15000);
	
	},
	

	success: function(text)
	{
       $.blockUI({ 
            message: '<b>'+text+'</b>', 
            fadeIn: 100, 
            centerY: true,
            css: {
		textAlign:      'center',
                left: '35%',
                width: '400px', 
                border: 'none', 
                padding: '10px',
		fontSize: '11px',
                backgroundColor: '#000', 
                opacity: .6, 
                color: '#fff' ,
'border-radius': '6px',
'-moz-border-radius':'6px',
'-khtml-border-radius': '6px'
            },
	    showOverlay: false
	   
			/*overlayCSS:  {
				backgroundColor: '#000',
				opacity:          .3
			}*/
        
        });
       setTimeout("$.unblockUI()", 2000);
	},
	

	loading: function()
	{
	        $.blockUI({ 
            message: "<i class='fa fa-spinner fa-spin' style='font-size:32px;'></i>", 
            fadeIn: 100, 
            centerY: true,
            css: {
		textAlign:      'center',
                left: '47%',
                width: '70px', 
                border: 'none', 
                padding: '10px',
                backgroundColor: '#000', 
                opacity: .6, 
                color: '#fff' ,
'border-radius': '6px',
'-moz-border-radius':'6px',
'-khtml-border-radius': '6px'
            },
	    showOverlay: false
	   
			/*overlayCSS:  {
				backgroundColor: '#000',
				opacity:          .3
			}*/
        
        }); 
	},
	

	loadingClose: function()
	{
		$.unblockUI();
	}
}