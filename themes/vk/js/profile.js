
function updateStatus(obj)
	{

/*$("#change_status").css('background', '#FFFCD1');*/
	$.ajax({
		type: "POST",
		url: obj.attr('action'),
		data: obj.serialize(),
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                        YiiAlert.error('1.', 404);
                },
		beforeSend: function(){
                        

		},
		success: function(responce){
$("#statusForm").hide();
			$("#change_status").text($("textarea", obj).val());
			$("#change_status").css('color', '#000');
				/*$("#change_status").stop().animate({ backgroundColor: "#fff"}, 1500);*/
		if ($("textarea", obj).val()=='') {
			$("#change_status").html('изменить статус');
			$("#change_status").css('color', '#AAAAAA');
		}
			
		}
	});

	
        }

jQuery(function($) {
	
$("a#change_status, #statusForm").live('click', function() {
	//$("a#change_status").parent().hide();
	$("#statusForm").show();
	return false;
});

	$(document).mouseup(function (e) {
    var container = $("#statusForm");
    if (container.has(e.target).length === 0){
        container.hide();
    }
});



$("#friendshipRequest a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		cache: false,
		dataType: 'json',
		beforeSend: function(){
		   $("#friendshipRequest").html('<div style="padding:10px;margin-bottom:5px; text-align:center;"><i class="fa fa-spinner fa-spin" style="font-size:18px;color:#246C9E;"></i></div>');
		},
		success: function(responce){
			$("#friendshipRequest").html('<div style="padding:10px;margin-bottom:5px;background:#F7F7F7;color:#626262;text-align:center; font-size:11px;">'+responce.textStatus+'</div>');
			
		}
	});
	return false;
});


	
$("#likePages a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		data: '&_a=wall',
		cache: false,
		beforeSend: function(){
		   
		},
		success: function(html){
			
			$("#getLike").html(html);
		}
	});
	return false;
});


						
							$("#showModalLikes").live('click', function() {
							
							$.arcticmodal({
								type: 'ajax',
								url: $(this).attr("href"),
								ajax: {
									type: 'GET',
									cache: false,
									//dataType: 'json',
									success: function(data, el, responce) {
										
										var h = $('<div class="box-modal" id="modalPhotoBox">' +
												'<div class="box-modal_close arcticmodal-close">закрыть</div>' +
												'<p><b /></p><p />' +
												'</div>');
										$('B', h).html('<span id="title">Вы понравились</span>');
										$('P:last', h).html(responce);
										data.body.html(h);
									}
								},
								beforeOpen: function(data, el) {
									$('.navbar').attr('style','margin-right:17px;');
								},
								afterClose: function(data, el) {
									$('.navbar').attr('style','');
								},
								openEffect: {
									type: 'none'
								},
								closeEffect: {
									type: 'none'
								}
							});
							return false;
						});
                                                        
                                                        
                                                        

	$("#addlike").live('click', function() {
		
	$.ajax({
		type: "GET",
		url: $('a', this).attr('href'),
		cache: false,
		beforeSend: function(){
			
		},
		success: function(responce){
			
		}
	});
		
		$(this).hide();
       $.blockUI({ 
            message: "<b>Отмечено как понравилось.</b>", 
            fadeIn: 100, 
            centerY: true,
            css: {
		textAlign:      'center',
                left: '40%',
                width: '400px', 
                border: 'none', 
                padding: '10px',
                backgroundColor: '#000', 
                opacity: .6, 
                color: '#fff' ,
'border-radius': '6px',
'-moz-border-radius':'6px',
'-khtml-border-radius': '6px'
            },
	    showOverlay: false
	   
			/*overlayCSS:  {
				backgroundColor: '#000',
				opacity:          .3
			}*/
        
        });
       setTimeout("$.unblockUI()", 2000);
		return false;
	});

    $("#showFull_information").toggle(
      function () {
        $("#showFull_information").text("Свернуть анкету");                                    
        $("#full_information").show();
      },
      function () {
        $("#showFull_information").text("Показать всю анкету");
        $("#full_information").hide();
      }
    );

/*$("#showFull_information").live('click', function() {
	$("#showFull_information").text("Свернуть анкету");
			$("#full_information").toggle();
	
});*/




$("#AddToBlacklist a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		dataType: 'json',
		cache: false,
		beforeSend: function(){
		   
		},
		success: function(data){
			if (data.status=='re_block') {
				$("#AddToBlacklist a").html('<i class="fa fa-ban"></i>Заблокировать</a>');
			} else {
				$("#AddToBlacklist a").html('<i class="fa fa-check-circle"></i>Разблокировать</a>');	
			}
		}
	});
	return false;
});




$("#wallForm").ajaxForm({
		dataType: 'json',
		type: "POST",
		cache: false,
		beforeSend: function(){
		   $("textarea").attr("value", "");
		},
		success: function(data){
		
		$("#errorSummary").hide().empty();
		
		if (data.html) {
		$("#content_wall").fadeTo('speed', 1.0);
			$("#content_wall").html(data.html);
			return;
		}
			
$.each(data, function(key, value) { 

  $("#wallForm #errorSummary").prepend('<div>'+value+'</div>');

});
$("#wallForm #errorSummary").fadeTo('speed', 1.0);
		}
		});

$("#wallPages a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		data: '&_a=wall',
		cache: false,
		beforeSend: function(){
		   
		},
		success: function(html){
			$("body").scrollTo( "#wall_widget", 0 );
			$("#content_wall").html(html);
		}
	});
	return false;
});

});

function deleteWall(url) {
        //$("#commentsPages .active a").attr('href')
	$.ajax({
		type: "GET",
		url: url,
		cache: false,
		dataType: 'json',
		beforeSend: function(){
		   
		},
		success: function(responce){
			
			$("#wall_"+responce.id).html('<div class="alert alert-info">Запись на стене удалена.</div>');
		}
	});
}
