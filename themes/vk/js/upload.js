jQuery(function($) {
	$("#uploadPhotoModal").live('click', function() {

							$.arcticmodal({
								type: 'ajax',
								url: $(this).attr("href"),
								ajax: {
									type: 'GET',
									cache: false,
									//dataType: 'json',
									success: function(data, el, responce) {
										
										var h = $('<div class="box-modal" id="modalPhotoBox">' +
												'<div class="box-modal_close arcticmodal-close">закрыть</div>' +
												'<p><b /></p><p />' +
												'</div>');
										$('B', h).html('<span id="title">Отправить сообщение</span>');
										$('P:last', h).html(responce);
										data.body.html(h);
                                                                                
									}
								},
								beforeOpen: function(data, el) {
									$('.navbar').attr('style','margin-right:17px;');
								},
								afterClose: function(data, el) {
									$('.navbar').attr('style','');
								},
								openEffect: {
									type: 'none'
								},
								closeEffect: {
									type: 'none'
								}
							});
							return false;
						});
});

