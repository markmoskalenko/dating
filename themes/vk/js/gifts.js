
gifts = {

	init: function()
	{
		$("#showModalGifts").live('click', function() {
							
			$.arcticmodal({
				type: 'ajax',
				url: $(this).attr("href"),
				ajax: {
					type: 'GET',
					cache: false,
					//dataType: 'json',
					success: function(data, el, responce) {
										
										var h = $('<div class="box-modal" id="modalPhotoBox">' +
												'<div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>' +
												'<p><b /></p><p />' +
												'</div>');
										$('B', h).html('<span id="title" style="font-weight:bold; color:#246C9E;">Подарки</span>');
										$('P:last', h).html('<div class="recording-wall" id="content_gifts" style="width:600px;">'+responce+'</div>');
										data.body.html(h);
					}
				},
				beforeOpen: function(data, el) {
					$('.navbar').attr('style','margin-right:17px;');
				},
				afterClose: function(data, el) {
					$('.navbar').attr('style','');
				},
				openEffect: {
					type: 'none'
				},
				closeEffect: {
					type: 'none'
				}
			});
			return false;
		});
		
		$("#donateGifts a").live('click', function() {
							
			gifts.send($(this).attr("href"));
			return false;
		});
		
		$("a[rel=selectGift]").live('click', function() {
							
			
			/*if (userBalance()>$(this).attr('data-price')) {
				$("#giftSendForm").hide();
				$("#giftBalance").show();
			} else {
				$("#giftSendForm").show();
			}*/
			$("#giftForm #priceGift").text($(this).attr('data-price-text'));
			$("#giftsSelect").hide();
			$("#giftForm").show();	
			$("#giftForm #giftImg").attr("src", $("img",this).attr('src'));
			$("#giftSendForm input#giftId").attr("value", $(this).attr('id'));
			return false;
		});
		
		$("#donateGifts_back").live('click', function() {
							
			$("#giftsSelect").show();	
			$("#giftForm").hide();
			return false;
		});
	},
	
	send: function(url)
	{
			$.arcticmodal({
				type: 'ajax',
				url: url,
				ajax: {
					type: 'GET',
					cache: false,
					//dataType: 'json',
					success: function(data, el, responce) {
										
										var h = $('<div class="box-modal" id="modalPhotoBox">' +
												'<div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>' +
												'<p><b /></p><p />' +
												'</div>');
										$('B', h).html('<span id="title">Выберите подарок</span>');
										$('P:last', h).html(''+responce+'');
										data.body.html(h);
					}
				},
				beforeOpen: function(data, el) {
					$('.navbar').attr('style','margin-right:17px;');
				},
				afterClose: function(data, el) {
					$('.navbar').attr('style','');
				},
				openEffect: {
					type: 'none'
				},
				closeEffect: {
					type: 'none'
				}
			});
	},
	
	add: function(obj)
	{
	//console.log(obj.attr('action'));
        $("input").focus();
	$.ajax({
		type: "POST",
		url: obj.attr('action'),
		data: obj.serialize(),
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                        //YiiAlert.error('Фотография удалена.', 404);
                },
		beforeSend: function(){
                        
                   $("input", obj).attr('disabled', 'disabled');
		},
		success: function(responce){
			$("#giftSendForm #errorSummary").hide().empty();
			/*$("button", obj).button('reset');*/
                        //$("input", obj).show();
			$("input", obj).attr('disabled', '');
                        
                        
			if (responce.status=='1') {
                                
		   $("#giftSendForm").html('<div align="center"><b>Подарок отправлен.</b></div>');
				return;
			}

			
$.each(responce, function(key, value) { 

			if (value=='refill_your_account') {
				$("#giftSendForm").hide();
				$("#giftBalance").show();
				return;
			}

  $("#giftSendForm #errorSummary").prepend('<div>'+value+'</div>');

});
$("#giftSendForm #errorSummary").fadeTo('speed', 1.0);

			
			
		}
	});
	
	
        }
}

jQuery(function($) {
	gifts.init();
});