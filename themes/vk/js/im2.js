
IM = {
	userId: 0,
	toUserId: 0,
	userName: 0,
	check: 0,
	
	newCountMessages: 0,
	_stmg: 0,
	_stmgTimer: 0,
	
	title: 'Мгновенные сообщения',
	
	init: function(hash)
	{
		$("#chatlog").html('Загрузка...');
		
		if (navigator.userAgent.toLowerCase().indexOf('chrome') != -1) {
			socket = io.connect('http://node.fastpromo.in:3000/', {'transports': ['xhr-polling']});
		} else {
			socket = io.connect('http://node.fastpromo.in:3000/');
		}
		
		socket.on('connect', function () {
			$("#chatlog").show();
			$("#systemlog").hide();
			$("#chatlog").empty();
			IM.loading();
		});
		
		socket.on('reconnecting', function () {
			$("#chatlog").hide();
			$("#systemlog").show().html('<div style="color:red;">Соединение с сервером потеряно, переподключаемся...</div>');
		});
		
			//socket.on('message'+IM.userId+IM.toUserId, function (msg) {
			socket.on(hash, function (msg) {
				if (msg.event=='messageReceived') {
					IM.newCountMessages++;
					//IM.sound('qip');
					$("#chatlog").append('<div	class="message info">'+msg.login+' <span>'+msg.time+'</span><div class="message m">'+unescape(msg.text)+'</div></div>');
				}
				else if (msg.event=='userJoined') {
					$("#chatlog").append('<div	class="message sys info">'+msg.login+' <span>'+msg.time+'</span><div class="message m">Пользователь зашел в диалог.</div></div>');
					
					//$("#dialog_status").html('В диалоге с вами');
				}
				else if (msg.event=='userSplit') {
					$("#chatlog").append('<div	class="message sys info">'+msg.login+' <span>'+msg.time+'</span><div class="message m">Пользователь покинул диалог.</div></div>');
					//$("#dialog_status").html('');
				}
				IM.chatscroll();
			});
			
			
			socket.on('message', function (msg) {
				
				$("#chatlog").append('<div	class="message my info">Я <span>'+msg.time+'</span><div class="message my m">'+unescape(msg.text)+'</div></div>');
				
				IM.chatscroll();
			});
		
		
		socket.on('news', function (data) {
			/*console.log(data);*/
			socket.emit('my other event', { 'userId': IM.userId, 'toUserId': IM.toUserId, 'login': IM.userName, 'check': IM.check});
		});
		
		socket.on('disconnect', function () {
			
			$("#chatlog").hide();
			$("#systemlog").show().html('<div style="color:red;">Соединение с сервером разорвано.</div>');
				
		});
		
		socket.on('error', function (e) {
			$("#chatlog").hide();
			$("#systemlog").show().html('<div style="color:red;">Ошибка: ' + (e ? e : 'неизвестная ошибка.')+'</div>');

		});
		
		setInterval('IM.newStatus()',500);
		
		$('#sendMessageForm textarea').live('keydown', function(e) {
			if (e.ctrlKey && e.keyCode == 13) {
				$("#sendMessageForm").submit();
			}
		});
		
		$("#sendMessageForm").submit(function() {
			greeting = $("textarea", this).val();
			if (!greeting) {
				alert("Введите сообщение");
				return false;
			}
			
			$.ajax({
				type: "POST",
				dataType:"json",
				url: $(this).attr("action"),
				data: $(this).serialize(),
				cache: false,
				beforeSend: function()
				{
				
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$("#chatlog").html('<div style="color:red;">SYSTEM ERROR '+XMLHttpRequest.text+':</div><div>Произошла ошибка сервиса мгновенных сообщений, пожалуйста, повторите попытку позже.</div><div>Если ошибка выскакивает постоянно, то, обратитесь в тех. поддержку.</div>');
				},
				success: function(data)
				{
					/*console.log(data);*/
					/*socket.json.send(data);*/
					socket.emit('message', data);
				}
			});
			
			$("textarea", this).attr('value', '');
			IM.chatscroll();
			return false;
		});
		
		$("body").mouseover(function(){
			IM.newCountMessages=0;
			IM._stmgTimer=0;
			/*if (navigator.userAgent.toLowerCase().indexOf('msie') != -1) {
				$().attr('title', IM.title);
			} else {
				$("title").html(IM.title);
			}*/
			$().attr('title', IM.title);
		 });
	},

	loading: function()
	{
		$.ajax({
			type: "GET",
			dataType:"json",
			url: $("#sendMessageForm").attr("action"),
			data: "cmd=loading",
			cache: false,
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$("#chatlog").html('<div style="color:red;">SYSTEM ERROR '+XMLHttpRequest.status+':</div><div>Произошла ошибка сервиса мгновенных сообщений, пожалуйста, повторите попытку позже.</div><div>Если ошибка выскакивает постоянно, то, обратитесь в тех. поддержку.</div>');
			},
			beforeSend: function()
			{
				
			},
			success: function(data)
			{
				$.each(data,function(index,row)
				{
					if (row.isMy)
					{
						$("#chatlog").prepend('<div	class="message my info">Я <span>'+row.date+'</span><div class="message my m">'+row.text+'</div></div>');
					} else {
						$("#chatlog").prepend('<div	class="message info">'+row.login+' <span>'+row.date+'</span><div class="message m">'+row.text+'</div></div>');
					}
				});
				IM.chatscroll();
			}
		});
	},
	
	newStatus: function()
	{
		if (IM.newCountMessages>0) {
			if (IM._stmg==1) {
				IM._stmg=0;
				/*if (navigator.userAgent.toLowerCase().indexOf('msie') != -1) {
				
				} else {
					$("title").text('...');
				}*/
				$().attr('title', '******');
			} else {
				/*if (navigator.userAgent.toLowerCase().indexOf('msie') != -1) {
				
				} else {
					$("title").text(IM.title);
				}*/
				$().attr('title', IM.title);
				IM._stmg=1;
			}
		}
		IM._stmgTimer++;
	},
	
	chatscroll:function()
	{
		if (navigator.userAgent.toLowerCase().indexOf('msie') != -1)
		{
			$("#chatlog").scrollTo( '100%', 200 );
		} else {
			document.querySelector('#chatlog').scrollTop = document.querySelector('#chatlog').scrollHeight;
		}
	},
	
	sound:function(path)
	{
		$(document.body).append('<div id="soundContainer"></div>');
		var sound = '/images/lifemeet/'+path+'.swf';
		if (IM._stmgTimer>5) {
			if ($.browser.msie) {
				document.getElementById('soundContainer').innerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0"><param name="movie" value="'+sound+'"><param name="quality" value="high"></object>';
			} else {
				$('#soundContainer').html('<embed src="'+sound+'" width="1" height="1" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash"></embed>');
			}
		}
	}
}