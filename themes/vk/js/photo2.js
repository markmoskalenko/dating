

photoShow = {
	
	num: 0,
	data: 0,
        total: 0,
        url: '',
        pageStart: 1,
        page: 1,
        real_num: 0,

	init: function()
	{
                $("#showModalPhotoBox").live('click', function() {
                        photoShow.url=$(this).attr("href");
			$.arcticmodal({
				type: 'ajax',
				url: photoShow.url,
				ajax: {
                                        type: 'GET',
                                        data: "page="+photoShow.pageStart,
					cache: false,
					dataType: 'json',
					success: function(data, el, responce) {
                                                if (!responce.nums) {
                                                        $.arcticmodal('close');
                                                        YiiAlert.error('Фотография удалена или перемещена.', 404);
                                                        return;
                                                }
                                                if (!responce.nums[responce.photoId]) {
                                                        $.arcticmodal('close');
                                                        YiiAlert.error('Фотография удалена или перемещена.', 404);
                                                        return;
                                                }
                                                photoShow.data=responce;
                                                console.log(responce.nums[responce.photoId].key);
                                               
                                                photoShow.num=responce.nums[responce.photoId].key;
                                                photoShow.total=responce.photos.length;

                                                
                                                photoShow.real_num=(responce.limit*(photoShow.pageStart-1));
                                                
                                                photoComments.init(responce.photos[photoShow.num].commentsUrl);
                                                //alert(responce.photos.length);
                                                if (1==responce.count) {
                                                        title="Просмотр фотографии";
                                                } else {
                                                        title='Фотография <span id="total">'+(photoShow.real_num+photoShow.num+1)+'</span> из '+responce.count;
                                                }
                                                
						var h = $('<a href="#" id="back_modalPhotoBox"><i class="fa fa-chevron-left"></i></a><div class="box-modal" id="modalPhotoBox">' +
								'<div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>' +
								'<p><b /></p><p />' +
								'</div>');
						$('B', h).html('<span class="title_photo">'+title+' <span id="loading_stat" style="display:none;"><img src="'+imageUrl('loading.gif')+'" align="absmiddle" /></span>');
						$('P:last', h).html('<div class="row" align="center" style="min-width:800px;"><a href="#" style="margin:10px;" id="next_modalPhotoBox"><img src="'+responce.photos[photoShow.num].image+'" border="0" /></a></div>'+
                                                                    '<br> <p id="descr">'+responce.photos[photoShow.num].descr+'</p> '+
                                                                    '<p class="descr">Добавлена: <span id="date">'+responce.photos[photoShow.num].date+'</span>'+
                                                                    '<a href="#" id="likePhoto" rel="'+photoShow.data.photos[photoShow.num].likeUrl+'" onclick="likePhoto.like(this);return false;" class="">Мне нравится<i class="fa fa-heart"></i></a><a href="'+photoShow.data.photos[photoShow.num].likeUrl+'" id="showModalLikePhoto"><span id="countLikePhoto">'+photoShow.data.photos[photoShow.num].likes+'</span></a>'+
                                                                    '<div id="comments" align="center"><img src="'+imageUrl('loading.gif')+'" align="absmiddle" />  Загрузка...</div>'+
                                                                    '');
						data.body.html(h);
                                                
                                                photoShow.resizeImage();     
					}
				},
				beforeOpen: function(data, el) {
                                        photoShow.page=photoShow.pageStart;
					$('.navbar').attr('style','margin-right:17px;');
				},
				afterClose: function(data, el) {
                                        photoShow.page=photoShow.pageStart;
					$('.navbar').attr('style','');
				},
				openEffect: {
					type: 'none'
				},
				closeEffect: {
					type: 'none'
				}
			});
                        return false;
		});
                
		$('body').live('keydown', function(e) {
                        /*alert(e.keyCode);*/
			if (e.keyCode == 39) {
				$("#next_modalPhotoBox").click();
			}
			if (e.keyCode == 37) {
				$("#back_modalPhotoBox").click();
			}
		});
                
                $("#next_modalPhotoBox").live('click', function() {
                        photoShow.num=photoShow.num+1;
                        
                         if (photoShow.data.count<(photoShow.real_num+photoShow.num+1)) {
                                
                                $.arcticmodal('close');
                        }
                        else if (photoShow.data.photos.length<(photoShow.num+1)) {
                                photoShow.preloading();
                                //$.arcticmodal('close');
                        } else {
                                $("#modalPhotoBox #next_modalPhotoBox img").attr("src", ""+photoShow.data.photos[photoShow.num].image+"");
                                $("#modalPhotoBox #total").text((photoShow.real_num+photoShow.num+1));
                                $("#modalPhotoBox #date").html(photoShow.data.photos[photoShow.num].date);
                                //$("#modalPhotoBox #countLikePhoto").html(photoShow.data.photos[photoShow.num].likes);
                                $("#modalPhotoBox #showModalLikePhoto").attr("href", photoShow.data.photos[photoShow.num].likeUrl);
                                $("#modalPhotoBox #countLikePhoto").html(photoShow.data.photos[photoShow.num].likes);
                                $("#modalPhotoBox #likePhoto").attr("rel", photoShow.data.photos[photoShow.num].likeUrl);
                                $("#modalPhotoBox #descr").html(photoShow.data.photos[photoShow.num].descr);
                                photoComments.init(photoShow.data.photos[photoShow.num].commentsUrl);
				$('#modalPhotoBox #next_modalPhotoBox img').fadeTo(0, 0.3);
                                photoShow.resizeImage();
                        }
                        return false;
                });

                $("#back_modalPhotoBox").live('click', function() {

                        photoShow.num=photoShow.num-1;

                        if (photoShow.num==-1) {
                                $.arcticmodal('close');
                        } else {
                                $("#modalPhotoBox #next_modalPhotoBox img").attr("src", ""+photoShow.data.photos[photoShow.num].image+"");
                                $("#modalPhotoBox #total").text((photoShow.real_num+photoShow.num+1));
                                $("#modalPhotoBox #date").html(photoShow.data.photos[photoShow.num].date);
                                //$("#modalPhotoBox #countLikePhoto").html(photoShow.data.photos[photoShow.num].likes);
                                $("#modalPhotoBox #showModalLikePhoto").attr("href", photoShow.data.photos[photoShow.num].likeUrl);
                                $("#modalPhotoBox #countLikePhoto").html(photoShow.data.photos[photoShow.num].likes);
                                $("#modalPhotoBox #likePhoto").attr("rel", photoShow.data.photos[photoShow.num].likeUrl);
                                $("#modalPhotoBox #descr").html(photoShow.data.photos[photoShow.num].descr);
                                photoComments.init(photoShow.data.photos[photoShow.num].commentsUrl);
				$('#modalPhotoBox #next_modalPhotoBox img').fadeTo(0, 0.3);
                                photoShow.resizeImage();
                        }
                        return false;
                });
                
                likePhoto.init();
        },
        
        preloading: function(type)
	{
               	$.ajax({
		type: "GET",
		url: photoShow.url,
                data: "page="+(photoShow.page+1),
		cache: false,
		dataType: 'json',
		beforeSend: function(){
			/*$("#modalPhotoBox #loading_stat").show();*/
		},
		success: function(responce){
                        /*$("#modalPhotoBox #loading_stat").hide();*/
                        
                        photoShow.num=0;
                                photoShow.page=(photoShow.page+1);

                        photoShow.data.photos=responce.photos;
photoShow.real_num=(responce.limit*(photoShow.page-1));
                        
                                $("#modalPhotoBox #next_modalPhotoBox img").attr("src", ""+photoShow.data.photos[photoShow.num].image+"");
                                $("#modalPhotoBox #total").text((photoShow.real_num+photoShow.num+1));
                                $("#modalPhotoBox #date").html(photoShow.data.photos[photoShow.num].date);
                               // $("#modalPhotoBox #countLikePhoto").html(photoShow.data.photos[photoShow.num].likes);
                                $("#modalPhotoBox #showModalLikePhoto").attr("href", photoShow.data.photos[photoShow.num].likeUrl);
                                $("#modalPhotoBox #countLikePhoto").html(photoShow.data.photos[photoShow.num].likes);
                                $("#modalPhotoBox #likePhoto").attr("rel", photoShow.data.photos[photoShow.num].likeUrl);
                                $("#modalPhotoBox #descr").html(photoShow.data.photos[photoShow.num].descr);
                                photoComments.init(photoShow.data.photos[photoShow.num].commentsUrl);
                                photoShow.resizeImage();
                }
                });      
        },
        
        resizeImage: function()
	{
$('#modalPhotoBox #next_modalPhotoBox img').load(function(){

		$('#modalPhotoBox #next_modalPhotoBox img').fadeTo(0, 1.0);
		
		
    var w  = photoShow.data.photos[photoShow.num].width;
    var h = photoShow.data.photos[photoShow.num].height;

                var nh=600;   
                var nw=890;  
                var k1=nh/nw;
                var k2=h/w;

                if (k1>k2)
                {
                    h=h*(nw/w);
                    w=nw;
                } else {
                    w=w*(nh/h);
                    h=nh;
                }
                $('#next_modalPhotoBox img').width(w);
               $('#next_modalPhotoBox img').height(h);
	       
	       
}); 
                
        }
}

likePhoto = {
        page: 1,
	_like: 0,
        
	init: function()
	{
		$("#showModalLikePhoto").live('click', function() {
							
			$.arcticmodal({
				type: 'ajax',
				url: $(this).attr("href"),
				ajax: {
					type: 'GET',
					cache: false,
					//dataType: 'json',
					success: function(data, el, responce) {
										
										var h = $('<div class="box-modal" id="modalPhotoBox">' +
												'<div class="box-modal_close arcticmodal-close">закрыть</div>' +
												'<p><b /></p><p />' +
												'</div>');
										$('B', h).html('<span id="title">Оценили фотографию</span>');
										$('P:last', h).html(responce);
										data.body.html(h);
					}
				},
				beforeOpen: function(data, el) {
					$('.navbar').attr('style','margin-right:17px;');
				},
				afterClose: function(data, el) {
					$('.navbar').attr('style','');
				},
				openEffect: {
					type: 'none'
				},
				closeEffect: {
					type: 'none'
				}
			});
			return false;
		});
        },
        
	setLike: function(like)
	{
                if (like) {
                        $("#likePhoto").attr("class", "link");
                        likePhoto._like=1;
                } else {
                        $("#likePhoto").attr("class", "");
                        likePhoto._like=0;
                }
        },
        
	like: function(obj)
	{
$("input").focus();
                if (isGuest()) {
                        return;
                }
                if (likePhoto._like) {
                        
                        $.ajax({
                                type: "GET",
                                url: $(obj).attr("rel"),
                                data: '&like=0',
                                cache: false,
                                beforeSend: function(){
		   
                                },
                                success: function(html){
                                        num=Math.floor($("#countLikePhoto").text())-1;
                                        $("#countLikePhoto").text(num);
                                        $("#likePhoto").attr("class", "");
                                        likePhoto._like=0;
                                        
                                }
                        });
                } else {
                        $.ajax({
                                type: "GET",
                                url: $(obj).attr("rel"),
                                data: '&like=1',
                                cache: false,
                                beforeSend: function(){
                                        
                                },
                                success: function(html){
                                        num=Math.floor($("#countLikePhoto").text())+1;
                                        $("#countLikePhoto").text(num);
                                       $("#likePhoto").attr("class", "link");
                                        likePhoto._like=1;
                                        
                                }
                        });
                }
        }
}

photoComments = {


	init: function(url)
	{
               	$.ajax({
		type: "GET",
		url: url,
		data: 'content=all',
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                        YiiAlert.error('Фотография удалена.', 404);
                        $('#modalPhotoBox #comments').html('');
                },
		beforeSend: function(){
			/*$('#modalPhotoBox #comments').html('<img src="'+imageUrl('loading.gif')+'" align="absmiddle" />');*/
                        $('#modalPhotoBox #comments').fadeTo(0, 0.3);
                                $("#modalPhotoBox #countLikePhoto").html('');
                                        $("#likePhoto").attr("class", "");
		},
		success: function(responce){
                        $('#modalPhotoBox #comments').fadeTo(0, 1.0);
                        $('#modalPhotoBox #comments').html(responce.comments);
                        
                        likePhoto.setLike(responce.myLike);
                                $("#modalPhotoBox #countLikePhoto").html(responce.likes);
                }
                });
                
        },
        

	add: function(obj)
	{
	//console.log(obj.attr('action'));
        $("input").focus();
	$.ajax({
		type: "POST",
		url: obj.attr('action'),
		data: obj.serialize(),
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                        YiiAlert.error('Фотография удалена.', 404);
                },
		beforeSend: function(){
                        
                   $("input", obj).hide();
                   $("#loading_add_comment").show();
		},
		success: function(responce){
			$("#commentsForm #errorSummary").hide().empty();
			/*$("button", obj).button('reset');*/
                        $("input", obj).show();
                   $("#loading_add_comment").hide();
                        
			if (responce.captcha) {
                                
                                spamProtection.captcha(responce.captcha, 'addComment($("#commentsForm")); return false;');
				return;
			}
                        
                        
			if (responce.html) {
                                
		   $("#commentsForm textarea").attr("value", "");
				$("#commentsRows").html(responce.html);
				return;
			}
$.each(responce, function(key, value) { 

  $("#commentsForm #errorSummary").prepend('<div>'+value+'</div>');

});
$("#commentsForm #errorSummary").fadeTo('speed', 1.0);

			
			
		}
	});
	
	
	
	
	//alert("1");
//$("#wallForm").html('хуй нах через ажакс'); 
        },
        
        deleteComment: function(url) {
                //$("#commentsPages .active a").attr('href')
                $.ajax({
                        type: "GET",
                        url: url,
                        cache: false,
                        dataType: 'json',
                        beforeSend: function(){
		   
                        },
                        success: function(responce){
			
                                $("#comment_"+responce.id).html('<div class="alert">Комментарий удален.</div>');
                        }
                });
        }
}

jQuery(function($) {
    
$("#modalPhotoBox a#setAvatar").live('click', function() {

	$.ajax({
		type: "GET",
		url: $(this).attr("href"),
		cache: false,
		dataType: 'json',
		beforeSend: function(){
                        
		},
		success: function(responce){
                        YiiAlert.success('<i class="fa fa-check"></i>Фотография установлена как аватарка.');
		}
	});

        return false;     
});


$("#modalPhotoBox a#deletePhoto").live('click', function() {
        
        
	$.ajax({
		type: "GET",
		url: $(this).attr("href"),
		cache: false,
		dataType: 'json',
		beforeSend: function(){
                        
		},
		success: function(responce){
                        YiiAlert.success('<i class="fa fa-times"></i>Фотография удалена.');
			$("#i"+responce.photoId).hide();
		}
	});
        
        $.arcticmodal('close');
 
        
        
	/*$("a#next_modalPhotoBox").attr("onclick", "$.arcticmodal('close');");
        
        $('#modalPhotoBox #title').hide();
        $('#modalPhotoBox #date').parent().parent().hide();
         $('#modalPhotoBox #comments').html('<div class="alert alert-warning">Фотография удалена.</div>');
*/
        return false;     
});

$("#modalPhotoBox a#editPhoto").live('click', function() {
        //alert($(this).attr("href"));
        
							$.arcticmodal({
								type: 'ajax',
								url: $(this).attr("href"),
								ajax: {
									type: 'GET',
									cache: false,
									success: function(data, el, responce) {
										
										var h = $('<div class="box-modal" id="modalPhotoBox">' +
												'<div class="box-modal_close arcticmodal-close">закрыть</div>' +
												'<p><b /></p><p />' +
												'</div>');
										$('B', h).html('<span id="title">Редактировать фотографию</span>');
										$('P:last', h).html(''+responce+'');
										data.body.html(h);
									}
								},
								beforeOpen: function(data, el) {
									$('.navbar').attr('style','margin-right:17px;');
								},
								afterClose: function(data, el) {
									$('.navbar').attr('style','');
								},
								openEffect: {
									type: 'none'
								},
								closeEffect: {
									type: 'none'
								}
							});

        return false;     
});
    
$("#commentsPages a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		data: '&_a=wall',
		cache: false,
		beforeSend: function(){
		   
		},
		success: function(html){
			
			$("#commentsRows").html(html);
		}
	});
	return false;
});
    
                     photoShow.init();                                   

});