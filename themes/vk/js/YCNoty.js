
YCNoty = {

	init: function(hashUser, host)
	{
		host=$.base64('decode', host);
		
		if (navigator.userAgent.toLowerCase().indexOf('chrome') != -1) {
			socket = io.connect('http://'+host+'/', {'transports': ['xhr-polling']});
		} else {
			socket = io.connect('http://'+host+'/');
		}
		
		socket.on('connect', function () {
			console.log('io connect');
		});
		
		socket.on('reconnecting', function () {
			console.log('io reconnecting');
		});
		
		socket.on('disconnect', function () {
			console.log('io disconnect');
		});
		
		socket.on('error', function (e) {
			console.log('io Ошибка: ' + (e ? e : 'неизвестная ошибка.'));
		});
		
			socket.on('message'+hashUser, function (msg) {
				if (msg.event=='messageReceived') {

        var n = noty({
            text        : "<div class='noty_title_main'>Новое сообщение</div><div class='noty_title'><img class='left noty_img' src='"+(msg.image ? msg.image : imageUrl('camera_s.png'))+"' width='55' height='55'>"+unescape(msg.name)+"</div><div>"+unescape(msg.message)+"</div>",
	    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"><i class="fa fa-times"></i></div></div>',
            type        : 'alert',
            dismissQueue: true,
            layout      : 'bottomLeft',
            theme       : 'someOtherTheme',
            closeWith   : ['button', 'click'],
            maxVisible  : 20,
            modal       : false,
	    animation: {
		open: {height: 'toggle'},
		close: {height: 'toggle'},
		easing: 'swing',
		speed: 300 // opening & closing animation speed
	    },
	    callback: {

        afterClose: function() {
		
		location.href = '/messages/dialogue?id='+msg.to_userId; 
	}
    },
	     force: true
        });
					$("#newMessagesCounter").show();
					count=Math.round($("#newMessagesCounter label").text())+1
					$("#newMessagesCounter label").text(count);
					$.ionSound.play("new_message");
				}
			});
		
			socket.on('friendsRequests'+hashUser, function (msg) {
				if (msg.event=='messageReceived') {

        var n = noty({
            text        : "<div class='noty_title_main'>Новое уведомление</div><div class='noty_title'><img class='left noty_img' src='"+(msg.image ? msg.image : imageUrl('camera_s.png'))+"' width='55' height='55'>"+unescape(msg.name)+" <span>Хочет добавить вас в друзья</span></div></div>",
	    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"><i class="fa fa-times"></i></div></div>',
            type        : 'alert',
            dismissQueue: true,
            layout      : 'bottomLeft',
            theme       : 'someOtherTheme',
            closeWith   : ['button', 'click'],
            maxVisible  : 20,
            modal       : false,
	    animation: {
		open: {height: 'toggle'},
		close: {height: 'toggle'},
		easing: 'swing',
		speed: 300 // opening & closing animation speed
	    },
	    callback: {

        afterClose: function() {
		
		location.href = '/friends?section=requests'; 
	}
    },
	     force: true
        });
					$("#requestFriendsCounter").show();
					count=Math.round($("#requestFriendsCounter label").text())+1
					$("#requestFriendsCounter label").text(count);
					$.ionSound.play("noty");
				}
			});
		
		
			socket.on('notifications'+hashUser, function (msg) {
				if (msg.event=='messageReceived') {
					title = 'Новое оповещение';
					text = '';
					photo = '';
					name = unescape(msg.name);
					ava = (msg.image ? msg.image : imageUrl('camera_s.png'));
					if (msg.type=='4') {
						title = 'Ваша фотография понравилась';
						text = 'оценил'+(msg.sex==2 ? 'а' : '')+' Вашу фотографию';
						photo = "<img class='right noty_img' src='"+msg.photo+"' width='40' height='40' style='margin:0px -5px -5px -5px;'>";
					}
					if (msg.type=='5') {
						title = 'Новый комментарий';
						text = msg.text;
						photo = "<img class='right noty_img' src='"+msg.photo+"' width='40' height='40' style='margin:0px -5px -5px -5px;'>";
					}
					if (msg.type=='7') {
						title = 'Заявка принята';
						text = 'принял'+(msg.sex==2 ? 'а' : '')+' Вашу заявку в друзья';
					}
					if (msg.type=='8') {
						title = 'Заявка отклонена';
						text = 'отклонил'+(msg.sex==2 ? 'а' : '')+' Вашу заявку в друзья';
					}
					if (msg.type=='9') {
						title = 'Вас удалили из друзей';
						text = 'удалил'+(msg.sex==2 ? 'а' : '')+' Вас из друзей';
					}
					if (msg.type=='6') {
						title = 'Вы понравились';
						text = 'добавил'+(msg.sex==2 ? 'а' : '')+' Вас в закладки';
					}
					if (msg.type=='10') {
						title = 'Новый подарок';
						if (msg.text) text = msg.text; else text = 'подарил'+(msg.sex==2 ? 'а' : '')+'<br>Вам подарок';
						photo = "<img class='right noty_img' src='"+imageUrl('gifts/gift_'+msg.photo+'.png')+"' width='40' height='40' style='margin:0px -5px -5px -5px;'>";
					}
					if (msg.type=='1') {
						title = 'Фотография одобрена';
						text = '<br>'+msg.text;
						photo = "<img class='right noty_img' src='"+msg.photo+"' width='40' height='40' style='margin:0px -5px -5px -5px;'>";
						name = 'Служба фотографий';
						ava = imageUrl('gear_s.png');
					}
					if (msg.type=='2') {
						title = 'Фотография перемещена';
						text = '<br>'+msg.text;
						photo = "<img class='right noty_img' src='"+msg.photo+"' width='40' height='40' style='margin:0px -5px -5px -5px;'>";
						name = 'Служба фотографий';
						ava = imageUrl('gear_s.png');
					}
					if (msg.type=='3') {
						title = 'Фотография удалена';
						text = '<br>'+msg.text;
						name = 'Служба фотографий';
						ava = imageUrl('gear_s.png');
					}
					
        var n = noty({
            text        : "<div class='noty_title_main'>"+title+"</div><div class='noty_title'><img class='left noty_img' src='"+ava+"' width='55' height='55'>"+name+" <span>"+text+"</span>"+photo+"</div></div>",
	    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"><i class="fa fa-times"></i></div></div>',
            type        : 'alert',
            dismissQueue: true,
            layout      : 'bottomLeft',
            theme       : 'someOtherTheme',
            closeWith   : ['button', 'click'],
            maxVisible  : 20,
            modal       : false,
	    animation: {
		open: {height: 'toggle'},
		close: {height: 'toggle'},
		easing: 'swing',
		speed: 300 // opening & closing animation speed
	    },
	    callback: {

        afterClose: function() {
		
		location.href = '/notifications'; 
	}
    },
	     force: true
        });
					$("#notificationsCounter").show();
					count=Math.round($("#notificationsCounter label").text())+1
					$("#notificationsCounter label").text(count);
					$.ionSound.play("noty");
				}
			});

	}
}

