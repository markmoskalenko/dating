
YCNoty = {
	socket: [],
	hashUser: '',
	dialogueId: '',
	messagesCounter: 0,
	requestFriendsCounter: 0,
	notificationsCounter: 0,
	mail: 0,

	init: function(hashUser, host)
	{
		host=$.base64('decode', host);
		
		/*if (navigator.userAgent.toLowerCase().indexOf('chrome') != -1) {
			socket = io.connect('http://'+host+'/', {'transports': ['xhr-polling']});
		} else {*/
			socket = io.connect('http://'+host+'/');
		//}
		
		socket.on('connect', function () {
			console.log('io connect');
		});
		
		socket.on('reconnecting', function () {
			console.log('io reconnecting');
		});
		
		socket.on('disconnect', function () {
			console.log('io disconnect');
		});
		
		socket.on('error', function (e) {
			console.log('io Ошибка: ' + (e ? e : 'неизвестная ошибка.'));
		});
		
		YCNoty.socket=socket;
		YCNoty.hashUser=hashUser;
		
			socket.on('message'+hashUser, function (msg) {
				if (msg.event=='messageReceived') {
					
					if (YCNoty.dialogueId!=msg.to_userId) {
						$("#newMessagesCounter, #newMessagesCounter2").show();
						YCNoty.messagesCounter=YCNoty.messagesCounter+1
						$("#newMessagesCounter, #newMessagesCounter2").text(YCNoty.messagesCounter);
						$.ionSound.play("new_message");
					}
					
					if (YCNoty.mail) {
						$.ajax({
							type: "GET",
							url: '/im'+YCNoty.dialogueId,
							data: '_a=container_mail',
							cache: false,
							beforeSend: function(xhr){
							},
							success: function(html){
								$("#container_mail").html(html);
							}
						});
					}
				}
			});
			
			socket.on('friendsRequests'+hashUser, function (msg) {
				if (msg.event=='messageReceived') {
					$("#requestFriendsCounter, #requestFriendsCounter2").show();
					//count=Math.round($("#requestFriendsCounter, #requestFriendsCounter2").text())+1
					YCNoty.requestFriendsCounter=YCNoty.requestFriendsCounter+1;
					$("#requestFriendsCounter, #requestFriendsCounter2").text(YCNoty.requestFriendsCounter);
					$.ionSound.play("noty");
				}
			});
			
			socket.on('notifications'+hashUser, function (msg) {
				if (msg.event=='messageReceived') {
					$("#notificationsCounter, #notificationsCounter2").show();
					//count=Math.round($("#notificationsCounter, #notificationsCounter2").text())+1
					YCNoty.notificationsCounter=YCNoty.notificationsCounter+1;
					$("#notificationsCounter, #notificationsCounter2").text(YCNoty.notificationsCounter);
					$.ionSound.play("noty");
				}
			});
	}
}