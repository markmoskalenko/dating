
photoShow = {
	
	num: 0,
	data: 0,
        total: 0,
        url: '',
        pageStart: 1,
        page: 1,
        real_num: 0,
        responce: 0,
        title: '',

	init: function()
	{
		photoShow.responce = jQuery.parseJSON( photoShow.data );
		
                if (!photoShow.responce.nums) {
                        $("#photoShow").html('<p class="text-primary">Фотография удалена или перемещена.</p>');
                        //YiiAlert.error('Фотография удалена или перемещена.', 404);
                        return;
                }
                if (!photoShow.responce.nums[photoShow.responce.photoId]) {
                        
                        $("#photoShow").html('<p class="text-primary">Фотография удалена или перемещена.</p>');
                        //YiiAlert.error('Фотография удалена или перемещена.', 404);
                        return;
                }
		
		photoShow.num=photoShow.responce.nums[photoShow.responce.photoId].key;
		photoShow.total=photoShow.responce.photos.length;
		photoShow.real_num=(photoShow.responce.limit*(photoShow.pageStart-1));
		
		$("#photoShow img").attr('src', photoShow.responce.photos[photoShow.num].image);
		
                if (1==photoShow.responce.count) {
                        photoShow.title="Просмотр фотографии";
                } else {
                        photoShow.title='Фотография <span id="total">'+(photoShow.real_num+photoShow.num+1)+'</span> из '+photoShow.responce.count;
                }
		
		$("#photoShow_title").html(photoShow.title);
		photoComments.init(photoShow.responce.photos[photoShow.num].commentsUrl);
		
		$("#photoShow").swipe( { swipeLeft:photoShow.swipeNext, swipeRight:photoShow.swipeBack, swipeStatus:photoShow.swipe, allowPageScroll:"auto"} );
		
		$("#photoShow").on('click', function() {
			photoShow.next();
		});
		 
		$('#photoShow img').load(function(){
			$('#photoShow img').fadeTo(0, 1.0);
		}); 
        },
	next: function()
	{
		$('#photoShow img').fadeTo(0, 0.5);
		photoShow.num=photoShow.num+1;
		
                if (photoShow.responce.count<(photoShow.real_num+photoShow.num+1)) {
                        photoShow.all_photos();
                }
                else if (photoShow.responce.photos.length<(photoShow.num+1)) {
                        photoShow.preloading();
                } else {
			history.pushState(null, '', photoShow.responce.photos[photoShow.num].url);
			$("#photoShow_title #total").text((photoShow.real_num+photoShow.num+1));
			$("#photoShow img").attr("src", ""+photoShow.responce.photos[photoShow.num].image+"");
			photoComments.init(photoShow.responce.photos[photoShow.num].commentsUrl);
		}
        },
	back: function()
	{
		$('#photoShow img').fadeTo(0, 0.5);
		
		photoShow.num=photoShow.num-1;
		
		if (photoShow.num==-1) {
			photoShow.all_photos();
		} else {
			history.pushState(null, '', photoShow.responce.photos[photoShow.num].url);
			$("#photoShow_title #total").text((photoShow.real_num+photoShow.num+1));
			$("#photoShow img").attr("src", ""+photoShow.responce.photos[photoShow.num].image+"");
			photoComments.init(photoShow.responce.photos[photoShow.num].commentsUrl);
		}
        },
	
	all_photos: function()
	{
		
		$("a.back-photo-gallery").click();

        },
	
	swipeNext: function(event, direction)
	{
		photoShow.next();
        },
	
	swipeBack: function(event, direction)
	{
		photoShow.back();
        },
	
	swipe: function(event, phase, direction, distance)
	{
		
        },
        
        preloading: function()
	{
               	$.ajax({
		type: "GET",
		url: '/top/photos?photoId='+photoShow.responce.photoId,
                data: "&preloading=1&page="+(photoShow.page+1),
		cache: false,
		dataType: 'json',
		beforeSend: function(){
			/*$("#modalPhotoBox #loading_stat").show();*/
		},
		success: function(responce){
                        /*$("#modalPhotoBox #loading_stat").hide();*/
                        
                        photoShow.num=0;
                                photoShow.page=(photoShow.page+1);

                        photoShow.responce=responce;
			
			console.log(responce);
photoShow.real_num=(photoShow.responce.limit*(photoShow.page-1));
                        
			$("#photoShow_title #total").text((photoShow.real_num+photoShow.num+1));
			$("#photoShow img").attr("src", ""+photoShow.responce.photos[photoShow.num].image+"");
			photoComments.init(photoShow.responce.photos[photoShow.num].commentsUrl);
                }
                });      
        },
        
        remove: function(url)
	{
			if (confirm('Удалить фото?')) {
				
		$("#photoShow_comments").html('<p class="text-primary">Фотография удалена.</p>');
		
	$.ajax({
		type: "GET",
		url: url,
		cache: false,
		dataType: 'json',
		beforeSend: function(){
                        $("#photoShow_comments").html('<p class="text-muted">Удаляется...</p>');
		},
		success: function(responce){
                       
			$("#photoShow_comments").html('<p class="text-primary">Фотография удалена.</p>');
		}
	});
		
		return false;
			} else {
				return false;
			}
	},
        
        setAvatar: function(url)
	{
	$.ajax({
		type: "GET",
		url: url,
		cache: false,
		dataType: 'json',
		beforeSend: function(){
                        $("#setava").html('...');
		},
		success: function(responce){
                       
			$("#setava").html('Аватар установлен.');
		}
	});
	}
}


photoComments = {


	init: function(url)
	{
               	$.ajax({
		type: "GET",
		url: url,
		data: 'content=all',
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
			$('#photoShow_comments').fadeTo(0, 1.0);
			$("#photoShow_comments").html('<p class="text-primary">Фотография удалена.</p>');
                },
		beforeSend: function(){
			/*$('#modalPhotoBox #comments').html('<img src="'+imageUrl('loading.gif')+'" align="absmiddle" />');*/
                        $('#photoShow_comments').fadeTo(0, 0.3);
		},
		success: function(responce){
                        $('#photoShow_comments').fadeTo(0, 1.0);
                        $('#photoShow_comments').html(responce.comments);
                        
                }
                });
                
        },
        

	add: function(obj)
	{
	//console.log(obj.attr('action'));
        $("input").focus();
	$.ajax({
		type: "POST",
		url: obj.attr('action'),
		data: obj.serialize(),
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                        YiiAlert.error('Фотография удалена.', 404);
                },
		beforeSend: function(){
                        
                   $("input", obj).hide();
                   $("#loading_add_comment").show();
		},
		success: function(responce){
			$("#commentsForm #errorSummary").hide().empty();
			/*$("button", obj).button('reset');*/
                        $("input", obj).show();
                   $("#loading_add_comment").hide();
                        
			if (responce.captcha) {
                                
                                spamProtection.captcha(responce.captcha, 'addComment($("#commentsForm")); return false;');
				return;
			}
                        
                        
			if (responce.html) {
                                
		   $("#commentsForm textarea").attr("value", "");
				$("#commentsRows").html(responce.html);
				return;
			}
$.each(responce, function(key, value) { 

  $("#commentsForm #errorSummary").prepend('<div>'+value+'</div>');

});
$("#commentsForm #errorSummary").fadeTo('speed', 1.0);

			
			
		}
	});
	
	
	
	
	//alert("1");
//$("#wallForm").html('хуй нах через ажакс'); 
        },
        
        deleteComment: function(url) {
                //$("#commentsPages .active a").attr('href')
                $.ajax({
                        type: "GET",
                        url: url,
                        cache: false,
                        dataType: 'json',
                        beforeSend: function(){
		   
                        },
                        success: function(responce){
			
                                $("#comment_"+responce.id).html('<div class="alert">Комментарий удален.</div>');
                        }
                });
        }
}