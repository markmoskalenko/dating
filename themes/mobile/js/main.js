YiiAlert = {
	error: function(text, code)
	{
		if (text) {
			text="<b>Ошибка "+code+"</b>: "+text;
		} else {
			text="Неизвестная ошибка.";
		}
		
	        $("#maincol").html('Неизвестная ошибка');
	
	},
	

	success: function(text)
	{
       $.blockUI({ 
            message: '<b>'+text+'</b>', 
            fadeIn: 100, 
            centerY: true,
            css: {
		textAlign:      'center',
                left: '35%',
                width: '400px', 
                border: 'none', 
                padding: '10px',
		fontSize: '11px',
                backgroundColor: '#000', 
                opacity: .6, 
                color: '#fff' ,
'border-radius': '6px',
'-moz-border-radius':'6px',
'-khtml-border-radius': '6px'
            },
	    showOverlay: false
	   
			/*overlayCSS:  {
				backgroundColor: '#000',
				opacity:          .3
			}*/
        
        });
       setTimeout("$.unblockUI()", 2000);
	},
	

	loading: function()
	{
		$('#maincol').fadeTo(0, 0.3);
	},
	

	loadingClose: function()
	{
		$('#maincol').fadeTo(0, 1.0);
	}
}

YCMail = {
	add: function(obj)
	{
	//console.log(obj.attr('action'));
		$("input").focus();
		$.ajax({
		type: "POST",
		url: obj.attr('action'),
		data: obj.serialize(),
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                       
                },
		beforeSend: function(){
                        $("input", obj).hide();
			$("#dialogueForm textarea").val('');
			$("#loading_sentForm", obj).show();
		},
		success: function(responce){
			$("input", obj).show();
			$("#dialogueForm #errorSummary").hide().empty();
                        
			$("#loading_sentForm", obj).hide();
			if (responce.html) {
                                
				$("#container_mail").html(responce.html);
				return;
			}
			$.each(responce, function(key, value) { 
				$("#dialogueForm #errorSummary").prepend('<div>'+value+'</div>');
			});
			$("#dialogueForm #errorSummary").fadeTo('speed', 1.0);
		}
		});
        }
}


YCWall = {
	add: function(obj)
	{
	//console.log(obj.attr('action'));
		$("input").focus();
		$.ajax({
		type: "POST",
		url: obj.attr('action'),
		data: obj.serialize(),
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                       
                },
		beforeSend: function(){
                        $("input", obj).hide();
			$("#wallForm textarea").val('');
			$("#loading_sentForm", obj).show();
		},
		success: function(responce){
			$("input", obj).show();
			$("#wallForm #errorSummary").hide().empty();
                        
			$("#loading_sentForm", obj).hide();
			if (responce.html) {
                                
				$("#container_wall").html(responce.html);
				return;
			}
			$.each(responce, function(key, value) { 
				$("#wallForm #errorSummary").prepend('<div>'+value+'</div>');
			});
			$("#wallForm #errorSummary").fadeTo('speed', 1.0);
		}
		});
        }
}