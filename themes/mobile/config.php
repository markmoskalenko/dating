<?php

// this contains the application parameters that can be maintained via GUI
return array(
        'topPhotos_limit'=>24,
        'topUsers_limit'=>24,
        'mainRandomUsers_limit'=>24,
        'searchUsers_limit'=>12,
        'friends_limit'=>10,
        'photos_limit'=>60,
        'albums_limit'=>4,
        'albumsAll_limit'=>12,
        'commentsPhotos_limit'=>5,
        'messages_limit'=>10,
        'notifications_limit'=>15,
        'guests_limit'=>10,
        'favePhotos_limit'=>20,
        'faveUsers_limit'=>16,
        'likeUsers_limit'=>16,
);
