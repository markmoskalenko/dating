<?php

class CGeoIP extends CApplicationComponent
{
	public $dataFile='GeoIPCity.dat';
	public $debug=true;
	
	private $_model = null;
	private $_geoip_region_name = array();
	
	public function getCountry()
	{
		if($gi_record = $this->getModel())
		{
			return $gi_record->country_name;
		}
	}
	
	public function getCountryCode()
	{
		if($gi_record = $this->getModel())
		{
			return $gi_record->country_code;
		}
	}
	
	public function getCity()
	{
		if($gi_record = $this->getModel())
		{
			return $this->_geoip_region_name[$gi_record->country_code][$gi_record->region];
		}
	}
	
	private function getModel()
	{
		if ($this->_model === null)
		{
			if ($this->debug) Yii::beginProfile($this->dataFile);
			include(dirname(__FILE__)."/geoipcity.inc");
			$this->_geoip_region_name=require(dirname(__FILE__).'/geoipregionvars.php');
			$gi = geoip_open(dirname(__FILE__)."/".$this->dataFile, GEOIP_STANDARD);
			$this->_model = geoip_record_by_addr($gi, Yii::app()->request->userHostAddress);
			if (!$this->_model) $this->_model=false;
			if ($this->debug) Yii::endProfile($this->dataFile);
			if ($this->debug) Yii::log(print_r($this->_model,true));
		}
		return $this->_model;
	}
}