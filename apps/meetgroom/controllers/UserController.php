<?php

class UserController extends Controller
{
	public $layout = 'col_main';
	
	public function init()
	{

	}
	
	public function actions()
	{
		return array(
                    //  captcha.
                     'captcha'=>array(
                        'class'=>'CaptchaAction',
                        'maxLength'=> 3,
                        'minLength'=> 4,
                        'testLimit'=> 1,
                    )
                );
	}
	
	public function actionIndex()
	{
		$this->actionLogin();
	}
	
	public function actionRegister()
	{
		if (!Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->returnUrl);
			
		$user=new User;
		$user->setScenario('register');
		$this->performAjaxValidation($user);
		if(Yii::app()->request->isPostRequest)
		{
			$user->attributes=$_POST['User'];
			if($user->validate())
			{
				Yii::app()->vc->processStatField(array('reg'=>1));
				$user->subaccount_id=Yii::app()->vc->subaccountId;
				if ($user->save(false))
				{
					$auth=new LoginForm;
					$auth->username=$_POST['User']['email'];
					$auth->password=$_POST['User']['password'];
					$auth->rememberMe=true;
					$auth->login();
					$this->redirect(array('site/index'));
				}
			}
		}
		$this->render('register', array('model'=>$user));
	}
	
	public function actionLostpassword()
	{
		if (!Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->returnUrl);
			
		$model=new LostpasswordForm;
		$user=false;
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['LostpasswordForm'];
			if($model->validate())
			{
				$userAuth = UserAuthorization::model()->find('email=:email', array(':email'=>$model->attributes['email']));
				$user = User::model()->findByPk($userAuth->user_id);
				if ($user->id)
				{
					$mail=Yii::app()->Smtpmail;
					$mail->CharSet='utf-8';
					$mail->SetFrom($mail->Username, Yii::app()->request->domain);
					$mail->Subject    = 'Восстановление пароля к '.Yii::app()->request->domain;
					$mail->MsgHTML($this->render('/email/lostpassword', array('email'=>$userAuth->email, 'password'=>$userAuth->password, 'name'=>$user->name), true));
					$mail->AddAddress($userAuth->email, "");
					if(!$mail->Send()) {
						throw new CHttpException(500,'Mailer Error.');
						//$model->addError('email', 'Mailer Error: ' . $mail->ErrorInfo);
					}else {
						Yii::app()->user->setFlash('sentemail',$userAuth->email);
						$this->refresh();
					}
				}
				else
					$model->addError('email', 'A user with this email was not found');
			}
		}
		
		$this->render('lostpassword', array('model'=>$model, 'user'=>$user));
	}
	
	public function actionLogin()
	{
		if (!Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->returnUrl);
			
		if (isset($_GET['auto']))
		{
			$auto=explode(";", base64_decode($_GET['auto']));
			$auth=new LoginForm;
			$auth->username=$auto['0'];
			$auth->password=$auto['1'];
			$auth->rememberMe=true;
			$auth->login();
			$this->redirect(array('site/index'));
			Yii::app()->end();
		}
			
		$model=new LoginForm;
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			
			if($model->validate() && $model->login())
			{
				$this->redirect(array('site/index'));
			}
		}
		
		$this->render('login', array('model'=>$model));
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='registerForm')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}