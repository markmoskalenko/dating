<?php

class ProfileController extends Controller
{
	public $layout='col_main';

	//public $profile;
	
	public $section=array();
	
	private $_field_values;
	private $_user;
	private $_wall;
	private $_wallForm;
	private $_friend;
	private $_like;
	
	public function getProfileFields()
	{
		foreach(ProfileFieldValues::model()->cache(3600)->with('field')->findAll() as $row) {
			$this->_field_values[$row->field->name][$row->value]=Yii::t('profile_field_values', $row->depending_on_sex==1 ? $row->field->name.'_'.$row->value.'_'.$this->user->sex : $row->field->name.'_'.$row->value);
		}
		
		$field=ProfileField::model()->cache(3600)->findAll(array(
			'condition'=>'certain_sex=0 OR certain_sex=:sex',
			'params'=>array(':sex'=>$this->user->sex),
			'order'=>'sort DESC, id ASC'
		));
		
		foreach($field as $row) {
			
			//Yii::log(CVarDumper::dumpAsString($this->user->profile->{$row->name}));
			if ($this->user->profile->{$row->name}) {
				$fields[$row->section_id][]=array(
					'title'=>Yii::t('profile_field', $row->name),
					'value'=>$this->fieldTypes($row),
				);
			}
		}
		
		$section=ProfileFieldSection::model()->cache(3600)->findAll();
		foreach($section as $row) {
			$section_row[]=array(
				'title'=>Yii::t('profile_field', 'Section_'.$row->name),
				'name'=>$row->name,
				'field'=>$fields[$row->id] ? $fields[$row->id] : array(),
			);
		}
		return $section_row;
	}
	
	public function fieldTypes($row)
	{
		if ($row->presentation=='checkBoxList') { 
			foreach($this->user->profile->{$row->name} as $k=>$r) {
				if($k>0) $t.= ", ";
				$t.=$this->_field_values[$row->name][$r];
			}
		} elseif (in_array($row->presentation, array('radioButtonList', 'dropDownList'))) {
			$t=$this->_field_values[$row->name][$this->user->profile->{$row->name}];
			
		} elseif (in_array($row->presentation, array('textField', 'textArea'))) {
			$t=$this->user->profile->{$row->name};
		} elseif (in_array($row->presentation, array('textFieldBirthday'))) {
			$t=$this->user->profile->{$row->name};
		} elseif (in_array($row->presentation, array('dropDownListCountries'))) {
			$model=GeoCountries::model()->cache(86400)->findByPk($this->user->profile->{$row->name}, array('select'=>'title'));
			$t=$model->title;
		} elseif (in_array($row->presentation, array('dropDownListRegions'))) {
			$model=GeoRegions::model()->cache(86400)->findByPk($this->user->profile->{$row->name}, array('select'=>'title'));
			$t=$model->title;
		} elseif (in_array($row->presentation, array('dropDownListCity'))) {
			$model=GeoCity::model()->cache(86400)->findByPk($this->user->profile->{$row->name}, array('select'=>'title'));
			$t=$model->title;
		}
		
		return $t;
	}

	public function getUser()
	{
		if ($this->_user === null) {
			if ($name=Yii::app()->request->getQuery('user'))
				$userId=UserPages::model()->find('name=:name', array(':name'=>$name))->user_id;
			else
				$userId=Yii::app()->request->getQuery('userId') ? Yii::app()->request->getQuery('userId') : Yii::app()->user->id;
			
			$this->_user=User::model()->with('profile')->findByPk($userId);
		}
		return $this->_user;
	}

	public function getWall($limit='5')
	{
		
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`userId`=:userId',
				'params'=>array(':userId'=>$this->user->id),
				'order'=>'t.date DESC',
			));
			
			if (!Yii::app()->user->model->isReal) {
				$criteria->addCondition('t.written_userId='.$this->user->id);
			}
			
			$count = Wall::model()->cache(120, new DbCacheDependency('update_wall', $this->user->id))->count($criteria);
			
			$criteria->with=array('user', 'photo');
			
			$pages = new CPagination($count);
			$pages->pageSize=$limit;
			$pages->pageVar='wall';
			$pages->route='profile/index';
			$pages->params=array('userId'=>$this->user->id);
			$pages->applyLimit($criteria);
			
			$wall=Wall::model()->cache(120, new DbCacheDependency('update_wall', $this->user->id))->findAll($criteria);
		
		
	
		return $this->renderPartial('_wall', array('wall'=>$wall, 'pages'=>$pages, 'count'=>$count), true);
	

	}

	public function getFriend()
	{
		if ($this->_friend === null)
		{
			if (!$this->isOwn)
				$this->_friend=Friends::model()->find('userId=:userId AND friend_userId=:friend_userId', array(':userId'=>Yii::app()->user->id, ':friend_userId'=>$this->user->id));
		}
		return $this->_friend;
	}


	public function getIsFriend()
	{
		if ($this->friend)
			return true;
		
		return false;
	}

	public function getFriends($limit=5, $online=false)
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`userId`=:userId',
			'params'=>array(':userId'=>$this->user->id),
			'with'=>'user',
			'order'=>'rand()',
			'limit'=>$limit,
		));
		if ($online) {
			$criteria->addCondition('('.time().'-user.last_date)/60<=15');
			//$count=Yii::app()->cache->get($this->user->id);
			//if($count===false)
			//{
				$count=Friends::model()->/*cache(60)->*/count($criteria);
				//Yii::app()->cache->set($this->user->id,$count);
			//}
		}
		else {
			$count=$this->user->counter->friends;
		}
		$friends=Friends::model()->findAll($criteria);
		
		
		$this->renderPartial('_friends', array('friends'=>$friends, 'online'=>$online, 'count'=>$count));
	}

	public function actionIndex()
	{
		if($this->user===null)
			throw new CHttpException(404,Yii::t('app','Page was deleted or does not exist yet.'));
		
		if ($bId=Blacklist::checkByUser($this->user->id)) {
			
			$this->render('black', array());
			Yii::app()->end();
		}
		
		if(!$this->isOwn && !Yii::app()->user->isGuest)
		{
			if(Yii::app()->user->model->have_photo!='1')
				$this->redirect(array('site/index'));
		}  
		
		if(Yii::app()->request->isPostRequest && isset($_POST['Profiles']['status']))
		{
			if (!$this->user->isReal)
				throw new CHttpException(402,'Чтобы изменять статус необходим Real-статус');
			
			$profile=Profiles::model()->findByPk($this->user->id);
			$profile->setScenario('updateStatus');
			$profile->attributes=$_POST['Profiles'];
			if($profile->validate())
			{
				if ($profile->save(false))
				{
					echo CJSON::encode(array('html'=>$profile->status));
					Yii::app()->end();
				}
			} else {
				throw new CHttpException(402,CHtml::errorSummary($profile));
			}
			Yii::app()->end();
		}
		
		
		$wallForm=new WallForm;
		//$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest && $_POST['WallForm'])
		{
			$wallForm->attributes=$_POST['WallForm'];
			if($wallForm->validate())
			{
				$wall=new Wall;
				$wall->userId=$this->user->id;
				$wall->message=$wallForm->message;
				if ($wall->save(false))
				{
					if (Yii::app()->request->isAjaxRequest)
					{
						$html=$this->renderPartial('wall', array(), true);
						echo CJSON::encode(array('html'=>$html));
						return;
					}
					$this->redirect(array('profile/index', 'userId'=>$this->user->id));
				}
			}
			elseif (Yii::app()->request->isAjaxRequest)
			{
				echo CActiveForm::validate($wallForm);
				Yii::app()->end();
			}
			
			$this->render('wall', array('wall'=>$wall, 'wallForm'=>$wallForm));
			return;
		}
			
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getQuery('_a')=='wall')
		{
			$this->layout = FALSE;
			$this->renderPartial('wall');
			return;
		}
		
		$this->pageTitle=$this->user->name;
		
		//$this->getJsWall();
		
		$this->render('index', array('wall'=>$wall, 'wallForm'=>$wallForm));
	}
	
	public function actionWall()
	{
		if (Yii::app()->request->getQuery('delWallId')) {
			$this->deleteWall(Yii::app()->request->getQuery('delWallId'));
			Yii::app()->end();
		}
		
	}
	
	public function actionAddlike()
	{
		if($this->user===null)
			throw new CHttpException(404,'Page was deleted or does not exist yet.');
		
		if (!$this->isLike) {
			$like=new UserLikes;
			$like->own_userId=$this->user->id;
			$like->userId=Yii::app()->user->id;
			$like->save();
			
			$criteria=new CDbCriteria(array(
				'condition'=>'own_userId=:own_userId',
				'params'=>array(':own_userId'=>$this->user->id),
			));
			$this->user->saveAttributes(array('likes'=>UserLikes::model()->count($criteria)));
			Notifications::add(Notifications::LIKE, array(
				'userId'=>$this->user->id,
			));
		}
		
	}
	
	public function getIsLike()
	{
		if ($this->_like === null) {
			$criteria=new CDbCriteria(array(
				'condition'=>'own_userId=:own_userId AND userId=:userId',
				'params'=>array(':own_userId'=>$this->user->id, ':userId'=>Yii::app()->user->id),
			));
			$this->_like=UserLikes::model()->find($criteria);
			if ($this->_like!==null) {
				return true;
			}
		}
		
		return false;
	}
	
	public function actionLike()
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'like.own_userId=:own_userId',
			'params'=>array(':own_userId'=>Yii::app()->user->id),
		));
		$count = User::model()->with('like')->count($criteria);
		
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['likeUsers_limit'];
		$pages->applyLimit($criteria);
		
		$like=User::model()->with('like')->findAll($criteria);
		$this->render('like', array('like'=>$like, 'pages'=>$pages, 'count'=>$count));
	}
	
	public function getIsOwn()
	{
		return Yii::app()->user->id==$this->user->id;
	}
	
	public function getPhotos($limit='5')
	{
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`user_id`=:user_id',
				'params'=>array(':user_id'=>$this->user->id),
				'order'=>'sort DESC, id DESC',
				'limit'=>$limit,
				//'with'=>array('user'=>array('select'=>'login, sex')),
			));

		$photos=Photos::model()->cache(120, new DbCacheDependency('update_photos', $this->user->id))->findAll($criteria);
		$this->renderPartial('_photos', array('photos'=>$photos));
	}
	
	public function getGifts($limit='3')
	{
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`userId`=:userId',
				'params'=>array(':userId'=>$this->user->id),
				'order'=>'t.id DESC',
				'limit'=>$limit,
				//'with'=>array('user'=>array()),
			));

		$gifts=GiftsUsers::model()->findAll($criteria);
		$this->renderPartial('_gifts', array('gifts'=>$gifts));
	}
	
	public function getAlbums($limit='2')
	{
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`user_id`=:user_id',
				'params'=>array(':user_id'=>$this->user->id),
				'order'=>'rand()',
				'limit'=>$limit,
				//'with'=>array('user'=>array('select'=>'login, sex')),
			));

		$photos=Albums::model()->findAll($criteria);
		$this->renderPartial('_albums', array('photos'=>$photos));
	}
	
	public function deleteWall($id)
	{
		$comments=Wall::model()->findByPk($id);

		if (Yii::app()->user->checkAccess('deleteComment', array('author_id'=>$comments->userId, 'own_id'=>$comments->written_userId)))
		{
			//Notifications::model()->deleteAll('photoCommentId=:photoCommentId AND userId=:userId AND from_userId=:from_userId AND type=:type', array(':photoCommentId'=>$comments->id,':userId'=>$photos->user_id, ':from_userId'=>$comments->userId, ':type'=>Notifications::COMMENTED));
			
			//User::model()->updateCounters(array('notifications'=>-1),'id=:id',array(':id'=>$photos->user_id));
			$comments->delete();
			echo CJSON::encode(array('id'=>$id));
		}
		else
			throw new CHttpException(402,'checkAccess');
	}
	
	public function getJsWall11()
	{
$script=<<<JS





$("a.delete").live('click', function() {
	alert($(this).attr('href'));
	return false;
});
JS;
		Yii::app()->clientScript->registerScript('wall', $script, CClientScript::POS_READY);
	}
	
	public function actionUploadphoto()
	{
		$this->render('uploadphoto', array());
	}
	
	public function actionPickup()
	{
		if (isset($_GET['go'])) {
			if (Yii::app()->user->balance>=Yii::app()->user->getTransactionsTypes('lifting_up_page')) {
				Yii::app()->user->model->position->saveAttributes(array('time'=>time()));
				Yii::app()->user->model->saveAttributes(array('balance'=>new CDbExpression('`balance`-'.Yii::app()->user->getTransactionsTypes('lifting_up_page'))));
				echo CJSON::encode(array('status'=>'1'));
			}
			else
				echo CJSON::encode(array('status'=>'0'));
				Yii::app()->end();
		}
		
		$this->render('pickup', array());
	}
}