<?php

class SearchController extends Controller
{
	public $layout='col_main';
	
	public $searchEngine='Sphinx';
	//public $searchEngine='Mysql';


	public function accessRules()
	{
		return array(
			array('deny',
			      'roles'=>array('guest'),
			),
		);
	}
	
	public function init()
	{
		parent::init();
		
		if (!Yii::app()->user->model->isReal && is_array($_GET['c'])) {
			$this->redirect(array('activation/index'));
			Yii::app()->end();
		}
		
		if ($_GET['c']) {
			$cookie = new CHttpCookie('searchCriteria', serialize($_GET['c']));
			$cookie->expire = time()+60*60*24*180;
			Yii::app()->request->cookies['searchCriteria'] = $cookie;
		}
		if ($cookie = Yii::app()->request->cookies['searchCriteria']) {
			$_GET['c']=unserialize($cookie->value);
		}
	}

	public function actionIndex()
	{
		if (Yii::app()->user->model->isReal) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $this->getQuickSearch();
				return;	
			}
			$this->render('index');
		} else {
			$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`isBot`=1 AND photo.approve=1',
			//'params'=>array(':user_id'=>$this->profile->id),
				'with'=>array(
					'profile'=>array(),
					//'position'=>array(),
					'country',
					'region',
					'city',
					'photo'
				),
				'order'=>'rand()',
				'limit'=>Yii::app()->params['mainRandomUsers_limit'],
				
			));
			$profiles=User::model()->findAll($criteria);
			$this->render('/site/index', array('profiles'=>$profiles));
		}
	}
	
	public function actionExtended()
	{

		
		$this->render('extended');
	}
	
	public function getQuickSearch()
	{
		return $this->{getQuickSearch.Yii::app()->params['searchEngine']}();
	}
	
	public function getQuickSearchSphinx()
	{
		$searchCriteria = new stdClass();
		$pages = new CPagination();
		$pages->pageSize = Yii::app()->params['searchUsers_limit'];
		$pages->params=array();
		
		$searchCriteria->select = '*';
		
		if ($_GET['c']['country_id'])
			$searchCriteria->filters['country_id'] = intval($_GET['c']['country_id']);
			
		if ($_GET['c']['country_id'] && $_GET['c']['region_id'])
			$searchCriteria->filters['region_id'] = intval($_GET['c']['region_id']);
			
		if ($_GET['c']['country_id'] && $_GET['c']['region_id'] && $_GET['c']['city_id'])
			$searchCriteria->filters['city_id'] = intval($_GET['c']['city_id']);
			
		if ($_GET['c']['sex'])
			$searchCriteria->filters['sex'] = intval($_GET['c']['sex']);
			
		if (is_numeric($_GET['c']['age_from']) OR is_numeric($_GET['c']['age_to']))
			$searchCriteria->filtersRange['age'] = array(intval($_GET['c']['age_from']), intval($_GET['c']['age_to'] ? $_GET['c']['age_to'] : 80));
		
		if ($_GET['c']['new'])
			$searchCriteria->filtersRange['reg_date'] = array(time() - 60*60*24, time());
			
		if ($_GET['c']['photo'])
			$searchCriteria->filters['have_photo'] = 1;
			
		if ($_GET['c']['real'])
			$searchCriteria->filters['isReal'] = 1;
			
		if ($_GET['c']['tg']) {
			
			foreach($_GET['c']['tg'] as $target) {
				//$searchCriteria->setFilterRange('target')=intval($target);
				$searchCriteria->query.=' @target '.intval($target).'*';
			}
		}	
		/*if ($_GET['tg']=='3')
		{
			$searchCriteria->query.=' @target 1';
			//$searchCriteria->query.=' @target 2';
		}
			
		if ($_GET['tg']=='2')
		{
			$searchCriteria->query.=' @target 12';
		}*/
			
			
		//if ($_GET['q'])
		//	$searchCriteria->query = htmlspecialchars(substr($_GET['q'],0,100));
			
		$searchCriteria->paginator = $pages;
		$searchCriteria->orders = 'position DESC';
		
		if ($_GET['c']['online'])
			$searchCriteria->from = 'usersonline';
		else
			$searchCriteria->from = 'users';
		
		$result = Yii::app()->sphinx->searchRaw($searchCriteria);
		
		//print_r($result);
		
		if ($result['matches'])
		{
			$ids = array_keys($result['matches']);
			//print_r($result['matches']);
			//print_r($ids);
			//Yii::log(CVarDumper::dump($result['matches'], 10,true));
			//Yii::log(print_r($result, true));
			
			$count=$result['total_found'];
			$criteria=new CDbCriteria;
			//$criteria->select='id, img, last_date, login, sex, count_photo, count_friends, rating';
			$criteria->with=array(
				'profile'=>array(
					//'select'=>'birthday, name, lastname, orientation',
					'with'=>array(),
				),
				'country',
				'region',
				'city'
			);
			$criteria->addInCondition('id',$ids);
			$criteria->order='FIELD(id, '.implode(',', $ids).')';
			$profiles=User::model()->findAll($criteria);
		}
		else {
			$profiles=array();
			$pages=null;
			$count=0;
		}
		
		return $this->renderPartial('_rows', array('profiles'=>$profiles, 'pages'=>$pages, 'count'=>$count), true);
	}
	
	public function getQuickSearchMysql()
	{
		Yii::beginProfile('Search result');
		//Yii::log('Search engine: ');
		$criteria=new CDbCriteria(array(
			//'condition'=>'`t`.`user_id`=:user_id',
			//'params'=>array(':user_id'=>$this->profile->id),
			'with'=>array(
				'profile'=>array(),
				'position'=>array(),
			),
			'order'=>'position.time DESC',
				
		));
		
		if ($_GET['c']['sex'])
			$criteria->addCondition('`t`.`sex`='.intval($_GET['c']['sex']));
		
		if ($_GET['c']['age_from']) {
			$criteria->addCondition("`t`.`birthday`<'".date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")-$_GET['c']['age_from']))."' AND `birthday`>'0000-12-31'");
		}
		
		if ($_GET['c']['age_to']) {
			$criteria->addCondition("`t`.`birthday`>'".date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")-$_GET['c']['age_to']-1))."'");
		}
		
		if ($_GET['c']['country_id'])
			$criteria->addCondition('`t`.`country_id`='.intval($_GET['c']['country_id']));
		
		if ($_GET['c']['region_id'] && $_GET['c']['region_id'])
			$criteria->addCondition('`t`.`region_id`='.intval($_GET['c']['region_id']));
		
		if ($_GET['c']['country_id'] && $_GET['c']['region_id'] && $_GET['c']['city_id'])
			$criteria->addCondition('`t`.`city_id`='.intval($_GET['c']['city_id']));
		
		//$criteria->addInCondition("`profile`.`target`", array('1'));
		
		//$criteria->addSearchCondition('`profile`.`target`', '13', true, 'OR');
		//$criteria->addSearchCondition('`profile`.`target`', '7', true, 'OR');
		
		
		if ($_GET['tg']=='2')
		{
			$criteria2=new CDbCriteria;
			$criteria2->addSearchCondition('`profile`.`target`', '12', true, 'OR');
			$criteria->mergeWith($criteria2);
		}
		elseif ($_GET['tg']=='3')
		{
			$criteria2=new CDbCriteria;
			$criteria2->addSearchCondition('`profile`.`target`', '1', true, 'OR');
			$criteria2->addSearchCondition('`profile`.`target`', '2', true, 'OR');
			$criteria2->addSearchCondition('`profile`.`target`', '9', true, 'OR');
			$criteria2->addSearchCondition('`profile`.`target`', '10', true, 'OR');
			$criteria->mergeWith($criteria2);
		}
		
		
		$count = User::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['searchUsers_limit'];
		$pages->applyLimit($criteria);
		
		$profiles=User::model()->findAll($criteria);
		
		Yii::endProfile('Search result');
		return $this->renderPartial('_rows', array('profiles'=>$profiles, 'pages'=>$pages, 'count'=>$count, 'setNum'=>6), true);
	}
}