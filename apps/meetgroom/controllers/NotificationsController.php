<?php

class NotificationsController extends Controller
{
	public function accessRules()
	{
		return array(
			array('deny',
			      //'actions'=>array('index'),
			      'users'=>array('?'),
			),
		);
	}
	
	public function actionIndex()
	{
		if (Yii::app()->user->model->notifications!=0)
			Yii::app()->user->model->saveAttributes(array('notifications'=>0));
		
		$criteria=new CDbCriteria(array(
			//'select'=>'',
			'condition'=>'t.userId=:userId',
			'params'=>array(':userId'=>Yii::app()->user->id),
			'order'=>'t.id DESC',
		));
		
		$count = Notifications::model()->count($criteria);
			
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['notifications_limit'];
		$pages->applyLimit($criteria);
			
		$notifications=Notifications::model()->with('user')->with('photo')->with('photoComment')->findAll($criteria);
		
		$this->render('index',array('notifications'=>$notifications, 'pages'=>$pages));
	}
	

	
	
	
}