<?php

class PayController extends Controller
{
	public function accessRules()
	{
		return array(
			array('deny',
			      //'actions'=>array('index'),
			      'users'=>array('?'),
			),
		);
	}
	
	public function actionIndex()
	{
		$kassaForm=new KassaForm;
		
		if (Yii::app()->request->isPostRequest)
		{
			$kassaForm->attributes=$_POST['KassaForm'];
			if($kassaForm->validate())
			{
				
				//$this->render('step2',array('kassaForm'=>$kassaForm));
				
				$step='2';
				$sum_array=array(
		'1'=>'10',
		'5'=>'50',
		'10'=>'100',
		'20'=>'200',
		'30'=>'300',
		'50'=>'500',
		'100'=>'1000',
						 );
				
				
				$kassa=new Kassa;
				$kassa->user_id=Yii::app()->user->id;
				$kassa->coins=$kassaForm->amount;
				$kassa->sum=$sum_array[$kassaForm->amount];
				$kassa->pay_status=0;
				$kassa->date=time();
				$kassa->save();
				
				
// Исходные данные
$shop_id = '11824';		// ID-магазинаы
$pass = '712cf92d6fe26aa10da8e0547de95863';			// Пароль магазина
$secret = 'c814dd85265b778fe3fc122c1b2e3202';		// Секретный ключ
$order = $kassa->id;		// Номер счета
$currency = 'RUR';		// RUR, USD, EUR, UAH
$sum = $sum_array[$kassaForm->amount];			// Сумма счета
$prop = array(
	      Yii::app()->user->model->name,
	      Yii::app()->user->auth->email,
	      $kassaForm->phone
	      );	// Доп свойства

// Подготовка пакета $xml
$xml = "
<request>
	<order>$order</order>
	<shop>$shop_id</shop>
	<pass>$pass</pass>
	<sum>$sum</sum>
	<currency>$currency</currency>
	<property>
		<p0>$prop[0]</p0>
		<p1>$prop[1]</p1>
		<p2>$prop[2]</p2>
		<p3>$prop[3]</p3>
		<p4>$prop[4]</p4>
		<p5>$prop[5]</p5>
		<p6>$prop[6]</p6>
		<p7>$prop[7]</p7>
		<p8>$prop[8]</p8>
		<p9>$prop[9]</p9>
	</property>
</request>";
//echo $xml;
// Кодирование пакета $xml методом base64_encod
$xml_encoded = base64_encode($xml);
// Создание подписи
$lqsignature = base64_encode(sha1($secret.$xml_encoded.$secret,1));
				
				
				
				
				/*$result = Yii::app()->CURL->run('http://www.free-kassa.ru/merchant/cash.php', TRUE, array(
					'm' => 2581,
					'oa' => $kassaForm->amount,
				));*/
				
				/*$kassa=new Kassa;
				$kassa->user_id=Yii::app()->user->id;
				$kassa->amount=$kassaForm->amount;
				$kassa->pay_status=0;
				$kassa->date=date("Y-m-d H:i:s");
				$kassa->save();
				
				$secret_word='fgdhdghgjju65y5hgdd';
				$merchant_id=2581;
				$order_id=$kassa->id;
				
				header('Location: http://www.free-kassa.ru/merchant/cash.php?m='.$merchant_id.'&oa='.$kassaForm->amount.'&s='.md5($merchant_id.":".$kassaForm->amount.":".$secret_word.":".$order_id).'&o='.$order_id);
				
				*/
				
				
				$balance=$kassaForm->amount;
				
				
				//$user=Users::model()->findByPk(Yii::app()->user->id);
				
				//Yii::app()->user->model->saveAttributes(array('balance'=>new CDbExpression('`balance`+'.$balance)));
				
					//Yii::app()->user->setFlash('success','Баланс пополнен.');
					//$this->refresh();

			}	
		}
		
		if ($step=='2') {

		
		}
		$this->render('kassa',array('kassaForm'=>$kassaForm, 'step'=>$step, 'xml_encoded'=>$xml_encoded, 'lqsignature'=>$lqsignature));
	}
	
	public function actionSuccess()
	{
		$this->render('success');
	}
	
	public function actionFail()
	{
		$this->render('fail');
	}
	
	public function actionHistory()
	{
		$history=Transaction::model()->findAll(array('condition'=>'user_id='.Yii::app()->user->id, 'order'=>'id DESC'));
		$this->render('history',array('history'=>$history));
	}
}