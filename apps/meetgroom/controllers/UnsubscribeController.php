<?php

class UnsubscribeController extends Controller
{
	public $layout = 'col_main';
	
	public function actions()
	{
		return array(
                    //  captcha.
                     'captcha'=>array(
                        'class'=>'CaptchaAction',
                    )
                );
	}
	
	public function actionIndex()
	{
		$model=new UnsubscribeForm;
		$model->setScenario('support');
		if(isset($_POST['UnsubscribeForm']))
		{
			$model->attributes=$_POST['UnsubscribeForm'];
			if($model->validate())
			{
				$result=$model->unsubscribe();
				//print_r($result);
				if ($result['status']=='ok')
				{
					$this->redirect($result['url']);
				} else {
					$model->addError('phone',Yii::t('app','У Вас нет подписок на Контент.'));
				}
				
				////Yii::app()->user->setFlash('ok','1');
				//$this->refresh();
			}
		}
		$this->render('index',array('model'=>$model));
	}
}