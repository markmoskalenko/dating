<?php

class GuestsController extends Controller
{
	public $layout='col_main';

	public function accessRules()
	{
		return array(
			array('deny',
			      'roles'=>array('guest'),
			),
		);
	}
	
	public function actionIndex()
	{
$profiles=array();
			$this->render('index', array('profiles'=>$profiles));
	}
}