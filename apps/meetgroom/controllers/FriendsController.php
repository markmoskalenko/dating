<?php

class FriendsController extends Controller
{
	public $layout='col_main';
	
	private $_sentFriendsCounter;
	
	public function accessRules()
	{
		return array(
			array('deny',
			      'roles'=>array('guest'),
			),
		);
	}
	
	public function actionIndex()
	{
		$this->pageTitle=$this->isOwn ? Yii::t('app', 'My Friends')  : Yii::t('app', 'Friends');
		
		if ($this->isOwn) {
			$this->breadcrumbs=array(
				Yii::t('app', 'My Friends')
			);
		} else {
			$user=User::model()->findByPk(Yii::app()->request->getQuery('userId'));
			
			if($user===null)
				throw new CHttpException(404,'Page was deleted or does not exist yet.');
			
			$this->breadcrumbs=array(
				$user->name=>array('profile/index', 'userId'=>$user->id),
				Yii::t('app', 'Friends')
			);
		}
		
		$friends=array();
		if ($_GET['section']=='' OR $_GET['section']=='online')
		{
			if ($_GET['cmd']=='delete')
			{
				$criteria=new CDbCriteria(array(
					'condition'=>'`t`.`userId`=:userId AND `t`.`friend_userId`=:friend_userId',
					'params'=>array(':userId'=>Yii::app()->user->id, ':friend_userId'=>$_GET['id']),
				));
				
				$friends=Friends::model()->find($criteria);
				
				if($friends===null)
					throw new CHttpException(404,'Contact not found.');
				
				Notifications::add(Notifications::FRIEND_DELETE, array(
					'userId'=>$friends->friend_userId,
				));
				$friends->delete();
				
				$criteria=new CDbCriteria(array(
					'condition'=>'`t`.`userId`=:userId AND `t`.`friend_userId`=:friend_userId',
					'params'=>array(':userId'=>$_GET['id'], ':friend_userId'=>Yii::app()->user->id),
				));
				$friends=Friends::model()->find($criteria);
				$friends->delete();
				
				//UserCounters::recalculationFriends(Yii::app()->user->id);
				//UserCounters::recalculationFriends($_GET['id']);
				
				$this->redirect(array('friends/index'));
			}
			
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`userId`=:userId',
				'params'=>array(':userId'=>$_GET['userId'] ? $_GET['userId'] : Yii::app()->user->id),
				'with'=>array('user'=>array('together'=>false)),
				'order'=>'t.time DESC',
			));
			
			if ($_GET['section']=='online') {
				$criteria->addCondition('('.time().'-user.last_date)/60<=15');
			}
			
			$count = Friends::model()->count($criteria);
			$pages = new CPagination($count);
			$pages->pageSize=Yii::app()->params['friends_limit'];
			$pages->applyLimit($criteria);
			
			$friends=Friends::model()->findAll($criteria);
		}
		elseif ($_GET['section']=='requests' && $this->isOwn)
		{
			if ($_GET['cmd']=='decline' OR $_GET['cmd']=='accept')
			{
				$criteria=new CDbCriteria(array(
					'condition'=>'`t`.`userId`=:userId AND `t`.`friend_userId`=:friend_userId',
					'params'=>array(':userId'=>Yii::app()->user->id, ':friend_userId'=>$_GET['id']),
				));
				
				$friends=FriendsRequests::model()->find($criteria);
				
				if($friends===null)
					throw new CHttpException(404,'Contact not found.');
				
				if ($_GET['cmd']=='accept')
				{
					if (!Friends::model()->find('userId=:userId AND friend_userId=:friend_userId', array(':userId'=>$friends->friend_userId, ':friend_userId'=>Yii::app()->user->id)))
					{
						$model=new Friends;
						$model->userId=$friends->friend_userId;
						$model->friend_userId = Yii::app()->user->id;
						if ($model->save()){
							
						}
					}
					
					if (!Friends::model()->find('userId=:userId AND friend_userId=:friend_userId', array(':userId'=>Yii::app()->user->id, ':friend_userId'=>$friends->friend_userId)))
					{
						$model=new Friends;
						$model->userId=Yii::app()->user->id;
						$model->friend_userId = $friends->friend_userId;
						if ($model->save()){
							
						}
					}
					
					Notifications::add(Notifications::FRIEND_REQUEST_ACCEPTED, array(
						'userId'=>$friends->friend_userId,
					));
					
					//UserCounters::recalculationFriends(Yii::app()->user->id);
					//UserCounters::recalculationFriends($_GET['id']);
				}
				if ($_GET['cmd']=='decline')
				{
					Notifications::add(Notifications::FRIEND_REQUEST_DECLINE, array(
						'userId'=>$friends->friend_userId,
					));
				}
				
				$friends->delete();
				
				$this->redirect(array('friends/index', 'section'=>'requests'));
			}
			
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`userId`=:userId',
				'params'=>array(':userId'=>Yii::app()->user->id),
				'with'=>array('user'=>array('on'=>'`t`.`friend_userId`=`user`.`id`','together'=>false)),
				'order'=>'t.time DESC',
			));
			
			$count = FriendsRequests::model()->count($criteria);
			$pages = new CPagination($count);
			$pages->pageSize=Yii::app()->params['friends_limit'];
			$pages->applyLimit($criteria);
			
			$friends=FriendsRequests::model()->findAll($criteria);
		}
		elseif ($_GET['section']=='sent' &&  $this->isOwn)
		{
			if ($_GET['cmd']=='cancel')
			{
				$criteria=new CDbCriteria(array(
					'condition'=>'`t`.`userId`=:userId AND `t`.`friend_userId`=:friend_userId',
					'params'=>array(':userId'=>$_GET['id'], ':friend_userId'=>Yii::app()->user->id),
				));
				
				$friends=FriendsRequests::model()->find($criteria);
				
				if($friends===null)
					throw new CHttpException(404,'Contact not found.');
				
				$friends->delete();
				
				$this->redirect(array('friends/index', 'section'=>'sent'));
			}
			
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`friend_userId`=:userId',
				'params'=>array(':userId'=>Yii::app()->user->id),
				'with'=>array('user'=>array('on'=>'`t`.`userId`=`user`.`id`','together'=>false)),
				'order'=>'t.time DESC',
			));
			
			$count = FriendsRequests::model()->count($criteria);
			$pages = new CPagination($count);
			$pages->pageSize=Yii::app()->params['friends_limit'];
			$pages->applyLimit($criteria);
			
			$friends=FriendsRequests::model()->findAll($criteria);
		} else {
			throw new CHttpException(404,'Contact not found.');
		}
		$this->render('index', array('friends'=>$friends, 'pages'=>$pages, 'count'=>$count, 'user'=>$user));
	}
	
	public function actionApply()
	{
		$user=User::model()->findByPk($_GET['id']);
		
		if($user===null)
			throw new CHttpException(404,'Contact not found.');
		
		if($user->id==Yii::app()->user->id)
			throw new CHttpException(404,'It is impossible to write a message.');
		
		if (Friends::model()->find('userId=:userId AND friend_userId=:friend_userId', array(':userId'=>Yii::app()->user->id, ':friend_userId'=>$user->id)))
		{
			$status=Yii::t('app', 'You already are friends');
		}
		elseif (Friends::model()->find('userId=:userId AND friend_userId=:friend_userId', array(':userId'=>$user->id, ':friend_userId'=>Yii::app()->user->id)))
		{
			$status=Yii::t('app', 'You already are friends');
		}
		elseif (FriendsRequests::model()->find('userId=:userId AND friend_userId=:friend_userId', array(':userId'=>$user->id, ':friend_userId'=>Yii::app()->user->id)))
		{
			$status=Yii::t('app', 'You have already applied for');
		}
		elseif (FriendsRequests::model()->find('userId=:userId AND friend_userId=:friend_userId', array(':userId'=>Yii::app()->user->id, ':friend_userId'=>$user->id)))
		{
			$status=Yii::t('app', 'User :user you have applied for', array(':user'=>$user->name));
		}
		else
		{
			$model=new FriendsRequests;
			$model->userId=$user->id;
			$model->friend_userId = Yii::app()->user->id;
			if ($model->save()){
				$status=Yii::t('app', 'Application sent to friends');
			}
			
		}
		if (Yii::app()->request->isAjaxRequest) {
			echo CJSON::encode(array(
				'textStatus' => $status,
			));
			Yii::app()->end();
		}
		else
			$this->render('apply', array('status'=>$status));
	}
	
	public function getSentFriendsCounter()
	{
		if ($this->_sentFriendsCounter === null)
		{
			$criteria = new CDbCriteria;
			$criteria->condition='`t`.`friend_userId`='.Yii::app()->user->id;
			$this->_sentFriendsCounter=FriendsRequests::model()->count($criteria);
		}
		return $this->_sentFriendsCounter;
		
	}
	
	public function getIsOwn()
	{
		return Yii::app()->user->id==$_GET['userId'] OR !$_GET['userId'];
	}
}