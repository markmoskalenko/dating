<?php

class AppApiController extends Controller
{
	public $layout = FALSE;
	
	public function actionGetProfile()
	{
		$criteria=new CDbCriteria;
		
		$criteria->addInCondition('id',explode(",", $_GET['uid']));
		
header("Content-Type: text/xml");
header("Expires: Thu, 19 Feb 1998 13:24:18 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0");
header("Cache-Control: max-age=0");
header("Pragma: no-cache");
		
		echo '<?xml version="1.0" encoding="UTF-8"?>
<profiles>';
		foreach(User::model()->findAll($criteria) as $key=>$row) {
			if ($row->sex=='1') {
				$sex='M';
			} else {
				$sex='F';
			}
echo '
    <user>
        <uid>'.$row->id.'</uid>
        <first_name>'.$row->name.'</first_name>
        <last_name></last_name>
        <nickname>'.$row->name.'</nickname>
        <birthday>'.$row->birthday.'</birthday>
        <sex>'.$sex.'</sex>
        <avatar_url>'.$row->getImage(104,138, 'camera_a.png').'</avatar_url>
        <country>'.$row->country->name.'</country>
        <city>'.$row->city->name.'</city>
    </user>';
		}
		echo '</profiles>';

	}
	
	public function actionGetFriends()
	{
		$criteria=new CDbCriteria;
		
		$criteria->addCondition('`t`.`userId`='.intval($_GET['uid']));
		
		
header("Content-Type: text/xml");
header("Expires: Thu, 19 Feb 1998 13:24:18 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0");
header("Cache-Control: max-age=0");
header("Pragma: no-cache");
		
		echo '<?xml version="1.0" encoding="UTF-8"?>
<friends>';
		foreach(Friends::model()->findAll($criteria) as $key=>$row) {

echo '<friend_id>'.$row->friend_userId.'</friend_id>';
		}
		echo '</friends>';

	}
}