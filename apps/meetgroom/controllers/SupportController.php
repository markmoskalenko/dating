<?php

class SupportController extends Controller
{
	public $layout = 'col_main';
	
	public function actions()
	{
		return array(
                    //  captcha.
                     'captcha'=>array(
                        'class'=>'CaptchaAction',
                    )
                );
	}
	
	public function actionIndex()
	{
		$model=new ContactForm;
		$model->setScenario('support');
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject.' ('.Yii::app()->request->domain.')',$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('index',array('model'=>$model));
	}
}