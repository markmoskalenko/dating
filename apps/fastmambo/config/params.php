<?php

// this contains the application parameters that can be maintained via GUI
return array(
        'searchEngine'=>'Sphinx', // 'Sphinx' or 'Mysql'
        'noty'=>true,
        'noty_hostServer'=>'127.0.0.1:1101',
        'noty_hostClient'=>'127.0.0.1:3000',
        'adminEmail'=>'lifemeet@yandex.ru',
	'title'=>'My page',
	'copyrightInfo'=>'Copyright &copy; 2009 by My Company.',
);
