<?php

class m140919_073834_newsletter_adds extends CDbMigration
{
	public function up()
	{
        $this->addColumn('user_authorization', 'auth_token', 'VARCHAR(255) NULL');
        $this->addColumn('user_authorization', 'no_subscribe', 'TINYINT(1) NULL');
	}

	public function down()
	{
		$this->dropColumn('user_authorization', 'auth_token');
        $this->dropColumn('user_authorization', 'no_subscribe');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}