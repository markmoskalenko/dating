<?php

class m141117_144204_profile_visits_create_table extends CDbMigration
{
	public function up()
	{
        $sSql = <<<SQL
CREATE TABLE IF NOT EXISTS `user_visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `visited_user_id` int(11) NOT NULL,
  `visit_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


SQL;
        Yii::app()->db->createCommand($sSql)->execute();
	}

	public function down()
	{
		$this->dropTable('user_visits');
        $this->dropColumn('users', 'users_visitors');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}