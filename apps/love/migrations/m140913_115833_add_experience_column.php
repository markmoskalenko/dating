<?php

class m140913_115833_add_experience_column extends CDbMigration
{
	public function up()
	{
        $this->addColumn('user_authorization', 'token', 'VARCHAR(255) NULL');
        $this->addColumn('user_authorization', 'date_experience', 'DATETIME NULL');
	}

	public function down()
	{
        $this->dropColumn('user_authorization', 'token');
        $this->dropColumn('user_authorization', 'date_experience');
//		echo "m140913_115833_add_experience_column does not support migration down.\n";
//		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}