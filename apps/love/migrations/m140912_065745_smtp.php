<?php

class m140912_065745_smtp extends CDbMigration
{
	public function up()
	{
        $sSql = <<<SQL
CREATE TABLE IF NOT EXISTS `smtp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `port` varchar(255) NOT NULL,
  `encryption` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `smtp`
--

INSERT INTO `smtp` (`id`, `host`, `username`, `password`, `port`, `encryption`) VALUES
(1, 'smtp.gmail.com', 'mail@gmail.com', 'password', '465', 'tls');
SQL;

        Yii::app()->db->createCommand($sSql)->execute();

	}

	public function down()
	{
		echo "m140912_065745_smtp does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}