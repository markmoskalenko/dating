<?php

class m140923_073006_mail_send_log extends CDbMigration
{
	public function up()
	{
        $sSql = <<<SQL
CREATE TABLE IF NOT EXISTS `mail_send_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

SQL;

        Yii::app()->db->createCommand($sSql)->execute();

	}

	public function down()
	{
		$this->dropTable('mail_send_log');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}