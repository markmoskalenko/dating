<?php

class m140925_234527_add_coins_column extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users', 'add_coins', 'int(11)');
	}

	public function down()
	{
		$this->dropColumn('users', 'add_coins');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}