<?php

class m140924_123641_add_email_confirm_status extends CDbMigration
{
	public function up()
	{
        $this->addColumn('user_authorization', 'email_confirm', 'tinyint(1)');
        $this->addColumn('users', 'not_new', 'tinyint(1)');
	}

	public function down()
	{
		$this->dropColumn('user_authorization', 'email_confirm');
        $this->dropColumn('users', 'not_new');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}