<?php

class m140916_154951_create_newsletter_table extends CDbMigration
{
	public function up()
	{
        $sSql = <<<SQL
CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `check_code` text NOT NULL,
  `email_list_code` text NOT NULL,
  `template_variables` text NULL,
  `template` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


INSERT INTO `newsletters` (`id`, `title`, `check_code`, `email_list_code`, `template_variables`, `template`) VALUES
(4, 'Тестовая рассылка', 'return false;', 'return array(''example@gmail.com'');', 'return array(\r\n''var1''=>''value1'',\r\n''var2''=>''value2'',\r\n);', 'return \r\n''<h1>Hello</h1>'';');

SQL;

        Yii::app()->db->createCommand($sSql)->execute();
	}

	public function down()
	{
        $this->dropTable('newsletters');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}