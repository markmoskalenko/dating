<?php

class m141105_082640_add_table_leaders extends CDbMigration
{
	public function up()
	{
        $sSql = <<<SQL
CREATE TABLE IF NOT EXISTS `leaders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `date_start` datetime NOT NULL,
  `left_time` varchar(255),
  `is_current` tinyint(1),
  `photo_id` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

SQL;

        Yii::app()->db->createCommand($sSql)->execute();
	}

	public function down()
	{
        $this->dropTable('leaders');
    }

}