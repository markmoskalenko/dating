<?php

// this contains the application url rules
return array(
        '/'=>array('site/index', 'urlSuffix'=>'', ),
        '<_c>'=>array('<_c>/index', 'urlSuffix'=>'', 'defaultParams'=>array()),
        '<_c>/<_a>'=>array('<_c>/<_a>', 'urlSuffix'=>'', 'defaultParams'=>array()),
);