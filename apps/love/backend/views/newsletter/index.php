<?php

$this->pageTitle = 'Рассылки';

$this->breadcrumbs = array(
    'Home'=>$this->createUrl('/admin'),
    'Рассылки',
);

?>
<div class="row">

    <a href="<?= $this->createUrl('newsletter/create') ?>" class="btn btn-primary">Добавить рассылку</a>

<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'id'=>'productsListGrid',
//    'filter'=>$model,
    'columns'=>array(
        array(
            'name'=>'title',
        ),
//        array(
//            'name'=>'check_code',
//        ),
//        array(
//            'name'=>'email_list_code',
//        ),
//        array(
//            'name'=>'template_variables',
//        ),
//        array(
//            'name'=>'template',
//        ),
        // Buttons
        array(
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
));
?>
</div>