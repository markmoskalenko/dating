<?php
/* @var $this SportsmanController */
/* @var $model Sportsman */
/* @var $form CActiveForm */
?>

<div class="form">
    <style>
        .field-description {
            font-size: 8pt;
        }
    </style>

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'newsletter-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array(
            'class'=>'form-horizontal',
        ),
    )); ?>

    <div class="form-group">
        <?php echo $form->errorSummary($model); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'title', array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'title'); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model,'check_code', array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textArea($model,'check_code',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'check_code'); ?>
            <span class="field-description">Код должен содержать конструкцию return. <br />Пример: return $var;</span>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model,'email_list_code', array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textArea($model,'email_list_code',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'email_list_code'); ?>
            <span class="field-description">Код должен содержать конструкцию return. Код должен вернуть массив электронных почтовых адресов. <br />Пример: return array('example@email.com');</span>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model,'template_variables', array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textArea($model,'template_variables',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'template_variables'); ?>
            <span class="field-description">Код должен содержать конструкцию return. Код должен вернуть массив переменных. <br />Пример: return array('var'=>'value');</span>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model,'template', array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textArea($model,'template',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'template'); ?>
            <span class="field-description">Код должен содержать конструкцию return. Код должен вернуть строку. <br />Пример: return "&lt;h1&gt;Hello!&lt;/h2&gt;";</span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-10">
            <?=Html::submitButton('Сохранить', array('class'=>'btn btn-primary'))?>
            <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('load.gif')?>' align='absmiddle' /></span>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
