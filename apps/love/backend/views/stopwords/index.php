
<div class="row">
<div class="page-header" style="margin-top:8px;">
  Стоп слова
</div>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'form-horizontal'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,

)); ?>
<?php echo CHtml::errorSummary($model);?>
	


  <div class="form-group">
    <?php echo $form->labelEx($model,'word', array('label' => 'Слово:', 'class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
    <?php echo $form->textField($model,'word',array('class'=>'form-control input-sm')); ?>
    </div>
  </div>

  <div class="form-group">
    <?php echo $form->labelEx($model,'replacement', array('label' => 'Замена:', 'class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
    <?php echo $form->textField($model,'replacement',array('class'=>'form-control input-sm')); ?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
      <?=Html::submitButton('Добавить', array('class'=>'btn btn-primary'))?>
	<span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('load.gif')?>' align='absmiddle' /></span>
    </div>
  </div>
	

	
	
				
<?php $this->endWidget(); ?>

<table class="table table-striped table-hover"> 
<thead>
  <tr><?php $sortClasses=array(null=>'', true=>'active', false=>'active'); ?>
    <th>Слово</th>
    <th>Замена</th>
    <th></th>
  </tr>
</thead>
<tbody>
<?php foreach($items as $num=>$row):?>
  <tr id="itemID_<?php echo $row->id;?>">
    <td><?php echo $row->word;?></td>
    <td><?php echo $row->replacement;?></td>
    <td><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span> Удалить', CHtml::normalizeUrl(array('stopwords/index', 'del'=>$row->id)), array('data-toggle'=>'tooltip', 'title'=>'Удалить'));?></li>

        
    </td>

  </tr>

<?php endforeach; ?>
</tbody>

</table>

</div>