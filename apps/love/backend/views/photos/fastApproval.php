<div class="row">
	
<div class="page-header" style="margin-top:8px;">
  Быстрая модерация
</div>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'userForm',
    //'htmlOptions'=>array('class'=>'search-partners'),
)); ?>
<div class="row">
<?php foreach($photos as $num=>$row):?>
<input type="hidden" name="to_user[]" value="<?php echo $row->id;?>">

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?=$row->imageSource?>" alt="...">
      <div class="caption">

	
<div class="btn-group" data-toggle="buttons">
  <?php if (!$row->approve):?>
  <label class="btn btn-success btn-sm active" for="FastApproval_<?php echo $row->id;?>_1">
    <input type="radio" name="FastApproval[<?php echo $row->id;?>]" id="FastApproval_<?php echo $row->id;?>_1" value="approvals" checked> Одобрить
  </label>
  <?php endif;?>
<?php if ($row->album_id==0):?>
  <label class="btn btn-warning btn-sm" for="FastApproval_<?php echo $row->id;?>_2">
    <input type="radio" name="FastApproval[<?php echo $row->id;?>]" id="FastApproval_<?php echo $row->id;?>_2" value="reject"> В "Разное"
  </label>
<?php endif;?>
<?php if (!$row->album->censor):?>
  <label class="btn btn-info btn-sm" for="FastApproval_<?php echo $row->id;?>_3">
    <input type="radio" name="FastApproval[<?php echo $row->id;?>]" id="FastApproval_<?php echo $row->id;?>_3" value="censor"> В "18+"
  </label>
<?php endif;?>
  <label class="btn btn-danger btn-sm" for="FastApproval_<?php echo $row->id;?>_4">
    <input type="radio" name="FastApproval[<?php echo $row->id;?>]" id="FastApproval_<?php echo $row->id;?>_4" value="remove"> Удалить
  </label>
</div>

	<div style="margin-top:5px;">
	<?php if ($row->album->name && $row->album->system=='1'):
	echo '<span class="label label-warning">'.$row->album->name.'</span>';
    elseif ($row->album->name && $row->album->system=='2'):
	 echo '<span class="label label-danger">'.$row->album->name.'</span>';
    elseif ($row->album->name && $row->album->system=='0'):
	 echo '<span class="label label-default">'.$row->album->name.'</span>';
    else:
	 echo '<span class="label label-info">Фотографии со страницы</span>';  endif;?>
	
	
</div>
	
<?php if ($row->prior_approval): echo '<div style="margin-top:5px;"><span class="label label-success">Предварительно одобренная</span></div>'; endif;?>


<?php if ($row->user->isReal) echo '<div style="margin-top:5px;"><span class="label label-success">Real</span></div>';?>

<?php echo $row->user->sex=='2' ? 'Жен.' : 'Муж.';?> <a href="/id<?php echo $row->user->id;?>" target="_blank" style="color:#999999;"><span class="glyphicon glyphicon-share-alt"></span></a>
      </div>
      
      
      
    </div>
  </div>


<?php endforeach; ?>
</div>
<button type="button" class="btn btn-primary  btn-lg btn-block" id="goApproval" onclick="$('#userForm').submit(); $('#goApproval').hide(); YiiAlert.loading();"> Применить</button>

<?php $this->endWidget(); ?>



  

</div>