<div>

    <div class="pull-left">

<div class="btn-group">
<?php if (!$photo->approve):?><button type="button" class="btn btn-success btn-sm" onclick="photoModerate.approvals('<?php echo CHtml::normalizeUrl(array('photos/index', 'photoId'=>$photo->id));?>');"><span class="glyphicon glyphicon-hand-up"></span> Одобрить</button><?php endif;?>

<?php if ($photo->album_id==0):?><button type="button" class="btn btn-warning btn-sm" onclick="photoModerate.reject('<?php echo CHtml::normalizeUrl(array('photos/index', 'photoId'=>$photo->id));?>');"><span class="glyphicon glyphicon-hand-down"></span> Переместить в альбом "Разное"</button><?php endif;?>


<?php if (!$photo->album->censor):?><button type="button" class="btn btn-info btn-sm" onclick="photoModerate.censor('<?php echo CHtml::normalizeUrl(array('photos/index', 'photoId'=>$photo->id));?>');"><span class="glyphicon glyphicon-hand-down"></span> Переместить в альбом "18+"</button><?php endif;?>

<button type="button" class="btn btn-danger btn-sm" onclick="photoModerate.remove('<?php echo CHtml::normalizeUrl(array('photos/index', 'photoId'=>$photo->id));?>');"><span class="glyphicon glyphicon-remove"></span> Удалить</button>
</div>
    </div>
    <div class="pull-right" style="margin-left:15px;width:200px;">
<?php if ($photo->approve && $photo->album_id==0):?>
<div class="alert alert-success">Фотография одобрена</div>
<?php elseif ($photo->album_id==0):?>

<div class="alert alert-warning">Фотография не одобрена</div>
<?php endif;?>

<div class="benefits" style="margin-bottom:25px;">
<div><h4 style="margin-bottom:5px;">Отправитель</h4></div>
<?php
	echo CHtml::link(
		$photo->user->name, 
		CHtml::normalizeUrl(array('users/index', 'c[search]'=>'id:'.$photo->user->id))
	);
?>

<div><h4 style="margin-bottom:5px;margin-top:5px;">Альбом</h4></div>
<?php
	echo CHtml::link(
		$photo->album->name ? $photo->album->name : 'Фотографии со страницы', 
		CHtml::normalizeUrl(array('photos/index', 'userId'=>$photo->user_id, 'albumId'=>$photo->album_id))
	);
?>
		</div>
		
</div>
    </div>
<div class="clearfix"></div>
</div>