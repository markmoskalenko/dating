<div class="row">
  <div class="col-xs-12 col-sm-6 col-md-10">
	
<div class="page-header" style="margin-top:8px;">
  Всего фотографий <span class="badge"><?php echo $count;?></span>
</div>

<div class="table-responsive">
<table class="table table-striped table-hover"> 
<thead>
  <tr>
    <th>ID</th>
    <th>Фото</th>
    <th>Имя</th>
    <th>Пол</th>
    <th>Дата загр.</th>
    <th>Лайков</th>
    <th>Альбом</th>
    <th>Модерация</th>
  </tr>
</thead>
<tbody>
<?php foreach($photos as $num=>$row):?>
  <tr class="danger1">
    <td><?php echo $row->id;?></td>
    <td><?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(100,100)
		), 
		CHtml::normalizeUrl(CMap::mergeArray(array('photos/index', 'photoId'=>$row->id), is_array($_GET['c']) ? $_GET : array())),
		array('id'=>'showModalPhotoBox')
	);
?></td>
    <td><?php echo $row->user->name;?><?php if ($row->user->isReal) echo ' <span class="label label-success">Real</span>';?><?php if ($row->user->isBot) echo ' <span class="label label-danger">Bot</span>';?> <a href="/id<?php echo $row->user->id;?>" target="_blank" style="color:#999999;"><span class="glyphicon glyphicon-share-alt"></span></a></td>
    <td><?php echo $row->user->sex=='2' ? 'Женский' : 'Мужской';?></td>
    <td><?php echo Html::date($row->date);?></td>
    <td><?php echo $row->likes;?></td>
    <td><?php if ($row->album->name && $row->album->system=='1'):
	echo '<span class="label label-warning">'.$row->album->name.'</span>';
    elseif ($row->album->name && $row->album->system=='2'):
	 echo '<span class="label label-danger">'.$row->album->name.'</span>';
    elseif ($row->album->name && $row->album->system=='0'):
	 echo '<span class="label label-default">'.$row->album->name.'</span>';
    else:
	 echo '<span class="label label-info">Фотографии со страницы</span>';  endif;?></td>
    <td><?php if ($row->approve): echo '<span class="label label-success">Одобрено</span>'; elseif (!$row->approve): echo '<span class="label label-primary">Нет</span>'; endif;?>
    <?php if ($row->prior_approval): echo '<span class="label label-success">Предварительная</span>'; endif;?>
    </td>

  </tr>

<?php endforeach; ?>
</tbody>

</table>
</div>
<?php $this->widget('bootstrap.widgets.TbPager', array(
       'id'=>'wallPages',
        'pages'=>$pages,
	'header'=>false,
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'cssFile'=>Html::cssUrl('pager.css'),
        'nextPageLabel'=>'>',
	'prevPageLabel'=>'<',
        'lastPageLabel'=>'>>',
	'firstPageLabel'=>'<<',
	//'maxButtonCount'=>25,
        ));
?>
  <script type="text/javascript">
 $(document).ready(function(){
  photoShow.pageStart=<?=$_GET['page'] ? $_GET['page'] : 1?>;
 });
</script>
  
  </div>
  <div class="col-xs-4 col-md-2">
  
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'filterForm',
    'method'=>'GET',
    'action'=>CHtml::normalizeUrl(array('photos/index'))
)); ?>
  
  <div class="form-group">
    <label for="exampleInputPassword1">Показать</label>
    <?php echo CHtml::dropDownList('c[show]', $_GET['c']['show'], array(''=>'Все', '1'=>'Непроверенные', '2'=>'Проверенные'), array('class'=>'form-control input-sm'));?>
    

  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Альбом</label>
    <?php echo CHtml::dropDownList('c[sys_album]', $_GET['c']['sys_album'], array(''=>'Все', '1'=>'Фотографии со страницы', '2'=>'18+', '3'=>'Разное'), array('class'=>'form-control input-sm'));?>
    

  </div>
  <div class="checkbox">
    <label>
        
    <?php echo CHtml::checkBox('c[prior_approval]', $_GET['c']['prior_approval']);?> Предварительно одобренные

    </label>
  </div>

  <div class="checkbox">
    <label>
        
    <?php echo CHtml::checkBox('c[bots]', $_GET['c']['bots']);?> Фотографии ботов

    </label>
  </div>
  <button type="submit" class="btn btn-primary">Обновить</button>

<?php $this->endWidget(); ?>






  
  </div>
</div>