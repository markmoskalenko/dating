<?php
Yii::app()->clientScript->registerCssFile(Html::cssUrl('signin.css'));
Yii::app()->clientScript->registerCoreScript('jquery');
//Yii::app()->clientScript->registerCoreScript('form');
Yii::app()->clientScript->registerCoreScript('cookie');
Yii::app()->clientScript->registerCssFile(Html::cssUrl('bootstrap.css'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('bootstrap.min.js'));;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>YiiLifeMeet::Control panel</title>
  </head>

  <body>

    <div class="container">
        
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'auhtForm',
    'htmlOptions'=>array('class'=>'form-signin'),
)); ?>

<?php echo CHtml::errorSummary($model);?>
        <h2 class="form-signin-heading">Администрирование</h2>
         <?php echo $form->textField($model,'username',array('class'=>'form-control', 'placeholder'=>'Login', 'autofocus'=>'autofocus')); ?>
        <?php echo $form->passwordField($model,'password',array('class'=>'form-control', 'placeholder'=>'Password')); ?>
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <input type="submit" value="Войти" class="btn btn-lg btn-primary btn-block" />

<?php $this->endWidget(); ?>
    </div> <!-- /container -->

  </body>
</html>
