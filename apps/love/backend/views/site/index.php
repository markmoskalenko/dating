
<div class="row">
  <div class="col-md-5">
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Общая информация</div>


  <!-- List group -->
<ul class="list-group">
  <li class="list-group-item">
    <span class="badge"><?php echo $count_user?></span>
    Всего анкет на сайте
  </li>
  <li class="list-group-item">
    <span class="badge"><?php echo $count_photo?></span>
    Всего фотографий
  </li>
  <li class="list-group-item">
    <span class="badge"><?php echo $this->countPhotoRequiring?></span>
    Непроверенные фотографии
  </li>
</ul>
</div>





<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Администраторы</div>

    
    
<table class="table table-striped table-hover"> 
<thead>
  <tr>
    <th>ID</th>
    <th>Фото</th>
    <th>Имя</th>
    <th>Посл. акт.</th>
    <th>Роль</th>

  </tr>
</thead>
<tbody>
<?php foreach($admins as $num=>$row):?>
  <tr id="uID_<?php echo $row->id;?>">
    <td><?php echo $row->id;?></td>
    <td><?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(55,55, 'camera_s.png')
		), 
		CHtml::normalizeUrl(array('photos/index', 'c[user_id]'=>$row->id)),
		array('id'=>'showModalPhotoBox11')
	);
?></td>
    <td><?php echo $row->name;?></td>
    <td><?php echo Html::date($row->last_date);?></td>
    <td><span class="label label-primary"><?php echo $row->role;?></span></td>



  </tr>

<?php endforeach; ?>
</tbody>

</table>
    
    
    
    
    

</div>




  </div>
  <div class="col-md-4">

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Статистика датинга</div>
  <div class="panel-body">
    
  </div>
</div>


  </div>
  <div class="col-md-3">
    
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Система</div>

  <!-- List group -->
<ul class="list-group">
  <li class="list-group-item">
    Версия PHP: <?php echo PHP_VERSION?>
  </li>
  <li class="list-group-item">
    Версия Yii: <?php echo Yii::getVersion();?>
  </li>
  <li class="list-group-item">
    
    Версия Mysql: <?php echo Yii::app()->db->createCommand('SELECT VERSION()')->queryScalar();?>
  </li>
</ul>

</div>
  </div>
</div>








