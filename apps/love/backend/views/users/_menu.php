<?php $this->widget('Menu', array(
    'encodeLabel'=>false,
    'htmlOptions'=>array('class'=>'nav nav-pills'),
			'items'=>array(
				array('label'=>'Основное', 'url'=>array('users/edit', 'id'=>$_GET['id']), 'linkOptions'=>array()),
				array('label'=>'Настройки', 'url'=>array('users/setting', 'id'=>$_GET['id']), 'linkOptions'=>array()),
				array('label'=>'Настройки бота', 'url'=>array('users/botsetting', 'id'=>$_GET['id']), 'linkOptions'=>array(), 'visible'=>$user->isBot),
			),
)); ?>
<br>