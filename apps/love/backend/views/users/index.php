<script type="text/javascript">
function checkAllCheckboxes(object, prefix){  
  var re = new RegExp('^' + prefix);
  var checkboxes = document.getElementsByTagName('input');
  var total = checkboxes.length;
  for(i = 0; i < total; i++){
    if(checkboxes.item(i).id.match(re)){
      if(object.checked){
        checkboxes.item(i).checked = true;
      }
      else{
        checkboxes.item(i).checked = false;
      }
    }
  }
}
$(document).ready(function(){
	$('#userForm').ajaxForm({
		dataType: 'json',
		url: $(this).attr("action"),
		type: "POST",
		cache: false,
		beforeSend: function(){
		},
		success: function(data) {
			YiiAlert.success('Выделенные объекты поставлены в очередь на удаление');
			
$('input[type=checkbox]').each(function () {
           if (this.checked) {
               console.log($(this).val());
	       $('#uID_'+$(this).val()).fadeTo('speed', 0.4);

           }
});
			
		}
	});
	
    $("#c_bots").change(function() { 
		
		
		if ($(this).attr('checked')=='checked') {
			$("#c_bots_m").parent().parent().show();
		}
		else {
			$("#c_bots_m").parent().parent().hide();
		}

		
	});
	<?php echo $_GET['c']['bots'] ? '$("#c_bots_m").parent().parent().show();' : '$("#c_bots_m").parent().parent().hide();'?>
});
</script>
<script type="text/javascript">

editBot = {
	
	CSRFTOKEN: '<?php echo Yii::app()->request->csrfToken;?>',
	formAction: '<?=CHtml::normalizeUrl(array('users/botsetting')); ?>',

	save: function(id)
	{
	$.ajax({
		type: "POST",
		url: editBot.formAction+'?id='+id,
		data: 'CSRFTOKEN='+editBot.CSRFTOKEN+'&Bots[messages]='+$("#Botmessages_"+id).val(),
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                        YiiAlert.error(x.responseText, x.status);
                },
		beforeSend: function(){
                        

		},
		success: function(responce){
			YiiAlert.success('Список сообщений сохранен.');
		}
	});
	}
}
</script>
<div class="row">
  <div class="col-xs-12 col-sm-6 col-md-10">
<div class="page-header" style="margin-top:8px;">
  Всего пользователей <span class="badge"><?php echo $count;?></span>
</div>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'userForm',
    //'htmlOptions'=>array('class'=>'search-partners'),
)); ?>
<?php echo CHtml::hiddenField('_CMD', 'actionUser'); ?>
<table class="table table-striped table-hover"> 
<thead>
  <tr><?php $sortClasses=array(null=>'', true=>'active', false=>'active'); ?>
    <th><input id="checkAll" onclick="checkAllCheckboxes(this, 'to_user');" type="checkbox" class="checkbox"></th>
    <th class="<?php echo $sortClasses[$sort->getDirection('id')]; ?>"><?php echo $sort->link('id', 'ID');?></th>
    <th>Фото</th>
    <th>Имя</th>
    <th>Пол</th>
<?php if (!$_GET['c']['bots']):?>
    <th>Возр.</th>
    <th>Город</th>
    <th class="<?php echo $sortClasses[$sort->getDirection('reg_date')]; ?>"><?php echo $sort->link('reg_date', 'Дата рег.');?></th>
    <th class="<?php echo $sortClasses[$sort->getDirection('last_date')]; ?>"><?php echo $sort->link('last_date', 'Посл. акт.');?></th>
    <th class="<?php echo $sortClasses[$sort->getDirection('balance')]; ?>"><?php echo $sort->link('balance', 'Баланс');?></th>
    <th>Роль</th>
<?php else:?>
    <th class="col-sm-7">Сообщения</th>
<?php endif;?>
    <th></th>
  </tr>
</thead>
<tbody>
<?php foreach($users as $num=>$row):?>
  <tr id="uID_<?php echo $row->id;?>">
    <td><input id="to_user_<?php echo $row->id;?>" type="checkbox" name="to_user[]" value="<?php echo $row->id;?>" class="checkbox"></td>
    <td><?php echo $row->id;?></td>
    <td><?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(55,55, 'camera_s.png')
		), 
		CHtml::normalizeUrl(array('photos/index', 'c[user_id]'=>$row->id)),
		array('id'=>'showModalPhotoBox11')
	);
?></td>
    <td><?php echo $row->name;?><?php if ($row->remove=='1') echo ' <span class="label label-danger">Пользователь будет удален</span>';?><?php if ($row->isReal) echo ' <span class="label label-success">Real</span>';?><?php if ($row->isBot) echo ' <span class="label label-danger">Bot</span>';?> <a href="/id<?php echo $row->id;?>" target="_blank" style="color:#999999;"><span class="glyphicon glyphicon-share-alt"></span></a></td>
    <td><?php echo $row->sex=='2' ? 'Жен' : 'Муж';?></td>

<?php if (!$_GET['c']['bots']):?>
    <td><?php echo Html::age($row->birthday);?></td>
    <td><?php echo $row->geo->country;?>, <?php echo $row->geo->city;?></td>
    <td><?php echo Html::date($row->reg_date);?></td>
    <td><?php echo Html::date($row->last_date);?></td>
    <td><?php echo $row->balance;?></td>
    <td><?php echo $row->role;?></td>
<?php else:?>
    <td><?php echo CHtml::textArea('Bots[messages]',$row->bot->messages,array('onblur'=>'editBot.save("'.$row->id.'");', 'id'=>'Botmessages_'.$row->id, 'class'=>'form-control input-sm col-sm-10', 'style'=>'height:90px;')); ?></td>
<?php endif;?>
    <td>
        
<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
    Действие <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo CHtml::link('<span class="glyphicon glyphicon-pencil"></span> Редактировать', CHtml::normalizeUrl(array('users/edit', 'id'=>$row->id)), array('data-toggle'=>'tooltip', 'title'=>'Ред.'));?></li>
    <li><?php echo CHtml::link('<span class="glyphicon glyphicon-ban-circle"></span> Заблокировать', CHtml::normalizeUrl(array('users/ban', 'id'=>$row->id)), array('data-toggle'=>'tooltip', 'title'=>'Заблокировать'));?></li>
    <li><?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Войти в акк', 'http://lavsdate.com/user/login?auto='.base64_encode($row->auth->email.';'.$row->auth->password.';invisible_being'), array('data-toggle'=>'tooltip', 'title'=>'Войти в аккаунт'));?></li>

    <li class="divider"></li>
    <li><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span> Удалить', CHtml::normalizeUrl(array('users/del', 'id'=>$row->id)), array('data-toggle'=>'tooltip', 'title'=>'Удалить'));?></li>
  </ul>
</div>
        
        
        
        
    </td>

  </tr>

<?php endforeach; ?>
</tbody>

</table>
<div class="clearfix">
<?php $this->widget('bootstrap.widgets.TbPager', array(
       'id'=>'wallPages',
        'pages'=>$pages,
	'header'=>false,
	'htmlOptions'=>array('class'=>'pagination pagination-sm', 'style'=>'margin-top:0px;'),
        //'cssFile'=>Html::cssUrl('pager.css'),
        'nextPageLabel'=>'>',
	'prevPageLabel'=>'<',
        'lastPageLabel'=>'>>',
	'firstPageLabel'=>'<<',
	//'maxButtonCount'=>25,
        ));
?>

  <div class="btn-group" style="float:right;">

<button type="button" class="btn btn-danger btn-sm" onclick="if (confirm('Вы уверены что хотите удалить выделенные аккаунты?')) { $('#userForm').submit(); }"><span class="glyphicon glyphicon-remove"></span> Удалить</button>
</div>
</div>

<?php $this->endWidget(); ?>
  </div>
  <div class="col-xs-4 col-md-2">
  
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'filterForm',
    'method'=>'GET',
    'action'=>CHtml::normalizeUrl(array('users/index'))
)); ?>
  <?php echo CHtml::hiddenField('sort', $_GET['sort']);?> 
  <div class="form-group">
    <label for="exampleInputEmail1">Найти</label>
    
        <?php echo CHtml::textField('c[search]', $_GET['c']['search'], array('class'=>'form-control input-sm', 'placeholder'=>'Логин, email, ip, phone'));?> 

  </div>
  
  <div class="form-group">
    <label for="exampleInputPassword1">Пол</label>
    
    <?php echo CHtml::dropDownList('c[sex]', $_GET['c']['sex'], array(''=>'Все', '1'=>'Мужской', '2'=>'Женский'), array('class'=>'form-control input-sm'));?>
    
  </div>
  
  <div class="form-group">
    <label for="exampleInputPassword1">С фото</label>
    
    <?php echo CHtml::dropDownList('c[photo]', $_GET['c']['photo'], array(''=>'--', 'yes'=>'Да', 'no'=>'Без'), array('class'=>'form-control input-sm'));?>
    
  </div>

  <div class="checkbox">
    <label>
        
    <?php echo CHtml::checkBox('c[real]', $_GET['c']['real']);?> Real

    </label>
  </div>
  <div class="checkbox">
    <label>
        
    <?php echo CHtml::checkBox('c[bots]', $_GET['c']['bots']);?> Боты

    </label>
  </div>
  
  <div class="checkbox">
    <label>
        
    <?php echo CHtml::checkBox('c[bots_m]', $_GET['c']['bots_m']);?> Боты с сообщениями

    </label>
  </div>
  
  <button type="submit" class="btn btn-primary">Обновить</button>
<?php $this->endWidget(); ?>





  
  </div>
</div>