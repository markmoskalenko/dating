<div class="row">
  <div class="col-xs-12 col-sm-6 col-md-10">
<div class="page-header" style="margin-top:8px;">
  Редактировать пользователя
</div>

<?php $this->renderPartial('_menu', array('user'=>$model)); ?>


    <div class="alert alert-success" style="display:none;" id="flash-success">
    Изменения сохранены.
    </div>


<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('CActiveForm', array(
    'id'=>'editForm',
    'htmlOptions'=>array('class'=>'form-horizontal'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
           'afterValidate' => 'js: function(form, data, hasError) {
if (!hasError) {
        $.ajax({
          type: "POST",
          url: $("#editForm").attr("action"),
          data: $("#editForm").serialize(),
	beforeSend: function(){
			$("#loading_stat").show();
		},
          success: function(data){
	  $("#loading_stat").hide();
	  $("#flash-success").fadeTo(\'speed\', 1.0);
	   $("body").scrollTo( 0, 310 );
	  setTimeout("$(\'#flash-success\').fadeTo(\'speed\', 0.4);", 1500);

	  }

        });
        return false;
        }}'
    ),
)); ?>
   <div class="form-group">
 <?php echo $form->labelEx($model,'name',array('class'=>'col-lg-3 control-label')); ?>
    <div class="col-lg-4" style="width:250px;">
<?php echo $form->textField($model, 'name', array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'name'); ?>
    </div>
    </div>

 <div class="form-group">
<?php echo $form->labelEx($model,'sex',array('label'=>'Ваш пол:', 'class'=>'col-lg-3 control-label')); ?>
    <div class="col-lg-4" style="width:250px;">
		<?php echo $form->dropDownList($model, 'sex', array('1'=>'Парень', '2'=>'Девушка'),array('class'=>'form-control input-sm')); ?>
		<?php echo $form->error($model,'sex'); ?>
</div>
</div>
 <div class="form-group">
<?php echo $form->labelEx($model,'birthday',array('label'=>'Дата рождения:', 'class'=>'col-lg-3 control-label')); ?>
<div class="col-lg-8">
	
<?php $this->widget('EHtmlDateSelect',
                        array(
                              'time'=>$model->birthday,
                              'field_array'=>'User[ItemsBirthday]',
                              'prefix'=>'',
                              'field_order'=>'DMY',
                              'start_year'=>1940,
                              'end_year'=>date("Y")-14,
			      'day_extra'=>'style="width:60px;" class="form-control input-sm pull-left"',
			      'month_extra'=>'style="width:100px;" class="form-control input-sm pull-left"',
			      'year_extra'=>'style="width:70px;" class="form-control input-sm pull-left"',
			      'day_empty'=>'День',
			      'month_empty'=>'Месяц',
			      'year_empty'=>'Год',
                             )
                       );
	
	 echo $form->hiddenField($model,'birthday');
	?>
    <?php //echo $form->dropDownList($model,'day_birth', range(1,31), array('empty' => 'День', 'style'=>'width:70px;')); ?>
        <?php //echo $form->dropDownList($model,'month_birth', array('01'=>'Январь','02'=>"Февраль",'03'=>"Март",'04'=>"Апрель",'05'=>"Май",'06'=>"Июнь",'07'=>"Июль",'08'=>"Август",'09'=>"Сентябрь",'10'=>"Октябрь",'11'=>"Ноябрь",'12'=>"Декабрь"), array('empty' => 'Месяц', 'style'=>'width:170px;')); ?>
        <?php //echo $form->dropDownList($model,'year_birth', range(date("Y")-14,1940), array('empty' => 'Год', 'style'=>'width:80px;')); ?>
	
	
							<?php /*$this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,
            'attribute' => 'birthday',
               
            'options'=>array(
        'showAnim'=>'fold',

        'changeYear'=>true,
        'changeMonth'=>true,
        'firstDay'=>1,
                'dateFormat'=>'yy-mm-dd',
		'yearRange'=>'1931:1994',

                ),
            'htmlOptions'=>array(
                //'style'=>'height:20px;'
            ),
            'language' => 'ru'
        ));*/?>
									<?php echo $form->error($model,'birthday'); ?>
</div>
</div>


<?php// echo $form->dropDownListRow($model, 'country_id', CHtml::listData(GeoCountry::model()->findAll(), 'id_country', 'name')); ?>

<?php

		$cs=Yii::app()->getClientScript();
		$jsonurl=CHtml::normalizeUrl(array('site/geo'));
		$script=<<<END
$("#User_country_id").change(function() {
        id=$("#User_country_id").attr('value');
	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "country_id="+id,
		cache: false,
		beforeSend: function(){
			$("#loading_region").show();
		},
		success: function(data) {
			$("#loading_region").hide();
                        var setoptions = '';
			
                        for (var i = 0; i < data.length; i++) {
                                setoptions += '<option value="' + data[i].id_region + '">' + data[i].name + '</option>';
                        }
                        $("#User_region_id").html(setoptions);
			
			$("#User_region_id").select2({
				placeholder: "Выберите регион"
			});
			
			$("#User_region_id").change();
                }  
	});
});

$("#User_region_id").change(function() {
        id=$("#User_region_id").attr('value');

	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "region_id="+id,
		cache: false,
		beforeSend: function(){
                       $("#loading_city").show();
		},
		success: function(data) {
			$("#loading_city").hide();
			var setoptions = '';
			
                        for (var i = 0; i < data.length; i++) {
                                setoptions += '<option value="' + data[i].id_city + '">' + data[i].name + '</option>';
                        }
                        $("#User_city_id").html(setoptions); 
			
				$("#User_city_id").select2({
					placeholder: "Выберите город"
				});
                }  
	});
});

END;
		$cs->registerScript(__CLASS__.'#geo1', $script);
?>
 <div class="form-group">
<?php echo $form->labelEx($model,'country_id',array('label'=>'Страна:', 'class'=>'col-lg-3 control-label')); ?>
<div class="col-lg-5">
<?php $this->widget('ext.select2.ESelect2',array(
  'model'=>$model,
  'attribute'=>'country_id',
  'data'=>CHtml::listData(GEOCountry::model()->findAll(), 'id_country', 'name'),
  'htmlOptions'=>array(
    'style'=>'width:220px;',
  ),
  'options'=>array(
    'placeholder'=>'Выберите страну',
    //'allowClear'=>true,
  ),
)); ?>
<?php echo $form->error($model,'country_id'); ?>
</div>
</div>

 <div class="form-group">
<?php echo $form->labelEx($model,'region_id',array('label'=>'Регион:', 'class'=>'col-lg-3 control-label')); ?>
<div class="col-lg-5">
<?php $this->widget('ext.select2.ESelect2',array(
  'model'=>$model,
  'attribute'=>'region_id',
  'data'=>CHtml::listData(GEORegion::model()->findAll('id_country=:id_country', array('id_country'=>$model->country_id)), 'id_region', 'name'),
  'htmlOptions'=>array(
    'style'=>'width:220px;',
  ),
  'options'=>array(
    'placeholder'=>'Выберите регион',
    //'allowClear'=>true,
  ),
)); ?>

<span id="loading_region" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
<?php echo $form->error($model,'region_id'); ?>
</div>
</div>

 <div class="form-group">
<?php echo $form->labelEx($model,'city_id',array('label'=>'Город:', 'class'=>'col-lg-3 control-label')); ?>
<div class="col-lg-5">
<?php $this->widget('ext.select2.ESelect2',array(
  'model'=>$model,
  'attribute'=>'city_id',
  'data'=>CHtml::listData(GEOCity::model()->findAll('id_region=:id_region AND id_country=:id_country', array('id_region'=>$model->region_id, 'id_country'=>$model->country_id)), 'id_city', 'name'),
  'htmlOptions'=>array(
    'style'=>'width:220px;',
  ),
  'options'=>array(
    'placeholder'=>'Выберите город',
    //'allowClear'=>true,
  ),
)); ?>

<span id="loading_city" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
<?php echo $form->error($model,'city_id'); ?>
</div>
</div>
 
  <div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
<input type="submit" value="Сохранить" class="btn btn-success" />

 <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
    </div>
  </div>


<?php $this->endWidget(); ?>








  </div>
  <div class="col-xs-4 col-md-2">
    
    <label for="exampleInputEmail1"><?php echo $model->name?></label>
    
<?php 
	echo CHtml::link(
		CHtml::image( 
			$model->getImage(150,150, 'camera_s.png')
		), 
		CHtml::normalizeUrl(array('photos/index', 'c[user_id]'=>$row->id)),
		array('id'=>'showModalPhotoBox11')
	);
?>
  </div>
</div>