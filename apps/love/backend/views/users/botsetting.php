<div class="row">
  <div class="col-xs-12 col-sm-6 col-md-10">
<div class="page-header" style="margin-top:8px;">
  Редактировать
</div>


<?php $this->renderPartial('_menu', array('user'=>$user)); ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'form-horizontal'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>true,
            'validateOnChange'=>true,

           'afterValidate' => 'js: function(form, data, hasError) {
if (!hasError) {
        $.ajax({
          type: "POST",
          url: $("#registerForm").attr("action"),
          data: $("#registerForm").serialize(),
	beforeSend: function(){
			$("#loading_stat").show();
		},
          success: function(data){
	  $("#loading_stat").hide();
	  
	  $("a[href=\''.CHtml::normalizeUrl(array('subaccount/index')).'\']").click();


	  }

        });
        return false;
        }}',
            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),

)); ?>
<?php echo CHtml::errorSummary($model);?>
	
	<ul id="errorSummary" style="display:none;" class="list-unstyled alert alert-danger">
		</ul>
	
  
  <div class="form-group">
    <?php echo $form->labelEx($model,'messages', array('label' => 'Сообщение (каждое сообщение через ";")', 'class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-7">
	    <?php echo $form->textArea($model,'messages',array('class'=>'form-control input-sm', 'style'=>'height:90px;')); ?>
    </div>
    <?php echo $form->error($model,'messages',array('class'=>'text-danger')); ?>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
      <?=Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class'=>'btn btn-primary'))?>
      <input type="button" value="Отмена" onclick="history.back();" class="btn btn-default" />
	<span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('load.gif')?>' align='absmiddle' /></span>
    </div>
  </div>


<?php $this->endWidget(); ?>





  </div>
</div>