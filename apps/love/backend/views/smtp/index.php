
<div class="row">
    <div class="page-header" style="margin-top:8px;">
        Настройки SMTP
    </div>

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'registerForm',
        'htmlOptions'=>array('class'=>'form-horizontal'),
        'enableClientValidation'=>false,
        'enableAjaxValidation'=>false,

    )); ?>
    <?php echo CHtml::errorSummary($model);?>



    <div class="form-group">
        <?php echo $form->labelEx($model,'host', array('label' => 'Хост:', 'class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textField($model,'host',array('class'=>'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'username', array('label' => 'Имя пользователя:', 'class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textField($model,'username',array('class'=>'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'password', array('label' => 'Пароль:', 'class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textField($model,'password',array('class'=>'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'port', array('label' => 'Порт:', 'class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textField($model,'port',array('class'=>'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'encryption', array('label' => 'Encryption:', 'class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <?php echo $form->textField($model,'encryption',array('class'=>'form-control input-sm')); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-10">
            <?=Html::submitButton('Сохранить', array('class'=>'btn btn-primary'))?>
            <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('load.gif')?>' align='absmiddle' /></span>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>