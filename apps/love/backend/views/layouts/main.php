<?php
Yii::app()->clientScript->registerCssFile(Html::cssUrl('style.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('select2-bootstrap.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('jquery.arcticmodal-0.3.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('font-awesome.css'));
Yii::app()->clientScript->registerCoreScript('jquery');
//Yii::app()->clientScript->registerCoreScript('form');
Yii::app()->clientScript->registerCoreScript('cookie');
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('main.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.scrollTo-min.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.blockUI.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.form.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/jquery.arcticmodal-0.3.min.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));
//Yii::app()->bootstrap->register();
//Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/cusel-min-2.4.1.js'));
//Yii::app()->bootstrap->registerCoreScripts();
//Yii::app()->bootstrap->registerCoreCss();

//Yii::app()->clientScript->registerCssFile(Html::cssUrl('bootstrap.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('bootstrap.css'));
//Yii::app()->clientScript->registerCssFile(Html::cssUrl('bootstrap-theme.css'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('bootstrap.min.js'));
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('bootstrap/js/holder.js'));

?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title><?php echo $this->pageTitle ? $this->pageTitle : 'Администрирование сайтом'; ?></title>
</head>
<script type="text/javascript">
 function imageUrl(url){
  return '<?=Html::imageUrl('')?>'+url;
 }
 function isGuest(){
  return <?=Yii::app()->user->isGuest ? Yii::app()->user->isGuest : 0?>;
 }
 function userBalance(){
  return '<?=Yii::app()->user->balance?>';
 }
</script>
<body>
    <!-- Wrap all page content here -->
    <div id="wrap">

      <!-- Fixed navbar -->
      <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('site/index'));?>"><span class="glyphicon glyphicon-home" style="font-size:16px;"></span></a>
          </div>
          <div class="collapse navbar-collapse">
		<?php $this->widget('BackendMenu',array(
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
                        'htmlOptions'=>array('class'=>'nav navbar-nav'),
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
                        'items'=>array(
                                array('label'=>'<span class="glyphicon glyphicon-user"></span> Пользователи', 'url'=>array('users/index'), 'visible'=>Yii::app()->user->role=='admin'),
                                array('label'=>'<span class="glyphicon glyphicon-camera"></span> Модерация <span class="badge">'.$this->countPhotoRequiring.'</span>', 'url'=>array('photos/fastApproval')),
                                array('label'=>'<span class="glyphicon glyphicon-camera"></span> Фото', 'url'=>array('photos/index'), 'visible'=>Yii::app()->user->role=='admin'),
                                array('label'=>'<span class="glyphicon glyphicon-warning-sign"></span> Жалобы', 'url'=>array('complaints/index')),
                                array('label'=>'<span class="glyphicon glyphicon-globe"></span> Тикеты', 'url'=>array('tickets/index')),
                                array('label'=>'Ещё', 'url'=>'#', 'items'=>array(
					//array('label'=>'<span class="glyphicon glyphicon-pencil"></span> Управление ботами', 'url'=>array('botsmanager/index')),
                                        array('label'=>'<span class="glyphicon glyphicon-th"></span> Стоп слова', 'url'=>array('stopwords/index'), 'visible'=>Yii::app()->user->role=='admin'),
                                        array('label'=>'<span class="glyphicon glyphicon-th"></span> Сервисы', 'url'=>array('services/index'), 'visible'=>Yii::app()->user->role=='admin'),
                                        array('label'=>'<span class="glyphicon glyphicon-cog"></span> Настройки', 'url'=>array('settings/index'), 'visible'=>Yii::app()->user->role=='admin'),
                                        array('label'=>'<span class="glyphicon glyphicon-th"></span> Настройки SMTP', 'url'=>array('smtp/index'), 'visible'=>Yii::app()->user->role=='admin'),
                                        array('label'=>'<span class="glyphicon glyphicon-th"></span> Настройка рассылок', 'url'=>array('newsletter/index'), 'visible'=>Yii::app()->user->role=='admin'),
                                        array('label'=>'<span class="glyphicon glyphicon-off"></span> Выйти', 'url'=>array('site/logout')),
                                )),
                                //array('label'=>'<span class="glyphicon glyphicon-comment"></span> Чат', 'url'=>array('chat/index')),
                        ),
		)); ?>
                

                
                <p class="navbar-text navbar-right"><a href="<?php echo CHtml::normalizeUrl(array('site/index'));?>" class="navbar-link"><?php echo Yii::app()->user->model->login?> (<?php echo Yii::app()->user->role?>)</a></p>
          </div><!--/.nav-collapse -->
          
          
          
        </div>
      </div>

      <!-- Begin page content -->
      <div class="container">
<?php echo $content; ?>
      </div>
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted credit">Powered by YiiLifeMeet <span class="pull-right"><?php echo round(Yii::getLogger()->executionTime, 3);?>c</span></p>
      </div>
    </div>
</body>
</html>
<script type="text/javascript">
 $(document).ready(function(){
  var time_zone = (new Date().getTimezoneOffset()/60)*(1);
  var dateoffset = time_zone*60*60*1000;

$.cookie('time_zone', time_zone, {path: "/"});

$("body").ajaxError(function(x,e, settings, exception) {
	 YiiAlert.error(e.responseText, e.status);
});
 });
</script>