<?php

class UsersController extends BackendController
{
	public $layout='main';
	
	public function actionIndex()
	{
		if ($_POST['_CMD']=='actionUser')
		{
			if (!is_array($_POST['to_user'])) {
				throw new CHttpException(202,'Object is not selected.');
			}
			
			foreach($_POST['to_user'] as $item) {
				User::model()->updateByPk($item,array('remove'=>1));
			}
			
			if (Yii::app()->request->isAjaxRequest) {
				echo CJSON::encode(array());
				Yii::app()->end();
			}
		}
		
		$criteria=new CDbCriteria;
		
		if ($_GET['c']['sex'])
			$criteria->addCondition('`t`.`sex`='.intval($_GET['c']['sex']));
		
		if (preg_match("!id:(.*?)!si", $_GET['c']['search']) && $id=preg_replace("!id:(.*?)!si","\\1",$_GET['c']['search']))
			$criteria->addCondition('`t`.`id`='.intval($id));
		elseif ($_GET['c']['search'])
		{
			$criteria2=new CDbCriteria;
			$criteria2->addSearchCondition('`t`.`name`', $_GET['c']['search'], true, 'OR');
			$criteria2->addSearchCondition('`t`.`login`', $_GET['c']['search'], true, 'OR');
			$criteria2->addSearchCondition('`auth`.`email`', $_GET['c']['search'], true, 'OR');
			$criteria2->addSearchCondition('`auth`.`phone`', $_GET['c']['search'], true, 'OR');
			$criteria2->with='auth';
			//$criteria2->addSearchCondition('`t`.`last_ip`', $_GET['c']['search'], true, 'OR');
			$criteria->mergeWith($criteria2);
		}
		
		if ($_GET['c']['photo']=='yes')
			$criteria->addCondition('`t`.`have_photo`!=0');
			
		if ($_GET['c']['photo']=='no')
			$criteria->addCondition('`t`.`have_photo`=0');
			
		if ($_GET['c']['bots']=='1') {
			$criteria->addCondition('`t`.`isBot`=1');
			$criteria->with=array('bot');
			if ($_GET['c']['bots_m']=='1') {
				$criteria->addCondition('`bot`.`messages`!=""');
			}
		} else {
			$criteria->addCondition('`t`.`isBot`=0');
		}
		
		if ($_GET['c']['real']=='1')
			$criteria->addCondition('`t`.`isReal`=1 AND `t`.`isBot`=0');
		
		$count = User::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=20;
		$pages->applyLimit($criteria);
		
		$sort=new CSort('User');
		$sort->defaultOrder='t.id desc';
		$sort->multiSort=false;
		$sort->attributes=array(
			'id'=>array('default'=>'desc', 'asc'=>'t.id ASC', 'desc'=>'t.id DESC'),'login'=>array('default'=>'desc'),'reg_date'=>array('default'=>'desc'),'last_date'=>array('default'=>'desc'),'balance'=>array('default'=>'desc'),
		);
		$sort->applyOrder($criteria);
		
		$users=User::model()->with(array('country', 'city'))->findAll($criteria);
		
		$this->render('index', array('users'=>$users, 'pages'=>$pages, 'count'=>$count, 'sort'=>$sort));
	}
	

	public function actionEdit()
	{
		$model=User::model()->findByPk($_GET['id']);
		
		$this->pageTitle='Редактировать профиль';
		$this->breadcrumbs=array(
			'Редактировать профиль'
			//'Общее',
		);
		
		$model->setScenario('edit');
		$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['User'];
			
			if($model->validate())
			{
				if ($model->save()) {
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			}
		}
		
		
		$this->render('edit', array('model'=>$model));
	}
	
	public function actionBotsetting()
	{
		$user=User::model()->findByPk($_GET['id']);
		$model=Bots::model()->find('user_id=:user_id', array(':user_id'=>$_GET['id']));
		if (!$model->id) {
			$model=new Bots;
			$model->user_id=$_GET['id'];
		}
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['Bots'];
			
			if($model->validate())
			{
				if ($model->save()) {
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			}
		}
		
		$this->render('botsetting', array('model'=>$model, 'user'=>$user));
	}
	
	public function actionSetting()
	{
		$model=User::model()->findByPk($_GET['id']);
		
		$model->setScenario('adminEdit');
		$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['User'];
			
			if($model->validate())
			{
				if ($model->save()) {
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			}
		}
		
		$this->render('setting', array('model'=>$model));
	}
	
	public function actionBan()
	{
		echo '[entry:cannot]';
	
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']))
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}