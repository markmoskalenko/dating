<?php

class StopwordsController extends BackendController
{
	public $layout='main';
	
	public function actionIndex()
	{
		if ($_GET['del']) {
			$sw=Stopwords::model()->findByPk($_GET['del']);
			$sw->delete();
			$this->redirect(array('stopwords/index'));
			Yii::app()->end();
		}
		
		
		$model=new Stopwords;
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['Stopwords'];
			if($model->validate())
			{
				if ($model->save(false))
				{
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					}
					else
						$this->redirect(array('stopwords/index'));
				}
			} elseif (Yii::app()->request->isAjaxRequest) {
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
		
		$criteria=new CDbCriteria;
		$criteria->order='id DESC';
		$items=Stopwords::model()->findAll($criteria);
		$this->render('index', array('model'=>$model, 'items'=>$items));
	}
}