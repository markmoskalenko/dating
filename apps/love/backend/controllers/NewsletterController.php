<?php

class NewsletterController extends BackendController {

    public $layout='main';

    /**
     * Display list.
     */
    public function actionIndex()
    {
        $model = new Newsletter('search');

        if (!empty($_GET['Newsletter']))
            $model->attributes = $_GET['Newsletter'];

        $dataProvider = $model->search();
//        $dataProvider->pagination->pageSize = News::PAGE_SIZE;

        $this->render('index', array(
            'model'=>$model,
            'dataProvider'=>$dataProvider
        ));
    }

    public function actionCreate()
    {
        $oModel = new Newsletter();


        if( isset($_POST['Newsletter'])){

            $oModel->attributes = $_POST['Newsletter'];

            if( $oModel->save() ){
                $this->redirect('index');
            }

        }

//        $form = new CForm('application.modules.news.views.admin.default.newsForm', $oModel);


        $this->render('create', array(
            'model'=>$oModel,
//            'form'=>$form,
        ));
    }

    public function actionUpdate($id)
    {

        $oModel = $this->_loadModel($id);


        if( isset($_POST['Newsletter'])){

            $oModel->attributes = $_POST['Newsletter'];

            if( $oModel->save() ){
                $this->redirect('index');
            }

        }

//        $form = new CForm('application.modules.news.views.admin.default.newsForm', $oModel);


        $this->render('create', array(
            'model'=>$oModel,
//            'form'=>$form,
        ));

    }

    /**
     * Delete page by Pk
     */
    public function actionDelete()
    {
        if (Yii::app()->request->isPostRequest)
        {
            $model = Newsletter::model()->findAllByPk($_REQUEST['id']);

            if (!empty($model))
            {
                foreach($model as $news)
                    $news->delete();
            }

            if (!Yii::app()->request->isAjaxRequest)
                $this->redirect('index');
        }
    }

    protected function _loadModel( $id ){

        $oModel = Newsletter::model()->findByPk((int)$id);

        if (!$oModel)
            throw new CHttpException(404, 'Страница не найдена.');

        return $oModel;

    }
}