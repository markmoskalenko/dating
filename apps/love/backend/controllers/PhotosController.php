<?php

class PhotosController extends BackendController
{
	public $layout='main';
	
	public function accessRules()
	{
		return array(
			array('allow',
			      'roles'=>array('admin'),
			),
			array('allow',
			      'actions'=>array('fastApproval'),
			      'roles'=>array('moderator'),
			),
			array('deny',
			      //'roles'=>array('blocked'),
			),
		);
	}
	
	public function actionIndex()
	{
		if ($_GET['photoId'] && $_GET['_cmd']=='moderateApprovals' && Yii::app()->request->isAjaxRequest)
			$this->moderateApprovals($_GET['photoId']);
			
		if ($_GET['photoId'] && $_GET['_cmd']=='moderateReject'  && Yii::app()->request->isAjaxRequest)
			$this->movePhoto($_GET['photoId']);
			
		if ($_GET['photoId'] && $_GET['_cmd']=='moderateCensor'  && Yii::app()->request->isAjaxRequest)
			$this->movePhoto($_GET['photoId'], 2);
			
		if ($_GET['photoId'] && $_GET['_cmd']=='moderateRemove'  && Yii::app()->request->isAjaxRequest)
			$this->removePhoto($_GET['photoId']);
		
		if (isset($_GET['_cmd'])) {
			echo CJSON::encode(array(
				'success' => true,
			));
			Yii::app()->end();
		}
		
		if (Yii::app()->request->isAjaxRequest)
		{
			$this->layout=false;
		
			$criteria=new CDbCriteria(array(
				'order'=>'t.id DESC',
			));
			

		$criteria->with=array('album', 'user');

		if ($_GET['c']['user_id'])
			$criteria->addCondition('`t`.`user_id`='.intval($_GET['c']['user_id']));
			

		if ($_GET['c']['sys_album']=='1')
			$criteria->addCondition('`t`.`album_id`=0');
			
		if ($_GET['c']['sys_album']=='2')
			$criteria->addCondition('`album`.`system`=2');
			
		if ($_GET['c']['sys_album']=='3')
			$criteria->addCondition('`album`.`system`=1');
			
		if ($_GET['c']['show']=='1')
			$criteria->addCondition('`t`.`approve`=0');
			
		if ($_GET['c']['show']=='2')
			$criteria->addCondition('`t`.`approve`=1');
			
		if (isset($_GET['c']['prior_approval']))
			$criteria->addCondition('`t`.`prior_approval`=1');
			
		if (isset($_GET['c']['bots']))
			$criteria->addCondition('`user`.`isBot`=1');
		elseif (!isset($_GET['c']['user_id']))
			$criteria->addCondition('`user`.`isBot`=0');
			
			
		$count = Photos::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=20;
		$pages->applyLimit($criteria);
			
			$photos=Photos::model()->findAll($criteria);
			foreach ($photos as $key=>$row) {
				
				$p = new stdClass();
				$p->id=$row->id;
				$p->user_id=$row->user_id;
				$p->filename=$row->filename;
				$p->descr=$row->descr;
				$p->width=$row->width;
				$p->height=$row->height;
				$p->likes=$row->likes;
				$p->image=$row->imageSource;
				$p->date=Html::date($row->date);
				$p->commentsUrl=CHtml::normalizeUrl(array('photos/comments', 'id'=>$row->id));
				$p->likeUrl=CHtml::normalizeUrl(array('photos/like', 'id'=>$row->id));
				$photos_all[]=$p;
			}
			
			foreach ($photos as $key=>$row) {
				$nums[$row->id]=array('key'=>$key);
			}
		
			echo CJSON::encode(array(
				'photoId' => Yii::app()->request->getQuery('photoId'),
				'photos' => $photos_all,
				'nums' => $nums,
				'count' => $count,
				'limit' => $pages->pageSize,
			));
			Yii::app()->end();
		}
		
		$criteria=new CDbCriteria;
		$criteria->with=array('album', 'user');

		if ($_GET['c']['user_id'])
			$criteria->addCondition('`t`.`user_id`='.intval($_GET['c']['user_id']));
			

		if ($_GET['c']['sys_album']=='1')
			$criteria->addCondition('`t`.`album_id`=0');
			
		if ($_GET['c']['sys_album']=='2')
			$criteria->addCondition('`album`.`system`=2');
			
		if ($_GET['c']['sys_album']=='3')
			$criteria->addCondition('`album`.`system`=1');
			
		if ($_GET['c']['show']=='1')
			$criteria->addCondition('`t`.`approve`=0');
			
		if ($_GET['c']['show']=='2')
			$criteria->addCondition('`t`.`approve`=1');
		
		if (isset($_GET['c']['prior_approval']))
			$criteria->addCondition('`t`.`prior_approval`=1');
			
		if (isset($_GET['c']['bots']))
			$criteria->addCondition('`user`.`isBot`=1');
		elseif (!isset($_GET['c']['user_id']))
			$criteria->addCondition('`user`.`isBot`=0');
			
		$criteria->order='t.id DESC';
		
		$count = Photos::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=20;
		$pages->applyLimit($criteria);
			
		$photos=Photos::model()->findAll($criteria);
		
		$this->render('index', array('photos'=>$photos, 'pages'=>$pages, 'count'=>$count));
	}
	
	public function actionFastApproval()
	{
		$criteria=new CDbCriteria;
		$criteria->with=array('album', 'user');
		if (isset($_GET['c']['prior_approval']))
			$criteria->addCondition('`t`.`prior_approval`=1');
		$criteria->addCondition('`t`.`approve`=0');
		$criteria->addCondition('`user`.`isBot`=0');
		$criteria->order='t.id DESC';
		$criteria->limit='250';
		$photos=Photos::model()->findAll($criteria);
		
		
		if (Yii::app()->request->isPostRequest) {
			
			foreach($_POST['to_user'] as $item) {
				if ($_POST['FastApproval'][$item]=='approvals')
					$this->moderateApprovals($item);
				elseif ($_POST['FastApproval'][$item]=='reject')
					$this->movePhoto($item);
				elseif ($_POST['FastApproval'][$item]=='censor')
					$this->movePhoto($$item, 2);
				elseif ($_POST['FastApproval'][$item]=='remove')
					$this->removePhoto($item);
			}
			$this->refresh();
			Yii::app()->end();
		}
		
		print_r($_POST['FastApproval']);
		
		$this->render('fastApproval', array('photos'=>$photos, 'pages'=>$pages, 'count'=>$count));
	}
	
	public function actionComments()
	{

		$photo=Photos::model()->with(array('user', 'album'))->findByPk($_GET['id']);
		if($photo===null)
			throw new CHttpException(404,'Photo not found.');
		$this->layout=false;
			echo CJSON::encode(array(
				'comments' => $this->render('_comments', array('photo'=>$photo), true),
				//'myLike'=>Like::model()->exists('userId=:userId AND like_userId=:like_userId AND like_photoId=:like_photoId', array(':userId'=>Yii::app()->user->id, ':like_userId'=>$photo->user_id, ':like_photoId'=>$photo->id)),
				//'likes'=>$photo->likes,
			));
			Yii::app()->end();

	}
	
	public function moderateApprovals($id) {
		
		$photo=Photos::model()->findByPk($id);
		
		if($photo===null)
			return;//throw new CHttpException(404,'Photo not found.');
		
		$photo->saveAttributes(array('approve'=>1));
		
		if ($photo->album->id==0) {
					Notifications::add(Notifications::PHOTO_APPROVED, array(
						'userId'=>$photo->user_id,
						'photoId'=>$photo->id,
						'text'=>'Фотография одобрена',
					));
		}
		
	}
	
	public function movePhoto($id, $t=1) {
		
		$photo=Photos::model()->findByPk($id);
		
		if($photo===null)
			return;//throw new CHttpException(404,'Photo not found.');
		
		if ($t==1) {
			Notifications::add(Notifications::PHOTO_MOVED, array(
				'userId'=>$photo->user_id,
				'photoId'=>$photo->id,
				'text'=>'Фотография перемещена в альбом "Разное"',
			));
			
			$album=Albums::model()->find('user_id=:user_id AND system=1', array(':user_id'=>$photo->user_id));
			if (!$album->id) {
				$album=new Albums;
				$album->user_id=$photo->user_id;
				$album->system=1;
				$album->name=Yii::t('app', 'Miscellanea');
				$album->save();
			}
		} elseif ($t==2) {
			Notifications::add(Notifications::PHOTO_MOVED, array(
				'userId'=>$photo->user_id,
				'photoId'=>$photo->id,
				'text'=>'Фотография перемещена в альбом "18+"',
			));
			
			$album=Albums::model()->find('user_id=:user_id AND system=2', array(':user_id'=>$photo->user_id));
			if (!$album->id) {
				$album=new Albums;
				$album->user_id=$photo->user_id;
				$album->system=2;
				$album->censor=1;
				$album->name=Yii::t('app', '18+');
				$album->save();
			}
		}
		
		// updatet by user times
		//UserCounters::model()->findByPk($photo->user_id)->saveAttributes(array('update_photos'=>time()));
		
		// delete wall photo
		if ($photo->album_id==0){
			foreach (Wall::model()->findAll('userId=:userId AND photoId=:photoId', array(':photoId'=>$photo->id, ':userId'=>$photo->user_id)) as $row) {
				$row->delete();
			}
		}
		$photo->approve=1;
		$photo->album_id=$album->id;
		$photo->save();
			$criteria=new CDbCriteria(array(
				'condition'=>'user_id=:user_id AND album_id=0',
				'params'=>array(':user_id'=>$photo->user_id),
				'order'=>'sort DESC',
			));
			$p=Photos::model()->find($criteria);
			if ($photo->sort>$p->sort) {
				$user=User::model()->findByPk($photo->user_id);
				if ($p->id) {
					$user->photoId=$p->id;
					$user->have_photo=1;
					$user->photo=$p->filename;
				} else {
					$user->photoId=0;
					$user->have_photo=0;
					$user->photo='';
				}
				$user->save();
			}
		
		//$photo->saveAttributes(array('approve'=>1));
	}

	public function removePhoto($id) {

		$photo=Photos::model()->findByPk($id);
		
		if($photo===null)
			return;//throw new CHttpException(404,'Photo not found.');
		
		if (is_file(Yii::app()->getBasePath()."/../../uploads/dating/photos/1/u".$photo->user_id."/".$photo->filename)) unlink(Yii::app()->getBasePath()."/../../uploads/dating/photos/1/u".$photo->user_id."/".$photo->filename);
		CommentsPhotos::model()->deleteAll('photoId=:photoId', array(':photoId'=>$photo->id));
		Like::model()->deleteAll('like_photoId=:photoId', array(':photoId'=>$photo->id));
		Notifications::model()->deleteAll('userId=:userId AND photoId=:photoId', array(':photoId'=>$photo->id, ':userId'=>$photo->user_id));
		if ($photo->album_id==0){
			foreach (Wall::model()->findAll('userId=:userId AND photoId=:photoId', array(':photoId'=>$photo->id, ':userId'=>$photo->user_id)) as $row) {
				$row->delete();
			}
		}
		$photo->delete();
		
		//UserCounters::model()->findByPk($photo->user_id)->saveAttributes(array('photos'=>Photos::model()->userById($photo->user_id)->count(), 'update_photos'=>time()));
		
					Notifications::add(Notifications::PHOTO_REMOVED, array(
						'userId'=>$photo->user_id,
						'text'=>'"Фотография '.$photo->id.'" удалена.',
					));
		
		if ($photo->album_id==0) {
			$criteria=new CDbCriteria(array(
				'condition'=>'user_id=:user_id AND album_id=0',
				'params'=>array(':user_id'=>$photo->user_id),
				'order'=>'sort DESC',
			));
			$p=Photos::model()->find($criteria);
			if ($photo->sort>$p->sort) {
				$user=User::model()->findByPk($photo->user_id);
				if ($p->id) {
					$user->photoId=$p->id;
					$user->have_photo=1;
					$user->photo=$p->filename;
				} else {
					$user->photoId=0;
					$user->have_photo=0;
					$user->photo='';
				}
				$user->save();
			}
		}
	}
}