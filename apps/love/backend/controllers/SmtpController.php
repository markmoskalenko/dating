<?php

class SmtpController extends BackendController
{
    public $layout='main';

    public function actionIndex()
    {
//        $message = new YiiMailMessage;
//        $message->setBody('Message content here with HTML', 'text/html');
//        $message->subject = 'My Subject';
//        $message->addTo('papvann@gmail.com');
//        $message->from = 'admin@mail.ru';
//
//        var_dump($message);
//        var_dump("now will send email");
//        Yii::app()->mail->send($message);
//        exit;


        $model = Smtp::model()->find();

        if(Yii::app()->request->isPostRequest)
        {
            $model->attributes=$_POST['Smtp'];
            if($model->validate())
            {
                if ($model->save(false))
                {
                    if (Yii::app()->request->isAjaxRequest) {
                        echo CJSON::encode(array('ok'=>'1'));
                        return;
                    }
                    else
                        $this->redirect(array('smtp/index'));
                }
            } elseif (Yii::app()->request->isAjaxRequest) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }

        $this->render('index', array('model'=>$model));
    }
}