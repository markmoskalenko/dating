<?php

class SiteController extends BackendController
{
	public $layout='main';
	
	public function accessRules()
	{
		return array(
			array('allow',
			      'actions'=>array('auth', 'error'),
			      //'users'=>array('?'),
			),
			array('allow',
			      'roles'=>array('admin'),
			),
			array('allow',
			      'actions'=>array('index', 'logout'),
			      'roles'=>array('moderator'),
			),
			array('deny',
			      //'roles'=>array('blocked'),
			),
		);
	}
	
	public function actionIndex()
	{
		$count_user=User::model()->count();
		$count_photo=Photos::model()->count();
		
		$criteria=new CDbCriteria;
		$criteria->condition='role="admin" OR role="moderator"';
		$admins=User::model()->findAll($criteria);
		
		$this->render('index', array('admins' => $admins, 'count_user'=>$count_user, 'count_photo'=>$count_photo));
	}
	
	public function actionAuth()
	{
		$this->layout=FALSE;
		if (!Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->returnUrl);
			
		$model=new LoginForm;
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			
			if($model->validate() && $model->login())
			{
				$this->redirect(array('site/index'));
			}
		}
		$this->render('auth', array('model'=>$model));
	}
	
	public function actionGeo()
	{
		$data=array();
		if ($_GET['country_id']) {
			foreach (GEORegion::model()->findAll(array('condition'=>'id_country=:id_country', 'params'=>array(':id_country'=>$_GET['country_id']))) as $row) {
				$data[]=array('id_region'=>$row->id_region, 'name'=>$row->name);
			}
			echo CJSON::encode($data);
		} elseif ($_GET['region_id']) {
			foreach (GEOCity::model()->findAll(array('condition'=>'id_region=:id_region', 'params'=>array(':id_region'=>$_GET['region_id']))) as $row) {
				$data[]=array('id_city'=>$row->id_city, 'name'=>$row->name);
			}
			echo CJSON::encode($data);
		}	
	}
	
	public function actionError()
	{
		if (Yii::app()->user->role!='admin')
		$this->layout=FALSE;
		
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}