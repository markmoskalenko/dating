<?php

class AdminAccessControlFilter extends CAccessControlFilter
{
	protected function accessDenied($user,$message)
	{
		if (Yii::app()->user->isGuest)
		{
			Yii::app()->runController(Yii::app()->controller->module->id.'/site/auth');
			Yii::app()->end();
		}
		else
			throw new CHttpException(403,$message);
	}
}