<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class BackendController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='col_main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public $pageTitle;
	
	private $_countPhotoRequiring;
	
	public function filters()
	{
		return array(
			//'accessControl',
			array(
				'AdminAccessControlFilter',
				'rules'=>$this->accessRules(),
			)
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
			      'roles'=>array('admin'),
			),
			array('deny',
			      //'roles'=>array('blocked'),
			),
		);
	}
	
	public function init()
	{
		if (Yii::app()->request->isAjaxRequest)
			$this->layout = FALSE;
	}
	
	public function beginContent($view=null,$data=array())
	{
		if (Yii::app()->request->isAjaxRequest)
			return;
		
		parent::beginContent($view,$data);
	}
	
	public function endContent()
	{
		if (Yii::app()->request->isAjaxRequest)
			return;
		
		parent::endContent();
	}
	
	public function getCountPhotoRequiring()
	{
		if($this->_countPhotoRequiring === null)
		{
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`approve`=0 AND `user`.`isBot`=0',
			));
			$this->_countPhotoRequiring = Photos::model()->with('user')->count($criteria);
		}
		return $this->_countPhotoRequiring;
	}
	
	/*protected function afterAction($action)
	{
		parent::afterAction($action);
		
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getQuery('rel'))
		{
			echo '<title>'.$this->pageTitle.'</title>';
		}
	}*/
}