<?php

class AccessControlFilter extends CAccessControlFilter
{
	protected function accessDenied($user,$message)
	{
		if (Yii::app()->user->isGuest)
		{
			Yii::app()->runController('/user/register');
			Yii::app()->end();
		}
		elseif (Yii::app()->user->checkAccess('activation'))
		{
			Yii::app()->runController('/activation/index'); 
			Yii::app()->end();
		}
		else
			throw new CHttpException(403,$message);
	}
}