<?php

class AppsController extends Controller
{
	public $layout='col_main';
	public $secretKey='54A0F08408663B1683384A8E59BFE78194BC41A7';
	public $site_id='6408';

	public function accessRules()
	{
		return array(
			array('deny',
			      'users'=>array('?'),
			),
			array('deny',
			      'actions'=>array('play'),
			      'roles'=>array('userUnreal'),
			)
		);
	}
	
	public function actionIndex()
	{
		$apps = CJSON::decode(file_get_contents("http://api2.nextgame.ru/api/?method=apps.getInfo&format=json"));

		$this->render('index', array('apps'=>$apps['data']));
	}
	
	public function actionPlay()
	{
		$sig=$this->generateSignature(array(
			'linktype'=>'1',
			'site_id'=>$this->site_id,
			'app_id'=>$_GET['id'],
			'user_id'=>Yii::app()->user->id,
			'usr_nickname'=>Yii::app()->user->model->name,
		), $this->secretKey);
		

		$this->render('play', array('sig'=>$sig));
	}
	
	public function generateSignature(array $params, $secretKey) {
    // ��������� ���������� �� �����
    ksort($params, SORT_STRING);
 
    // ���������� ���� ����=�������� � ������
    $paramsStr = '';
 
    foreach ($params as $key => $value) {
        $paramsStr .= $key.'='.$value;
    }
 
    // ��������� ��������� ���� � ��������� MD5-���
    $signature = md5($paramsStr.$secretKey);
 
    return $signature;
	}
}