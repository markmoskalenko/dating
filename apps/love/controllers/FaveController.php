<?php

class FaveController extends Controller
{
	public function accessRules()
	{
		return array(
			array('deny',
			      //'actions'=>array('index'),
			      'users'=>array('?'),
			),
		);
	}
	
	public function actionIndex()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$this->layout=false;
			$criteria=new CDbCriteria;
		
			$criteria=new CDbCriteria(array(
				'condition'=>'`fave`.`userId`=:userId',
				'params'=>array(':userId'=>Yii::app()->user->id),
				'order'=>'fave.time DESC',
				'with'=>'fave',
			));
			
			$count = Photos::model()->count($criteria);
			$pages = new CPagination($count);
			$pages->pageSize=Yii::app()->params['favePhotos_limit'];
			$pages->applyLimit($criteria);
			
			$photos=Photos::model()->findAll($criteria);
			foreach ($photos as $key=>$row) {
				
				$p = new stdClass();
				$p->id=$row->id;
				$p->user_id=$row->user_id;
				$p->filename=$row->filename;
				$p->descr=$row->descr;
				$p->width=$row->width;
				$p->height=$row->height;
				$p->likes=$row->likes;
				$p->date=Html::date($row->date);
				$p->image=$row->imageSource;
				$p->commentsUrl=CHtml::normalizeUrl(array('photos/comments', 'id'=>$row->id));
				$p->likeUrl=CHtml::normalizeUrl(array('photos/like', 'id'=>$row->id));
				$photos_all[]=$p;
			}
			
			foreach ($photos as $key=>$row) {
				$nums[$row->id]=array('key'=>$key);
			}
		
			echo CJSON::encode(array(
				'photoId' => Yii::app()->request->getQuery('photoId'),
				'photos' => $photos_all,
				'nums' => $nums,
				'count' => $count,
				'limit' => $pages->pageSize,
			));
			Yii::app()->end();
		}
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`fave`.`userId`=:userId',
			'params'=>array(':userId'=>Yii::app()->user->id),
			'order'=>'fave.time DESC',
			'with'=>'fave',
		));
		
		$count = Like::model()->count('`t`.`userId`=:userId', array(':userId'=>Yii::app()->user->id));
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['favePhotos_limit'];
		$pages->applyLimit($criteria);
			//echo $pages->PageCount;
		$photos=Photos::model()->with('user')->findAll($criteria);
		
		if (Yii::app()->request->isAjaxRequest) {
			$length=$pages->pageSize*$count;
			$html=$this->renderPartial('_photos', array('photos'=>$photos), true);
			echo CJSON::encode(array('html'=>$html, 'pageCount'=>$pages->pageCount));
			
		} else {
			$this->render('index',array('photos'=>$photos, 'pages'=>$pages));
		}
		
	}
	
	public function actionUsers()
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'fave.userId=:userId',
			'params'=>array(':userId'=>Yii::app()->user->id),
		));
		$count = UserLikes::model()->count('t.userId=:userId', array(':userId'=>Yii::app()->user->id));
		
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['faveUsers_limit'];
		$pages->applyLimit($criteria);
		
		$users=User::model()->with('city')->with('fave')->findAll($criteria);
		
		$this->render('users',array('users'=>$users, 'pages'=>$pages));
	}
}