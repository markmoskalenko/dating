<?php

class ApiController extends Controller
{
	public $layout = FALSE;
	
	public function actionBusinesskassa()
	{
		$secret='c814dd85265b778fe3fc122c1b2e3202';
		Yii::log('Businesskassa');
		Yii::log(print_r($_GET, true));
		Yii::log('POST: '.print_r($_POST, true));
///////////////////////////////////////////////////////////////////////////////////////////////////
// Получение ответа
// Получаем пакет
if ($_GET[xml_encoded] && $_GET[lqsignature]) {
	// Проверяем на наличие пробелов и заменяем их на знак "+"
    $_GET[xml_encoded] = ereg_replace(" ","+",$_GET[xml_encoded]);
    $_GET[lqsignature] = ereg_replace(" ","+",$_GET[lqsignature]);
    
    // Раскодируем первый пакет и получим данные
    $xml = base64_decode($_GET[xml_encoded]);
     Yii::log($xml);
    $reedxml = new SimpleXMLElement($xml);
    $shop = (int)$reedxml->shop;
    $pass = $reedxml->pass;
    // Уникальный номер платежа в BK
    $order = $reedxml->order;
    // Уникальный номер платежа в Вашем магазине
    $goods = $reedxml->goods;
	// Полученная магазином сумма
    $sum = (float)$reedxml->sum;
    // Оплаченная пользователем сумма
    $amount = (float)$reedxml->amount;
    // Валюта
    $currency = $reedxml->currency;
    // Статус заказа (2 - отменен ЭПС, 3 - отменен BK, 4 - Оплачен, 0 - Заблокирован)
    $status = (int)$reedxml->status;
    Yii::log(print_r($reedxml, true));
    // Проверяем на валидность полученный пакет
    if (is_numeric($shop)
        and is_numeric($sum)
        and ($currency == 'RUR' or $currency == 'USD' or $currency == 'EUR' or $currency == 'UAH')
        and preg_match ('#^[aA-zZ0-9]+$#', $order)
        and !preg_match('/`/', $order)
        and preg_match ('#^[aA-zZ0-9]+$#', $pass)
        and !preg_match('/`/', $pass)
        and is_numeric($status)) {
        
        // Проверяем на соответствие подписей $sign - Ваш секретный ключ
        $sign = $secret;					
        $sign = base64_encode(sha1($sign.$_GET[xml_encoded].$sign,1)); 
        if ($sign == $_GET[lqsignature]) {
		Yii::log($goods);
        // Далее вы должны найти счет в Вашей системе ($goods)
	$kass=Kassa::model()->findByPk($goods);
	$user=User::model()->findByPk($kass->user_id);
	if ($user->id) {
		Yii::log('Businesskassa user_id '.$user->id);
		$kass->saveAttributes(array('pay_status'=>1));
		$user->saveAttributes(array('balance'=>new CDbExpression('`balance`+'.$kass->coins)));
		$t=new Transaction();
		$t->coins=$kass->coins;
		$t->user_id=$kass->user_id;
		if ($user->isReal) {
			$t->comment_payment='Пополнение баланса с помощью BusinessKassa.';
		} else {
			$t->comment_payment='Активация Real с помощью BusinessKassa.';
			$user->saveAttributes(array('isReal'=>1));
		}
		$t->save();
		echo 'good';
	}
        
        } else echo 'Подписи не соответствуют';
    } else echo 'Ответ не прошел валидацию';
} else echo 'Получен пустой пакет';

	}
	
	public function actionFreekassa()
	{
		@header("HTTP/1.0 200 OK");
		$merchant_id=2581;
		$secret_word='34545656@#$$%%##@$';
		Yii::log('EBALA: '.md5($merchant_id.":".$_GET['AMOUNT'].":".$secret_word.":".$_GET['MERCHANT_ORDER_ID']).' = SIGN:'.$_GET['SIGN'].'&AMOUNT:'.$_GET['AMOUNT'].'&MERCHANT_ORDER_ID:'.$_GET['MERCHANT_ORDER_ID'].'&');

		if ($_GET['SIGN'] != md5($merchant_id.":".$_GET['AMOUNT'].":".$secret_word.":".$_GET['MERCHANT_ORDER_ID'])) {
			echo 'hesh ne verniy';
			return;
			//throw new CHttpException(402,'hacking attempt.');
		}
		
		$kass=Kassa::model()->findByPk($_GET['MERCHANT_ORDER_ID']);
		$user=User::model()->findByPk($kass->user_id);
		
		if (!$kassa->pay_status) {
			
			$user->saveAttributes(array('balance'=>new CDbExpression('`balance`+'.$kass->coins)));
			$kass->saveAttributes(array('pay_status'=>1));
			
			$t=new Transaction();
			$t->coins=$kass->coins;
			$t->user_id=$kass->user_id;
			if ($user->isReal) {
				$t->comment_payment='Пополнение баланса с помощью BusinessKassa.';
			} else {
				$t->comment_payment='Активация Real с помощью BusinessKassa.';
				$user->saveAttributes(array('isReal'=>1));
			}
			$t->save();
			echo "Yes";
		}
	}
	
	public function actionRechargeUser()
	{
		if ($_GET['hash']!=md5($_GET['user_id'].$_GET['coins'].'dfshts345trwes')) {
			throw new CHttpException(402,'check error');
		}
		
		$user=User::model()->findByPk($_GET['user_id']);
		$user->saveAttributes(array('balance'=>new CDbExpression('`balance`+'.$_GET['coins'])));
		$t=new Transaction();
		$t->coins=$_GET['coins'];
		$t->user_id=$user->id;
		$t->comment_payment=$_GET['comment_payment'];

		if ($_GET['comment_payment']) $t->save();
		
		echo CJSON::encode(array('status'=>'ok'));
	}
}