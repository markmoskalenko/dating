<?php

//namespace aplication\controllers\AlbumsController;
//use aplication\components\Controller;
class AlbumsController extends Controller
{
	public $layout='col_main';

	public function actionIndex()
	{
		if (Yii::app()->request->isAjaxRequest) {
			echo 1;
		 return;	
		}
		if (Yii::app()->request->getQuery('userId') && Yii::app()->request->getQuery('albumId')) {
			$this->actionView();
		} else {
		
		
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`user_id`=:user_id',
				'params'=>array(':user_id'=>Yii::app()->request->getQuery('userId')),
				'order'=>'id DESC',
				//'limit'=>$limit,
				//'with'=>array('user'=>array('select'=>'login, sex')),
			));

		$albums=Albums::model()->findAll($criteria);
		
		
		
			$this->render('index', array('albums'=>$albums));
		}
		
	}
	
	public function actionView()
	{
		if (Yii::app()->request->isAjaxRequest) {
			echo 1;
		 return;	
		}
		
		$album=Albums::model()->with('user')->findByPk(Yii::app()->request->getQuery('albumId'));
		
		$this->render('view', array('album'=>$album));
		
	}
	
	public function actionCreate()
	{
		$model=new Albums;
		$model->setScenario('create');
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['Albums'];
			if($model->validate())
			{
				if ($model->save(false))
				{
					$this->redirect(array('albums/index', 'userId'=>Yii::app()->user->id, 'albumId'=>$model->id));
					//Yii::app()->user->setFlash('edit','��������� ���������.');
					//$this->refresh();
				}
			}
		}
		$this->render('create', array('model'=>$model));
		
	}
}