<?php

class PhotosController extends Controller
{
	public $layout='col_main';
	
	private $_user;
	private $_albums;
	private $_album;
	
	public function accessRules()
	{
		return array(
			array('deny',
			      'actions'=>array('upload', 'createAlbum', 'editAlbum', 'editPhoto', 'addComment'),
			      'users'=>array('?'),
			),
		);
	}
	
	public function actions()
	{
		return array(
			'startUpload'=>array(
				'class'=>'xupload.actions.XUploadAction',
				'path' =>Yii::app() -> getBasePath() . "/../uploads",
				'publicPath' => Yii::app() -> getBaseUrl() . "/uploads",
			),
		);
	}

	public function actionIndex()
	{
		if (Yii::app()->request->getQuery('delPhoto')) {
			$this->deletePhoto(Yii::app()->request->getQuery('delPhoto'));
			return;
		}
		
		if (Yii::app()->request->getQuery('setAvatar')) {
			$this->setAvatar(Yii::app()->request->getQuery('setAvatar'));
			return;
		}
		
		if (Yii::app()->request->getQuery('delCommId')) {
			$this->deleteComment(Yii::app()->request->getQuery('delCommId'));
			return;
		}
		
		//$update_photos=Yii::app()->user->getCounter($_GET['userId'])->update_photos;
		
		if (Yii::app()->request->isAjaxRequest)
		{
			$this->layout=false;
			$criteria=new CDbCriteria;
		
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`user_id`=:user_id',
				'params'=>array(':user_id'=>$_GET['userId']),
				'order'=>'t.sort DESC, t.id DESC',
			));
			
			if (isset($_GET['albumId']))
				$criteria->addCondition('`t`.`album_id`='.intval($_GET['albumId']));
			
			$count = Photos::model()->count($criteria);
			
			
			$photos=Photos::model()->findAll($criteria);
			foreach ($photos as $key=>$row) {
				
				$p = new stdClass();
				$p->id=$row->id;
				$p->user_id=$row->user_id;
				$p->filename=$row->filename;
				$p->descr=$row->descr;
				$p->width=$row->width;
				$p->height=$row->height;
				$p->likes=$row->likes;
				$p->image=$row->imageSource;
				$p->date=Html::date($row->date);
				$p->commentsUrl=CHtml::normalizeUrl(array('photos/comments', 'id'=>$row->id));
				$p->likeUrl=CHtml::normalizeUrl(array('photos/like', 'id'=>$row->id));
				$photos_all[]=$p;
			}
			
			foreach ($photos as $key=>$row) {
				$nums[$row->id]=array('key'=>$key);
			}
		
			echo CJSON::encode(array(
				'photoId' => Yii::app()->request->getQuery('photoId'),
				'photos' => $photos_all,
				'nums' => $nums,
				'count' => $count,
				'limit' => $pages->pageSize,
			));
			Yii::app()->end();
		}
		/*if (Yii::app()->request->isAjaxRequest)
		{
			$criteria=new CDbCriteria;
			
			if ($_GET['fave']) {
				$criteria->condition='`fave`.`userId`=:userId';
				$criteria->params=array(':userId'=>Yii::app()->user->id);
				$criteria->with='fave';
			}
			
			if ($_GET['photoId']) {
				$photo=Photos::model()->with('user')->findByPk($_GET['photoId'], $criteria);
			} else {
				$user=User::model()->findByPk($_GET['userId']);
			
				$photo=Photos::model()->find('filename=:filename', array(':filename'=>$user->photo));
			}
			
			if($photo===null)
				throw new CHttpException(404,'Page was deleted or does not exist yet.');
				
			if ($_GET['fave']) {
				$criteria=new CDbCriteria(array(
					'condition'=>'`fave`.`userId`=:userId',
					'params'=>array(':userId'=>Yii::app()->user->id),
					'order'=>'fave.time DESC',
					'limit'=>1,
					'with'=>'fave',
				));
			} else {
				$criteria=new CDbCriteria(array(
					'condition'=>'`t`.`user_id`=:user_id',
					'params'=>array(':user_id'=>$_GET['userId']),
					'order'=>'sort DESC',
					'limit'=>1,
					//'with'=>array('user'=>array('select'=>'login, sex')),
				));
			}
			
			if (isset($_GET['albumId']))
				$criteria->addCondition('`t`.`album_id`='.intval($_GET['albumId']));
			
			$count1=Photos::model()->count($criteria);
			
			if ($_GET['fave']) {
				$criteria->addCondition('`fave`.`time`<'.$photo->fave->time);
			} else {
				$criteria->addCondition('`t`.`sort`<'.$photo->sort);
			}
			
			$photoNext=Photos::model()->find($criteria);
			$count=Photos::model()->count($criteria);
			
			echo CJSON::encode(array(
				'id' => $photo->id,
				'width' => $photo->width,
				'height' => $photo->height,
				'image' => '/uploads/photos/u'.$photo->user_id.'/'.$photo->filename,
				'url' => $photoNext->id ? $photoNext->normalizeUrl : '#close',
				'date' => Html::date($photo->date),
				'descr' => $photo->descr,
				'title' => Yii::t('app', 'Photo :photoNumber of :photoTotal', array(':photoNumber'=>$count1-$count, ':photoTotal'=>$count1)),
				'comments'=>$this->render('_comments', array('photo'=>$photo, 'commentsForm'=>new CommentsPhotos), true),
				'likes'=>$photo->likes,
				'likeUrl'=>CHtml::normalizeUrl(array('photos/like', 'id'=>$photo->id)),
				'myLike'=>Like::model()->exists('userId=:userId AND like_userId=:like_userId AND like_photoId=:like_photoId', array(':userId'=>Yii::app()->user->id, ':like_userId'=>$photo->user_id, ':like_photoId'=>$photo->id)),
			));
			
			return;	
		}*/
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`user_id`=:user_id',
			'params'=>array(':user_id'=>Yii::app()->request->getQuery('userId')),
			'order'=>'sort DESC, id DESC',
		));
		
		if (isset($_GET['albumId']))
			$criteria->addCondition('`t`.`album_id`='.intval($_GET['albumId']));
		
		$count = Photos::model()->cache(120, new DbCacheDependency('update_photos', $_GET['userId']))->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['photos_limit'];
		$pages->applyLimit($criteria);
			//echo Yii::app()->user->counter->update_photos;
		$photos=Photos::model()->cache(120, new DbCacheDependency('update_photos', $_GET['userId']))->findAll($criteria);
		
		
		
		if (isset($_GET['albumId'])) {
			
			$this->breadcrumbs=array(
				$this->user->name=>array('profile/index', 'user'=>$this->user->login),
				Yii::t('app', 'All photos')=>array('photos/index', 'userId'=>$this->user->id),
				$this->album->name ? $this->album->name : Yii::t('app', 'Photos from'),
			);
		} else {
			$this->breadcrumbs=array(
				$this->user->name=>array('profile/index', 'user'=>$this->user->login),
				Yii::t('app', 'All photos')
			);
		}
		

		
		$this->render('index', array('photos'=>$photos, 'pages'=>$pages, 'albums'=>$albums));
		
	}
	
	public function actionAddComment()
	{
		$photo=Photos::model()->findByPk($_GET['photoId']);
		
		if($photo===null)
			throw new CHttpException(404,'Page not found.');
		
		$commentsPhotos=new CommentsPhotos;
		//$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest && $_POST['CommentsPhotos'])
		{
			$commentsPhotos->attributes=$_POST['CommentsPhotos'];
			if($commentsPhotos->validate())
			{
				$commentsPhotos->photoId=$_GET['photoId'];
						//echo CJSON::encode(array('captcha'=>CHtml::normalizeUrl(array('site/spamProtection'))));
						//return;
				if ($commentsPhotos->save(false))
				{
					Notifications::add(Notifications::COMMENTED, array(
						'userId'=>$photo->user_id,
						'photoId'=>$photo->id,
						'photoCommentId'=>$commentsPhotos->id
					));
					
					if (Yii::app()->request->isAjaxRequest)
					{
						echo CJSON::encode(array('html'=>$this->getComments($_GET['photoId'])));
						return;
					}
					//$this->redirect(array('profile/index', 'userId'=>$this->user->id));
				}
			}
			elseif (Yii::app()->request->isAjaxRequest)
			{
				echo CActiveForm::validate($commentsPhotos);
				Yii::app()->end();
			}
		}
		
	}
	
	public function actionComments()
	{
		if ($_GET['content']=='all') {
		$photo=Photos::model()->with(array('user', 'album'))->findByPk($_GET['id']);
		if($photo===null)
			throw new CHttpException(404,'Page not found.');
		
		$this->layout=false;
			echo CJSON::encode(array(
				'comments' => $this->render('_comments', array('photo'=>$photo, 'commentsForm'=>new CommentsPhotos), true),
				'myLike'=>Like::model()->exists('userId=:userId AND like_userId=:like_userId AND like_photoId=:like_photoId', array(':userId'=>Yii::app()->user->id, ':like_userId'=>$photo->user_id, ':like_photoId'=>$photo->id)),
				'likes'=>$photo->likes,
			));
			Yii::app()->end();
		} else {
			echo $this->getComments();
			
		}
	}
	
	public function getComments($photoId=0)
	{
		$photoId=$photoId ? $photoId : $_GET['id'];
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`photoId`=:photoId',
			'params'=>array(':photoId'=>$photoId),
			'with'=>array('user'=>array()),
			'order'=>'t.id DESC',
		));
		$count = CommentsPhotos::model()->count($criteria);
			
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['commentsPhotos_limit'];
		//$pages->pageVar='wall';
		$pages->route='photos/comments';
		$pages->params=array('id'=>$photoId);
		$pages->applyLimit($criteria);
			
		$comments=CommentsPhotos::model()->findAll($criteria);
		
		return $this->render('_commentsRow', array('comments'=>$comments, 'pages'=>$pages), true);
	}
	
	public function deleteComment($id)
	{
		$comments=CommentsPhotos::model()->findByPk($id);
		$photos=Photos::model()->findByPk($comments->photoId);
		if (Yii::app()->user->checkAccess('deleteComment', array('author_id'=>$comments->userId, 'own_id'=>$photos->user_id)))
		{
			Notifications::model()->deleteAll('photoCommentId=:photoCommentId AND userId=:userId AND from_userId=:from_userId AND type=:type', array(':photoCommentId'=>$comments->id,':userId'=>$photos->user_id, ':from_userId'=>$comments->userId, ':type'=>Notifications::COMMENTED));
			
			//User::model()->updateCounters(array('notifications'=>-1),'id=:id',array(':id'=>$photos->user_id));
			$comments->delete();
			echo CJSON::encode(array('id'=>$id));
		}
		else
			throw new CHttpException(402,'checkAccess');
	}
	
	public function getIsOwn()
	{
		return Yii::app()->user->id==$this->user->id;
	}
	
	public function getUser()
	{
		if ($this->_user === null)
		{
			$this->_user=User::model()->findByPk(Yii::app()->request->getQuery('userId'));
		}
		return $this->_user;
	}
	
	public function getAlbum()
	{
		if ($this->_album === null)
		{
			if ($_GET['albumId'])
				$this->_album=Albums::model()->userById($_GET['userId'])->findByPk($_GET['albumId']);
		}
		return $this->_album;
	}
	
	public function getAlbums()
	{
		if ($this->_albums === null)
		{
			
/*$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`user_id`=:user_id',
				'params'=>array(':user_id'=>Yii::app()->request->getQuery('userId')),
				//'order'=>'t.id ASC',
				'limit'=>5,
				'group'=>'t.album_id, t.user_id',
				'with'=>array('album'=>array('order'=>'album.id DESC')),
			));
			
			$this->_albums=Photos::model()->findAll($criteria);*/
			
			
			
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`user_id`=:user_id',
				'params'=>array(':user_id'=>Yii::app()->request->getQuery('userId')),
				'order'=>'`t`.`system` DESC, `t`.`updated` DESC',
				'limit'=>Yii::app()->params['albums_limit'],
				//'with'=>array('photo'),
			));
			

			
			$this->_albums=Albums::model()->findAll($criteria);
		}
		return $this->_albums;
	}
	
	public function actionEditAlbum()
	{
		$model=Albums::model()->user()->findByPk($_GET['id']);
		
		if($model===null)
			throw new CHttpException(404,'Album not found.');
		
		if($model->system)
			throw new CHttpException(402,'This album is not editable.');
		
			$this->breadcrumbs=array(
				Yii::t('app', 'My photos')=>array('photos/index', 'userId'=>$model->user_id),
				$model->name=>array('photos/index', 'userId'=>$model->user_id, 'albumId'=>$model->id),
				Yii::t('app', 'Edit album'),
			);
			
		$model->setScenario('edit');
		$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['Albums'];
			if($model->validate())
			{
				if ($model->save(false))
				{
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','save.');
						$this->refresh();
					}
				}
			}
		}
		$this->render('editAlbum', array('model'=>$model));
		
	}
	
	public function actionCreateAlbum()
	{
			$this->breadcrumbs=array(
				Yii::t('app', 'My photos')=>array('photos/index', 'userId'=>Yii::app()->user->id),
				Yii::t('app', 'Create album'),
			);
		
		$model=new Albums;
		$model->setScenario('create');
		$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['Albums'];
			if($model->validate())
			{
				$model->user_id = Yii::app()->user->id;
				if ($model->save(false))
				{
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1', 'redirect'=>CHtml::normalizeUrl(array('photos/upload', 'albumId'=>$model->id))));
						return;
					} else {
						Yii::app()->user->setFlash('edit','save.');
						$this->refresh();
					}
				}
			}
		}
		$this->render('createAlbum', array('model'=>$model));
		
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='registerForm')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionUpload()
	{		
			
		if ($_GET['albumId']) {
			$album=Albums::model()->userById(Yii::app()->user->id)->findByPk($_GET['albumId']);
			if (!$album->id)
				throw new CHttpException(404,'Album does not exist');
		}
		if(Yii::app()->request->isPostRequest)
		{
			$model = new UploadForm;
			$model->attributes=$_POST['UploadForm'];
			$model->image = EUploadedImage::getInstance($model,'file');
			$model->image->maxWidth = 1600;
			$model->image->maxHeight = 1600;
			
			$model->mime_type = $model->image->getType();
			$model->size = $model->image->getSize();
			$model->name = $model->image->getName();
			
			$path = Yii::app()->getBasePath()."/../../uploads/dating/photos/1/u".Yii::app()->user->id."/";
			if(!is_dir($path)){
				//mkdir($path);
				mkdir($path, 0777);
			}
			$filename=substr(md5(rand()), 0, 9).'.jpg';
			
			if ($model->validate()) {
				if ($model->image->saveAs($path.$filename)) {
					//Yii::app()->faceDetector->detect($path.$filename);
					//copy($path.$filename, Yii::app()->faceDetector->toJpeg());
					//$photoMinSort=Photos::model()->orderBySort()->find('', array(''=>''));
					//$photoMaxSort=Photos::model()->findBySql('SELECT MAX(id) as wer FROM photos WHERE user_id = '.Yii::app()->user->id.' ORDER BY sort DESC');
					//$max = (int) Yii::app()->db->createCommand("SELECT MAX(sort) FROM photos WHERE user_id = ".Yii::app()->user->id."")->queryScalar();
					$photos=new Photos();
					$photos->filename=$filename;
					$photos->width=$model->image->width;
					$photos->height=$model->image->height;
					$photos->sort=time();
					$photos->album_id=$_GET['albumId'];
					$photos->save();
				
					if (!$_GET['albumId']) {
						$user=User::model()->findByPk(Yii::app()->user->id);
						$user->photoId=$photos->id;
						$user->have_photo=1;
						$user->photo=$filename;
						$user->save();
						
						$wall=new Wall;
						$wall->userId=Yii::app()->user->id;
						$wall->photoId=$photos->id;
						$wall->save();
					}
					
					//saveAttributes(array('photos'=>Photos::model()->user()->count(), 'update_photos'=>time()));
					
					echo json_encode(array(
						'html' => $this->getEditPhotosRows($photos->id),
					));
				}
				else
					throw new CHttpException(500,'error upload.');
			} else {
				//echo CVarDumper::dumpAsString($model->getErrors());
				Yii::log("FileUpload: ".CVarDumper::dumpAsString($model->getErrors()), CLogger::LEVEL_ERROR, "application.controllers.EditController");
				throw new CHttpException(402, "Could not upload file");
			}
			return;
		}
		
		$this->breadcrumbs=array(
			Yii::t('app', 'My photos')=>array('photos/index', 'userId'=>Yii::app()->user->id),
			$this->album->name ? $this->album->name : Yii::t('app', 'Photos from')=>array('photos/index', 'userId'=>Yii::app()->user->id, 'albumId'=>Yii::app()->request->getQuery('albumId', 0)),
			Yii::t('app', 'Manager upload photos'),
		);
			
		$this->render('upload', array('model' => $model));
	}
	
	public function getEditPhotosRows($id)
	{
		//$this->layout=false;
		$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`user_id`=:user_id AND `t`.`id`=:id',
				'params'=>array(':user_id'=>Yii::app()->user->id, ':id'=>$id),
			//'condition'=>'`t`.`id`=:id',
			//'params'=>array(':id'=>$id),
			'limit'=>'5',
			'order'=>'id DESC',
		));
		

		$photos=Photos::model()->findAll($criteria);
		
		return $this->render('_editPhotosRows', array('photos'=>$photos), true);
	}
	
	public function actionAllAlbums()
	{
		$this->breadcrumbs=array(
			$this->user->name=>array('profile/index', 'user'=>$this->user->login),
			Yii::t('app', 'Albums'),
		);
		
		$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`user_id`=:user_id',
				'params'=>array(':user_id'=>Yii::app()->request->getQuery('userId')),
				'order'=>'`t`.`system` DESC, `t`.`updated` DESC',
				//'with'=>array('photo'),
		));
		
		$count = Albums::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['albumsAll_limit'];
		$pages->applyLimit($criteria);
		
		$albums=Albums::model()->findAll($criteria);
		
		$this->render('all_albums', array('albums'=>$albums, 'pages'=>$pages, 'count'=>$count));
	}
	
	public function actionEditPhoto()
	{
		$criteria=new CDbCriteria;
		//$criteria->condition='album_id!=0';
		$model=Photos::model()->user()->findByPk($_GET['id'], $criteria);
		
		if($model===null)
			throw new CHttpException(404,'Page not found.');
			
		$model->setScenario('edit');
		$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['Photos'];
			if($model->validate())
			{
				if ($model->save(false))
				{
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','save.');
						$this->refresh();
					}
				}
			}
		}
		
		$this->render('editPhoto', array('model'=>$model));
	}
	
	public function deletePhoto($id)
	{
		$photo=Photos::model()->user()->findByPk($id);
		if (!$photo->id)
			return;
		
		unlink(Yii::app()->getBasePath()."/../../uploads/dating/photos/1/u".$photo->user_id."/".$photo->filename);
		CommentsPhotos::model()->deleteAll('photoId=:photoId', array(':photoId'=>$photo->id));
		Like::model()->deleteAll('like_photoId=:photoId', array(':photoId'=>$photo->id));
		Notifications::model()->deleteAll('userId=:userId AND photoId=:photoId', array(':photoId'=>$photo->id, ':userId'=>$photo->user_id));
		if ($photo->album_id==0){
			foreach (Wall::model()->findAll('userId=:userId AND photoId=:photoId', array(':photoId'=>$photo->id, ':userId'=>$photo->user_id)) as $row) {
				$row->delete();
			}
		}
		
		$photo->delete();
		
		//Yii::app()->user->counter->saveAttributes(array('photos'=>Photos::model()->user()->count(), 'update_photos'=>time()));
		
		if ($photo->album_id==0) {
			$criteria=new CDbCriteria(array(
				'condition'=>'album_id=0',
				'order'=>'sort DESC',
			));
			$p=Photos::model()->user()->find($criteria);
			if ($photo->sort>$p->sort) {
				$user=User::model()->findByPk(Yii::app()->user->id);
				if ($p->id) {
					$user->photoId=$p->id;
					$user->have_photo=1;
					$user->photo=$p->filename;
				} else {
					$user->photoId=0;
					$user->have_photo=0;
					$user->photo='';
				}
				$user->save();
			}
		}
		echo CJSON::encode(array('photoId'=>$id));
		return;	
	}
	
	public function setAvatar($id)
	{
		$photo=Photos::model()->user()->findByPk($id);
		if (!$photo->id)
			return;
		if ($photo->album_id!=0)
			return;
		
		$user=User::model()->findByPk(Yii::app()->user->id);
		$user->photoId=$photo->id;
		$user->have_photo=1;
		$user->photo=$photo->filename;
		$user->save();
		
		$photo->saveAttributes(array('sort'=>time()));
		UserCounters::recalculationPhotos(Yii::app()->user->id);
		
		echo CJSON::encode(array('photoId'=>$id));
		return;	
	}
	
	public function actionLike()
	{
		$photo=Photos::model()->findByPk($_GET['id']);
		
		if($photo===null)
			throw new CHttpException(404,'Page not found.');
		
		if (isset($_GET['like']) && Yii::app()->request->isAjaxRequest) {
			if ($like=Like::model()->find('userId=:userId AND like_userId=:like_userId AND like_photoId=:like_photoId', array(':userId'=>Yii::app()->user->id, ':like_userId'=>$photo->user_id, ':like_photoId'=>$photo->id)))
			{
				if ($like->delete()) {
					$photo->saveAttributes(array('likes'=>new CDbExpression('`likes`-1')));
					
					Notifications::model()->deleteAll('photoId=:photoId AND userId=:userId AND from_userId=:from_userId AND type=:type', array(':photoId'=>$photo->id,':userId'=>$photo->user_id, ':from_userId'=>$like->userId, ':type'=>Notifications::LIKE_PHOTO));
			
					//User::model()->updateCounters(array('notifications'=>-1),'id=:id',array(':id'=>$photo->user_id));
				}
			} else {
				$like=new Like;
				$like->userId=Yii::app()->user->id;
				$like->like_userId=$photo->user_id;
				$like->like_photoId=$photo->id;
				if ($like->save()) {
					$photo->saveAttributes(array('likes'=>new CDbExpression('`likes`+1')));
					
					Notifications::add(Notifications::LIKE_PHOTO, array(
						'userId'=>$photo->user_id,
						'photoId'=>$photo->id,
					));
					
				}
			}
			Yii::app()->end();
		}
		
		$criteria=new CDbCriteria(array(
			'condition'=>'likePhoto.like_userId=:like_userId AND likePhoto.like_photoId=:like_photoId',
			'params'=>array(':like_userId'=>$photo->user_id, ':like_photoId'=>$photo->id),
		));
		$count = User::model()->with('likePhoto')->count($criteria);
		
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['likeUsers_limit'];
		$pages->applyLimit($criteria);
		
		$like=User::model()->with('likePhoto')->findAll($criteria);
		$this->render('like', array('like'=>$like, 'pages'=>$pages, 'count'=>$count));
		
	}
}