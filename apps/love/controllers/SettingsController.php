<?php

class SettingsController extends Controller
{
	public $layout='col_main';
	
	public function accessRules()
	{
		return array(
			array('deny',
			      'roles'=>array('userNoActivated', 'guest'),
			),
		);
	}
	
	public function actionIndex()
	{
		$model=User::model()->findByPk(Yii::app()->user->id);
		
		$this->pageTitle='Настройки';
		$this->breadcrumbs=array(
			'Настройки',
		);
		
		$model->setScenario('edit');
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['User'];
			
			if($model->validate())
			{
				if ($model->save()) {
					Yii::app()->user->setFlash('edit','Изменения сохранены.');
					$this->refresh();
				}
			}
		}
		

		
		$this->render('index', array('model'=>$model));
	}
	
	public function actionPassword()
	{
		$this->pageTitle='Смена пароля';
		
		$this->breadcrumbs=array(
			'Настройки',
			'Смена пароля',
		);
		
		$user=UserAuthorization::model()->findByPk(Yii::app()->user->id);
		$user->setScenario('editPassword');
		$this->performAjaxValidation($user);
		if(Yii::app()->request->isPostRequest)
		{
			$user->attributes=$_POST['UserAuthorization'];
			if ($user->validate())
			{
				$user->password = $user->new_password;
				$user->hash_password = UserAuthorization::hashPassword($user->new_password);
				
				if ($user->save(false)) {
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			} else {
				echo CActiveForm::validate($user);
				Yii::app()->end();
			}
		}
		$this->render('password', array('model'=>$user));
	}
	
	public function actionLogin()
	{
		$this->pageTitle='Смена логина';
		
		$this->breadcrumbs=array(
			'Настройки'=>array('settings/index'),
			'Смена логина',
		);
		
		$user=User::model()->findByPk(Yii::app()->user->id);
		$userPages=UserPages::model()->findByPk(Yii::app()->user->id);
		$user->setScenario('editLogin');
		$this->performAjaxValidation($user);
		if(Yii::app()->request->isPostRequest)
		{
			$user->attributes=$_POST['User'];
			if ($user->validate())
			{
				if ($user->save(false)) {
					$userPages->name=$user->login;
					$userPages->save();
					
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			} else {
				echo CActiveForm::validate($user);
				Yii::app()->end();
			}
		}
		$this->render('login', array('model'=>$user));
	}
	
	public function actionEmail()
	{
		$this->pageTitle='Смена email';
		
		$this->breadcrumbs=array(
			'Настройки'=>array('settings/index'),
			'Смена email',
		);
		
		$user=UserAuthorization::model()->findByPk(Yii::app()->user->id);
		$user->setScenario('editEmail');
		$this->performAjaxValidation($user);
		if(Yii::app()->request->isPostRequest)
		{
			$user->attributes=$_POST['UserAuthorization'];
			if ($user->validate())
			{
				if ($user->save(false)) {

					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			} else {
				echo CActiveForm::validate($user);
				Yii::app()->end();
			}
		}
		$this->render('email', array('model'=>$user));
	}
	
	public function actionPhone()
	{
		$this->pageTitle='Смена телефона';
		
		$this->breadcrumbs=array(
			'Настройки'=>array('settings/index'),
			'Смена телефона',
		);
		
		$user=UserAuthorization::model()->findByPk(Yii::app()->user->id);
		$user->setScenario('editPhone');
		$this->performAjaxValidation($user);
		if(Yii::app()->request->isPostRequest)
		{
			$user->attributes=$_POST['UserAuthorization'];
			if ($user->validate())
			{
				if ($user->save(false)) {
					
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			} else {
				echo CActiveForm::validate($user);
				Yii::app()->end();
			}
		}
		$this->render('phone', array('model'=>$user));
	}
	
	public function actionPrivacy()
	{
		$userSettings=UserSettings::model()->findByPk(Yii::app()->user->id);
		$this->performAjaxValidation($userSettings);
		if(Yii::app()->request->isPostRequest)
		{
			$userSettings->attributes=$_POST['UserSettings'];
			if ($userSettings->validate())
			{
				if ($userSettings->save(false)) {
					
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			} else {
				echo CActiveForm::validate($userSettings);
				Yii::app()->end();
			}
		}
		
		$this->render('privacy', array('model'=>$userSettings));
	}
	
	public function actionRemove()
	{
		
		//$themes=Themes::model()->findAll();
		
		$this->render('remove', array('users'=>$users));
	}
	
	public function actionBlacklist()
	{
		if (Yii::app()->request->isAjaxRequest && $_GET['bId'] && $_GET['bId']!=Yii::app()->user->id) {
			
			$b=Blacklist::checkBlackByUser($_GET['bId']);
			if ($b->id) {
				$b->delete();
				if (Yii::app()->request->isAjaxRequest) {
					echo CJSON::encode(array('status'=>'re_block', 'id'=>$_GET['bId']));
					Yii::app()->end();
				} else {
					//$this->refresh();
				}
			} else {
				$b=new Blacklist;
				$b->userId=Yii::app()->user->id;
				$b->black_userId=$_GET['bId'];
				$b->save();
				if (Yii::app()->request->isAjaxRequest) {
					echo CJSON::encode(array('status'=>'block', 'id'=>$_GET['bId']));
					Yii::app()->end();
				} else {
					//$this->refresh();
				}
			}
			
		}
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`blacklist`.`userId`=:userId',
			'params'=>array(':userId'=>Yii::app()->user->id),
			'with'=>'blacklist',
		));
		$users=User::model()->findAll($criteria);
		
		$this->render('blacklist', array('users'=>$users));
	}
	
	public function actionThemes()
	{
		$model=User::model()->findByPk(Yii::app()->user->id);
		
		$this->pageTitle='Настройки дизайна страницы';
		$this->breadcrumbs=array(
			'Настройки дизайна страницы',
		);
		
		if($_GET['set'])
		{
			$theme=Themes::model()->findByPk($_GET['set']);
			//if ($theme->name)
				$model->saveAttributes(array('theme'=>$theme->name));
				
			$this->redirect(array('profile/index', 'user'=>Yii::app()->user->model->login));
		}
		
		$themes=Themes::model()->findAll();
		
		$this->render('themes', array('model'=>$model, 'themes'=>$themes));
	}
	
	public function actionNotify()
	{
		$userSettings=UserSettings::model()->findByPk(Yii::app()->user->id);
		$this->performAjaxValidation($userSettings);
		if(Yii::app()->request->isPostRequest)
		{
			$userSettings->attributes=$_POST['UserSettings'];
			if ($userSettings->validate())
			{
				if ($userSettings->save(false)) {
					
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			} else {
				echo CActiveForm::validate($userSettings);
				Yii::app()->end();
			}
		}
		
		$this->render('notify', array('model'=>$userSettings));
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='registerForm')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}