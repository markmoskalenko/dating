<?php
Yii::import('vc.components.VCActivationController');

class ActivationController extends VCActivationController
{
	public $pageTitle;
	public $layout='col_main';
	
	public function filters()
	{
		return array(
			//'accessControl',
			array(
				'AccessControlFilter',
				'rules'=>$this->accessRules(),
			)
		);
	}
	
	public function accessRules()
	{
		return array(
			array('deny',
			      'roles'=>array('guest'),
			),
		);
	}
	
	/*public function actions()
	{
		return array(   
			'handler' => 'vc.actions.VCActivationAction'   
		);   
	}*/
	
	public function actionIndex()
	{
		$country=VC_BillCountry::model()->findByAttributes(array('code'=>Yii::app()->vc->countryCode, 'status'=>'1'));
		if (empty($country->id)) {
			
			$this->actionBk();
			Yii::app()->end();
		}
		$this->render('index', array());
	}
	
	public function actionBk()
	{
				$kassa=new Kassa;
				$kassa->user_id=Yii::app()->user->id;
				$kassa->coins=20;
				$kassa->sum=$sum = Yii::app()->vc->subaccount->mc_tarif;
				$kassa->pay_status=0;
				$kassa->date=time();
				$kassa->save();
				
				
// Исходные данные
$shop_id = '11824';		// ID-магазинаы
$pass = '712cf92d6fe26aa10da8e0547de95863';			// Пароль магазина
$secret = 'c814dd85265b778fe3fc122c1b2e3202';		// Секретный ключ
$order = $kassa->id;		// Номер счета
$currency = 'RUR';		// RUR, USD, EUR, UAH
$sum = Yii::app()->vc->subaccount->mc_tarif;			// Сумма счета
//$sum = 50;			// Сумма счета
$prop = array(
	      Yii::app()->user->model->name,
	      Yii::app()->user->auth->email,
	      $kassaForm->phone
	      );	// Доп свойства

// Подготовка пакета $xml
$xml = "
<request>
	<order>$order</order>
	<shop>$shop_id</shop>
	<pass>$pass</pass>
	<sum>$sum</sum>
	<currency>$currency</currency>
	<property>
		<p0>$prop[0]</p0>
		<p1>$prop[1]</p1>
		<p2>$prop[2]</p2>
		<p3>$prop[3]</p3>
		<p4>$prop[4]</p4>
		<p5>$prop[5]</p5>
		<p6>$prop[6]</p6>
		<p7>$prop[7]</p7>
		<p8>$prop[8]</p8>
		<p9>$prop[9]</p9>
	</property>
</request>";
//echo $xml;
// Кодирование пакета $xml методом base64_encod
$xml_encoded = base64_encode($xml);
// Создание подписи
$lqsignature = base64_encode(sha1($secret.$xml_encoded.$secret,1));
				
		
		$this->render('bk', array('xml_encoded'=>$xml_encoded, 'lqsignature'=>$lqsignature));
	}
	
	
	public function activatedPseudo($how_many_sms=3) {
		parent::activated();
		$t=new Transaction();
		$t->coins=5;
		$t->user_id = Yii::app()->user->id;
		$t->comment_payment='Пополнение баланса через смс.';
		$t->save();
		
		$c=Transaction::model()->count('user_id='.Yii::app()->user->id);
		Yii::app()->user->model->saveAttributes(array('balance'=>new CDbExpression('`balance`+5')));
		
		if ($how_many_sms<=1) {
			return 'Ответьте на смс, чтобы получить на счет 5 монет и активировать Real-статус.';
		} elseif ($how_many_sms<=2) {
			return 'Ответьте на смс, чтобы активировать функцию отправки сообщений со своей анкеты другим участникам.';
		} else {
			Yii::app()->user->model->saveAttributes(array('isReal'=>1));
			return 'Real-статус активирован.';
		}
	}
	
	public function actionSubscribe()
	{
		$skey='dsg544shrehb5e4bsgfj';
		$serviceId='3342';
		
		if ($_GET['hash']!=md5($skey.$_GET['action'].$_GET['subscriptionId'].$_GET['msisdn'])) {
			Yii::log('[MT Подписки] [Возврат] [Ошибка хеша] '.$_GET['hash'].'=='.md5($skey.$_GET['action'].$_GET['subscriptionId'].$_GET['msisdn']));
			throw new CHttpException(402,'Неверные параметры подписки.');
		}
		
		if ($_GET['action']=='NEW') {
			if ($_GET['statusCode']=='0') {
				$phonesLog=VC_PhonesLog::model()->find('sidKey=:sidKey', array(':sidKey'=>md5(Yii::app()->user->id)));
				
				$phonesLog->saveAttributes(array('subscription_id'=>$_GET['subscriptionId']));
				
				Yii::app()->user->model->saveAttributes(array('isReal'=>1, 'balance'=>new CDbExpression('`balance`+5')));
				$status='new_ok';
				$this->redirect(array('site/index'));
			} else {
				Yii::log('[MT Подписки] [Возврат] [Ошибка] Статус: '.$_GET['statusCode'].', subscriptionId: '.$_GET['subscriptionId'].', Абонент: '.$_GET['msisdn'].', Метод: '.$_GET['action']);
				throw new CHttpException(402,'Ошибка подписки.');
			}
		}
		else if ($_GET['action']=='CLOSE') {
			if ($_GET['statusCode']=='0') {
				Yii::app()->user->model->saveAttributes(array('isReal'=>0));
				$status='close_ok';
			} else {
				Yii::log('[MT Подписки] [Возврат] [Ошибка] Статус: '.$_GET['statusCode'].', subscriptionId: '.$_GET['subscriptionId'].', Абонент: '.$_GET['msisdn'].', Метод: '.$_GET['action']);
				throw new CHttpException(402,'Ошибка отписки.');
			}
		}
		
		$this->render('subscribe', array('status'=>$status));
	}
	
	public function activatedSms($how_many_sms=3) {
		parent::activated();
		$t=new Transaction();
		$t->coins=5;
		$t->user_id = Yii::app()->user->id;
		$t->comment_payment='Пополнение баланса через смс.';
		$t->save();
		
		$c=Transaction::model()->count('user_id='.Yii::app()->user->id);
		Yii::app()->user->model->saveAttributes(array('balance'=>new CDbExpression('`balance`+5')));
		
		if ($c<=1) {
			return 'Вам требуется отправить второе смс-сообщение со своего мобильного телефона, чтобы получить на счет 5 монет и активировать Real-статус.';
		} elseif ($c<=2) {
			return 'Вам требуется отправить последнее смс-сообщение со своего мобильного телефона, чтобы активировать функцию отправки сообщений со своей анкеты другим участникам.';
		} else {
			Yii::app()->user->model->saveAttributes(array('isReal'=>1));
			return 'Real-статус активирован.';
		}
	}
	public function activatedMc() {
		parent::activated();
		$t=new Transaction();
		$t->coins=20;
		$t->user_id = Yii::app()->user->id;
		$t->comment_payment='Пополнение баланса через мобильный платеж.';
		$t->save();
		
		$c=Transaction::model()->count('user_id='.Yii::app()->user->id);
		Yii::app()->user->model->saveAttributes(array('balance'=>new CDbExpression('`balance`+20')));
		Yii::app()->user->model->saveAttributes(array('isReal'=>1));
		return 'Real-статус активирован.';
		
	}
}