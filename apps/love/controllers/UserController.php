<?php

class UserController extends Controller
{
	public $layout = 'col_main';
	
	public function init()
	{

	}
	
	public function actions()
	{
		return array(
                    //  captcha.
                     'captcha'=>array(
                        'class'=>'CaptchaAction',
                        'maxLength'=> 3,
                        'minLength'=> 4,
                        'testLimit'=> 1,
                    )
                );
	}
	
	public function actionIndex()
	{
		$this->actionLogin();
	}
	
	public function actionRegister()
	{

        if (!Yii::app()->user->isGuest)
            $this->redirect('/site/index');
			
		$user=new User;
        if (Yii::app()->theme->name == 'meetdate'){
            $this->layout = 'main_col_reg';
            $user->setScenario('registerMeetDate');
        } else {
            $user->setScenario('register');
        }
		$this->performAjaxValidation($user);
		if(Yii::app()->request->isPostRequest)
		{
			$user->attributes=$_POST['User'];
			if($user->validate())
			{
				//Yii::app()->vc->processStatField(array('reg'=>1));
				//$user->subaccount_id=Yii::app()->vc->subaccountId;
				/*if (Yii::app()->vc->freeReal)*/ $user->isReal=1;
				if ($user->save(false))
				{
                    UserAuthorization::sendConfirmEmail($user->id); //отправка писма для подтверждения email'а
					$auth=new LoginForm;
					$auth->username=$_POST['User']['email'];
					$auth->password=$_POST['User']['password'];
					$auth->rememberMe=true;
					$auth->login();
					$this->redirect(array('site/index'));
				}
			} else {
//                var_dump($user->getErrors()); exit;
            }
		}
		$this->render('register', array('model'=>$user, 'login'=> new LoginForm()));
	}
	
	public function actionLostpassword()
	{
		if (!Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->returnUrl);
			
		$model=new LostpasswordForm;
		$user=false;
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['LostpasswordForm'];
			if($model->validate())
			{
				$userAuth = UserAuthorization::model()->find('email=:email', array(':email'=>$model->attributes['email']));
				$user = User::model()->findByPk($userAuth->user_id);
				if ($user->id)
				{
                    $token = $userAuth->generateRestoreToken();
                    $userAuth->token = $token;
                    $userAuth->date_experience = (new \DateTime('+10 hours'))->format('Y-m-d H:i:s');
                    $userAuth->save();

                    $message = new YiiMailMessage;
                    $message->view = 'lostpassword';
                    $message->setBody(array('domain'=> 'http://'.Yii::app()->request->domain, 'userId' => $userAuth->user_id,'token' => $userAuth->generateRestoreToken()), 'text/html');
                    $message->subject = 'Восстановление пароля к '.Yii::app()->request->domain;
                    $message->addTo($userAuth->email);
                    $message->from = 'admin@'.Yii::app()->request->domain;


					if(!(Yii::app()->mail->send($message))) {
						throw new CHttpException(500,'Mailer Error.');
						//$model->addError('email', 'Mailer Error: ' . $mail->ErrorInfo);
					}else {
						Yii::app()->user->setFlash('sentemail',$userAuth->email);
						$this->refresh();
					}
				}
				else
					$model->addError('email', 'A user with this email was not found');
			}
		}
		
		$this->render('lostpassword', array('model'=>$model, 'user'=>$user));
	}

    public function actionChangepswd($id, $token)
    {
        if (!Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->user->returnUrl);
        $model = new ChangePasswordForm();
        $userAuth = UserAuthorization::model()->find('user_id=:id', array(':id'=>$id));
        if ($token == $userAuth->token && $userAuth->date_experience >= (new \DateTime())->format('Y-m-d H:i:s')){
            if(Yii::app()->request->isPostRequest){
                $model->attributes=$_POST['ChangePasswordForm'];
                if($model->validate()){
                    $userAuth = UserAuthorization::model()->find('user_id=:id', array(':id'=>$model->user_id));
                    $userAuth->hash_password = UserAuthorization::hashPassword($model->password);
                    $userAuth->password = $model->password;
                    if ($userAuth->save()){
                        Yii::app()->user->setFlash('success', 'Пароль изменен');
                    } else {
                        Yii::app()->user->setFlash('error', 'При попытке изменить пароль произошла ошибка');
                    }
//                    $this->refresh();
                }
            } else {
                $userAuth = UserAuthorization::model()->find('user_id=:id', array(':id'=>$id));
                $model->user_id = $userAuth->user_id;

//                $this->render('changePassword', array('model'=>$model));
            }
        } else {
            Yii::app()->user->setFlash('success', 'Данная ссылка не активна из-за неверного кода или она сгенерирована более 10 часов назад.');
        }
        $this->render('changePassword', array('model'=>$model));
    }
	
	public function actionLogin()
	{
		if (!Yii::app()->user->isGuest)
			$this->redirect('/site/index');
			
		if (isset($_GET['auto']))
		{
			$auto=explode(";", base64_decode($_GET['auto']));
			$auth=new LoginForm;
			$auth->username=$auto['0'];
			$auth->password=$auto['1'];
			$auth->rememberMe=true;
			$auth->login();
			$this->redirect(array('site/index'));
			Yii::app()->end();
		}
			
		$model=new LoginForm;
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			
			if($model->validate() && $model->login())
			{
				$this->redirect(array('site/index'));
			}
		}

        $register =new User;
        if (Yii::app()->theme->name == 'meetdate'){
            $this->layout = 'main_col_reg';
            $register->setScenario('registerMeetDate');
        } else {
            $register->setScenario('register');
        }

		$this->render('login', array('model'=>$model, 'register'=>$register));
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='registerForm')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionConfirmEmail($email, $token)
    {
        $oUser = UserAuthorization::model()->find('email = :email AND token = :token', array('email'=>$email, 'token'=>$token));

        if ($oUser) {
            $oUser->email_confirm = true;
            $oUser->save();
        }

        $this->render('confirmEmail', array('model'=>$oUser));
    }

    public function actionSendConfirmEmail($userId)
    {
        $res = UserAuthorization::sendConfirmEmail($userId); //отправка писма для подтверждения email'а

        $this->render('sendConfirmEmail', array('result'=>$res));
    }
}