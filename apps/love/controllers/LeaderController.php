<?php

class LeaderController extends Controller
{
//    public $layout='col_main';

//    public function accessRules()
//    {
//        return array(
//            array('deny',
//                //'actions'=>array('index'),
//                'users'=>array('?'),
//            ),
//            array('deny',
//                'roles'=>array('userUnreal'),
//            )
//        );
//    }

    public function actionIndex()
    {
        $photos = Yii::app()->user->model->getUserApprovedPhotos();

        return $this->render('index', array('photos'=>$photos));
    }

    public function actionRate($id)
    {
        $leaders = Leader::getLeaders();

        if( isset($_POST['Leader'])){

            $oModel = new Leader();
            $oModel->date_start = date('Y-m-d H:i:s');
            $oModel->user_id = Yii::app()->user->id;
            $oModel->photo_id = $id;

            $oModel->attributes = $_POST['Leader'];

            if (Yii::app()->user->balance >= $_POST['Leader']['rate']) {
                if ($oModel->save()) {
                    $user = Yii::app()->user->model;
                    $user->balance -=  $_POST['Leader']['rate'];
                    $user->save();
                    if (Yii::app()->request->isAjaxRequest) {
                        return $this->render('success');
                    } else {
                        return $this->redirect('/');
                    }
                }
            } else {
                if (Yii::app()->request->isAjaxRequest) {
                    return $this->render('fail', array('message'=>'У Вас недостаточно средств на счете, чтобы стать лидером с такой ставкой.'));
                }
            }

        }

        return $this->render('rate', array('id'=>$id, 'leaders'=>$leaders));
    }
}