<?php

class SiteController extends Controller
{
	public $layout='col_main';

	public function actions()
	{
		return array(
                    //  captcha.
                     'captcha'=>array(
                        'class'=>'CaptchaAction',
                        'maxLength'=> 3,
                        'minLength'=> 4,
                        'testLimit'=> 1,
                    )
                );
	}

	public function actionIndex()
	{
		if (Yii::app()->user->isGuest)
		{
            $this->redirect('/user/register');
//			$criteria=new CDbCriteria(array(
//			'condition'=>'`t`.`isBot`=1 AND photo.approve=1',
//				'with'=>array(
//					'profile',
//					'country',
//					'region',
//					'city',
//					'photo'
//				),
//			));
//
//			$criteria->order='rand()';
//			$criteria->limit=Yii::app()->params['mainRandomUsers_limit'];
//
//			$profiles=User::model()->findAll($criteria);
//			$this->render('index', array('profiles'=>$profiles));
		}
		else {
			Yii::app()->runController('profile/index');
		}
	}
	
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
	
	public function actionRaiseProfile()
	{
		if (Yii::app()->user->balance>=1) {
			Yii::app()->user->model->position->saveAttributes(array('time'=>time()));
			Yii::app()->user->model->saveAttributes(array('balance'=>new CDbExpression('`balance`-1')));
			$this->redirect(array('site/index'));
		}
		else
			$this->redirect(array('pay/index'));
	}
	
	public function actionGeo()
	{
		$data=array();
		if ($_GET['country_id']) {
			foreach (GEORegion::model()->findAll(array('condition'=>'id_country=:id_country', 'params'=>array(':id_country'=>$_GET['country_id']))) as $row) {
				$data[]=array('id_region'=>$row->id_region, 'name'=>$row->name);
			}
			echo CJSON::encode($data);
		} elseif ($_GET['region_id']) {
			foreach (GEOCity::model()->findAll(array('condition'=>'id_region=:id_region', 'params'=>array(':id_region'=>$_GET['region_id']))) as $row) {
				$data[]=array('id_city'=>$row->id_city, 'name'=>$row->name);
			}
			echo CJSON::encode($data);
		}	
	}
	
	public function actionSpamProtection()
	{
		$model=new SpamProtectionForm;
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['SpamProtectionForm'];
			if($model->validate())
			{
				if (Yii::app()->request->isAjaxRequest)
				{
					echo CJSON::encode(array('ok'=>1));
					return;
				}
			}
			elseif (Yii::app()->request->isAjaxRequest)
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
		$this->render('spamProtection', array('model'=>$model));
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}