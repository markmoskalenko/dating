<?php

class NewsletterController extends Controller
{
    public $layout='col_main';

    public function accessRules()
    {
        return array(
            array('deny',
                //'actions'=>array('index'),
                'users'=>array('?'),
            ),
            array('deny',
                'roles'=>array('userUnreal'),
            )
        );
    }

    public function actionUnsubscribe($user_id, $email)
    {
        $oUser = UserAuthorization::model()->findByPk($user_id);

        if (!$oUser || $oUser->email != $email){
            throw new CHttpException(404, 'Пользователь не найден');
        }

        $oUser->no_subscribe = true;
        if (!$oUser->save()){
            $message = 'Произошла ошибка при отписке от рассылки.';
        } else {
            $message = 'Вы отписаны от рассылки.';
        }

        $this->render('unsubscribe', array('message'=>$message));
    }
}