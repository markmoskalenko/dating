<?php

class ChatController extends Controller
{
	public $layout='col_main';

	public function accessRules()
	{
		return array(
			array('deny',
			      //'actions'=>array('index'),
			      'users'=>array('?'),
			),
			array('deny',
			      'roles'=>array('userUnreal'),
			)
		);
	}
	
	public function actionIndex()
	{
		if ($id=Yii::app()->request->getQuery('deleteMessageId')) {
			$chat=Chat::model()->findByPk($id);

			if (Yii::app()->user->checkAccess('deleteComment', array('author_id'=>$chat->userId, 'own_id'=>$chat->userId)))
			{
				$chat->delete();
				echo CJSON::encode(array('id'=>$id));
			}
			else
				throw new CHttpException(402,'checkAccess');
			
			Yii::app()->end();
		}
		
		$model=new Chat;
		if(Yii::app()->request->isPostRequest && !Yii::app()->user->isGuest)
		{
			$model->attributes=$_POST['Chat'];
			if($model->validate())
			{
				$model->userId = Yii::app()->user->id;
				
				if ($model->save())
				{
					Yii::app()->user->model->saveAttributes(array('balance'=>new CDbExpression('`balance`-'.Yii::app()->user->getTransactionsTypes('chat'))));
					if (Yii::app()->request->isAjaxRequest)
					{
						echo CJSON::encode(array('html'=>$this->render('_chatBlock', array(), true)));
						Yii::app()->end();
					}
					Yii::app()->user->setFlash('sent','to');
					$this->refresh();
				}
			}
			elseif (Yii::app()->request->isAjaxRequest)
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
		
		//if (Yii::app()->request->isAjaxRequest) {
			$this->render('_chatBlock', array());
		//}
	}
}