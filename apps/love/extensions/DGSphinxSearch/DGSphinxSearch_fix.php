<?php

Yii::import('ext.DGSphinxSearch.DGSphinxSearch');



class DGSphinxSearch_fix extends DGSphinxSearch
{
   /* private $client;
    
    public function init()
    {
        parent::init();

    }*/
    
    public function setCriteria($criteria)
    {
        if (!is_object($criteria)) {
            throw new DGSphinxSearchException('Criteria does not set.');
        }
        if (isset($criteria->paginator)) {
            if (!is_object($criteria->paginator)) {
                throw new DGSphinxSearchException('Criteria paginator invalid.');
            }
            $this->limit($_GET[$criteria->paginator->pageVar] ? ($_GET[$criteria->paginator->pageVar]*10-10) : 0, $criteria->paginator->getLimit());

            $this->criteria->paginator = $criteria->paginator;
        }

        // set select expression
        if (isset($criteria->select)) {
            $this->select($criteria->select);
        }
        // set from criteria
        if (isset($criteria->from)) {
            $this->from($criteria->from);
        }
        // set where criteria
        if (isset($criteria->query)) {
            $this->where($criteria->query);
        }
        // set grouping
        if (isset($criteria->groupby)) {
            $this->groupby($criteria->groupby);
        }
        // set filters
        if (isset($criteria->filters)) {
            $this->filters($criteria->filters);
        }

        // set field ordering
        if (isset($criteria->orders) && $criteria->orders) {
            $this->orderby($criteria->orders);
        }
    }
    
   /* public function orderby($orders = null)
    {
        
            $this->client->SetSortMode(SPH_SORT_EXTENDED, $orders);
        
        return $this;
    }*/
}
