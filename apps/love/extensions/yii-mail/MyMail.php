<?php
class MyMail extends YiiMail
{
    public function init() {
        parent::init();
        $this->transportOptions = Smtp::getTransportOptions();
    }
}