<?php

class CFaceDetector extends CApplicationComponent{
        
	private $_face_detect;

	public function init(){
		include dirname(__FILE__)."/FaceDetector.php";
                $this->_face_detect = new Face_Detector(dirname(__FILE__).'/detection.dat');
	}

	public function detect($file){
		$this->_face_detect->face_detect($file);
	}

	public function toJpeg(){
		return $this->_face_detect->toJpeg();
	}
	
}//end of method