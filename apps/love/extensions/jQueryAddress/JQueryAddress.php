<?php

class JQueryAddress extends CApplicationComponent
{

	public function init()
	{
		if (!Yii::app()->request->isAjaxRequest && !Yii::app()->user->isGuest) {
			$this->publishAssets();
		
			Yii::app()->clientScript->registerScript('JQueryAddress', $this->getAddressInitJs(), CClientScript::POS_READY);
		}
	}
		
	public function publishAssets()
	{
		$assets = dirname(__FILE__) . '/assets';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		if (is_dir($assets)) {
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.address-1.4.min.js');
		} else {
			throw new CHttpException(500, 'JQueryAddress - Error: Couldn\'t find assets to publish.');
		}
	}
		
	private function getAddressInitJs(){
		$js = <<<EOD
		state = window.history.pushState !== undefined;


if (state) {

    $('a[rel=ajax]').address(function() {

str = 1;
       return $(this).attr('href').replace('http://'+location.hostname, '');
	

    });  
		
		$.address.state('').init(function() {

str = 0;
                
            }).internalChange(function(fn) {

	   
	    }).externalChange(function(fn) {
str = 1;
	    }).change(function(event) {
	    
if (str) { CMenu(event.path, 'nav'); }
 var value = $.address.state() + event.path;

if (!str) return;





                var handler = function(data) {

		    $( '#content').html(data);

                   /* $.address.title(/>([^<]*)<\/title/.exec(data)[1]);*/
                };


                // Loads the page content and inserts it into the content area
                $.ajax({
                    url: event.value,
		   
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        handler(XMLHttpRequest.responseText);
			loading.end();
                    },
		    beforeSend: function(){
			loading.init();
		    },
                    success: function(data, textStatus, XMLHttpRequest) {
                        handler(data);
			loading.end();
                    }
                });
		
			$('#liCounter').html("<img src='http://counter.yadro.ru/hit?t41.8;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='1' height='1'>");
		
            }).crawlable(0);
	    
}
EOD;
		return $js;
	}
}
