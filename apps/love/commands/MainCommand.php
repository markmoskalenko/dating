<?php

class MainCommand extends CConsoleCommand
{
    /*public function run($args) {
        // тут делаем то, что нам нужно
    }*/
	
	public function init()
	{
		parent::init();
	}
	
	
	public function actionBotsOnline()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`isBot`=1');
		$criteria->limit='1000';
		$criteria->order='rand()';
		
		foreach (User::model()->findAll($criteria) as $row) {
			$row->last_date=time()-rand(0, 60);
			$row->save();
		}
		//echo date("H:i:s", time());
	}
	
	public function actionRemoveUsers()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`remove`=1');
		
		foreach (User::model()->findAll($criteria) as $row) {
			foreach (Photos::model()->findAll('user_id='.$row->id) as $key=>$photoRow) {
				if (is_file(Yii::app()->getBasePath()."/../../uploads/dating/photos/1/u".$photoRow->user_id."/".$photoRow->filename)) unlink(Yii::app()->getBasePath()."/../../uploads/dating/photos/1/u".$photoRow->user_id."/".$photoRow->filename);
				CommentsPhotos::model()->deleteAll('photoId=:photoId', array(':photoId'=>$photoRow->id));
				Like::model()->deleteAll('like_photoId=:photoId', array(':photoId'=>$photoRow->id));
				$photoRow->delete();
			}
			foreach (Friends::model()->findAll('friend_userId=:friend_userId', array(':friend_userId'=>$row->id)) as $key=>$fRow) {
				$fRow->delete();
			}
			Notifications::model()->deleteAll('userId=:userId', array(':userId'=>$row->id));
			Wall::model()->deleteAll('userId=:userId', array(':userId'=>$row->id));
			Messages::model()->deleteAll('from_userId=:from_userId', array(':from_userId'=>$row->id));
			UserLikes::model()->deleteAll('own_userId=:own_userId', array(':own_userId'=>$row->id));
			GiftsUsers::model()->deleteAll('userId=:userId', array(':userId'=>$row->id));
			Profiles::model()->deleteAll('user_id=:user_id', array(':user_id'=>$row->id));
			UserAuthorization::model()->deleteAll('user_id=:user_id', array(':user_id'=>$row->id));
			UserCounters::model()->deleteAll('user_id=:user_id', array(':user_id'=>$row->id));
			UserPages::model()->deleteAll('user_id=:user_id', array(':user_id'=>$row->id));
			UserPosition::model()->deleteAll('user_id=:user_id', array(':user_id'=>$row->id));
			UserSettings::model()->deleteAll('user_id=:user_id', array(':user_id'=>$row->id));
			Friends::model()->deleteAll('userId=:userId', array(':userId'=>$row->id));
			FriendsRequests::model()->deleteAll('userId=:userId', array(':userId'=>$row->id));
			FriendsRequests::model()->deleteAll('friend_userId=:friend_userId', array(':friend_userId'=>$row->id));
			Albums::model()->deleteAll('user_id=:user_id', array(':user_id'=>$row->id));
			Blacklist::model()->deleteAll('userId=:userId', array(':userId'=>$row->id));
			Transaction::model()->deleteAll('user_id=:user_id', array(':user_id'=>$row->id));
			CommentsPhotos::model()->deleteAll('userId=:userId', array(':userId'=>$row->id));
			foreach (Wall::model()->findAll('written_userId=:written_userId', array(':written_userId'=>$row->id)) as $key=>$wallRow) {
				$wallRow->delete();
			}
			$row->delete();
		}
	}
	
	public function actionA()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`isBot`=1');
		//$criteria->with=array('counters');

		
		foreach (User::model()->findAll($criteria) as $row) {
			UserCounters::recalculationWall($row->id);
		}
		//echo date("H:i:s", time());
	}
	
	public function actionB()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`isBot`=1');
		//$criteria->with=array('counters');

		
		foreach (User::model()->findAll($criteria) as $row) {
			foreach (Photos::model()->findAll('user_id='.$row->id) as $key=>$rowPhoto) {
				
				//$s=time()-$rowPhoto->date;
				$t=time()-rand(60*60*24*$key, 60*60*24*(20+$key));
				Wall::model()->updateAll(array('date'=>$t), 'photoId=:photoId', array(':photoId'=>$rowPhoto->id));
				$rowPhoto->date=$t;
				$rowPhoto->save();
				echo date("Y-m-d H:s:i", $t)."\r\n";
			}
		}
		//echo date("H:i:s", time());
	}
	
	/*public function actionF()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`isBot`=1 AND `t`.`sex`=2');
		//$criteria->with=array('counters');

		foreach (User::model()->findAll($criteria) as $row) {
			for ($i=1; $i<=rand(7,24); $i++)
			{
				$u=User::model()->find(array('condition'=>'`t`.`isBot`=1 AND `t`.`sex`=1', 'order'=>'rand()'));
				
				$f=Friends::model()->find('userId=:userId AND friend_userId=:friend_userId', array(':userId'=>$row->id, ':friend_userId'=>$u->id));
				if (!$f->id) {
				$friends=new Friends;
				$friends->userId=$row->id;
				$friends->friend_userId=$u->id;
				$friends->save();
				UserCounters::recalculationFriends($row->id);
				
				$friends=new Friends;
				$friends->userId=$u->id;
				$friends->friend_userId=$row->id;
				$friends->save();
				UserCounters::recalculationFriends($u->id);
				}
				
			}
		}
		//echo date("H:i:s", time());
	}*/
	
	/*public function actionG()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`isBot`=1');
		//$criteria->with=array('counters');
		$cityArray=array(
			'Москва',
			'Санкт-Петербург',
			'Новосибирск',
			'Екатеринбург',
			'Нижний Новгород',
			'Казань',
			'Самара',
			'Омск',
			'Челябинск',
			'Ростов-на-Дону',
			'Уфа',
			'Волгоград',
			'Красноярск',
			'Пермь',
			'Воронеж',
		);
		//$cityModel=GEOCity::model()->find('name=:name', array(':name'=>$cityArray[rand(0,14)]));
		//echo $cityModel->id_city;
		foreach (User::model()->findAll($criteria) as $row) {
			$cityModel=GEOCity::model()->find('name=:name', array(':name'=>$cityArray[rand(0,14)]));
			
			$row->country_id=$cityModel->id_country;
			$row->region_id=$cityModel->id_region;
			$row->city_id=$cityModel->id_city;
			if ($cityModel->id_city) $row->save();
		}
		//echo date("H:i:s", time());
	}*/
	
	public function actionRaz()
	{

			foreach (Photos::model()->findAll('width=0') as $key=>$rowPhoto) {
				
					list($width, $height, $type, $attr) = getimagesize(Yii::app()->getBasePath()."/../../uploads/dating/photos/1/u".$rowPhoto->user_id."/".$rowPhoto->filename); 
					$rowPhoto->width=$width;
					$rowPhoto->height=$height;
					$rowPhoto->save();
				
			}
	}
	
	public function actionBots()
	{
		$command=Yii::app()->db->createCommand("SELECT * FROM users t
			LEFT OUTER JOIN bots_users b
			   
		");

		
		foreach (User::model()->findAll($criteria) as $row) {
			foreach (Photos::model()->findAll('user_id='.$row->id) as $key=>$rowPhoto) {
				
				//$s=time()-$rowPhoto->date;
				$t=time()-rand(60*60*24*$key, 60*60*24*(20+$key));
				Wall::model()->updateAll(array('date'=>$t), 'photoId=:photoId', array(':photoId'=>$rowPhoto->id));
				$rowPhoto->date=$t;
				$rowPhoto->save();
				echo date("Y-m-d H:s:i", $t)."\r\n";
			}
		}
		//echo date("H:i:s", time());
	}
	
	public function actionAutoActivation()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`isReal`=0');
		
		foreach (User::model()->findAll($criteria) as $row) {
			$count=Transaction::model()->count('user_id=:user_id', array(':user_id'=>$row->id));
			if ($count>0) {
				//saveAttributes
				echo "$count\r\n";
				
				$mail=Yii::app()->Smtpmail;
				$mail->SetFrom($mail->Username, 'LifeMeet');
				$mail->Subject    = 'Real';
				$mail->MsgHTML($this->render('/email/lostpassword', array('user'=>$user, 'userpass'=>$userpass), true));
				$mail->AddAddress($user->email, "");
				$mail->Send();
				
				
			}
		}
		//echo date("H:i:s", time());
	}
}