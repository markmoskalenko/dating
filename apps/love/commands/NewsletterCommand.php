<?php

class NewsletterCommand extends CConsoleCommand
{

    public function actionStart($id = null)
    {
        Newsletter::startNewsletters($id);
    }
}