<?php

class BotsCommand extends CConsoleCommand
{
    /*public function run($args) {
        // тут делаем то, что нам нужно
    }*/
	
	public function init()
	{
		parent::init();
	}
	
	public function actionBotsOnline()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`isBot`=1');
		$criteria->limit='1000';
		$criteria->order='rand()';
		
		foreach (User::model()->findAll($criteria) as $row) {
			$row->last_date=time()-rand(0, 60);
			$row->save();
		}
	}
	
	public function actionUpdateInterval()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`message_step`=0');
		$criteria->order='t.id DESC';
		
		foreach (BotsUsers::model()->findAll($criteria) as $row) {
			$bot=Bots::model()->find('user_id='.$row->bot_user_id);
			$interval_posts=array();
			$msgArray=explode(";", $bot->messages);
			foreach ($msgArray as $kys=>$rowMsgs) {
				$interval_posts[]=time()+rand(60,600);
			}
			asort($interval_posts);
			
			$row->saveAttributes(array('interval_posts'=>implode(";",$interval_posts)));
		}
	}
	
	public function actionSending()
	{
		$criteria=new CDbCriteria;
		//$criteria->addCondition('`image`.`prior_approval`=1 AND `t`.`completed`=0 AND `user`.`sex`=1 AND `user`.`have_photo`=1 AND `user`.`isReal`=0');
		$criteria->addCondition('`t`.`completed`=0 AND `user`.`sex`=1 AND `user`.`have_photo`=1 AND `user`.`isReal`=0');
		//$criteria->limit='500';
		$criteria->order='t.id DESC';
		
		foreach (BotsUsers::model()->with('user')->findAll($criteria) as $row) {
			
			User::model()->findByPk($row->bot_user_id)->saveAttributes(array('last_date'=>time()));
			
			$bot=Bots::model()->find(array('condition'=>'user_id='.$row->bot_user_id));
			
			$msgArray=@explode(";", $bot->messages);
			$interval_postsArray=explode(";", $row->interval_posts);
			
			if (@$interval_postsArray[$row->message_step]) {
				$interval_posts=$interval_postsArray[$row->message_step];
			} else {
				$interval_posts=0;
			}
			
			if (@$msgArray[$row->message_step]) {
				$msg=$msgArray[$row->message_step];
			} else {
				$msg='';
			}
				
			if ($interval_posts<time())
			{
				if($msg)
				{
					$modelMessage=new Messages;
					$modelMessage->from_userId = $row->user_id;
					$modelMessage->to_userId = $bot->user_id;
					$modelMessage->unread = 1;
					$modelMessage->folder = 'inbox';
					$modelMessage->message = $msg;
					$modelMessage->time = $interval_postsArray[$row->message_step];
					$modelMessage->save(false);
				}
					
				if ($row->message_step>=$row->message_max) {
					$row->saveAttributes(array('completed'=>1));
				} else {
					$row->saveAttributes(array('message_step'=>new CDbExpression('`message_step`+1')));
				}
			}
		}
	}
}