<?php

class ModerationCommand extends CConsoleCommand
{
    /*public function run($args) {
        // тут делаем то, что нам нужно
    }*/
	
	public function init()
	{
		parent::init();
	}
	
	public function actionIndex()
	{
		$criteria=new CDbCriteria;
		$criteria->addCondition('`t`.`prior_approval`=0 AND `t`.`album_id`=0 AND `user`.`isBot`=0 AND `user`.`isReal`=1');
		$criteria->limit='10';
		//$criteria->addCondition('`t`.`prior_approval`=0 AND `t`.`album_id`=0 AND `user`.`isBot`=0');
		//$criteria->limit='10';
		$criteria->order='t.id DESC';
		$criteria->with=array('user');
		
		foreach (Photos::model()->findAll($criteria) as $row)
		{
			$result=CJSON::decode(Yii::app()->CURL->run('http://api.skybiometry.com/fc/faces/detect.json', false, array(
				'api_key'=>'ca666fd671574ceab0a66240477d98c4',
				'api_secret'=>'887726411bcb4059b7c065c8adc7dcfc',
				'urls'=>'http://5.199.134.15'.$row->imageSource,
				'attributes'=>'all',
			)));

			if (isset($result['photos']['0']['tags']) && count($result['photos']['0']['tags'])!=0) {
				if (count($result['photos']['0']['tags'])>1) {
					// перемстить в разное если групповое фото
					$this->movePhoto($row->id);
				} elseif ($result['photos']['0']['tags']['0']['attributes']['face']['value']=='true') {
					// предвариельно одобрить
					
					$row->saveAttributes(array('prior_approval'=>1));
				}
			} elseif (isset($result['status'])) {
				if ($result['status']=="success") {
					// перемстить в разное если лица не найдено
					$this->movePhoto($row->id);
				}
			}
		}
	}
	
	public function movePhoto($id, $t=1) {
		
		$photo=Photos::model()->findByPk($id);
		
		if($photo===null)
			return false;
		
		if ($t==1) {
			Notifications::add(Notifications::PHOTO_MOVED, array(
				'userId'=>$photo->user_id,
				'photoId'=>$photo->id,
				'text'=>'Фотография перемещена в альбом "Разное"',
			));
			
			$album=Albums::model()->find('user_id=:user_id AND system=1', array(':user_id'=>$photo->user_id));
			if (!isset($album->id)) {
				$album=new Albums;
				$album->user_id=$photo->user_id;
				$album->system=1;
				$album->name=Yii::t('app', 'Разное');
				$album->save();
			}
		} elseif ($t==2) {
			Notifications::add(Notifications::PHOTO_MOVED, array(
				'userId'=>$photo->user_id,
				'photoId'=>$photo->id,
				'text'=>'Фотография перемещена в альбом "18+"',
			));
			
			$album=Albums::model()->find('user_id=:user_id AND system=2', array(':user_id'=>$photo->user_id));
			if (!isset($album->id)) {
				$album=new Albums;
				$album->user_id=$photo->user_id;
				$album->system=2;
				$album->censor=1;
				$album->name=Yii::t('app', '18+');
				$album->save();
			}
		}
		
		// updatet by user times
		//UserCounters::model()->findByPk($photo->user_id)->saveAttributes(array('update_photos'=>time()));
		
		// delete wall photo
		if ($photo->album_id==0){
			foreach (Wall::model()->findAll('userId=:userId AND photoId=:photoId', array(':photoId'=>$photo->id, ':userId'=>$photo->user_id)) as $row) {
				$row->delete();
			}
		}
		//$photo->prior_approval=1;
		$photo->album_id=$album->id;
		$photo->save();
			$criteria=new CDbCriteria(array(
				'condition'=>'user_id=:user_id AND album_id=0',
				'params'=>array(':user_id'=>$photo->user_id),
				'order'=>'sort DESC',
			));
			$p=Photos::model()->find($criteria);
			$sort=isset($p->sort) ? $p->sort : 0;
			if ($photo->sort>$sort) {
				$user=User::model()->findByPk($photo->user_id);
				if (isset($p->id)) {
					$user->photoId=$p->id;
					$user->have_photo=1;
					$user->photo=$p->filename;
				} else {
					$user->photoId=0;
					$user->have_photo=0;
					$user->photo='';
				}
				$user->save();
			}
	}
}