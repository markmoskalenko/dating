<?php
/**
 * Image helper functions
 * 
 * @author Chris
 * @link http://con.cept.me
 */
class ImageHelper {

    /**
     * Directory to store thumbnails
     * @var string 
     */
    const THUMB_DIR = 'tmb';

    /**
     * Create a thumbnail of an image and returns relative path in webroot
     * the options array is an associative array which can take the values
     * quality (jpg quality) and method (the method for resizing)
     *
     * @param int $width
     * @param int $height
     * @param string $img
     * @param array $options
     * @return string $path
     */
    public static function thumb($width, $height, $userId, $fileName, $options = null)
    {
        $img='/uploads/dating/photos/1/u'.$userId.'/'.$fileName;
        if(!file_exists($img)){
            $img = str_replace('\\', '/', YiiBase::getPathOfAlias('uploadsroot').$img);
            if(!file_exists($img)){
                $img='/uploads/dating/img_delete.jpg';
                return Yii::app()->request->baseUrl.$img;
                //throw new ExceptionClass('Image not found');
            }
        }

        // Jpeg quality
        $quality = 95;
        // Method for resizing
        $method = 'adaptiveResize';

        if($options){
            extract($options, EXTR_IF_EXISTS);
        }

        $file_path='uploads/dating/'.self::THUMB_DIR.'/'.substr($fileName,0,1).'/'.substr($fileName,1,2).'/u'.$userId.'/';
        $pathinfo = pathinfo($img);
        $thumb_name = $pathinfo['filename'].'_'.$method.'_'.$width.'_'.$height.'.'.$pathinfo['extension'];
        $thumb_path = YiiBase::getPathOfAlias('uploadsroot').'/'.$file_path;
        if(!file_exists($thumb_path)){
           // Yii::log( $thumb_path);
            //mkdir($thumb_path, 0777);
            self::addir(YiiBase::getPathOfAlias('uploadsroot').'/uploads/dating', self::THUMB_DIR.'/'.substr($fileName,0,1).'/'.substr($fileName,1,2).'/u'.$userId.'/');
        }
        
        if(!file_exists($thumb_path.$thumb_name) || filemtime($thumb_path.$thumb_name) < filemtime($img)){
            
            Yii::import('ext.phpThumb.PhpThumbFactory');
            $options = array('jpegQuality' => $quality);
            $thumb = PhpThumbFactory::create($img, $options);
            $thumb->{$method}($width, $height);
            $thumb->save($thumb_path.$thumb_name);            
        }
        
        $relative_path = Yii::app()->request->baseUrl.'/'.$file_path.$thumb_name;
     //   Yii::log( $thumb_path.$thumb_name);
        return $relative_path;
    }
    
 public static function addir($path, $dir)
{
	$arr=explode("/", $dir);
	for ($i=0; $i<=count($arr)-2; $i++)
	{
		$fl=$fl."/".$arr[$i];
		if (!is_file($path.$fl))
		{
			//echo $fl."<br>";
			@mkdir($path.$fl);
			@chmod($path.$fl, 0777);
                         Yii::log( $path.$fl); 
		}
	}
}
}