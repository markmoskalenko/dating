<?php

class Html extends CHtml
{
	public static function imageUrl($url='') {

		return Yii::app()->baseUrl.'/themes/'.Yii::app()->theme->name.'/images/'.$url;
		//return Yii::app()->baseUrl.'/images/'.$url;
	}
        
	public static function cssUrl($url='') {
		return Yii::app()->baseUrl.'/themes/'.Yii::app()->theme->name.'/css/'.$url;
		//return Yii::app()->baseUrl.'/images/'.$url;
	}
        
	public static function jsUrl($url) {

			return Yii::app()->baseUrl.'/themes/'.Yii::app()->theme->name.'/js/'.$url;
	}
       
	public static function contentUrl($url) {

			return Yii::app()->baseUrl.'/uploads/dating/photos/'.$url;
	}
       
function tzdelta ( $iTime = 0 )
{
    if ( 0 == $iTime ) { $iTime = time(); }
    $ar = localtime ( $iTime );
    $ar[5] += 1900; $ar[4]++;
    $iTztime = gmmktime ( $ar[2], $ar[1], $ar[0],
        $ar[4], $ar[3], $ar[5], $ar[8] );
    return ( $iTztime - $iTime );
} 
       
        public static function date($date_time)
	{
		//return Html::tzdelta ( $date_time );
		
$timezones = array(
		'-14'=>'Etc/GMT-14',
		'-13'=>'Etc/GMT-13',
		'-12'=>'Etc/GMT-12',
		'-11'=>'Etc/GMT-11',
		'-10'=>'Etc/GMT-10',
		'-9'=>'Etc/GMT-9',
		'-8'=>'Etc/GMT-8',
		'-7'=>'Etc/GMT-7',
		'-6'=>'Etc/GMT-6',
		'-5'=>'Etc/GMT-5',
		'-4'=>'Etc/GMT-4',
		'-3'=>'Etc/GMT-3',
		'-2'=>'Etc/GMT-2',
		'-1'=>'Etc/GMT-1',
		'0'=>'Etc/GMT+0',
		'1'=>'Etc/GMT+1',
		'2'=>'Etc/GMT+2',
		'3'=>'Etc/GMT+3',
		'4'=>'Etc/GMT+4',
		'5'=>'Etc/GMT+5',
		'6'=>'Etc/GMT+6',
		'7'=>'Etc/GMT+7',
		'8'=>'Etc/GMT+8',
		'9'=>'Etc/GMT+9',
		'10'=>'Etc/GMT+10',
		'11'=>'Etc/GMT+11',
		'12'=>'Etc/GMT+12',
	);
if ($timezones[$_COOKIE['time_zone']]) {
	$tz_string = $timezones[$_COOKIE['time_zone']];
} else {
	$tz_string = "Etc/GMT-4";
}


    $tz_object = new DateTimeZone($tz_string);
   
    $datetime = new DateTime('@'.$date_time);
    $datetime->setTimezone($tz_object);
    
    $datetime1 = new DateTime();
    $datetime1->setTimezone($tz_object);
    

    $datetime2 = new DateTime('@'.(time()-86400));
    $datetime2->setTimezone($tz_object);

	/*$format="%e %b. %Y";
	$lang = 'ru';
	    if (substr(PHP_OS,0,3) == 'WIN') {
	           $_win_from = array ('%e',  '%T',       '%D');
	           $_win_to   = array ('%#d', '%H:%M:%S', '%m/%d/%y');
	           $format = str_replace($_win_from, $_win_to, $format);
	    }
		return   strftime($format, $date_time);*/
		//return  $datetime->format('j M. Y');
		
		$fivedays=3600;
		$timeAgo = time() - $date_time;
		if ($timeAgo==0) 			 return 'только что';
		if($timeAgo<$fivedays)
		{
			$timePer = array(
				'day' 	=> array(3600 * 24, 'дн.'),
				'hour' 	=> array(3600, ''),
				'min' 	=> array(60, 'мин.'),
				'sek' 	=> array(1, 'сек.'),
				);
			foreach ($timePer as $type =>  $tp) {
				$tpn = floor($timeAgo / $tp[0]);
				if ($tpn){
				
					switch ($type) {
						case 'hour':
							if (in_array($tpn, array(1, 21))){
								$tp[1] = 'час';
							}elseif (in_array($tpn, array(2, 3, 4, 22, 23)) ) {
								$tp[1] = 'часa';
							}else {
								$tp[1] = 'часов';
							}
							break;
					}
					return $tpn.' '.$tp[1].' назад';
				}

			}
		}
		elseif ($datetime->format('j M. Y')==$datetime1->format('j M. Y'))
		{
			return 'сегодня в '.$datetime->format('H:i:s');
		}
		elseif ($datetime->format('j M. Y')==$datetime2->format('j M. Y'))
		{
			return 'вчера в '.$datetime->format('H:i:s');
		}
		else
		{
			$lang = 'ru';
	/*$format="%e %b. %Y";
	
	    if (substr(PHP_OS,0,3) == 'WIN') {
	           $_win_from = array ('%e',  '%T',       '%D');
	           $_win_to   = array ('%#d', '%H:%M:%S', '%m/%d/%y');
	           $format = str_replace($_win_from, $_win_to, $format);
	    }
 	     		//return $date_time;
 	    if($date_time != '') {
	        $out = strftime($format, $date_time);
	    } else {
	        $out = '';
	    }
	    //return $out;*/
	   if (date("Y")==$datetime->format('Y')) $out = $datetime->format('j M'); else  $out = $datetime->format('j M Y');
		$strFrom = array(
				'january', 		'jan',	
				'february', 	'feb',	
				'march', 		'mar',	
				'april', 		'apr',	
				'may', 			'may',	
				'june',  	   'jun',	
				'july', 		'jul',	
				'august', 		'aug',	
				'september',	'sep',	
				'october',		'oct',	
				'november',		'nov',	
				'december',		'dec',
				'monday',	
				'tuesday',	
				'wednesday',	
				'thursday',	
				'friday',	
				'saturday',	
				'sunday',
				'mon',
				'tue',
				'wed',
				'thu',
				'fri',
				'sat',
				'sun',			
			);
			$strTo = array('ru' => array(
								'Январь',	'янв',	
								'Февраль',	'фев',	
								'Март',		'мар',	
								'Апрель',	'апр',	
								'Май',		'мая',	
								'Июнь',		'июн',	
								'Июль',		'июл',	
								'Август',	'авг',	
								'Сентябрь',	'сен',	
								'Октябрь',	'окт',
								'Ноябрь',	'ноя',	
								'Декабрь',	'дек',	
								'Понедельник',
								'Вторник',
								'Среда',
								'Четверг',
								'Пятница',
								'Суббота',
								'Воскресенье',
								'Пн',
								'Вт',
								'Ср',
								'Чт',
								'Пт',
								'Сб',
								'Вс',
							),
				);
			

 		$outOld = $out;
 		
		$out = str_replace($strFrom, $strTo[$lang], strtolower($out));
 		if ($out == strtolower($outOld)){
			$out = $outOld;
		}
 		$out = str_replace('Май', 'мая', $out);

	     		return $out.' в '.$datetime->format('H:i:s');
 		}
		
		
		
		
/*$localtime = localtime();
$localtime_assoc = localtime(time(), true);
print_r($localtime);
print_r($localtime_assoc);
    $tz_string = "Etc/GMT-6"; // Use one from list of TZ names http://php.net/manual/en/timezones.php
    $tz_object = new DateTimeZone($tz_string);
   
    $datetime = new DateTime('@'.$time);
    $datetime->setTimezone($tz_object); 
		
		return  $datetime->format('Y/m/d H:i:s');
		if (date("d-m-Y", $time)==date("d-m-Y") && $time!=0) {
			return Yii::t('app', 'Today in :time', array(':time'=>date("H:i:s", $time)));
		} elseif ($time!=0) {
			return Yii::t('app', ':date in :time', array(':time'=>date("H:i:s", $time), ':date'=>date("d.m.Y", $time)));
		} else {
			return "-";
		}*/
	}
	
	public static function age($birthday)
	{
		$bir_array=explode("-", $birthday);
		if ($bir_array[1]!=0 && $bir_array[2]!=0 && $bir_array[0]!=0) {
		$vozrast=intval((time()-mktime(0, 0, 0, $bir_array[1], $bir_array[2], $bir_array[0]))/31536000); $dater=date("d.m.Y",$dt[6]); $dney=Yii::t('profile',"year|year|years", $vozrast);
		if ($vozrast>20) {$ddays=substr($vozrast,-1);} else {$ddays=$vozrast;}
		if ($ddays=="1") {$dney=Yii::t('profile',"year|year|years", $vozrast);} if ($ddays=="2" or $ddays=="3" or $ddays=="4") {$dney=Yii::t('profile',"year|year|years", $vozrast);}
		return $vozrast.' '.$dney;
		} else return '';
	}
	
	public static function zodiak($string)
	{
		$dat_arr=explode("-", $string);
		$d=sprintf('%02d',$dat_arr[2]);
		$m=sprintf('%02d',$dat_arr[1]);
		if (($m=='03' AND $d>20) OR ($m=='04' AND $d<21)) $text = 'Oven';
		if (($m=='04' AND $d>20) OR ($m=='05' AND $d<22)) $text = 'Taurus';
		if (($m=='05' AND $d>21) OR ($m=='06' AND $d<22)) $text = 'Gemini';
		if (($m=='06' AND $d>21) OR ($m=='07' AND $d<23)) $text = 'Cancer';
		if (($m=='07' AND $d>22) OR ($m=='08' AND $d<24)) $text = 'Leo';
		if (($m=='08' AND $d>23) OR ($m=='09' AND $d<24)) $text = 'Virgo';
		if (($m=='09' AND $d>23) OR ($m=='10' AND $d<24)) $text = 'Libra';
		if (($m=='10' AND $d>23) OR ($m=='11' AND $d<23)) $text = 'Scorpion';
		if (($m=='11' AND $d>22) OR ($m=='12' AND $d<22)) $text = 'Sagittarius';
		if (($m=='12' AND $d>21) OR ($m=='01' AND $d<19)) $text = 'Capricorn';
		if (($m=='01' AND $d>20) OR ($m=='02' AND $d<19)) $text = 'Aquarius';
		if (($m=='02' AND $d>18) OR ($m=='03' AND $d<21)) $text = 'Fish';
		return Yii::t('profile', 'Zodiak_'.$text);
	}
	
	public static function message($text)
	{
		if (strstr($text, Yii::app()->request->domain)) {
	
			$text=preg_replace('((http\:\/\/)?(\w+\.)+\w+(\/[^\s]+)?)','<a href="$0" target="_blank" rel="nofollow">$0</a>',$text);
		} else {
			$text=preg_replace('((http\:\/\/)?(\w+\.)+\w+(\/[^\s]+)?)','<a href="/away?to=$0" target="_blank" rel="nofollow">$0</a>',$text);
		}
		
		return $text;
	}
	
	public static function translitIt($str) 
{
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya", " " => "_"
    );
    return strtr($str,$tr);
}

	public static function fioName($str, $c=9) 
	{
            $name = explode(' ',$str);
            //print_r($name);
	    if ($name[1])
			return mb_substr($name[0],0,1,'utf-8').'.'.mb_substr($name[1],0,$c-1,'utf-8');
		else
			return mb_substr($name[0],0,$c,'utf-8');
	}
	
	public static function fio2Name($str, $c=9, $b='<br>') 
	{
            $name = explode(' ',$str);
            //print_r($name);
	    if ($name[1])
			return mb_substr($name[0],0,$c,'utf-8').$b.mb_substr($name[1],0,$c,'utf-8');
		else
			return mb_substr($name[0],0,$c,'utf-8');
	}
	

	
	public static function ucwords($str) 
	{
            return mb_convert_case($str,MB_CASE_TITLE,'UTF-8');
	}
}