<?php
class WebUser extends CWebUser
{
	private $_model = null;
	private $_access=array();
	private $_newMessagesCounter;
    private $_chatCounter;
	private $_requestFriendsCounter;
	private $_position;
	private $_transactionsTypes;
	private $_counter;
	private $_auth;
	
	public function getRole()
	{
		if($user = $this->getModel())
		{
			// � ������� User ���� ���� role
			if (!$user->isReal) return 'userUnreal';
			return $user->role;
		}
	}
	
	public function getInfo()
	{
		if($user = $this->getModel())
		{
			return $user;
		}
	}
	
	public function getData()
	{
		if($user = $this->getModel())
		{
			return $user;
		}
	}
	
	public function getHavePhoto()
	{
		if($user = $this->getModel())
		{
			return $user->have_photo;
		}
	}
	
	public function getPosition()
	{
		if ($this->_position === null)
		{
			$criteria = new CDbCriteria;
			$criteria->condition='`t`.`time`>='.$this->model->position->time;
			$this->_position=UserPosition::model()->count($criteria);
		}
		return $this->_position;
		
	}
	
	public function getNewMessagesCounter()
	{
		if ($this->_newMessagesCounter === null)
		{
			$criteria = new CDbCriteria;
			$criteria->condition='`t`.`unread`=1 AND `t`.`from_userId`='.$this->id;
			$this->_newMessagesCounter=Messages::model()->count($criteria);
		}
		return $this->_newMessagesCounter;
		
	}
	
	public function getRequestFriendsCounter()
	{
		if ($this->_requestFriendsCounter === null)
		{
			$criteria = new CDbCriteria;
			$criteria->condition='`t`.`userId`='.$this->id;
			$this->_requestFriendsCounter=FriendsRequests::model()->count($criteria);
		}
		return $this->_requestFriendsCounter;
		
	}
	
	public function getSex()
	{
		if($user = $this->getModel())
		{
			return $user->sex;
		}
	}
	
	public function getEmail()
	{
		return $this->auth->email;
	}
	
	public function getPhone()
	{
		return $this->auth->phone;
	}
	
	public function getBalance()
	{
		if($user = $this->getModel())
		{
			return $user->balance;
		}
	}
	
	public function getModel()
	{
		if (!$this->isGuest && $this->_model === null)
		{
			$this->_model = User::model()->with('position')->findByPk($this->id);
			if (empty($this->_model)) {
				$this->logout();
				parent::loginRequired();
			}
				
			/*if (date("d.m.y", $this->_model->last_date)!=date("d.m.y")) {
				$this->_model->saveAttributes(array('day_visit'=>new CDbExpression('`day_visit`+1')));
			}
			
			if (!Yii::app()->request->isAjaxRequest && $this->_model->last_date<time()-30)
				$this->_model->saveAttributes(array('last_date'=>time()));*/
		}
		return $this->_model;
	}
	
	public function getTransactionsTypes($name)
	{
		if($this->_transactionsTypes[$name] === null)
		{
			$this->_transactionsTypes[$name]=TransactionsTypes::model()->find('name=:name', array(':name'=>$name))->price;
		}
		return $this->_transactionsTypes[$name];
	}
	
	public function getCounter($user_id=false)
	{
		$user_id=$user_id ? $user_id : Yii::app()->user->id;
		if($this->_counter[$user_id] === null)
		{
			$this->_counter[$user_id]=UserCounters::model()->find('user_id=:user_id', array(':user_id'=>$user_id));
		}
		return $this->_counter[$user_id];
	}
	
	public function getAuth()
	{
		if($this->_auth === null)
		{
			$this->_auth=UserAuthorization::model()->find('user_id=:user_id', array(':user_id'=>$this->id));
		}
		return $this->_auth;
	}
	
	/*public function loginRequired()
	{
		Yii::app()->getController()->widget('UserLogin');
		
		if (Yii::app()->request->isAjaxRequest) {
			echo "<script>location.href = '".Yii::app()->createUrl($this->loginUrl[0])."';</script>";
		} else
			parent::loginRequired();
	}*/
	
	// ������������� ������� ��� �������
	public function checkAccess($operation,$params=array(),$allowCaching=true)
	{
		//print_r($params);
		
		
		if($allowCaching && $params===array() && isset($this->_access[$operation]))
			return $this->_access[$operation];
		else
			return $this->_access[$operation]=Yii::app()->getAuthManager()->checkAccess($operation,$this->getId(),$params);
	}

    public function getEmailConfirm()
    {
        $oUA = UserAuthorization::model()->find('user_id = :user', array('user'=>$this->id));

        return $oUA->email_confirm;
    }

    public function getChatCounter()
    {
        if ($this->_chatCounter === null)
        {
            $criteria = new CDbCriteria;
            $criteria->condition='`t`.`userId`='.$this->id;
            $this->_chatCounter=Chat::model()->count($criteria);
        }
        return $this->_chatCounter;
    }
}