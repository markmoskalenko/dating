<?php

class EHttpRequest extends CHttpRequest
{
	public $csrfTokenName='CSRFTOKEN';
	
	/**
	 * @return string user IP address
	 */
	public function getUserHostAddress()
	{
		//return isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:'127.0.0.1';
		//return '178.215.113.95';
		return '94.50.163.100';
	}
	
	public function getDomain()
	{
		return str_replace("www.", "", getenv("HTTP_HOST"));
	}
	
	public function getIsAjaxRequest()
	{
		if (isset($_REQUEST['ajax'])){
			return true;
		}
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']==='XMLHttpRequest';
	}
	
	public function getIsMobile()
	{
		if (stripos(Yii::app()->request->userAgent,'android') !== false) {
			return true;
		}
		if (stripos(Yii::app()->request->userAgent,'ios') !== false) {
			return true;
		}
		return false;
	}
}