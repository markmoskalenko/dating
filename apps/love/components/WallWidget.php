<?php
Yii::import('zii.widgets.CPortlet');

class WallWidget extends CPortlet
{
	public $user_id;
	public $limit;
	
	protected function renderContent()
	{
		$wallForm=new Wall;
		//$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest && $_POST['Wall'])
		{
			$wallForm->attributes=$_POST['Wall'];
			if($wallForm->validate())
			{
				$wallForm->userId=$this->user_id;
				
				if ($wallForm->save(false))
				{
					if (Yii::app()->request->isAjaxRequest)
					{
						$html=$this->render('_wall', array(), true);
						echo CJSON::encode(array('html'=>$html));
						return;
					}
					//$this->redirect(array('profile/index', 'userId'=>$this->user->id));
				}
			}
			elseif (Yii::app()->request->isAjaxRequest)
			{
				echo CActiveForm::validate($wallForm);
				Yii::app()->end();
			}
			
		}
		
		
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`userId`=:userId',
			'params'=>array(':userId'=>$this->user_id),
			'with'=>array('user'=>array()),
		));
		
		$count = Wall::model()->count($criteria);
			
		$pages = new CPagination($count);
		$pages->pageSize=$this->limit;
		$pages->pageVar='wall';
		$pages->route='profile/index';
		$pages->params=array('userId'=>$this->user_id);
		$pages->applyLimit($criteria);
			
		$items=Wall::model()->findAll($criteria);
		
		$this->getJsWall();
		$this->render('wall', array('items'=>$items, 'pages'=>$pages, 'wallForm'=>$wallForm));		
	}
	
	public function getJsWall()
	{
$script=<<<JS

$("#wallForm").ajaxForm({
		dataType: 'json',
		type: "POST",
		cache: false,
		beforeSend: function(){
		   $("textarea").attr("value", "");
		},
		success: function(data){
		
		$("#errorSummary").hide().empty();
		
		if (data.html) {
		$("#content_wall").fadeTo('speed', 1.0);
			$("#content_wall").html(data.html);
			return;
		}
			
$.each(data, function(key, value) { 

  $("#errorSummary").prepend('<li>'+value+'</li>');

});
$("#errorSummary").fadeTo('speed', 1.0);
		}
		});

$("#wallPages a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		data: '&_a=wall',
		cache: false,
		beforeSend: function(){
		   
		},
		success: function(html){
			$("body").scrollTo( "#content_wall", 310 );
			$("#content_wall").html(html);
		}
	});
	return false;
});
JS;
		Yii::app()->clientScript->registerScript('wall', $script, CClientScript::POS_READY);
	}
}