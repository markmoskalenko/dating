<?php

class ProfileActiveForm extends CActiveForm
{
        public $typeForm;
	
	private $field_values=array();
        
	public function init()
	{
		parent::init();
		//$dependency=new CFileCacheDependency();
		foreach(ProfileFieldValues::model()->/*cache(3600)->*/with('field')->findAll(array('order'=>'`t`.`sort` DESC, `t`.`id` ASC')) as $row) {
			$this->field_values[$row->field->name][$row->value]=Yii::t('profile_field_values', $row->depending_on_sex==1 ? $row->field->name.'_'.$row->value.'_'.Yii::app()->user->sex : $row->field->name.'_'.$row->value);
		}
		//Yii::log(CVarDumper::dumpAsString($this->field_values));
	}
	
	public function checkBoxList($model,$attribute,$htmlOptions=array())
	{
		//Yii::log(CVarDumper::dumpAsString($this->field_values[$attribute]));
		return '<label class="checkbox">'.parent::checkBoxList($model, $attribute, $this->field_values[$attribute] ? $this->field_values[$attribute] : array(), $htmlOptions).'</label>';
	}
	
	public function dropDownList($model,$attribute,$htmlOptions=array())
	{
		return CHtml::activeDropDownList($model, $attribute, $this->field_values[$attribute] ? $this->field_values[$attribute] : array(), $htmlOptions);
	}
	
	public function radioButtonList($model,$attribute,$htmlOptions=array())
	{
		return '<label class="checkbox">'.parent::radioButtonList($model, $attribute, $this->field_values[$attribute] ? $this->field_values[$attribute] : array(), $htmlOptions).'</label>';
	}
	
	public function textField($model,$attribute,$htmlOptions=array())
	{
		if ($model->$attribute=='0') $model->$attribute='';
		return CHtml::activeTextField($model, $attribute, $htmlOptions);
	}
} 