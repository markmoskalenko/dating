<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='col_main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public $pageTitle;
	
	public function filters()
	{
		return array(
			//'accessControl',
			array(
				'AccessControlFilter',
				'rules'=>$this->accessRules(),
			)
		);
	}
	
	public function accessRules()
	{
		return array(
			/*array('deny',
			      'controllers'=>array('default', 'profile'),
			      //'actions'=>array('photo'),
			      //'users'=>array('?'),
			      'roles'=>array('userNoActivated'),
			),*/
		);
	}
	
	public function init()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$this->layout = FALSE;
		}
		
		if (!Yii::app()->user->isGuest && Yii::app()->controller->id!='edit' && Yii::app()->controller->id!='site')
		{
			if(!Yii::app()->user->model->name OR Yii::app()->user->model->birthday=='0000-00-00')
			{
				$this->redirect(array('edit/index'));
			}  
		}
		
	}

    public function beforeAction($action)
    {
        Yii::app()->user->returnUrl = Yii::app()->request->requestUri;

        if (isset($_REQUEST['authToken'])){
            // Аутентифицируем пользователя по имени и паролю
            $identity=new UserIdentity(null,null, $_REQUEST['authToken']);
            if($identity->authenticate())
                Yii::app()->user->login($identity);
            else
                echo $identity->errorMessage;
        }

        return true;
    }
}