<?php

class EClientScript extends CClientScript
{
	public function registerCssFile($url,$media='')
	{
		if (Yii::app()->request->isAjaxRequest)
			return;
			
		$this->hasScripts=true;
		$this->cssFiles[$url]=$media;
		$params=func_get_args();
		$this->recordCachingAction('clientScript','registerCssFile',$params);
		return $this;
	}

	public function registerCoreScript($name)
	{
		if (Yii::app()->request->isAjaxRequest)
			return;
		
		if(isset($this->coreScripts[$name]))
			return $this;
		if(isset($this->packages[$name]))
			$package=$this->packages[$name];
		else
		{
			if($this->corePackages===null)
				$this->corePackages=require(YII_PATH.'/web/js/packages.php');
			if(isset($this->corePackages[$name]))
				$package=$this->corePackages[$name];
		}
		if(isset($package))
		{
			if(!empty($package['depends']))
			{
				foreach($package['depends'] as $p)
					$this->registerCoreScript($p);
			}
			$this->coreScripts[$name]=$package;
			$this->hasScripts=true;
			$params=func_get_args();
			$this->recordCachingAction('clientScript','registerCoreScript',$params);
		}
		
		//echo print_r($this->coreScripts);
		
		return $this;
	}
}
