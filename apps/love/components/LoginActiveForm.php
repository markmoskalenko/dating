<?php

class LoginActiveForm extends CActiveForm
{
	private $_model;

	public function init()
	{
		parent::init();
		$this->_model=new LoginForm;
	}
	
	public function textField($attribute,$htmlOptions=array(),$backlitOptions=array())
	{
		CHtml::resolveNameID($this->_model,$attribute,$htmlOptions);
		if ($backlitOptions[value]) $this->backlit($htmlOptions[id],$backlitOptions);
		return parent::textField($this->_model, $attribute, $htmlOptions);
	}
	
	public function passwordField($attribute,$htmlOptions=array(),$backlitOptions=array())
	{
		CHtml::resolveNameID($this->_model,$attribute,$htmlOptions);
		if ($backlitOptions[value]) $this->backlit($htmlOptions[id],$backlitOptions);
		return parent::passwordField($this->_model, $attribute, $htmlOptions);
	}
	
	public function backlit($id,$backlitOptions=array())
	{
		$cs=Yii::app()->getClientScript();
		$script=<<<END
$("#$id").attr('value', '$backlitOptions[value]');
$("#$id").css('color', '$backlitOptions[blurColor]');
$("#$id").focus(function() {
	if ($(this).attr('value')=='$backlitOptions[value]') {
	$(this).attr('value', '');
	$(this).css('color', '$backlitOptions[focusColor]');
	}
});
$("#$id").blur(function() {
	if ($(this).attr('value')=='') {
	$(this).attr('value', '$backlitOptions[value]');
	$(this).css('color', '$backlitOptions[blurColor]');
	}
});
END;
		$cs->registerScript(__CLASS__.'#'.$id, $script);
	}
}