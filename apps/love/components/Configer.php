<?php

class Configer extends CComponent
{
	private $_model = null;

    public function init () {
	if (is_file(dirname(__FILE__).'/../../../themes/'.Yii::app()->theme->name.'/config.php'))
	Yii::app()->params=require(dirname(__FILE__).'/../../../themes/'.Yii::app()->theme->name.'/config.php');
          //$this->loadConfig();
         // $this->applyConfig();
    }

    private function loadConfig () {
       $this->_model = Configs::model()->findAll();

       foreach ($this->_model as $key) {
           Yii::app()->params[$key['name']] = $key['value'];
       }
    }

    private function applyConfig () {
        if (isset(Yii::app()->params['name']))
                Yii::app()->name = Yii::app()->params['name'];

        if (isset(Yii::app()->params['theme']))
                Yii::app()->theme = Yii::app()->params['theme'];
    }
}