<?php

class ThumbWidget extends CWidget
{
	public $width;
	public $height;
	public $model;
	
	public function run()
	{
		echo '<img src="'.ImageHelper::thumb($this->width, $this->height, $this->model->id, $this->model->photo, array('method' => 'adaptiveResize')).'" alt="" class="img-rounded1" style="margin-bottom:5px;" />';
	}


}