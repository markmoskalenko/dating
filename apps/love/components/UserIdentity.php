<?php

class UserIdentity extends CUserIdentity
{
    var $auth_encryption_key='ga4JWe32hFjEFNDbgf7Emv4JwsnWGFh5WG';
    
    private $_id;
    private $_name;
    private $_authToken;

    public function __construct($username = null,$password = null, $token = null)
    {
        if ($token){
            $this->_authToken = $token;
        } else {
            $this->username=$username;
            $this->password=$password;
        }
    }
    
    public function authenticate()
    {
        if ($this->_authToken){
            $record = UserAuthorization::getByAuthToken($this->_authToken);
        } else {
            $record = UserAuthorization::model()->with('pages')->find('(pages.name=:username OR t.phone=:username OR t.email=:username) AND t.hash_password=:password', array(':username'=>$this->username, ':password'=>UserAuthorization::hashPassword($this->password)));
        }

        
        //$record=User::model()->findByAttributes(array('login'=>$this->username));

       // echo $record->password;
        if($record===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($this->password && $record->hash_password!==UserAuthorization::hashPassword($this->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id=$record->user_id;
            $this->_name=$record->email;
            $this->setState('title', $this->_name);
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
    
    public function getName()
    {
       return $this->_name;
    }
}