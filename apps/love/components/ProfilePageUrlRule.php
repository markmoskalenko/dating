<?php

class ProfilePageUrlRule extends CBaseUrlRule
{
	public $connectionID='db';
	
	public function createUrl($manager, $route, $params, $ampersand)
	{
		if ($route=="profile/index")
			return $params['user'];
			
		return false;;
	}
	
	public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
	{
		if($page=UserPages::model()->find('name=?', array($pathInfo)))
		{
			$_GET['userId']=$page->user_id;
			return 'profile/index';
		}
		return false;
	}
} 