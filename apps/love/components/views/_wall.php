
<div class="col-action">
<?php foreach($this->getWall('10')->rows as $row):?>
					<div class="line-action">
						<a href="<?=CHtml::normalizeUrl(array('profile/index', 'userId'=>$row->user->id)); ?>"><img src="<?php echo $row->user->have_photo==1 ? ImageHelper::thumb(66,87, $row->user->id, $row->user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('question.png');?>" width="66" height="87" alt="" /></a>
						<div class="name-people">
							<a href="<?=CHtml::normalizeUrl(array('profile/index', 'userId'=>$row->user->id)); ?>"><?=$row->username ?></a><?php if ($row->user->isOnline) echo '<span style="color:#B9C1C1; margin-left:15px;font-size:11px;">Online</span>';?>
						</div>
						<div class="cell-action">
							<?=Html::message($row->message)?>
							<span class="date"><?php echo Html::date($row->date); ?> </span>							
						</div>
					</div>
<?php endforeach; ?>

<div class="pagination">
<?php $this->widget('bootstrap.widgets.TbPager', array(
        'id'=>'wallPages',
        'pages'=>$this->wall->pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	//'htmlOptions'=>array('class'=>'pages'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'>',
	'prevPageLabel'=>'<',
        'lastPageLabel'=>'>>',
	'firstPageLabel'=>'<<',
        ));
?>

</div>



</div>