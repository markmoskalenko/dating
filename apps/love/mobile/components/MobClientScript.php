<?php
/**
 * NLSClientScript v3.6
 * 
 * CClientScript extension for preventing multiple loading of javascript files.
 * Important! This extension does not prevent to load the same script content from different paths. 
 * So eg. if you published the same js file into different asset directories, this extension won't prevent to load both.
 * 
 * 
 * 
 * Usage: simply set the class for the clientScript component in /protected/config/main.php, like
 *  ...
 *   'components'=>array(
 *     ...
 *     'clientScript' => array('class'=>'your.path.to.NLSClientScript')
 *     ...
 *   )
 *  ...
 * 
 * The extension is based on the great idea of Eirik Hoem, see
 * http://www.eirikhoem.net/blog/2011/08/29/yii-framework-preventing-duplicate-jscss-includes-for-ajax-requests/
 * 
 */
class MobClientScript extends CClientScript {

/**
 * Applying global ajax post-filtering
 * original source: http://www.eirikhoem.net/blog/2011/08/29/yii-framework-preventing-duplicate-jscss-includes-for-ajax-requests/
*/
	public $processedPattern = '';
	public $ignoredPattern = '';
	
	private $_baseUrl;

	public function init() {

		parent::init();
		
		if (Yii::app()->request->isAjaxRequest)
			return;
		
		if (strpos(Yii::app()->request->userAgent,"MSIE",0))
			return;
		
		//we need jquery
		$this->registerCoreScript('jquery');
		
		//Minified code
		

//Source code:

		$this->registerScript('fixDuplicateResources', '

		$.ajaxSetup({
			global: true,
			cache: true,
			dataFilter: function(data,type) {
				
				//shortcut: only "text" and "html" dataType should be filtered
				if (type && type != "html" && type != "text")
					return data;

				' . ($this->processedPattern ? 'if (!this.url.match(' . $this->processedPattern . '))return data;' : '') . '
				' . ($this->ignoredPattern ? 'if (this.url.match(' . $this->ignoredPattern . '))return data;' : '') . '

				var tmp,i,res,ind,scripts,holder,firstScripts,firstTag,nextScript,resMap;
				
				//things to do only first time
				if (!$._nlsc) {

					$._nlsc = {
						resMap : {},
						holders : {
							"default" : $(document.createElement("div")),
							"tr" : $(document.createElement("tbody"))
						},
						selector : "script[src],link[rel=\"stylesheet\"]",
						preScriptRg : /^((<script[^>]*src=[^>]*\/?>(.*?<\/script>)?)|(<link[^>]*href=[^>]*\/?>(.*?<\/link>)?)|(\s*))+/i,
						simpleScriptRg : /<script/i,
						firstTagRg : /^<([a-z1-6]+)[^>]*>/i,
						urlGarbageRg : /([?&]+$)|(_=\d+)/g,
						wbrRg : /<wbr class="?nlsc"?\/?>/ig,
						head : document.getElementsByTagName("head")[0]
					};

					//fetching scripts from the DOM
					for(i=0,res=$(document).find($._nlsc.selector); i<res.length; i++) {
						tmp = res[i];
						ind = tmp.src ? tmp.src : tmp.href;
						ind = ind.replace($._nlsc.urlGarbageRg,"");
						if (tmp.href && tmp.parentNode!=$._nlsc.head)
							$._nlsc.head.appendChild(tmp);
						$._nlsc.resMap[ind] = 1;
					}

				}//if
				
				resMap = $._nlsc.resMap;

				//find linkings at the beginning...
				firstScripts = data.match($._nlsc.preScriptRg);//firstScripts[0] is at least ""
				//...and removing
				data = data.slice(firstScripts[0].length);
				firstScripts = "<wbr class=\"nlsc\"/>" + firstScripts[0];//tag fix needed for IE

				//finding the first inner script
				if (nextScript = data.match($._nlsc.simpleScriptRg)) {
					data = data.slice(0,nextScript.index) + firstScripts + data.slice(nextScript.index);
				} else {
					//...or to the end
					data += firstScripts;
				}

				
				//choosing holder
				if (firstTag = data.match($._nlsc.firstTagRg))
					holder = $._nlsc.holders[firstTag[1].toLowerCase()];
				if (!holder)
					holder = $._nlsc.holders["default"];
				holder[0].innerHTML = data;
				
				//console.log(data);console.log(holder[0].innerHTML);

				// iterate over new scripts and remove if source is already in DOM:
				for(i=0,res=holder.find($._nlsc.selector); i<res.length; i++) {
					tmp = res[i];
					ind = tmp.src ? tmp.src : tmp.href;
					ind = ind.replace($._nlsc.urlGarbageRg,"");
					if (resMap[ind])
						$(tmp).remove();
					else {
						if (tmp.href)
							$._nlsc.head.appendChild(tmp);
						else
							tmp.removeAttribute("defer");
						resMap[ind] = 1;
					}
				}
				
				//fixing possible chunking
				var testPos = data.toLowerCase().indexOf( holder[0].innerHTML.toLowerCase().slice(0,3) );
				return data.slice(0,testPos) + holder[0].innerHTML.replace($._nlsc.wbrRg,"");
			}
		});

	',	CClientScript::POS_HEAD);

		}

	public function getCoreScriptUrl()
	{
		return $this->_baseUrl=Yii::app()->getAssetManager()->publish(dirname(__FILE__).'/../resources');
	}
}
