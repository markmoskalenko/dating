<?php

class CMobileIplog extends CApplicationComponent
{
	
	public function init()
	{
		if(!Yii::app()->user->isGuest) {
			if (date("d.m.y", Yii::app()->user->model->last_date)!=date("d.m.y")) {
				Yii::app()->user->model->saveAttributes(array('day_visit'=>new CDbExpression('`day_visit`+1')));
			}
			
			if (Yii::app()->user->model->last_date<time()-30)
				Yii::app()->user->model->saveAttributes(array('last_date'=>time(), 'isMobile'=>1));
		}
        }
}