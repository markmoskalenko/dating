<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class MobileController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='col_main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public $pageTitle='Мобильные знакомства для взрослых';
	
	public $pjaxControl=true;
	
	public function filters()
	{
		return array(
			//'accessControl',
			array(
				'AccessControlFilter',
				'rules'=>$this->accessRules(),
			)
		);
	}
	
	public function accessRules()
	{
		return array(
			/*array('deny',
			      'controllers'=>array('default', 'profile'),
			      //'actions'=>array('photo'),
			      //'users'=>array('?'),
			      'roles'=>array('userNoActivated'),
			),*/
		);
	}
	
	public function init()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$this->layout = FALSE;
		}
		
		if (!Yii::app()->user->isGuest && Yii::app()->controller->id!='edit' && Yii::app()->controller->id!='site')
		{
			if(!Yii::app()->user->model->name OR Yii::app()->user->model->birthday=='0000-00-00')
			{
				$this->redirect(array('edit/index'));
			}  
		}
		
	}
	public function afterRender()
	{
		if (!Yii::app()->request->isPjaxRequest && Yii::app()->request->isAjaxRequest) return false;
		
		if (Yii::app()->request->isPjaxRequest) {
			header("Cache-Control: no-store, no-cache, must-revalidate");
		}
		
		$script='$("title").text("'.$this->pageTitle.'");';
		
		if (!Yii::app()->user->isGuest) {
			if (Yii::app()->user->newMessagesCounter) {
				$script.='$("#newMessagesCounter, #newMessagesCounter2").show();';
				$script.='$("#newMessagesCounter, #newMessagesCounter2").text("'.Yii::app()->user->newMessagesCounter.'");';
			} else {
				$script.='$("#newMessagesCounter, #newMessagesCounter2").hide();';
			}
			$script.='YCNoty.messagesCounter='.Yii::app()->user->newMessagesCounter.';';
			
			if (Yii::app()->user->requestFriendsCounter) {
				$script.='$("#requestFriendsCounter, #requestFriendsCounter2").show();';
				$script.='$("#requestFriendsCounter, #requestFriendsCounter2").text("'.Yii::app()->user->requestFriendsCounter.'");';
			} else {
				$script.='$("#requestFriendsCounter, #requestFriendsCounter2").hide();';
			}
			$script.='YCNoty.requestFriendsCounter='.Yii::app()->user->requestFriendsCounter.';';
			
			if (Yii::app()->user->model->notifications) {
				$script.='$("#notificationsCounter, #notificationsCounter2").show();';
				$script.='$("#notificationsCounter, #notificationsCounter2").text("'.Yii::app()->user->model->notifications.'");';
			} else {
				$script.='$("#notificationsCounter, #notificationsCounter2").hide();';
			}
			$script.='YCNoty.notificationsCounter='.Yii::app()->user->model->notifications.';';
		}
		Yii::app()->clientScript->registerScript('pjaxConf', 'jQuery(function($) { '.$script.' });', 0);
		
		if (Yii::app()->controller->id=='messages') Yii::app()->clientScript->registerScript('imConf', 'jQuery(function($) { YCNoty.mail=1; YCNoty.dialogueId="'.$_GET['id'].'"; });', 0);
		else Yii::app()->clientScript->registerScript('imConf', 'jQuery(function($) { YCNoty.mail=0; YCNoty.dialogueId="0"; });', 0);
	}
}