<?php

class UnsubscribeController extends Controller
{
	public $layout = 'col_main';
	
	public function actions()
	{
		return array(
                    //  captcha.
                     'captcha'=>array(
                        'class'=>'CaptchaAction',
                    )
                );
	}
	
	public function actionIndex()
	{
		$model=new UnsubscribeForm;
		$model->setScenario('support');
		if(isset($_POST['UnsubscribeForm']))
		{
			$model->attributes=$_POST['UnsubscribeForm'];
			if($model->validate())
			{
				Yii::app()->user->setFlash('ok','1');
				$this->refresh();
			}
		}
		$this->render('index',array('model'=>$model));
	}
}