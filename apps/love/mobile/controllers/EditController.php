<?php

class EditController extends MobileController
{
	public $layout='col_main';
	
	public function accessRules()
	{
		return array(
			array('deny',
			      'roles'=>array('guest'),
			),
		);
	}
	
	public function init()
	{
		parent::init();
		$this->menu=array(
			array('label'=>'Основное', 'url'=>array('edit/index'), 'itemOptions'=>array()),
			array('label'=>'Интересы', 'url'=>array('edit/profile', 'section'=>'personal'), 'itemOptions'=>array()),
			array('label'=>'Знакомства', 'url'=>array('edit/profile', 'section'=>'meet'), 'itemOptions'=>array()),
			array('label'=>'Типаж', 'url'=>array('edit/profile', 'section'=>'type'), 'itemOptions'=>array()),
			array('label'=>'Обо мне', 'url'=>array('edit/profile', 'section'=>'aboutme'), 'itemOptions'=>array()),
			array('label'=>'Сексуальные предпочтения', 'url'=>array('edit/profile', 'section'=>'sex'), 'itemOptions'=>array()),
		);
	}
	
	public function actionIndex()
	{
		$model=User::model()->findByPk(Yii::app()->user->id);
		
		$this->pageTitle='Редактировать профиль';
		$this->breadcrumbs=array(
			'Редактировать профиль'
			//'Общее',
		);
		
		$model->setScenario('edit');
		$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['User'];
			
			if($model->validate())
			{
				if ($model->save()) {
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						//$this->refresh();
						$this->redirect(['site/index']);
					}
				}
			}
		}
		

		
		$this->render('index', array('model'=>$model));
	}
	
	public function actionProfile()
	{
		$form_section=ProfileFieldSection::model()->find(array(
			'condition'=>'name=:name',
			'params'=>array('name'=>Yii::app()->request->getQuery('section', 'main')),
		));
		
		$this->pageTitle='Редактировать '.Yii::t('profile_field', 'Section_'.$form_section->name);
		$this->breadcrumbs=array(
			'Редактировать профиль'=>array('edit/index'),
			Yii::t('profile_field', 'Section_'.$form_section->name),
		);
		
		
		$model=Profiles::model()->findByPk(Yii::app()->user->id);

		$model->setScenario($form_section->scenario);
		$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest)
		{
			$model->attributes=$_POST['Profiles'];
			
			if($model->validate())
			{
				if ($model->save()) {
					if (Yii::app()->request->isAjaxRequest) {
						echo CJSON::encode(array('ok'=>'1'));
						return;
					} else {
						Yii::app()->user->setFlash('edit','Изменения сохранены.');
						$this->refresh();
					}
				}
			}
		}
		
		$forms=ProfileField::model()->findAll(array(
			'condition'=>'section_id=:section_id AND (certain_sex=0 OR certain_sex=:sex)',
			'params'=>array('section_id'=>$form_section->id, ':sex'=>Yii::app()->user->sex),
			'order'=>'sort DESC, id ASC'
		));
		
		foreach(ProfileFieldValues::model()->with('field')->findAll(array('order'=>'`t`.`sort` DESC, `t`.`id` ASC')) as $row) {
			$field_values[$row->field->name][$row->value]=Yii::t('profile_field_values', $row->depending_on_sex==1 ? $row->field->name.'_'.$row->value.'_'.Yii::app()->user->sex : $row->field->name.'_'.$row->value);
		}
		
		$this->render('profile', array('model'=>$model, 'forms'=>$forms, 'field_values'=>$field_values));
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']))
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}