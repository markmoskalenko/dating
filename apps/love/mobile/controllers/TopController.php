<?php

class TopController extends MobileController
{
	public $layout='col_main';

	public function accessRules()
	{
		return array(
			array('deny',
			      'actions'=>array('index'),
			      'users'=>array('?'),
			),
			array('deny',
			      'actions'=>array('photos'),
			      'roles'=>array('userUnreal'),
			)
		);
	}
	
	public function actionIndex()
	{
			$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`have_photo`=1',
			//'params'=>array(':user_id'=>$this->profile->id),
				'with'=>array(
					'country',
					'region',
					'city'
				),
				'order'=>'rand()',
				'limit'=>Yii::app()->params['topUsers_limit'],
				
			));
			$criteria->addCondition('`t`.`sex`='.intval(Yii::app()->user->model->sex==1 ? 2 : 1));
			$profiles=User::model()->findAll($criteria);
			$this->render('index', array('profiles'=>$profiles));
	}
	
	public function actionPhotos()
	{
		
		if ($_GET['photoId'])
		{
		
			$criteria=new CDbCriteria;
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`likes`!=0',
			//'condition'=>'`t`.`id`=:id',
		//'params'=>array(':id'=>Yii::app()->request->getQuery('photoId')),
			'order'=>'t.likes DESC, t.id DESC',
		));
			
		$count = Photos::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['topPhotos_limit'];
		$pages->applyLimit($criteria);
			
		$photos=Photos::model()->findAll($criteria);
			foreach ($photos as $key=>$row) {
				
				$p = new stdClass();
				$p->id=$row->id;
				$p->user_id=$row->user_id;
				$p->filename=$row->filename;
				$p->descr=$row->descr;
				$p->width=$row->width;
				$p->height=$row->height;
				$p->likes=$row->likes;
				$p->date=Html::date($row->date);
				$p->image=$row->imageSource;
				$p->commentsUrl=CHtml::normalizeUrl(array('photos/comments', 'id'=>$row->id));
				$p->likeUrl=CHtml::normalizeUrl(array('photos/like', 'id'=>$row->id));
				$photos_all[]=$p;
			}
			
			foreach ($photos as $key=>$row) {
				$nums[$row->id]=array('key'=>$key);
			}
		
			$data= CJSON::encode(array(
				'photoId' => Yii::app()->request->getQuery('photoId'),
				'photos' => $photos_all,
				'nums' => $nums,
				'count' => $count,
				'limit' => $pages->pageSize,
			));
			
			if ($_GET['preloading']) {
				$this->layout=false;
				echo $data;
				Yii::app()->end();
			} else {
			
		$script="jQuery(function($) {  photoShow.data='".$data."';photoShow.init(); });";
		Yii::app()->clientScript->registerScript('pjaxPhotoDataJson', $script, 0);
		
		$this->render('show', array('photo'=>$photo));
		Yii::app()->end();
			}
			

		}
		
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`likes`!=0',
			//'condition'=>'`t`.`id`=:id',
		//'params'=>array(':id'=>Yii::app()->request->getQuery('photoId')),
			'order'=>'t.likes DESC, t.id DESC',
		));
		$criteria->with='user';
		
		$count = Photos::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['topPhotos_limit'];
		$pages->applyLimit($criteria);
			
		$photos=Photos::model()->findAll($criteria);
		
		if (Yii::app()->request->isAjaxRequest && $_GET['_a']=='photo') {
			$this->render('_photosRows', array('photos'=>$photos, 'pages'=>$pages));
		} else {
			$this->render('photos', array('photos'=>$photos, 'pages'=>$pages));
		}
		
		
	}
	
	public function actionBestphotos()
	{
		
		if (Yii::app()->request->isAjaxRequest && !$_GET['_a'])
		{
		$this->layout=false;
			$criteria=new CDbCriteria;
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`likes`!=0',
			//'condition'=>'`t`.`id`=:id',
		//'params'=>array(':id'=>Yii::app()->request->getQuery('photoId')),
			'order'=>'t.likes DESC, t.id DESC',
		));
			
		$count = Photos::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['topPhotos_limit'];
		$pages->applyLimit($criteria);
			
			
			$photos=Photos::model()->findAll($criteria);
			foreach ($photos as $key=>$row) {
				
				$p = new stdClass();
				$p->id=$row->id;
				$p->user_id=$row->user_id;
				$p->filename=$row->filename;
				$p->descr=$row->descr;
				$p->width=$row->width;
				$p->height=$row->height;
				$p->likes=$row->likes;
				$p->date=Html::date($row->date);
				$p->image=$row->imageSource;
				$p->commentsUrl=CHtml::normalizeUrl(array('photos/comments', 'id'=>$row->id));
				$p->likeUrl=CHtml::normalizeUrl(array('photos/like', 'id'=>$row->id));
				$photos_all[]=$p;
			}
			
			foreach ($photos as $key=>$row) {
				$nums[$row->id]=array('key'=>$key);
			}
		
			echo CJSON::encode(array(
				'photoId' => Yii::app()->request->getQuery('photoId'),
				'photos' => $photos_all,
				'nums' => $nums,
				'count' => $count,
				'limit' => $pages->pageSize,
			));
			Yii::app()->end();
		}
		
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`likes`!=0',
			//'condition'=>'`t`.`id`=:id',
		//'params'=>array(':id'=>Yii::app()->request->getQuery('photoId')),
			'order'=>'t.likes DESC, t.id DESC',
		));
		$criteria->with='user';
		
		$count = Photos::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['topPhotos_limit'];
		$pages->applyLimit($criteria);
			
		$photos=Photos::model()->findAll($criteria);
		
		if (Yii::app()->request->isAjaxRequest && $_GET['_a']=='photo') {
			$this->render('_photosRows', array('photos'=>$photos, 'pages'=>$pages));
		} else {
			$this->render('bestphotos', array('photos'=>$photos, 'pages'=>$pages));
		}
		
		
	}
}