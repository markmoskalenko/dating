<?php

class SiteController extends MobileController
{
	public $layout='col_main';

	public function actions()
	{
		return array(
                    //  captcha.
                     'captcha'=>array(
                        'class'=>'CaptchaAction',
                        'maxLength'=> 3,
                        'minLength'=> 4,
                        'testLimit'=> 1,
                    )
                );
	}

	public function actionIndex()
	{
		if (Yii::app()->user->isGuest)
		{
			$criteria=new CDbCriteria(array(
			//'condition'=>'`t`.`user_id`=:user_id',
			//'params'=>array(':user_id'=>$this->profile->id),
				'with'=>array(
					'profile'=>array(),
					//'position'=>array(),
					'country',
					'region',
					'city'
				),
				'order'=>'rand()',
				'limit'=>Yii::app()->params['mainRandomUsers_limit'],
				
			));
			$profiles=User::model()->findAll($criteria);
			$this->render('index', array('profiles'=>$profiles));
		}
		else {
			Yii::app()->runController('profile/index'); 
		}
	}
	
	public function actionNav()
	{
		$this->render('nav', array());
	}
	
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
	
	public function actionGeo()
	{
		$data=array();
		if ($_GET['country_id']) {
			foreach (GEORegion::model()->findAll(array('condition'=>'id_country=:id_country', 'params'=>array(':id_country'=>$_GET['country_id']))) as $row) {
				$data[]=array('id_region'=>$row->id_region, 'name'=>$row->name);
			}
			echo CJSON::encode($data);
		} elseif ($_GET['region_id']) {
			foreach (GEOCity::model()->findAll(array('condition'=>'id_region=:id_region', 'params'=>array(':id_region'=>$_GET['region_id']))) as $row) {
				$data[]=array('id_city'=>$row->id_city, 'name'=>$row->name);
			}
			echo CJSON::encode($data);
		}	
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		if (Yii::app()->request->isPjaxRequest) {
			Yii::app()->clientScript->registerScript('pjaxConfLogout', 'jQuery(function($) { location.href="'.Yii::app()->homeUrl.'" });', 0);
			Yii::app()->end();
		}
		$this->redirect(Yii::app()->homeUrl);
	}
}