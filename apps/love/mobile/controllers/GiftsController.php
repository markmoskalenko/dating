<?php

class GiftsController extends MobileController
{
	public function accessRules()
	{
		return array(
			array('deny',
			      'actions'=>array('donate'),
			      'users'=>array('?'),
			),
		);
	}
	
	public function actionIndex()
	{
			$criteria=new CDbCriteria(array(
				'condition'=>'`t`.`userId`=:userId',
				'params'=>array(':userId'=>$_GET['userId']),
			));

			$pages = new CPagination($count);
			$pages->pageSize=10;
			$pages->params=array('userId'=>$_GET['userId']);
			$pages->applyLimit($criteria);
			
		$criteria->with=array('user');
		$criteria->order='t.id DESC';
		$gifts=GiftsUsers::model()->findAll($criteria);
		
		if (Yii::app()->request->isAjaxRequest) {
			$this->render('_row',array('gifts'=>$gifts, 'pages'=>$pages, 'count'=>$count));
		} else {
			$this->render('index',array('gifts'=>$gifts, 'pages'=>$pages, 'count'=>$count));
		}
	}
	
	public function actionDonate()
	{
		$user=User::model()->findByPk($_GET['id']);
		
		if($user===null)
			throw new CHttpException(404,'Contact not found.');
		
		if($user->id==Yii::app()->user->id)
			throw new CHttpException(404,'It is impossible to write a message.');
		
		$model=new GiftsUsers;
		//$this->performAjaxValidation($model);
		if(Yii::app()->request->isPostRequest && $_POST['GiftsUsers'])
		{
			$model->attributes=$_POST['GiftsUsers'];
			if($model->validate())
			{
				$model->userId=$user->id;
				if ($model->save(false))
				{
					Notifications::add(Notifications::SENT_GIFT, array(
						'userId'=>$user->id,
						'giftId'=>$model->giftId,
					));
					
					if (Yii::app()->request->isAjaxRequest)
					{
						echo CJSON::encode(array('status'=>1));
						return;
					}
				}
			}
			elseif (Yii::app()->request->isAjaxRequest)
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

		}
		
		$criteria=new CDbCriteria(array(
			'order'=>'id DESC',
		));
		
		$gifts=Gifts::model()->findAll($criteria);
		
		$this->render('donate',array('gifts'=>$gifts, 'model'=>$model, 'user'=>$user));
	}
}