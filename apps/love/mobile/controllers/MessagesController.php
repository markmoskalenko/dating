<?php

class MessagesController extends MobileController
{
	public $layout='col_main';
	
	public function accessRules()
	{
		return array(
			array('deny',
			      'actions'=>array('index', 'dialogue'),
			      'roles'=>array('guest'),
			),
		);
	}
	
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest && is_array($_POST['to_message']))
		{
			foreach($_POST['to_message'] as $item) {
				Messages::model()->deleteByPk($item, 'from_userId='.Yii::app()->user->id);
			}
			
			if (Yii::app()->request->isAjaxRequest) {
				echo CJSON::encode(array());
				return;
			}
		}
		
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`from_userId`=:user_id AND `t`.`folder`=:folder',
			'params'=>array(':user_id'=>Yii::app()->user->id, ':folder'=>$_GET['folder']=='outbox' ? 'outbox' : 'inbox'),
			'with'=>array('user'=>array()),
			'order'=>'t.time DESC',
				
		));
		
		$count = Messages::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['messages_limit'];
		$pages->applyLimit($criteria);
		
		$messages=Messages::model()->findAll($criteria);
		
		if (Yii::app()->request->isAjaxRequest && $_GET['_a']=='container_mail') {
			$this->render('_index', array('messages'=>$messages, 'pages'=>$pages, 'count'=>$count));
		} else {
			$this->render('index', array('messages'=>$messages, 'pages'=>$pages, 'count'=>$count));
		}
	}
	
	public function actionDialogue()
	{
		if (Yii::app()->request->isPostRequest && is_array($_POST['to_message']))
		{
			foreach($_POST['to_message'] as $item) {
				Messages::model()->deleteByPk($item, 'from_userId='.Yii::app()->user->id);
			}
			
			if (Yii::app()->request->isAjaxRequest) {
				echo CJSON::encode(array());
				return;
			}
		}
		
		$user=User::model()->findByPk($_GET['id']);
		
		if($user===null)
			throw new CHttpException(404,'Contact not found.');
		
		if($user->id==Yii::app()->user->id)
			throw new CHttpException(404,'It is impossible to write a message.');
		
		
		$model=new Messages;
		
		if(Yii::app()->request->isPostRequest && !Yii::app()->user->isGuest)
		{
			$model->attributes=$_POST['Messages'];
			if($model->validate())
			{
				$model->from_userId = $user->id;
				$model->to_userId = Yii::app()->user->id;
				$model->unread = 1;
				$model->folder = 'inbox';
				$model->time = time();
				
				if ($model->save(false))
				{
					$model=new Messages;
					$model->attributes=$_POST['Messages'];
					$model->from_userId = Yii::app()->user->id;
					$model->to_userId = $user->id;
					$model->folder = 'outbox';
					$model->unread = 0;
					$model->time = time();
					if ($model->save())
					{
						if (Yii::app()->request->isAjaxRequest)
						{
							echo CJSON::encode(array('html'=>$this->getDialogue($_GET['id'])));
							return;
						}
						Yii::app()->user->setFlash('sent','to');
						$this->refresh();
					}
				}
			}
			elseif (Yii::app()->request->isAjaxRequest)
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
		
		if (Yii::app()->request->isAjaxRequest && $_GET['_a']=='container_mail') {
			echo $this->getDialogue($user->id);
		} else {
			$this->render('dialogue', array('user'=>$user, 'model'=>$model));
		}
	}
	
	public function getDialogue($id)
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'`t`.`from_userId`=:user_id AND `t`.`to_userId`=:to_userId',
			'params'=>array(':user_id'=>Yii::app()->user->id, ':to_userId'=>$id),
			'with'=>array('user'=>array()),
			'order'=>'t.time DESC',
				
		));
		
		$count = Messages::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=Yii::app()->params['messages_limit'];
		$pages->applyLimit($criteria);
		
		$messages=Messages::model()->findAll($criteria);
		
		foreach($messages as $row) {
			if ($row->unread)
				$row->saveAttributes(array('unread'=>0));
		}

		return $this->renderPartial('_dialogue', array(
			'messages'=>$messages,
			'pages'=>$pages,
			'count'=>$count)
		, true);
	}
	
	public function actionSend()
	{
		$user=User::model()->findByPk($_GET['id']);
		
		if($user===null)
			throw new CHttpException(404,'Contact not found.');
		
		if($user->id==Yii::app()->user->id)
			throw new CHttpException(404,'It is impossible to write a message.');
		
		$model=new Messages;
		
		if(Yii::app()->request->isPostRequest && !Yii::app()->user->isGuest)
		{
			$model->attributes=$_POST['Messages'];
			if($model->validate())
			{
				
				$model->from_userId = $user->id;
				$model->to_userId = Yii::app()->user->id;
				$model->unread = 1;
				$model->folder = 'inbox';
				$model->time = time();
				
				if ($model->save(false))
				{
					$model=new Messages;
					$model->attributes=$_POST['Messages'];
					$model->from_userId = Yii::app()->user->id;
					$model->to_userId = $user->id;
					$model->folder = 'outbox';
					$model->unread = 0;
					$model->time = time();
					if ($model->save())
					{
						if (Yii::app()->request->isAjaxRequest)
						{
							echo CJSON::encode(array('ok'=>1));
							Yii::app()->end();
						}
						Yii::app()->user->setFlash('sent','to');
						$this->refresh();
					}
				}
			}
			elseif (Yii::app()->request->isAjaxRequest)
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
		
		$this->render('send', array('model'=>$model, 'user'=>$user));
	}
}