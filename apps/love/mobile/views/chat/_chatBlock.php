<?php foreach(Chat::model()->getChat() as $key=>$row): ?>
				<div class="message" id="chat_<?php echo $row->id;?>">
					<div class="head">
						<span class="name left"><a href="<?=$row->user->url; ?>"><?=$row->user->name?></a></span>
						<span class="new-msg right">&nbsp;</span>
					</div>
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'), '', array('class'=>'left')
		), 
		$row->user->url
	);
endif;
?>
					<p>
						<?php echo $row->message?>
					</p>
					<div class="foot">
						<a href="#" class="left" onclick="$('#sendChatModal').arcticmodal();return false;">Ответить</a>
						<span class="short-data right"><?php if (Html::age($row->user->birthday)):?><?=Html::age($row->user->birthday)?><?php endif;?> г. <?=$row->user->geo->city?><?php if (Yii::app()->user->checkAccess('deleteComment', array('author_id'=>$row->user->id, 'own_id'=>$row->user->id))):?><a class="delete" title="<?=$row->user->id?>" rel="tooltip" href="#" onclick="deleteChat('<?=CHtml::normalizeUrl(array('chat/index', 'deleteMessageId'=>$row->id)); ?>'); return false;"><i class="fa fa-times"></i></a><?php endif; ?></span>
						
					</div>	
				</div>
<?php endforeach; ?>