                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Служба поддержки пользователей</h3>
                        </div>      
                    </div>
			

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php //echo Yii::app()->user->getFlash('contact'); ?>
	
</div>

<?php else: ?>


<p>Благодарим Вас за обращение к нам. Мы постараемся ответить вам как можно скорее.</p>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'supportForm',
    'htmlOptions'=>array('class'=>'reg-form'),

)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php //echo $form->errorSummary($model); ?>

<div class="form-group">
<?php echo $form->labelEx($model,'name', array('label' => 'Ваше имя', 'class'=>'control-label')); ?>
<?php echo $form->textField($model,'name',array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'name',array('class'=>'text-danger')); ?>
</div>
<div class="form-group">
<?php echo $form->labelEx($model,'email', array('label' => 'Ваш E-mail', 'class'=>'control-label')); ?>
<?php echo $form->textField($model,'email',array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'email',array('class'=>'text-danger')); ?>
</div>
<div class="form-group">
<?php echo $form->labelEx($model,'subject', array('label' => 'Тема сообщения', 'class'=>'control-label')); ?>
<?php echo $form->textField($model,'subject',array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'subject',array('class'=>'text-danger')); ?>
</div>
<div class="form-group">
<?php echo $form->labelEx($model,'body', array('label' => 'Сообщение', 'class'=>'control-label')); ?>
<?php echo $form->textArea($model,'body',array('class'=>'form-control input-sm', 'style'=>'height:130px;')); ?>
<?php echo $form->error($model,'body',array('class'=>'text-danger')); ?>
</div>


<div class="form-group">
					<div class="cupcha left">
						<?php echo $form->labelEx($model,'verifyCode', array('label' => 'Введите код', 'class'=>'control-label')); ?>
						<div class="cupcha-box txt-right">
							<?php $this->widget('CCaptcha', array('captchaAction' => 'user/captcha', 'clickableImage'=>true, 'showRefreshButton'=>false, 'imageOptions'=>array('style'=>'cursor:pointer;border-radius:4px;', 'id'=>'captcha'))); ?>
						</div>	
					</div>	
					<?php echo $form->textField($model,'verifyCode',array('class'=>'form-control input-sm', 'style'=>'width:100px;')); ?>
</div>
				<?php echo $form->error($model,'verifyCode',array('class'=>'text-danger')); ?>

<div class="form-group">
	<input type="submit" value="Отправить" class="btn btn-primary btn-block btn-lg" />
</div>	
<?php $this->endWidget(); ?>
<?php endif; ?>