            <div class="title row">
                <div class="col-md-12">
                    <h3 class="pull-left"><?php if ($count):?><?=Yii::t('app','{n} person found|Found {n} people|Found {n} people', $count)?><? else:?><?=Yii::t('app','Your request returned no results')?>.<? endif;?></h3>
                </div>    
            </div>
            <div class="participants row">
		
<?php foreach($profiles as $num=>$row): ?>

                <div class="col-xs-4 col-sm-3 col-md-3">
                    <div class="user">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(104,138, 'camera_a.png'),
			'',
			array('class'=>'img-responsive')
		), 
		$row->url,
		array()
	);
?>                 
                        <span class="name"><?php
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('class'=>'name-user')
	);
?></span>
                        <span class="city"><?php echo $row->geo->city?></span>
                        <span class="age"><?php echo Html::age($row->birthday)?></span>
                        <?php if($row->isOnline && !$row->isMobile):?><label class="online">&nbsp;</label>
			<?php elseif($row->isOnline && $row->isMobile):?><label class="mobile">&nbsp;</label><?php endif;?>
                        <a class="add-friend" href="<?php echo CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$row->id)); ?>"><i class="fa fa-envelope"></i>Сообщение</a>
                    </div>
                </div>   


<?php endforeach;?>

</div>
<div class="text-center">
<?php $this->widget('ext.bootstrap3.widgets.BsPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>