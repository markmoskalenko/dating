<?php $script=<<<JS

$("#searchForm").submit(function() {
$.ajax({
			type: "GET",
			url: $(this).attr('action'),
			data: $(this).serialize()+'&_a=getQuickSearch',
			cache: false,
			beforeSend: function(){
				$('#getQuickSearch').fadeTo(0, 0.3);
			},
			success: function(html){
				$('#getQuickSearch').fadeTo(0, 1.0);
				
				$("#getQuickSearch").html(html);
			}
		});
	return false;
});
/*$(document).on('submit', '#searchForm', function(event) {
  //$.pjax.click(event, '#maincol');
  
      $.pjax({
        timeout: 2000,
        url: $(this).attr('action')+'?'+$(this).serialize(),
        container: '#maincol'
      });
  
	return false;
});*/
    
   
JS;
Yii::app()->clientScript->registerScript('statistic', $script, CClientScript::POS_READY);?>
<?php $this->pageTitle='Поиск';?>
<?php
$this->breadcrumbs=array('Поиск');
?>

<?php $this->renderPartial('_searchForm', array()); ?>

<?php if (!Yii::app()->user->isGuest):?>
<div class="text-center adv-pickup">
В поиске ваша страница находится на <strong id="position"><?php echo Yii::app()->user->position; ?></strong> месте.<br>Первый в поиске – это уникальная возможность привлечь к себе внимание и завести новые знакомства.
<div><a href="<?=CHtml::normalizeUrl(array('profile/pickup')); ?>" class="btn btn-primary btn-lg"><i class="fa fa-level-up"></i>Стать первым</a></div>
</div>		
<?php endif;?>
					
<div id="getQuickSearch">
<?php echo $this->getQuickSearch(); ?>
</div>