                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Поиск секс партнера</h3>
                        </div>      
                    </div>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'searchForm',
    'method'=>'GET',
    'htmlOptions'=>array('class'=>'search-partner'),
    'action'=>CHtml::normalizeUrl(array('search/index'))
)); ?>

<div class="form-group">
					<label class="control-label">
						Ищу
					</label>
					<?php echo CHtml::dropDownList('c[sex]', $_GET['c']['sex'], array('2'=>'Девушку', '1'=>'Парня'), array('class'=>'form-control input-sm', 'empty'=>'---'));?>	
</div>	
<div class="form-group">
					<label class="control-label">
						В возрасте
					</label>
					<?php echo CHtml::dropDownList('c[age_from]', $_GET['c']['age_from'], User::getRangedown(18,75), array('class'=>'form-control input-sm', 'empty'=>'---'));?>
					<label class="control-label">
						до
					</label>
					<?php echo CHtml::dropDownList('c[age_to]', $_GET['c']['age_to'], User::getRangedown(18,75), array('class'=>'form-control input-sm', 'empty'=>'---'));?>
</div>
<div class="form-group">
<label class="control-label">
	Страна:
</label>	
<?php echo CHtml::dropDownList('c[country_id]', $_GET['c']['country_id'], CHtml::listData(GEOCountry::model()->findAll(), 'id_country', 'name'), array('class'=>'form-control input-sm', 'empty'=>'Выберите страну'));?>

<label class="control-label">
	Регион:
</label>	
<?php echo CHtml::dropDownList('c[region_id]', $_GET['c']['region_id'], CHtml::listData(GEORegion::model()->findAll('id_country=:id_country', array('id_country'=>$_GET['c']['country_id'])), 'id_region', 'name'), array('class'=>'form-control input-sm', 'empty'=>'Выберите регион'));?>

<label class="control-label">
	Город:
</label>	
<?php echo CHtml::dropDownList('c[city_id]', $_GET['c']['city_id'], CHtml::listData(GEOCity::model()->findAll('id_region=:id_region AND id_country=:id_country', array('id_region'=>$_GET['c']['region_id'], 'id_country'=>$_GET['c']['country_id'])), 'id_city', 'name'), array('class'=>'form-control input-sm', 'empty'=>'Выберите город'));?>
</div>
<div class="btn-group1 btn-search-form" data-toggle="buttons">
  <label class="btn btn-default btn-sm <?php echo $_GET['c']['online'] ? 'active' : ''?>" for="nowOnly">
    <?php echo CHtml::checkBox('c[online]', $_GET['c']['online'], array('id'=>'nowOnly')); ?> Online
  </label>
    <label class="btn btn-default btn-sm <?php echo $_GET['c']['photo'] ? 'active' : ''?>" for="photo">
	<?php echo CHtml::checkBox('c[photo]', $_GET['c']['photo'], array('id'=>'photo')); ?> C фото
  </label>
    <label class="btn btn-default btn-sm <?php echo $_GET['c']['new'] ? 'active' : ''?>" for="newFaces">
    <?php echo CHtml::checkBox('c[new]', $_GET['c']['new'], array('id'=>'newFaces')); ?> новые лица
  </label>
  <label class="btn btn-default btn-sm <?php echo $_GET['c']['real'] ? 'active' : ''?>" for="realStatus">
    <?php echo CHtml::checkBox('c[real]', $_GET['c']['real'], array('id'=>'realStatus')); ?> с Real
  </label>
</div>

	
	
				<input type="submit" value="ИСКАТЬ" class="btn btn-primary btn-block btn-lg" />

<?php $this->endWidget(); ?>

<?php

		$cs=Yii::app()->getClientScript();
		$jsonurl=CHtml::normalizeUrl(array('site/geo'));
		$script=<<<END

$("select#c_country_id").change(function() {

        id=$(this).val();
	if (!id) {
		$("select#c_region_id").html('<option value="" selected="selected">Выберите регион</option>'); 
			$("select#c_region_id").change();
		return;
	}
	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "country_id="+id,
		cache: false,
		beforeSend: function(){
			$("#loading_result").show();
		},
		success: function(data) {
			$("#loading_result").hide();
                        var setoptions = '<option value="" selected="selected">Выберите регион</option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_region + '">' + data[i].name + '</option>';
				}
			}
			
			$("select#c_region_id").html(setoptions);
			/*$("select#region_id").select2({
				placeholder: "Выберите регион",
				allowClear: true
			});*/
			$("select#c_region_id").change();
                }  
	});
});

$("select#c_region_id").change(function() {
        id=$(this).val();
	if (!id) {
		$("select#c_city_id").html('<option value="" selected="selected">Выберите город</option>'); 
		return;
	}
	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "region_id="+id,
		cache: false,
		beforeSend: function(){
                       $("#loading_result").show();
		},
		success: function(data) {
			$("#loading_result").hide();
			var setoptions = '<option value="" selected="selected">Выберите город</option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_city + '">' + data[i].name + '</option>';
				}
			}
                        $("select#c_city_id").html(setoptions); 
			
			/*$("select#city_id").select2({
				placeholder: "Выберите город",
				allowClear: true
			});*/
                }  
	});
});

END;
		$cs->registerScript(__CLASS__.'#geo1', $script);
?>