<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));  ?>
<?php $script=<<<JS
$("#searchForm").change(function() {
	$("#searchForm").submit();
});
    
$("#searchForm").submit(function() {
        
$.ajax({
			type: "GET",
			url: $(this).attr('action'),
			data: $(this).serialize()+'&_a=getQuickSearch',
			cache: false,
			beforeSend: function(){
			   $("#loading_result").show();
			},
			success: function(html){
				$("#getQuickSearch").html(html);
				$("#loading_result").hide();
			}
		});
	return false;
});
    
JS;
Yii::app()->clientScript->registerScript('statistic', $script, CClientScript::POS_READY);?>
<?php $this->pageTitle='Поиск';?>
<?php
$this->breadcrumbs=array('Поиск');
?>

			<div class="title">
				<h3 class="left">Поиск секс партнера</h3>
				<a href="#" class="right extended-link">Расширенный поиск</a>
			</div>	
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'searchForm',
    'method'=>'GET',
    'htmlOptions'=>array('class'=>'search-partner'),
    'action'=>CHtml::normalizeUrl(array('search/index'))
)); ?>
				<div class="inputs">
					<label class="inline">
						Я
					</label>	
					<select class="short left">
						<option>Девушка</option>
						<option>Парень</option>
					</select>
					<label class="inline">
						Ищу
					</label>
<?php echo CHtml::dropDownList('c[sex]', $_GET['c']['sex'], array('2'=>'Девушку', '1'=>'Парня'), array('class'=>'short left', 'empty'=>'---'));?>	
					<label class="inline">
						В возрасте
					</label>
					<input type="text" value="" class="txt-field short left" />
					<label class="inline">
						до
					</label>
					<input type="text" value="" class="txt-field short left" />
				</div>	
				<div class="inputs special-inputs">
					<span>Отобрать только:</span>
					
					<input type="checkbox" value="" id="withPhoto">
					<label for="withPhoto">с фото</label>

					<input type="checkbox" value="" id="newFaces">
					<label for="newFaces">новые лица</label>

					<input type="checkbox" value="" id="nowOnly">
					<label for="nowOnly">сейчас на сайте</label>

					<input type="checkbox" value="" id="nearYou">
					<label for="nearYou">рядом с вами</label>

					<input type="checkbox" value="" id="realStatus">
					<label for="realStatus">с Real - статусом</label>
				</div>	
				<div class="inputs">
					<div class="col">
						<label class="inline">
							Страна:
						</label>	
						<select>
							<option>--- не имеет значения ---</option>
							<option>--- не имеет значения ---</option>
						</select>
					</div>	
					<div class="col">
						<label class="inline">
							Регион:
						</label>	
						<select>
							<option>--- не имеет значения ---</option>
							<option>--- не имеет значения ---</option>
						</select>
					</div>
					<div class="col">
						<label class="inline">
							Город:
						</label>	
						<select>
							<option>--- не имеет значения ---</option>
							<option>--- не имеет значения ---</option>
						</select>
					</div>
				</div>	
				<input type="submit" value="ИСКАТЬ" class="btn standart" />

<?php $this->endWidget(); ?>

<div id="getQuickSearch">
<?php echo $this->getQuickSearch(); ?>
</div>





<div class="well">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'searchForm',
    'method'=>'GET',
    'htmlOptions'=>array('class'=>'form-inline'),
    'action'=>CHtml::normalizeUrl(array('search/index'))
)); ?>
	
    <div class="container-fluid1">
    <div class="row">
    <div class="col-lg-3">
	
	<div class="form-group">
	  <label class="control-label">Хочу найти</label>
	<?php //echo CHtml::dropDownList('sex', $_GET['sex'], array('2'=>'Девушку', '1'=>'Парня'), array('empty'=>'', 'style'=>'width:180px;', 'tabindex'=>'2')); ?>	

	</div>

    <!--Sidebar content-->
    </div>
    <div class="col-lg-3">
	<div class="form-group">
	  <label class="control-label">Цель знакомства</label>
<?php //echo CHtml::dropDownList('tg', $_GET['tg'], array('1'=>'Дружба', '3'=>'Секс', '2'=>'Любовь', ), array('style'=>'width:180px;', 'empty'=>'Не важна')); ?>
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'c[tg]',
  'value'=>$_GET['c']['tg'],
  'data'=>ProfileField::getListData(3),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:93%','multiple'=>'multiple',
  ),
  'options'=>array(
    'placeholder'=>'Выберите цель',
    'allowClear'=>true,
    //'width'=>'element',
  ),
)); ?>
	</div>
    </div>
    
    <div class="col-lg-3">
	<div class="form-group">
	  <label class="control-label">Возраст:</label>
<?//=CHtml::textField('age_from', $_GET['age_from'], array('style'=>'width:60px;', 'class'=>'txt'));?>
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'c[age_from]',
  'value'=>$_GET['c']['age_from'],
  'data'=>User::getRangedown(18,75),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:33%',
  ),
  'options'=>array(
    'placeholder'=>'От',
    'allowClear'=>true,
    //'width'=>'element',
  ),
)); ?><span class="do">-</span><?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'c[age_to]',
  'value'=>$_GET['c']['age_to'],
  'data'=>User::getRangedown(18,75),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:33%',
  ),
  'options'=>array(
    'placeholder'=>'До',
    'allowClear'=>true,
    //'width'=>'element',
  ),
)); ?>
	</div>
    </div>
    
    <div class="col-lg-3">
	 <label class="control-label">&nbsp;</label>

    </div>
    
    
    </div>
    </div>
    
    <div class="container-fluid1">
    <div class="row">
    <div class="col-lg-3">
	
<?php

		$cs=Yii::app()->getClientScript();
		$jsonurl=CHtml::normalizeUrl(array('site/geo'));
		$script=<<<END
$("select#country_id").change(function() {
        id=$(this).attr('value');
	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "country_id="+id,
		cache: false,
		beforeSend: function(){
			$("#loading_result").show();
		},
		success: function(data) {
			$("#loading_result").hide();
                        var setoptions = '<option value="" selected="selected"></option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_region + '">' + data[i].name + '</option>';
				}
			}
			
			$("select#region_id").html(setoptions);
			/*$("select#region_id").select2({
				placeholder: "Выберите регион",
				allowClear: true
			});*/
			$("select#region_id").change();
                }  
	});
});

$("select#region_id").change(function() {
        id=$(this).attr('value');

	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "region_id="+id,
		cache: false,
		beforeSend: function(){
                       $("#loading_result").show();
		},
		success: function(data) {
			$("#loading_result").hide();
			var setoptions = '<option value="" selected="selected"></option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_city + '">' + data[i].name + '</option>';
				}
			}
                        $("select#city_id").html(setoptions); 
			
			/*$("select#city_id").select2({
				placeholder: "Выберите город",
				allowClear: true
			});*/
                }  
	});
});

END;
		$cs->registerScript(__CLASS__.'#geo1', $script);
?>
	<div class="form-group">
	  <label class="control-label">Страна</label>
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'c[country_id]',
  'id'=>'country_id',
  'value'=>$_GET['c']['country_id'],
  'data'=>CHtml::listData(GEOCountry::model()->findAll(), 'id_country', 'name'),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:93%',
  ),
  'options'=>array(
    'placeholder'=>'Выберите страну',
    'allowClear'=>true,
  ),
)); ?>

	</div>
    <!--Sidebar content-->
    </div>
    <div class="col-lg-3">
	<div class="form-group">
	  <label class="control-label">Регион</label>
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'c[region_id]',
  'id'=>'region_id',
  'value'=>$_GET['c']['region_id'],
  'data'=>CHtml::listData(GEORegion::model()->findAll('id_country=:id_country', array('id_country'=>$_GET['c']['country_id'])), 'id_region', 'name'),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:93%',
  ),
  'options'=>array(
    'placeholder'=>'Выберите регион',
    'allowClear'=>true,
  ),
)); ?>
	</div>
    </div>
    
    <div class="col-lg-3">
	<div class="form-group">
	  <label class="control-label">Город:</label>
<?php $this->widget('ext.select2.ESelect2',array(
  'name'=>'c[city_id]',
  'id'=>'city_id',
  'value'=>$_GET['c']['city_id'],
  'data'=>CHtml::listData(GEOCity::model()->findAll('id_region=:id_region AND id_country=:id_country', array('id_region'=>$_GET['c']['region_id'], 'id_country'=>$_GET['c']['country_id'])), 'id_city', 'name'),
  'htmlOptions'=>array(
    //'style'=>'width:180px;',
    'style'=>'width:93%',
  ),
  'options'=>array(
    'placeholder'=>'Выберите город',
    'allowClear'=>true,
  ),
)); ?>
	</div>
    </div>
    
    <div class="col-lg-3">
	 <label class="control-label">&nbsp;</label>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'success', 'icon'=>'search white', 'label'=>'Искать')); ?>
<?php //$this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'type'=>'success', 'icon'=>'search white', 'label'=>'Сброс', 'htmlOptions'=>array('onclick'=>'$("#searchForm").reset()'))); ?>
 <span id="loading_result" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
    </div>
    
    
    </div>
    </div>
    
    
									<?php echo CHtml::checkBox('c[new]', $_GET['c']['new']); ?>
									<span class="description">новые лица</span>
									<?php echo CHtml::checkBox('c[online]', $_GET['c']['online']); ?>
									<span class="description">сейчас на сайте</span>
    
    
    
    
<?php $this->endWidget(); ?>
</div>
	
	

