<?php if(!Yii::app()->user->model->name OR Yii::app()->user->model->birthday=='0000-00-00'):?>
                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Завершение регистрации</h3>
                        </div>      
                    </div>

    <div class="alert alert-danger" id="warning" style="margin-bottom:10px;font-size:12px;">
    <i class="fa fa-exclamation-triangle" style="font-size:15px;"></i>Чтобы продолжить работать  с сайтом укажите о себе основную информацию.
    </div>
<?php else:?>
                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Редактировать профиль</h3>
                        </div>      
                    </div>

<div class="mytabs" style="margin-top:-15px;">
<?php $this->widget('Menu', array(
	'htmlOptions'=>array('class'=>'nav nav-tabs nav-friends'),
    'items'=>$this->menu,
)); ?>

                        </div>
<?php endif;?>
 <?php if(Yii::app()->user->hasFlash('edit')): ?>
<script type="text/javascript">
$().ready(function() {
 setTimeout("$('#hasFlash').fadeTo('speed', 0.4);", 1500);
});
</script>
<div class="alert alert-info" id="hasFlash">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

    <div class="alert alert-info" style="display:none;" id="flash-success">
    Изменения сохранены.
    </div>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('CActiveForm', array(
    'id'=>'editForm',
    //'htmlOptions'=>array('class'=>'form-horizontal'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,

)); ?>

<div class="form-group">
<?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>
<?php echo $form->textField($model, 'name', array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'name', array('class'=>'text-danger')); ?>	
</div>	

<div class="form-group">
<?php echo $form->labelEx($model,'sex',array('label'=>'Ваш пол:', 'class'=>'control-label')); ?>
<?php echo $form->dropDownList($model, 'sex', array('1'=>'Парень', '2'=>'Девушка'),array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'sex'); ?>
</div>	
<div class="form-group">
<?php echo $form->labelEx($model,'birthday',array('label'=>'Дата рождения:', 'class'=>'control-label')); ?>
<div class="">
	
<?php $this->widget('EHtmlDateSelect',
                        array(
                              'time'=>$model->birthday,
                              'field_array'=>'User[ItemsBirthday]',
                              'prefix'=>'',
                              'field_order'=>'DMY',
                              'start_year'=>1940,
                              'end_year'=>date("Y")-14,
			      'day_extra'=>'style="margin-bottom:3px;" class="form-control input-sm"',
			      'month_extra'=>'style="margin-bottom:3px;" class="form-control input-sm"',
			      'year_extra'=>'style="" class="form-control input-sm"',
			      'day_empty'=>'День',
			      'month_empty'=>'Месяц',
			      'year_empty'=>'Год',
                             )
                       );
	
	 echo $form->hiddenField($model,'birthday');
	?>

									<?php echo $form->error($model,'birthday', array('class'=>'text-danger')); ?>
</div>	
						</div>	

<?php// echo $form->dropDownListRow($model, 'country_id', CHtml::listData(GeoCountry::model()->findAll(), 'id_country', 'name')); ?>

<?php

		$cs=Yii::app()->getClientScript();
		$jsonurl=CHtml::normalizeUrl(array('site/geo'));
		$script=<<<END
$("#User_country_id").change(function() {
        id=$("#User_country_id").val();
	if (!id) { $("#User_region_id").html('<option value="" selected="selected">Выберите регион</option>');
	$("#User_city_id").html('<option value="" selected="selected">Выберите город</option>'); return; }
	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "country_id="+id,
		cache: false,
		beforeSend: function(){
			$("#loading_region").show();
		},
		success: function(data) {
			$("#loading_region").hide();
                        var setoptions = '<option value="" selected="selected">Выберите регион</option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_region + '">' + data[i].name + '</option>';
				}
			}
                        $("#User_region_id").html(setoptions);
			
			/*$("#User_region_id").select2({
				placeholder: "Выберите регион"
			});*/
			
			$("#User_region_id").change();
                }  
	});
});

$("#User_region_id").change(function() {
        id=$("#User_region_id").val();
if (!id) { $("#User_city_id").html('<option value="" selected="selected">Выберите город</option>'); return; }
	$.ajax({  
		type: "GET",
                dataType:"json",
		url: "$jsonurl",
		data: "region_id="+id,
		cache: false,
		beforeSend: function(){
                       $("#loading_city").show();
		},
		success: function(data) {
			$("#loading_city").hide();
			var setoptions = '<option value="" selected="selected">Выберите город</option>';
			
			if (data) {
				for (var i = 0; i < data.length; i++) {
					setoptions += '<option value="' + data[i].id_city + '">' + data[i].name + '</option>';
				}
			}
                        $("#User_city_id").html(setoptions); 
			
				/*$("#User_city_id").select2({
					placeholder: "Выберите город"
				});*/
                }  
	});
});

END;
		$cs->registerScript(__CLASS__.'#geo1', $script);
?>
<div class="form-group">
<?php echo $form->labelEx($model,'country_id',array('label'=>'Страна:', 'class'=>'control-label')); ?>

<?php echo $form->dropDownList($model, 'country_id', CHtml::listData(GEOCountry::model()->findAll(), 'id_country', 'name'), array('class'=>'form-control input-sm', 'empty'=>'Выберите страну'));?>


<?php echo $form->error($model,'country_id', array('class'=>'text-danger')); ?>

						</div>

<div class="form-group">
<?php echo $form->labelEx($model,'region_id',array('label'=>'Регион:', 'class'=>'control-label')); ?>

<?php echo $form->dropDownList($model, 'region_id', CHtml::listData(GEORegion::model()->findAll('id_country=:id_country', array('id_country'=>$model->country_id)), 'id_region', 'name'), array('class'=>'form-control input-sm', 'empty'=>'Выберите регион'));?>


<span id="loading_region" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
<?php echo $form->error($model,'region_id', array('class'=>'text-danger')); ?>

						</div>

<div class="form-group">
<?php echo $form->labelEx($model,'city_id',array('label'=>'Город:', 'class'=>'control-label')); ?>

<?php echo $form->dropDownList($model, 'city_id', CHtml::listData(GEOCity::model()->findAll('id_region=:id_region AND id_country=:id_country', array('id_region'=>$model->region_id, 'id_country'=>$model->country_id)), 'id_city', 'name'), array('class'=>'form-control input-sm', 'empty'=>'Выберите город'));?>


<span id="loading_city" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
<?php echo $form->error($model,'city_id', array('class'=>'text-danger')); ?>
						</div>
 


						<div class="form-group">
							<input type="submit" value="<?php echo (!Yii::app()->user->model->name OR Yii::app()->user->model->birthday=='0000-00-00') ? 'Далее' : 'Сохранить'?>" class="btn btn-primary btn-block btn-lg" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>
 


<?php $this->endWidget(); ?>
