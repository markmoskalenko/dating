                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Редактировать профиль</h3>
                        </div>      
                    </div>

<div class="mytabs" style="margin-top:-15px;">
<?php $this->widget('Menu', array(
	'htmlOptions'=>array('class'=>'nav nav-tabs nav-friends'),
    'items'=>$this->menu,
)); ?>

                        </div>

<?php if(Yii::app()->user->hasFlash('edit')): ?>
<script type="text/javascript">
$().ready(function() {
 setTimeout("$('.flash-success').fadeTo('speed', 0.4);", 1500);
});
</script>
<div class="alert alert-info">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

    <div class="alert alert-success" style="display:none;" id="flash-success">
    Изменения сохранены.
    </div>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('CActiveForm', array(
    'id'=>'editForm',
    //'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
)); ?>
<?php foreach($forms as $row): ?>
   <div class="form-group">
 <?php echo $form->labelEx($model,$row->name,array('class'=>'control-label')); ?>
    <div class="checkboxes">
<?php if (is_array($field_values[$row->name]))
    echo $form->{$row->presentation}($model, $row->name, $field_values[$row->name], array('class'=>''));
else {

    $htmlOptions=array();
    
    if ($row->hint) $htmlOptions['hint']=$row->hint;
    //if ($row->width) $htmlOptions['style']='width:'.$row->width.'px;';
    if (in_array($row->presentation, array('textField'))) $htmlOptions['class']='form-control';
    if (in_array($row->presentation, array('textArea'))): $htmlOptions['class']='form-control'; $htmlOptions['style']=''; endif;
    echo $form->{$row->presentation}($model, $row->name, $htmlOptions);
    if ($row->append) echo ' <span class="hint" style="margin-left:5px;float:left;">'.$row->append.'</span>';
}
    ?>
    </div>
    
<?php echo $form->error($model,$row->name); ?>
    </div>
<?php endforeach; ?>

						<div class="form-group">
							<input type="submit" value="Сохранить" class="btn btn-primary btn-block btn-lg" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>
<?php $this->endWidget(); ?>