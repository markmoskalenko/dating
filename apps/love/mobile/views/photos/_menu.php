<div class="mytabs" style="margin-bottom:0px;">
<?php $this->widget('Menu', array(
	'htmlOptions'=>array('class'=>'nav nav-tabs'),
    'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'<i class="fa fa-reply"></i>', 'url'=>array($this->user->url), 'linkOptions'=>array()),
				array('label'=>'Обзор фотографий', 'url'=>array('photos/index', 'userId'=>$this->user->id), 'linkOptions'=>array()),
				//array('label'=>Yii::t('app', 'Photos from'), 'url'=>array('photos/index', 'userId'=>$this->user->id, 'albumId'=>0), 'linkOptions'=>array(), 'visible'=>!$_GET['albumId']),
				//array('label'=>$this->album->name, 'url'=>array('photos/index', 'userId'=>$this->user->id, 'albumId'=>$_GET['albumId']), 'linkOptions'=>array(), 'visible'=>$_GET['albumId']!=0),
				//array('label'=>$this->album->name ? $this->album->name : Yii::t('app', 'Photos from'), 'url'=>array('photos/index', 'userId'=>$this->user->id, 'albumId'=>$_GET['albumId']), 'linkOptions'=>array(), 'visible'=>isset($_GET['albumId'])),
				//array('label'=>'Редактировать альбом', 'url'=>array('photos/editAlbum', 'id'=>$_GET['albumId']), 'linkOptions'=>array(), 'visible'=>!$this->album->system && $this->album->id && $this->isOwn, 'icon'=>'pencil'),
				array('label'=>'Создать альбом', 'url'=>array('photos/createAlbum'), 'icon'=>'plus', 'linkOptions'=>array(), 'visible'=>$this->isOwn),
			),
)); ?>

</div>