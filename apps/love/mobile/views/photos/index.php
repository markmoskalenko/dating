
<?php $this->pageTitle='Мои Фотографии';?>

<?php $this->renderPartial('_menu'); ?>
<?php if ($this->isOwn/* && $_GET['albumId']!=0*/):?>

<a href="<?=CHtml::normalizeUrl(array('photos/upload', 'albumId'=>Yii::app()->request->getQuery('albumId', 0))); ?>" class="btn btn-primary btn-block btn-lg" style="margin-top:5px;margin-bottom:5px;"><i class="fa fa-camera"></i>Добавить новые фотографии</a>

<?php endif;?>
<?php if (!$this->album->system && $this->album->id && $this->isOwn):?>
<a href="<?=CHtml::normalizeUrl(array('photos/editAlbum', 'id'=>$_GET['albumId'])); ?>" class="btn btn-primary btn-block btn-lg" style="margin-top:5px;margin-bottom:5px;"><i class="fa fa-camera"></i>Редактировать альбом</a>
<?php endif;?>
<?php if (!isset($_GET['albumId']) && !isset($_GET['page']) && $this->albums):?>
                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Альбомы</h3>
                        </div>      
                    </div>

<div class="row profile-photos">
		<?php foreach($this->albums as $key=>$row): ?>
		<div class="col-md-4 col-xs-4 col-sm-4 profile-photos-row">
		<a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>$row->user_id, 'albumId'=>$row->id)); ?>"><img src="<?php echo $row->photo->filename ? ImageHelper::thumb(230, 200, $row->photo->user_id, $row->photo->filename, array('method' => 'adaptiveResize')) : Html::imageUrl('empty_album.png');?>" class="img-responsive" border="0" /></a>
		
		</div>
		<?php endforeach; ?>

</div>
<?php endif;?>


                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left"><?php if (!isset($_GET['albumId']) && !isset($_GET['page'])): echo 'Фотографии'; else: echo $this->album->name ? $this->album->name : Yii::t('app', 'Photos from'); endif;?></h3>
                        </div>     
                    </div>
		    

<div class="row profile-photos">
		<?php foreach($photos as $key=>$row): $params=[]; if (isset($_GET['albumId'])) $params['albumId']=$_GET['albumId'];?>
		<div id="i<?=$row->id; ?>" class="col-md-2 col-xs-3 col-sm-2 profile-photos-row ">
		<a href="<?=CHtml::normalizeUrl(array_merge(array('photos/show', 'userId'=>$row->user_id, 'photoId'=>$row->id), $params));; ?>"><img src="<?php echo ImageHelper::thumb(130, 100, $row->user_id, $row->filename, array('method' => 'adaptiveResize'));?>" class="img-responsive" border="0" /></a>
		
		</div>
		<?php endforeach; ?>
</div>
