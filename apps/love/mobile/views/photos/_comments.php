

		<div style="margin-top:10px;">
<?php if(Yii::app()->user->id==$photo->user_id && $photo->album_id==0):?>
<?php if($photo->approve):?>
		    <div class="" style="margin-bottom:10px;color:#247738;">Фото одобрено модератором <i class="fa fa-check"></i> </div>
<?php else:?>
		    <div class="" style="margin-bottom:10px;color:#B25900;">Фото еще не проверено модератором <i class="fa fa-clock-o"></i> </div>
<?php endif;?>
<?php endif;?>

		
</div>

<div class="photo-like"><a href="1">Мне нравится</a> <a href="<?php echo CHtml::normalizeUrl(array('photos/like', 'id'=>$photo->id)); ?>"><i class="fa fa-heart"></i><?php echo $photo->likes; ?></a></div>

<div class="photo-like">Добавлена: <?php echo Html::date($photo->date); ?></div>

                        <div class="menu-wall menu-photoShow clearfix">
                            <ul>
			    <li>Альбом:
							<br>
<?php
	echo CHtml::link(
		$photo->album->name ? $photo->album->name : 'Фотографии со страницы', 
		CHtml::normalizeUrl(array('photos/index', 'userId'=>$photo->user_id, 'albumId'=>$photo->album_id))
	);
?></li>
							<li>Отправитель:
							<br>
<?php
	echo CHtml::link(
		$photo->user->name, 
		$photo->user->url
	);
?> 
							</li>
                            </ul>
			    
			    
		<?php $this->widget('zii.widgets.CMenu',array(
			//'type'=>'list',
			'htmlOptions'=>array('class'=>'friend-nav'),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			'items'=>array(
				array('label'=>'<i class="fa fa-exclamation-triangle"></i>Пожаловаться', 'url'=>array('photos/abuse', 'id'=>$photo->id), 'visible'=>Yii::app()->user->id!=$photo->user_id),
				array('label'=>'<i class="fa fa-mail-reply-all"></i>Сделать аватаркой', 'url'=>'#', 'visible'=>Yii::app()->user->id==$photo->user_id && $photo->album_id==0, 'linkOptions'=>array('onclick'=>'photoShow.setAvatar("'.CHtml::normalizeUrl(array('photos/index', 'setAvatar'=>$photo->id)).'"); return false;'), 'itemOptions'=>['id'=>'setava']),
				array('label'=>'<i class="fa fa-edit"></i>Редактировать', 'url'=>array('photos/editPhoto', 'id'=>$photo->id), 'visible'=>Yii::app()->user->id==$photo->user_id && $photo->album_id!=0, 'linkOptions'=>array('id'=>'editPhoto')),
				array('label'=>'<i class="fa fa-times"></i>Удалить', 'url'=>'#', 'visible'=>Yii::app()->user->id==$photo->user_id, 'linkOptions'=>array('onclick'=>'photoShow.remove("'.CHtml::normalizeUrl(array('photos/index', 'delPhoto'=>$photo->id)).'"); return false;')),
				array('label'=>'<i class="fa fa-upload"></i>Загрузить оригинал на устройство', 'url'=>'#', 'linkOptions'=>array('onclick'=>'window.open("'.$photo->imageSource.'", "_blank"); return false;')),

			),
		)); ?>
                        </div>
			
                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Комментарии</h3>
                        </div>    
  
                    </div>
		    
<div class="recording-wall b-notice" id="commentsRows">
<?php echo $this->getComments($photo->id);?>
</div>

<?php if (Yii::app()->user->id==$photo->user_id OR Yii::app()->user->model->isReal):?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'commentsForm',
    'htmlOptions'=>array('class'=>'form-horizontal comment-form'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
    'action'=>CHtml::normalizeUrl(array('photos/addComment', 'photoId'=>$photo->id))
)); ?>


					<div class="">
<?php echo $form->textArea($commentsForm,'message',array('class'=>'form-control')); ?>
<?php echo $form->error($commentsForm,'message',array('class'=>'err')); ?>


						<div class="foot">
								<div id="errorSummary" class="alert alert-danger left" style="display:none; padding:4px;">
		</div>
							<input type="submit" value="ОТПРАВИТЬ" class="btn btn-primary btn-block btn-lg" onclick="photoComments.add($('#commentsForm')); return false;" />
							<span id="loading_add_comment" class="right" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
						</div>	
					</div>	


<?php $this->endWidget(); ?>



<?php elseif (Yii::app()->user->id!=$photo->user_id && !Yii::app()->user->isGuest):?>
					<div class="write-msg" style="background: #F3F9FC; padding:12px;">
						Писать комментарии могут только Реальные пользователи сайта. <a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>"><b>Подтвердить реальность</b></a>
					</div>
<?php else:?>
					<div class="write-msg" style="background: #F3F9FC;">
						Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать комментарий к фотографии.
					</div>
<?php endif;?>
