<div class="recording-wall">
<?php foreach($comments as $index=>$row):?>

                <?php if ($index>0):?><hr style="margin-top:5px;margin-bottom:15px;"/><?php endif;?>
		
                <div class="row" id="comment_<?=$row->id?>">
                    <div class="col-xs-3 col-sm-2 col-md-2 text-left" style="width:80px;">
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'), '', array('class'=>'')
		), 
		$row->user->url
	);
else:
	echo CHtml::image(Html::imageUrl('camera_s.png'), '', array('class'=>''));
endif;
?>                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="notice-text">
                                    <div class="head"><?php
	//echo CHtml::image(Html::imageUrl($row->user->sex==2 ? 'user_female.png' : 'user_male.png'));
	//echo ' ';
	echo CHtml::link(
		$row->user->name ? $row->user->name : 'DELETED', 
		$row->user->url,
		array('rel'=>'ajax1')
	);
?>
<?php if (Yii::app()->user->checkAccess('deleteComment', array('author_id'=>$row->user->id, 'own_id'=>$this->user->id))):?>

<a class="pull-right" title="удалить" rel="tooltip" href="#" onclick="photoComments.deleteComment('<?=CHtml::normalizeUrl(array('photos/index', 'delCommId'=>$row->id)); ?>'); return false;"><i class="fa fa-times"></i></a><?php endif; ?>
		

</div>  
<p>
								<?=Html::message($row->message)?></p>

  
                                    <div class="foot">
                                        <i class="fa fa-clock-o"></i>
                                         <?php echo Html::date($row->date); ?>
					 
			 
                                    </div>  
                                </div> 
                            </div>     
                        </div>
                    </div>
                </div>    

						
						
						



<?php endforeach; ?>



<? $this->widget('CLinkPager', array(
        'id'=>'commentsPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination'),
	'header'=>'',
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>



</div>