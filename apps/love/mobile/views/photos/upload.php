<?php //Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>

<script type="text/javascript">
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : 'server/php/';
    $('#fileupload').fileupload({
        url: '<?=CHtml::normalizeUrl(array('photos/upload', 'albumId'=>$_GET['albumId'])); ?>',
        dataType: 'json',
	formData: function (form) {
		
    return [{name: 'CSRFTOKEN',value: '<?php echo Yii::app()->request->csrfToken; ?>'}, {name: 'SESSION_ID',value: '<?php echo Yii::app()->session->sessionID; ?>'}];
},
    add: function (e, data) {
           /* $('.progress').attr(
                'class',
                'progress progress-striped active'
            );*/
        var goUpload = true;
        var uploadFile = data.files[0];
        if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
           //alert('You must select an image file only');
	   YiiAlert.error('Вы должны выбрать только файл изображения.', '402');
           goUpload = false;
        }
        if (uploadFile.size > 10000000) { // 2mb
           // alert('Please upload a smaller image, max size is 2 MB');
	   YiiAlert.error('Пожалуйста, загрузите изображение меньшего размера, максимальный размер равен 10 Мб.', '402');
            goUpload = false;
        }
        if (uploadFile.size < 20000) { // 20kb
           // alert('Please upload a smaller image, max size is 2 MB');
	   YiiAlert.error('Фотография слишком низкого качества.', '402');
            goUpload = false;
        }
        if (goUpload == true) {
        var jqXHR = data.submit()
            .success(function (result, textStatus, jqXHR) {

		$('#done').prepend(result.html);
		})
            .error(function (jqXHR, textStatus, errorThrown) {
		YiiAlert.error(jqXHR.responseText, jqXHR.status);
		})
            .complete(function (result, textStatus, jqXHR) {
           /* $('.progress').attr(
                'class',
                'progress'
            );*/
		});
        }
	
	
	

    },
        progressall: function (e, data) {
		var progress = Math.ceil((data.loaded / data.total) * 100);
            $('#progress').css(
                'width',
                progress + '%'
            );
	    
	    if (progress==100) {
	      $('#progress .sr-only').html(progress+'% Complete');
	    } else {
	      $('#progress .sr-only').html(progress+'% Loading');
	    }
        }
    });
});

editPhoto = {
	
	CSRFTOKEN: '<?php echo Yii::app()->request->csrfToken;?>',
	formAction: '<?=CHtml::normalizeUrl(array('photos/editPhoto')); ?>',

	save: function(id)
	{
	$.ajax({
		type: "POST",
		url: editPhoto.formAction+'?id='+id,
		data: 'CSRFTOKEN='+editPhoto.CSRFTOKEN+'&Photos[descr]='+$("#descr_"+id).val(),
		cache: false,
		dataType: 'json',
                error: function(x,e, settings, exception){
                        YiiAlert.error(x.responseText, x.status);
                },
		beforeSend: function(){
                        

		},
		success: function(responce){
			YiiAlert.success('Описание сохранено.');
		}
	});
	},
	
	del: function(url)
	{
	$.ajax({
		type: "GET",
		url: url,
		cache: false,
		dataType: 'json',
		beforeSend: function(){
                        
		},
		success: function(responce){
                        YiiAlert.success('<i class="fa fa-times"></i>Фотография удалена.');
			$("#i"+responce.photoId).hide();
		}
	});
	}
	
}
</script>
            <div class="title row">
                <div class="col-xs-12 col-md-12">
                    <h3 class="pull-left">Загрузка фотографии в профиль</h3>
                </div>     
            </div>

<?php if (!Yii::app()->user->model->have_photo):?>
					<div class="noreal-alert">
						<p>Пользователи не видят вас в результатах поиска.
						В вашей анкете нет ни одной фотографии.
						Чтобы начать знакомиться и просматривать анкеты необходимо загрузить хотябы одну фотографию
						</p>
					</div>
<?php endif;?>
			<div class="section-tabs">

				<div class="tab visible">
<div>
<span class="btn btn-primary btn-block btn-lg btn-file" id="UploadPhotos">
                            <i class="fa fa-camera"></i><span>Выбрать фотографию...</span>
                            <input type="file" name="UploadForm[file]" id="fileupload"/>
                        </span>

</div>

<br>
<div class="progress">
  <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    <span class="sr-only"></span>
  </div>
</div>
    
    <div id="done">
<?//=PhotosController::getEditPhotosRows(0)?>
    </div>

			</div>	
		</div>