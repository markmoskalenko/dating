            <div class="title row">
                <div class="col-md-12">
                    <h3 class="pull-left">Топ 100 участников</h3>
                </div>    
            </div>
            <div class="participants row">
		
<?php foreach($profiles as $num=>$row): ?>

                <div class="col-xs-4 col-sm-3 col-md-3">
                    <div class="user">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(104,138, 'camera_a.png'),
			'',
			array('class'=>'img-responsive')
		), 
		$row->url,
		array()
	);
?>                 
                        <span class="name"><?php
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('class'=>'name-user')
	);
?></span>
                        <span class="city"><?php echo $row->geo->city?></span>
                        <span class="age"><?php echo Html::age($row->birthday)?></span>
                        <?php if($row->isOnline && !$row->isMobile):?><label class="online">&nbsp;</label>
			<?php elseif($row->isOnline && $row->isMobile):?><label class="mobile">&nbsp;</label><?php endif;?>
                        <a class="add-friend" href="<?php echo CHtml::normalizeUrl(array('messages/send', 'id'=>$row->id)); ?>"><i class="fa fa-envelope"></i>Сообщение</a>
                    </div>
                </div>   


<?php endforeach;?>

            </div>





<?php $this->renderPartial('/search/_searchForm', array()); ?>
			<div class="title">
				<h3 class="left">Топ 100 участников</h3>
			</div>
			<div class="participants">
<?php foreach($profiles as $num=>$row): ?>
				<div class="user">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(104,138, 'camera_a.png'),
			'',
			array('class'=>'img-responsive')
		), 
		$row->url,
		array()
	);
?>
					<span class="name"><?php
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('class'=>'name-user')
	);
?></span>
					<span class="city"><?php echo $row->geo->city?></span>
					<span class="age"><?php echo Html::age($row->birthday)?></span>

					<?php if($row->isOnline):?><label class="online">&nbsp;</label><?php endif;?>
						<a href="<?php echo CHtml::normalizeUrl(array('messages/send', 'id'=>$row->id)); ?>" class="add-friend" id="showModalSendMessage" <?=Yii::app()->user->id==$row->id ? ' disabled="disabled"' : ''?>><i class="fa fa-envelope"></i>Сообщение</a>
					<?php if($row->isReal):?><label class="icn-real">&nbsp;</label><?php endif;?>
				</div>	
<?php endforeach;?>
			</div>
