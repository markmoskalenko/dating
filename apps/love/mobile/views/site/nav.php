
<!-- рур!-->


		    
                    <div class="title row">
                        <div class="col-xs-6 col-md-6">
                            <h3 class="pull-left">Навигация</h3>
                        </div>    
                        <div class="col-xs-6 col-md-6">
                                
                        </div>    
                    </div>

		    
                        <div class="menu-wall clearfix">
		<?php $this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
                        'htmlOptions'=>array('class'=>'main-nav'),
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
                'items'=>array(
			//array('label'=>'LIST HEADER'),
				array('label'=>'<i class="fa fa-search"></i>Поиск', 'url'=>array('search/index')),
				array('label'=>'<i class="fa fa-camera"></i>Лучшие фото', 'url'=>array('top/photos')),
				array('label'=>'<i class="fa fa-star"></i>Топ 100', 'url'=>array('top/index')),
				array('label'=>'<i class="fa fa-home"></i>Моя Страница', 'url'=>Yii::app()->user->model->url, 'icon'=>'home'),
				array('label'=>'<i class="fa fa-envelope"></i>Мои Cообщения <span class="count right" id="newMessagesCounter" style="display:none;">+<label>0</label></span>', 'url'=>array('messages/index'), 'icon'=>'envelope'),
				array('label'=>'<i class="fa fa-user"></i>Мои Друзья <span class="count right" id="requestFriendsCounter" style="display:none;">+<label>0</label></span>', 'url'=>array('friends/index')),
				array('label'=>'<i class="fa fa-camera"></i>Мои Фотографии', 'url'=>array('photos/index', 'userId'=>Yii::app()->user->id), 'itemOptions'=>array(), 'icon'=>'camera'),
				array('label'=>'<i class="fa fa-bell"></i>Уведомления<span class="count right" id="notificationsCounter" style="display:none;">+<label>0</label></span>', 'url'=>array('notifications/index'), 'itemOptions'=>array(), 'icon'=>'bell'),
				//array('label'=>'Мои Группы', 'url'=>array('groups/index', 'userId'=>Yii::app()->user->id), 'itemOptions'=>array()),
				//array('label'=>'<i class="fa fa-eye"></i>Мои Гости', 'url'=>array('guests/index'), 'icon'=>'eye-open'),
				array('label'=>'<i class="fa fa-star"></i>Мои Закладки', 'url'=>array('fave/index'), 'icon'=>'star-empty'),
				array('label'=>'<i class="fa fa-money"></i>Мой счёт <span class="count right">'.Yii::app()->user->balance.'</span>', 'url'=>array('pay/index')),
				array('label'=>'<i class="fa fa-gear"></i>Мои Настройки', 'url'=>array('settings/index')),
				array('label'=>'<i class="fa fa-share"></i>Выйти', 'url'=>array('site/logout'), 'visible'=>!Yii::app()->user->isGuest, 'icon'=>'off white'),
				/*array('label'=>'Мои Настройки', 'url'=>array('settings/index'), 'items'=>array(
                    array('label'=>'ТОП 100 Девушки', 'url'=>'#'),
                    array('label'=>'ТОП 100 Парни', 'url'=>'#'),
                )),*/
				//array('label'=>'Получить Real-статус', 'url'=>array('real/index'), 'itemOptions'=>array('class'=>'fav'), 'visible'=>!Yii::app()->user->isGuest),
			),
		)); ?>
			    
                        </div>
