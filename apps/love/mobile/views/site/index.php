<?php if (Yii::app()->user->isGuest): ?>
            <div class="registration visible-xs">
                <div class="row">
                    <div class="col-md-12">
                        <h4><a id="showModalRegisterBox" href="/register">Регистрация</a></h4>
                    </div>
                </div>
<?php $login=$this->beginWidget('LoginActiveForm', array(
    'action'=>CHtml::normalizeUrl(array('user/login')),
    'id'=>'auth6',
    'htmlOptions'=>array('class'=>'enter-site'),
)); ?>
                    <div class="inputs">
                       <?php echo $login->textField('username',array('class'=>'field', 'placeholder'=>'E-mail или логин')); ?>
                    </div>  
                    <div class="inputs">
                        <?php echo $login->passwordField('password',array('class'=>'field', 'placeholder'=>'Пароль')); ?>
                    </div>  
                    <div class="inputs">
                        <div class="checked pull-left">
                            <input type="checkbox" id="remind" value="">
                            <label for="remind">запомнить</label>
                        </div>
			<a href="<?=CHtml::normalizeUrl(array('user/lostpassword')); ?>" class="remind-link pull-right">Забыли пароль?</a>
                    </div>
                    <div class="inputs text-center">
                        <input type="submit" class="btn btn-primary standart" value="ВОЙТИ">
                    </div>
<?php $this->endWidget(); ?>
            </div>
<?php endif; ?>

            <div class="title row">
                <div class="col-md-12">
                    <h3 class="pull-left">Топ 100 участников</h3>
                </div>    
            </div>
            <div class="participants row">
		
<?php foreach($profiles as $num=>$row): ?>

                <div class="col-xs-4 col-sm-3 col-md-3">
                    <div class="user">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(104,138, 'camera_a.png'),
			'',
			array('class'=>'img-responsive')
		), 
		$row->url,
		array()
	);
?>                 
                        <span class="name"><?php
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('class'=>'name-user')
	);
?></span>
                        <span class="city"><?php echo $row->geo->city?></span>
                        <span class="age"><?php echo Html::age($row->birthday)?></span>
                        <?php if($row->isOnline && !$row->isMobile):?><label class="online">&nbsp;</label>
			<?php elseif($row->isOnline && $row->isMobile):?><label class="mobile">&nbsp;</label><?php endif;?>
                        <a class="add-friend" href="<?php echo CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$row->id)); ?>"><i class="fa fa-envelope"></i>Сообщение</a>
                    </div>
                </div>   


<?php endforeach;?>

            </div>