<script type="text/javascript">
jQuery(function($) {
$("#AddToBlacklist a").live('click', function() {
	$.ajax({
		type: "GET",
		url: $(this).attr('href'),
		data: '',
		cache: false,
		dataType: 'json',
		beforeSend: function(){
		   
		},
		success: function(data){
			$("#user_"+data.id).hide();

		}
	});
	return false;
});
});
</script>
			<div class="title">
				<h3 class="left">Черный список</h3>
			</div>	
			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>

				<div class="tab visible">

<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

<div class="flash-success" style="display:none;">
	Логин изменен.
</div>

<?php if(!$users):?>
Ваш чёрный список пуст.

<?php endif;?>

<?php foreach($users as $num=>$row):?>
				
				
					<div id="user_<?=$row->id?>" style="margin-bottom:5px;">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(30,30, 'camera_s.png'),
			'',
			array('class'=>'ava', 'align'=>'absmiddle', 'style'=>'width:30px; height:30px;')
		), 
		$row->url,
		array()
	);
?>

<span class="ttl"><?php
echo '';
	echo CHtml::link(
		$row->name, 
		$row->url,
		array('rel'=>'ajax1')
	);
?></span>


		<?php $this->widget('Menu',array(
			//'type'=>'list',
			'htmlOptions'=>array('class'=>'friend-nav right'),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			'items'=>array(
				array('label'=>'<i class="fa fa-check-circle"></i>Разблокировать', 'url'=>array('settings/blacklist', 'bId'=>$row->id), 'itemOptions'=>array('id'=>'AddToBlacklist')),
			),
		)); ?>
					</div>	

				

				


<?php endforeach; ?>


			</div>	
		</div>