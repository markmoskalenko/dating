<div class="mytabs" style="margin-top:-15px;">
<?php $this->widget('Menu', array(
	'htmlOptions'=>array('class'=>'nav nav-tabs nav-friends'),
    'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'Общее', 'url'=>array('settings/index'), 'linkOptions'=>array()),
				array('label'=>'Приватность', 'url'=>array('settings/privacy'), 'linkOptions'=>array()),
				//array('label'=>'Смена пароля', 'url'=>array('settings/password'), 'linkOptions'=>array()),
				array('label'=>'Чёрный список', 'url'=>array('settings/blacklist'), 'linkOptions'=>array()),
				array('label'=>'Баланс', 'url'=>array('pay/index'), 'linkOptions'=>array()),
			),
)); ?>
</div>