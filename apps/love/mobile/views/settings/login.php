                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Настройки</h3>
                        </div>      
                    </div>
			<div class="section-tabs">
<?php $this->renderPartial('_menu', array()); ?>

				<div class="tab visible">

<?php if(Yii::app()->user->hasFlash('edit')): ?>
<div class="alert alert-success">
	<?php echo Yii::app()->user->getFlash('edit'); ?>
</div>
<?php endif; ?>

<div class="alert alert-success" style="display:none;">
	Логин изменен.
</div>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,


)); ?>
<?php echo CHtml::errorSummary($model);?>

<div class="form-group">
<?php echo $form->labelEx($model,'login',array('class'=>'control-label')); ?>
<?php echo $form->textField($model, 'login',array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'login', array('class'=>'text-danger')); ?>
</div>
<div class="form-group">
	<input type="submit" value="Сохранить" class="btn btn-primary btn-block btn-lg" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
</div>
<?php $this->endWidget(); ?>

			</div>	
		</div>