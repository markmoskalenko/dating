<?php
$lpx=array('0','40','80', '120');
//Yii::app()->clientScript->registerCss('logo', 'a.logo { width:226px; height:38px; background-image:url("'.Html::imageUrl('logos.png').'"); background-position: 0px -'.$lpx[rand(0,3)].'px!important;}');
Yii::app()->clientScript->registerCssFile(Html::cssUrl('bootstrap.css'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('style.css?v4'));
Yii::app()->clientScript->registerCssFile(Html::cssUrl('noty.css'));
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('form');
Yii::app()->clientScript->registerCoreScript('cookie');
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('main.js'));
Yii::app()->clientScript->registerCoreScript('pjax');
Yii::app()->clientScript->registerCoreScript('base64');
Yii::app()->clientScript->registerCoreScript('socket.io');
Yii::app()->clientScript->registerCoreScript('ion.sound');
Yii::app()->clientScript->registerCssFile(Html::cssUrl('font-awesome.css'));
Yii::app()->clientScript->registerCoreScript('bootstrap');
Yii::app()->clientScript->registerCoreScript('touchSwipe');
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo.js'));
Yii::app()->clientScript->registerCoreScript('noty');
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('YCNoty.js'));
Yii::app()->clientScript->registerCoreScript('fileupload');
?>
<!doctype html>
<html ng-app="touchApp">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="x-pjax-version" content="v123"> 
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title><?php echo $this->pageTitle ? $this->pageTitle : 'Взрослая социальная сеть, знакомства для взрослых'; ?></title>

</head>
<body>
<script type="text/javascript">

jQuery(function($) {
        $.ionSound({
            sounds: [
                "new_message",
                "noty"
            ],
            path: "<?=Yii::app()->baseUrl.'/themes/'.Yii::app()->theme->name?>/sounds/",
            multiPlay: true,
            volume: "1.0"
        });
<?php if(Yii::app()->params['noty']):?>
	YCNoty.init('<?php echo md5(Yii::app()->user->id.'dsbetngaedgc')?>', '<?php echo base64_encode(Yii::app()->params['noty_hostClient'])?>');
<?php endif;?>
$(document).pjax('a', '#maincol', {timeout: null, maxCacheLength: 0, data: ''} );
$(document).on('pjax:send', function() {
   $('#maincol').fadeTo(0, 0.3);
})
$(document).on('pjax:complete', function() {
  $('#maincol').fadeTo(0, 1.0);
})

$(document).on('pjax:error', function(xhr, err) {
  // Prevent default timeout redirection behavior
  YiiAlert.error(err.responseText, err.status);
})
$(window).bind('popstate', function(event) {
	

	
$.ajax({
			type: "GET",
			url: location.href,
			cache: false,
			beforeSend: function(xhr){
				//$('#maincol').fadeTo(0, 0.3);
				return xhr.setRequestHeader('X-PJAX','true');
			},
			success: function(html){
				//$('#maincol').fadeTo(0, 1.0);
				
				$("#maincol").html(html);
			}
		});

    });
    });
</script>
<div class="container">
    <header id="header" class="row">
        <div class="col-md-9 col-md-push-3">
            <ul id="nav">
		<?php if (!Yii::app()->user->isGuest): ?>
                <li class="visible-xs"><a href="<?=CHtml::normalizeUrl(array('messages/index')); ?>"><i class="fa fa-envelope"></i><span class="count pull-right" id="newMessagesCounter" style="display:none;"></span></a></li>
                <li class="visible-xs"><a href="<?=CHtml::normalizeUrl(array('friends/index')); ?>"><i class="fa fa-user"></i><span class="count pull-right" id="requestFriendsCounter" style="display:none;"></span></a></li>
                <li class="visible-xs"><a href="<?=CHtml::normalizeUrl(array('notifications/index')); ?>"><i class="fa fa-bell"></i><span class="count pull-right" id="notificationsCounter" style="display:none;"></span></a></li>
                <?php endif; ?>
		<li class="first"><a href="<?=CHtml::normalizeUrl(array('search/index')); ?>" class="icn-dating"><i class="fa fa-search"></i><span class="hidden-xs">Поиск</span></a></li>
                <li class="hidden-xs"><a href="<?=CHtml::normalizeUrl(array('top/index')); ?>" class="icn-top"><i class="fa fa-star"></i>Топ-100</a></li>
                <li class="hidden-xs"><a href="<?=CHtml::normalizeUrl(array('top/photos')); ?>" class="icn-app"><i class="fa fa-camera"></i>Фотографии</a></li>
                <?php if (Yii::app()->user->isGuest): ?><li class="last hidden-xs"><a href="<?=CHtml::normalizeUrl(array('user/register')); ?>" class="icn-app"><i class="fa fa-user"></i>Создать анкету</a></a></li><?php endif; ?>
            </ul>
        </div>
        <div class="col-md-3 col-md-pull-9">
            <div class="visible-xs pull-left mainmenu-header">
                <?php if (!Yii::app()->user->isGuest): ?><a href="<?=CHtml::normalizeUrl(array('site/nav')); ?>" class="btn btn-default btn-sm dropdown-toggle btn-main-nav">
                    <span class="fa fa-bars"></span>
                </a><?php endif; ?>

            </div>
            <a class="logo<?php if (!Yii::app()->user->isGuest): ?> hidden-xs <?php endif; ?>" href="/"></a>
        </div>        
    </header>
    <section id="content" class="row">
        <div class="hidden-xs col-sm-1 col-md-4">
<?php if (!Yii::app()->user->isGuest): ?>
            <div class="vertical-nav">

                <ul class="nav nav-pills nav-stacked">
                    <li class="first">
                        <a href="<?=Yii::app()->user->model->url; ?>">
                            <i class="fa fa-home"></i>
                            <span class="hidden-sm">Моя Страница</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=CHtml::normalizeUrl(array('messages/index')); ?>">
                            <i class="fa fa-envelope"></i>
                            <span class="hidden-sm">Мои Cообщения</span>
			    <span class="count pull-right" id="newMessagesCounter2" style="display:none;"></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=CHtml::normalizeUrl(array('friends/index')); ?>">
                            <i class="fa fa-user"></i>
                            <span class="hidden-sm">
                                Мои Друзья 
                            </span>    
                            <span class="count pull-right" id="requestFriendsCounter2" style="display:none;"></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=CHtml::normalizeUrl(array('photos/index', 'userId'=>Yii::app()->user->id)); ?>">
                            <i class="fa fa-camera"></i>
                            <span class="hidden-sm">Мои Фотографии</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=CHtml::normalizeUrl(array('notifications/index')); ?>">
                            <i class="fa fa-bell"></i>
                            <span class="hidden-sm">Уведомления</span>
			    <span class="count pull-right" id="notificationsCounter2" style="display:none;"></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=CHtml::normalizeUrl(array('fave/index')); ?>">
                            <i class="fa fa-star"></i>
                            <span class="hidden-sm">Мои Закладки</span>
                        </a>
                    </li>
                    <li>    
                        <a href="<?=CHtml::normalizeUrl(array('pay/index')); ?>">
                            <i class="fa fa-money"></i>
                            <span class="hidden-sm">
                                Мой счёт 
                            </span>    
                            <span class="count pull-right"><?php echo Yii::app()->user->balance;?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=CHtml::normalizeUrl(array('settings/index')); ?>">
                            <i class="fa fa-gear"></i>
                            <span class="hidden-sm">Мои Настройки</span>
                        </a>
                    </li>
                    <li class="last">
                        <a href="<?=CHtml::normalizeUrl(array('site/logout')); ?>">
                            <i class="fa fa-share"></i>
                            <span class="hidden-sm">Выйти</span>
                        </a>
                    </li>
                </ul>
            </div>    
<?php else: ?>
            <div class="registration hidden-sm">
                <div class="row">
                    <div class="col-md-12">
                        <h4><a id="showModalRegisterBox" href="/register">Регистрация</a></h4>
                    </div>
                </div>
<?php $login=$this->beginWidget('LoginActiveForm', array(
    'action'=>CHtml::normalizeUrl(array('user/login')),
    'id'=>'auth7',
    'htmlOptions'=>array('class'=>'enter-site'),
)); ?>
                    <div class="inputs">
                       <?php echo $login->textField('username',array('class'=>'field', 'placeholder'=>'E-mail или логин')); ?>
                    </div>  
                    <div class="inputs">
                        <?php echo $login->passwordField('password',array('class'=>'field', 'placeholder'=>'Пароль')); ?>
                    </div>  
                    <div class="inputs">
                        <div class="checked pull-left">
                            <input type="checkbox" id="remind" value="">
                            <label for="remind">запомнить</label>
                        </div>
			<a href="<?=CHtml::normalizeUrl(array('user/lostpassword')); ?>" class="remind-link pull-right">Забыли пароль?</a>
                    </div>
                    <div class="inputs text-center">
                        <input type="submit" class="btn btn-primary standart" value="ВОЙТИ">
                    </div>
<?php $this->endWidget(); ?>
            </div>
<?php endif; ?>


        </div>
        <div id="maincol" class="col-xs-12 col-sm-11 col-md-8">

		

		
<?php echo $content; ?>

        </div>
    </section>
    <footer id="footer" class="row">
        <div class="col-xs-6 col-md-6 text-right">
            <a class="support-link left" href="/support">Служба поддержки</a>
        </div>
        <div class="col-xs-6 col-md-6 text-left">
            &copy; 2014 <?php echo Yii::app()->request->domain?>
        </div>
    </footer>
</div>
<script type="text/javascript">
 $(document).ready(function(){
  var time_zone = (new Date().getTimezoneOffset()/60)*(1);
  var dateoffset = time_zone*60*60*1000;

$.cookie('time_zone', time_zone, {path: "/"});


 });
</script>
</body>
</html>