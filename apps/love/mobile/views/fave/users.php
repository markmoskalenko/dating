<?php
$this->pageTitle='Мои закладки';
$this->breadcrumbs=array(
	'Мои закладки'
);
?>
            <div class="title row">
                <div class="col-md-12 col-xs-12">
                    <h3 class="pull-left">Мои закладки</h3>
                </div>   
            </div>
<?php $this->renderPartial('_menu', array()); ?>
<?php if (!$users): ?>
<p class="text-muted text-center">Закладки пусты.</p>
<?php endif;?>
<div class="row dialogue">
<div class="recording-wall b-notice" style="margin-left:0px;margin-right:0px;">
<?php foreach($users as $num=>$row):?>
                <?php if ($num>0):?><hr style="margin-top:5px;margin-bottom:10px;"/><?php endif;?>
		
                <div class="row" id="comment_<?=$row->id?>" style="margin-right:0px;">
                    <div class="col-xs-3 col-sm-2 col-md-2 text-left" style="width:80px;">
<?php
if ($row->id):
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(55,55, 'camera_s.png'),
			'',
			array('class'=>'ava')
		), 
		$row->url,
		array()
	);
else:
	echo CHtml::image( 
			Html::imageUrl('camera_s.png'),
			'',
			array('class'=>'ava')
		);
endif;
?>
                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="notice-text">
                                    <div class="head"><?php
	echo CHtml::link(
		$row->name ? $row->name : DELETED, 
		$row->url,
		array('rel'=>'ajax1')
	);
?>
<?php if ($row->isOnline):?>
								<label class="dialogue-online"><?php if (!$row->isMobile):?><i class="fa fa-circle"></i><?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?></label>
<?php endif;?>
				    </div>
				    <div class="menu-wall clearfix">
		<?php $this->widget('Menu',array(
			//'type'=>'list',
			'htmlOptions'=>array('class'=>'friend-nav'),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			'items'=>array(
				array('label'=>'<i class="fa fa-envelope"></i>Написать сообщение', 'url'=>array('messages/dialogue', 'id'=>$row->id), 'linkOptions'=>array('id'=>'showModalSendMessage')),
				array('label'=>'<i class="fa fa-camera"></i>Смотеть фотографии', 'url'=>array('photos/show', 'userId'=>$row->id, 'albumId'=>0, 'photoId'=>$row->photoId), 'linkOptions'=>array('id'=>'showModalPhotoBox')),
			),
		)); ?>
				    </div>
				    <div class="message"></div>

                                </div> 
                            </div>     
                        </div>
                    </div>
                </div> 
<?php endforeach; ?>
</div>

<div class="text-center">
<?php $this->widget('ext.bootstrap3.widgets.BsPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
</div>