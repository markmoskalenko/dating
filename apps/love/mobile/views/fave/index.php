<?php
$this->pageTitle='Мои закладки';
$this->breadcrumbs=array(
	'Мои закладки'
);
?>
            <div class="title row">
                <div class="col-md-12 col-xs-12">
                    <h3 class="pull-left">Мои закладки</h3>
                </div>   
            </div>
<?php $this->renderPartial('_menu', array()); ?>

<div class="photos-list" id="fPhotos">
<?php $this->renderPartial('_photos', array('photos'=>$photos)); ?>
</div>
