<?php if (!$photos): ?>
<p class="text-muted text-center">Закладки пусты.</p>
<?php endif;?>
<div class="row profile-photos">
		<?php foreach($photos as $key=>$row): $params=[]; if (isset($_GET['albumId'])) $params['albumId']=$_GET['albumId'];?>
		<div id="i<?=$row->id; ?>" class="col-md-2 col-xs-3 col-sm-2 profile-photos-row ">
		<a href="<?=CHtml::normalizeUrl(array_merge(array('photos/show', 'userId'=>$row->user_id, 'photoId'=>$row->id), $params));; ?>"><img src="<?php echo ImageHelper::thumb(130, 100, $row->user_id, $row->filename, array('method' => 'adaptiveResize'));?>" class="img-responsive" border="0" /></a>
		
		</div>
		<?php endforeach; ?>
</div>

<div class="text-center">
<?php $this->widget('ext.bootstrap3.widgets.BsPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>

