<?php
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('lib/socket.io.js'), CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Html::jsUrl('im2.js'), CClientScript::POS_BEGIN);
$this->pageTitle='Просмотр диалогов';
$this->breadcrumbs=array('Просмотр диалогов');
?>


	


<script type="text/javascript">

$().ready(function() {
	IM.userId=<?=Yii::app()->user->id?>;
	IM.toUserId=<?=$user->id?>;
	IM.userName='<?=Yii::app()->user->data->login?>';
	IM.check='<?=$check?>';
	IM.title='<?=$user->login?> - Мгновенные сообщения';
	
	
	IM.init('<?=$hash?>');

</script>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
)); ?>

<div class="liveChat span9">
	
	<div class="chat" id="chatlog">
		
		
	</div>
	
	<div class="chat" id="systemlog" style="display:none;">
		
		
	</div>
        
</div>
        
	<?php echo $form->textArea($model, 'message', array('class'=>'span9', 'rows'=>2)); ?>
        
        <div style="margin-top:5px;">
         <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'size'=>'large', 'label'=>'Отправить')); ?>
        </div>


<?php $this->endWidget(); ?>