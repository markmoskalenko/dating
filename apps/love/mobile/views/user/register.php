
                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Создание новой анкеты</h3>
                        </div>      
                    </div>
			


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'registerForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,


)); ?>
<?php //echo CHtml::errorSummary($model);?>

<div class="form-group">
<?php echo $form->labelEx($model,'sex',array('label'=>'Ваш пол', 'class'=>'control-label')); ?>
<?php echo $form->dropDownList($model, 'sex', array('1'=>'Парень', '2'=>'Девушка'),array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'sex', array('class'=>'text-danger')); ?>
</div>	

<div class="form-group">
<?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>
<?php echo $form->textField($model, 'name', array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'name', array('class'=>'text-danger')); ?>
</div>	

<div class="form-group">
<?php echo $form->labelEx($model,'email',array('class'=>'control-label')); ?>
<?php echo $form->textField($model, 'email', array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'email', array('class'=>'text-danger')); ?>
</div>	

<div class="form-group">
<?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
<?php echo $form->textField($model, 'password', array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'password', array('class'=>'text-danger')); ?>
</div>	

<div class="form-group">
<?php echo $form->labelEx($model,'confirm_password',array('class'=>'control-label')); ?>
<?php echo $form->textField($model, 'confirm_password', array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'confirm_password', array('class'=>'text-danger')); ?>
</div>	

<div class="form-group">


 					<div class="cupcha left">
						 <?php echo $form->labelEx($model,'verifyCode',array('label'=>'Введите код', 'class'=>'control-label')); ?>
						<div class="cupcha-box txt-right">
							<?php $this->widget('CCaptcha', array('captchaAction' => 'user/captcha', 'clickableImage'=>true, 'showRefreshButton'=>false, 'imageOptions'=>array('style'=>'cursor:pointer;', 'id'=>'captcha'))); ?>

						</div>	
					</div>
 
 
<?php echo $form->textField($model, 'verifyCode', array('class'=>'form-control input-sm', 'style'=>'width:100px;')); ?>
<?php echo $form->error($model,'verifyCode', array('class'=>'text-danger')); ?>
				</div>	


				
<div class="form-group">
	<input type="submit" value="Зарегистрироваться" class="btn btn-primary btn-block btn-lg" /> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
</div>	
<?php $this->endWidget(); ?>