                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Восстановление пароля</h3>
                        </div>      
                    </div>

<?php if ($email=Yii::app()->user->getFlash('sentemail')): ?>

<?php //$this->widget('application.extensions.email.Debug'); ?>

<p>

На Ваш email (<?php echo $email; ?>) отправлено письмо с паролем.<br>
Если письмо не пришло то посмотрите в папке спам, иногда туда попадают письма с сайтов.</p>

<?php else: ?>

<p>Введите свой E-mail указанный вами при регистрации, и вам будет выслан пароль.</p>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'lostpasswordForm',
    'htmlOptions'=>array('class'=>'reg-form'),
    //'enableClientValidation'=>true,
   // 'enableAjaxValidation'=>true,
    'clientOptions'=>array(
            //'validateOnSubmit'=>true,
    ),
)); ?>

<div class="form-group">
<?php echo $form->labelEx($model,'email', array('label' => 'Ваш E-mail', 'class'=>'control-label')); ?>
<?php echo $form->textField($model,'email',array('class'=>'form-control input-sm')); ?>
<?php echo $form->error($model,'email', array('class'=>'text-danger')); ?>
</div>

<div class="form-group">
					<div class="cupcha left">
						<?php echo $form->labelEx($model,'verifyCode', array('label' => 'Введите код', 'class'=>'control-label')); ?>
						<div class="cupcha-box txt-right">
							<?php $this->widget('CCaptcha', array('captchaAction' => 'user/captcha', 'clickableImage'=>true, 'showRefreshButton'=>false, 'imageOptions'=>array('style'=>'cursor:pointer;border-radius:4px;', 'id'=>'captcha'))); ?>
						</div>	
					</div>	
					<?php echo $form->textField($model,'verifyCode',array('class'=>'form-control input-sm', 'style'=>'width:100px;')); ?>
				<?php echo $form->error($model,'verifyCode', array('style'=>'margin-left:212px;')); ?>
				</div>
				
<div class="form-group">
<input type="submit" value="Восстановить" class="btn btn-primary btn-block btn-lg" />
</div>
<?php $this->endWidget(); ?>
<?php endif; ?>