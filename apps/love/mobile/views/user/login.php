
            <div class="registration">
                <div class="row">
                    <div class="col-md-12">
                        <h4><a id="showModalRegisterBox" href="/register">Регистрация</a></h4>
                    </div>
                </div>
<?php $login=$this->beginWidget('LoginActiveForm', array(
    'action'=>CHtml::normalizeUrl(array('user/login')),
    'id'=>'auth6',
    'htmlOptions'=>array('class'=>'enter-site'),
)); ?>
                    <div class="inputs">
                       <?php echo $login->textField('username',array('class'=>'field', 'placeholder'=>'E-mail или логин')); ?>
                    </div>  
                    <div class="inputs">
                        <?php echo $login->passwordField('password',array('class'=>'field', 'placeholder'=>'Пароль')); ?>
                    </div>  
                    <div class="inputs">
                        <div class="checked pull-left">
                            <input type="checkbox" id="remind" value="">
                            <label for="remind">запомнить</label>
                        </div>
			<a href="<?=CHtml::normalizeUrl(array('user/lostpassword')); ?>" class="remind-link pull-right">Забыли пароль?</a>
                    </div>
                    <div class="inputs text-center">
                        <input type="submit" class="btn btn-primary standart" value="ВОЙТИ">
                    </div>
<?php $this->endWidget(); ?>
            </div>
