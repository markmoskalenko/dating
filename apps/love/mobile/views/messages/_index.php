<?php if (!$messages): ?>
<p class="text-muted text-center">Здесь будет выводиться список Ваших сообщений.

<br>
Пожалуйста, выберите <a href="<?=CHtml::normalizeUrl(array('search/index')); ?>"><b>пользователя</b></a>, чтобы начать общение.</p>


<?php else: ?>	
			
			
<div class="recording-wall b-notice" style="margin-left:0px;margin-right:0px;">


<?php foreach($messages as $index=>$row):?>
                <?php if ($index>0):?><hr style="margin-top:5px;margin-bottom:10px;"/><?php endif;?>
		
                <div class="row" id="comment_<?=$row->id?>" style="margin-right:0px;">
                    <div class="col-xs-3 col-sm-2 col-md-2 text-left" style="width:80px;">
<a href="<?=CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$row->user->id)); ?>"><img src="<?php echo $row->user->getImage(55,55, 'camera_s.png');?>" /></a>
                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="notice-text">
                                    <div class="head">
						<a href="<?=CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$row->user->id)); ?>"><?=$row->user->id ? $row->user->name : 'DELETED'?></a>
<?php if ($row->user->isOnline):?>
								<label class="dialogue-online"><?php if (!$row->user->isMobile):?><i class="fa fa-circle"></i><?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?></label>
<?php endif;?>
				    </div>
				    <div class="message<?php if($row->unread) echo ' unread';?>"><a href="<?=CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$row->user->id)); ?>"><?=$row->message ?></a></div>
                                    <div class="foot">
                                        <i class="fa fa-clock-o"></i>
                                         <?php echo Html::date($row->time);?>
					 
			 
                                    </div>  
                                </div> 
                            </div>     
                        </div>
                    </div>
                </div> 

<?php endforeach;?>

</div>

<div class="text-center">
<?php $this->widget('ext.bootstrap3.widgets.BsPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>


<?php endif; ?>