<?php $this->pageTitle='Написать сообщение';?>
<?php 		$this->breadcrumbs=array(
			'Написать сообщение',
		);
?>




                    <div class="title row">
                        <div class="col-xs-12 col-md-12">
                            <h3 class="pull-left">Написать сообщение</h3>
                        </div>    
  
                    </div>
		    
		    
            <div class="section-tabs row">
		    
                <div class="col-md-12 tabs-container">
                    <div class="tab-content">
<?php if(Yii::app()->user->hasFlash('sent')): ?>
<div class="alert flash-success">
	Сообщение отправлено.
</div>
<?php endif; ?>



<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'messageForm',
    'htmlOptions'=>array('class'=>'well1'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
)); ?>
	<div id="errorSummary" class="alert alert-danger" style="display:none;">
		</div>
<?php echo CHtml::errorSummary($model, '','',['class'=>'alert alert-danger']);?>


<div class="media">
<a class="pull-left" href="<?=$user->url; ?>"><img src="<?php echo $user->have_photo==1 ? ImageHelper::thumb(55, 55, $user->id, $user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('camera_s.png');?>" class="media-object" /></a>

  <div class="media-body">
    <h4 class="media-heading">Кому: <a href="<?=$user->url; ?>"><?php echo $user->name; ?></a></h4>
<?php if (Yii::app()->user->model->isReal):?>
<?php echo $form->textArea($model, 'message', array('class'=>'form-control', 'style'=>'height:120px;')); ?>

<div style="margin-top:5px; font-size:11px;">	
<input type="submit" value="ОТПРАВИТЬ" class="btn btn-primary btn-block btn-lg" onclick="sendMessage($('#messageForm')); return false;" />

		<a href="<?=CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$user->id)); ?>" class="left">Перейти в диалог</a>
<?php elseif (!Yii::app()->user->isGuest):?>
				<div>
					<div class="about-service" style="background: #fff; padding:12px;">
						<p>Ваша анкета не активна! Писать сообщения могут только активные пользователи сайта с статусом Реал.</p>
						<a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>"><b>Подтвердить реальность Вашей анкеты с помощью активации Реал статуса!</b></a>
					</div>
				</div>
<?php else:?>
				<div>
					<div class="about-service" style="background: #fff; padding:12px;">
						<p>Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать сообщение.</p>
					</div>
				</div>
<?php endif;?>
  </div>
</div>


<?php $this->endWidget(); ?>  </div></div>
</div>