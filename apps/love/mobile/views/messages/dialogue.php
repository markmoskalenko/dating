<?php $this->pageTitle='Диалог';?>
<?php 		$this->breadcrumbs=array(
			'Диалог',
		);
?>

            <div class="title row">
                <div class="col-md-9 col-xs-9">
                    <h3 class="pull-left"><?php echo $user->name?><?php if ($user->isOnline):?>
								<label class="dialogue-online"><?php if (!$user->isMobile):?><i class="fa fa-circle"></i><?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?></label>
<?php endif;?></h3>
                </div>
		<div class="col-xs-3 col-md-3">
                        <a class="pull-right addphotos-link" href="<?php echo CHtml::normalizeUrl(array('messages/index')); ?>"><i class="fa fa-reply"></i></a>       
                 </div>     
            </div>

	<div class="row dialogue">

	<div >

<?php if (Yii::app()->user->model->isReal):?>
<?php if(Yii::app()->user->hasFlash('sent')): ?>
<div class="alert flash-success">
	Сообщение отправлено.
</div>
<?php endif; ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'dialogueForm',
    'htmlOptions'=>array('class'=>'dialogue-form'),
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>false,
    'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnType'=>false,
            'validateOnChange'=>false,
            'beforeValidate'=>'js:function(){

                return true;
             }',

            //'afterValidate'=>'js:afterValidateSettingsForm',
    ),

)); ?>
<?php echo CHtml::errorSummary($model);?>

<div id="errorSummary" class="alert alert-danger alert-form-validate" style="display:none;"></div>

<?php echo $form->textArea($model, 'message', array('class'=>'form-control')); ?>
<input type="submit" value="Отправить" class="btn btn-primary btn-lg" onclick="YCMail.add($('#dialogueForm')); return false;" style="width:150px;" />
<span id="loading_sentForm"><i class="fa fa-spinner fa-spin"></i></span>
<?php $this->endWidget(); ?>	
<?php else:?>

					<div class="noreal-alert">
						<p>Ваша анкета не активна!<br>Писать сообщения могут только активные пользователи сайта с статусом Реал.</p>
						<a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>" class="btn btn-primary btn-block btn-lg">Получить Real - статус</a>
					</div>


<?php endif;?>

			
<?php if (!$user->isOnline) {
	echo '<div class="offline-dialogue-text">';
	echo  $user->name.' ';
    echo $user->sex==1 ? 'был ' : 'была ';
    echo 'в сети ';
    echo Html::date($user->last_date);
	if ($user->isMobile) {
		echo ' <i class="fa fa-mobile"></i>';
	}
    echo '</div>';
}
?>
			
			
<div id="container_mail">
<?php echo $this->getDialogue($user->id);?>
</div>


</div>
</div>