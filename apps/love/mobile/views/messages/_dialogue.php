<div class="recording-wall b-notice" style="margin-left:0px;margin-right:0px;">
<?php if ($messages): ?>
<?php foreach($messages as $index=>$row):?>
                <?php if ($index>0):?><hr style="margin-top:5px;margin-bottom:10px;"/><?php endif;?>
		
                <div class="row" id="comment_<?=$row->id?>" style="margin-right:0px;">
                    <div class="col-xs-3 col-sm-2 col-md-2 text-left" style="width:80px;">
<a href="<?=$row->dialogueUser->url; ?>"><img src="<?php echo $row->dialogueUser->getImage(55,55, 'camera_s.png');?>" /></a>
                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="notice-text">
                                    <div class="head">
						<a href="<?=$row->dialogueUser->url; ?>"><?=$row->dialogueUser->id ? $row->dialogueUser->name : 'DELETED'?></a>
				    </div>
				    <div class="message"><?=$row->message ?></div>
                                    <div class="foot">
                                        <i class="fa fa-clock-o"></i>
                                         <?php echo Html::date($row->time);?>
					 
			 
                                    </div>  
                                </div> 
                            </div>     
                        </div>
                    </div>
                </div> 
<?php endforeach;?>
<?php else: ?>
<p class="text-muted text-center">История сообщений пуста.</p>
<?php endif;?>
</div>
<div class="text-center">
<?php $this->widget('ext.bootstrap3.widgets.BsPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
