<?php $this->pageTitle='Мои Cообщения';?>
<?php
$this->breadcrumbs=array('Сообщения');
?>
            <div class="title row">
                <div class="col-md-12 col-xs-12">
                    <h3 class="pull-left">Мои Cообщения</h3>
                </div>  
            </div>

<?php $this->renderPartial('_menu', array()); ?>

	<div class="row dialogue" id="container_mail">
<?php $this->renderPartial('_index', array('messages'=>$messages, 'pages'=>$pages, 'count'=>$count)); ?>
		</div>