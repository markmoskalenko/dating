            <div class="title row">
                <div class="col-xs-6 col-md-6">
                    <h3 class="pull-left"><?=$this->user->name?></h3>
                </div>    
                <div class="col-xs-6 col-md-6">

                </div>    
            </div>
            <div class="wall row">
                <div class="col-md-12">

<div class="media media-user-profile">

<?php if ($this->user->have_photo==1):?>
						<a href="<?=CHtml::normalizeUrl(array('photos/show', 'userId'=>$this->user->id, 'albumId'=>0, 'photoId'=>$this->user->photoId)); ?>" id="showModalPhotoBox" class="pull-left"><img src="<?php echo $this->user->have_photo==1 ? ImageHelper::thumb(75, 75, $this->user->id, $this->user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('photo-activation.jpg');?>" class="media-object" alt="" /></a>
<?php else:?>
	<?php if ($this->isOwn):?>
		<a href="#" onclick="$('#uploadPhotoModal').arcticmodal();return false;" class="noavatar pull-left"><i class="fa fa-camera"></i><div>Загрузить фотографию</div></a>
	<?php else:?>
		<div class="noavatar"><i class="fa fa-camera"></i></div>
	<?php endif;?>
<?php endif;?>
  <div class="media-body">
    <h4 class="media-heading"><?=$this->user->name?></h4>
<?php if ($this->user->isOnline) {
	
	if (!$this->user->isMobile) {
		echo '<i class="fa fa-circle"></i>Online';
	} else {
		echo '<i class="fa fa-mobile"></i>Mobile';
	}
	
}
else {
	
	if ($this->user->isMobile) {
		echo '<i class="fa fa-mobile"></i>';
	}
    echo $this->user->sex==1 ? 'заходил ' : 'заходила ';
    echo Html::date($this->user->last_date);
}
?>


<div><?php echo Html::age($this->user->birthday);?>, <?php echo $this->user->geo->country;?>, <?php echo $this->user->geo->city;?></div>

  </div>
</div>

<?php if ($this->isOwn):?>
<div class="user-profile-menu">
<a href="<?php echo CHtml::normalizeUrl(array('edit/index')); ?>" class="btn btn-primary btn-block btn-lg"><i class="fa fa-pencil-square-o"></i>Редактировать страницу</a>
</div>
<?php endif;?>


<?php foreach($this->profileFields as $row): ?>
<?php if ($this->isOwn OR count($row[field])):?>
                    <div class="title row">
                        <div class="col-xs-6 col-md-6">
                            <h3 class="pull-left"><?php echo $row[title]?></h3>
                        </div>    
                        <div class="col-xs-6 col-md-6">
                                <?php if ($this->isOwn):?><a class="pull-right addphotos-link" href="<?php echo CHtml::normalizeUrl(array('edit/profile', 'section'=>$row[name])); ?>">Редактировать</a><?php endif;?>	
                        </div>    
                    </div>
                    <div class="data-user row">
                        <div class="col-md-12">
                            <ul>  
	<?php foreach($row[field] as $row): ?>
							<li>
			<label><?php echo $row[title]?></label>
			<span><?php echo $row[value]?></span>
							</li>
	<?php endforeach; ?>
                            </ul>
                        </div>    
                    </div>
<?php endif;?>
<?php endforeach; ?>

<div class="user-profile-menu">

<a href="<?php echo $this->user->url; ?>" class="btn btn-primary btn-block btn-lg">Вернуться к странице</a>
</div>

                </div>
            </div>