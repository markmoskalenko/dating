            <div class="title row">
                <div class="col-xs-12 col-md-12">
                    <h3 class="pull-left">Поднять свою анкету на самый верх!</h3>
                </div>     
            </div>
<div class="pickup clearfix">

	
<div id="pickup">
	
<div class="pku-a1">Сейчас вы на <?php echo Yii::app()->user->position; ?> месте.</div>

<div class="pku-a2">Чем выше в результатах поиска вы находитесь и в галереи лиц,
тем больше <?php echo Yii::app()->user->model->sex==1 ? 'девушек' : 'парней'; ?> обратят на вас своё внимание.</div>
<div class="pku-a3">Стоимость сервиса: <b><?=Yii::app()->user->getTransactionsTypes('lifting_up_page')?> монет.</b></div>
	<?php if (Yii::app()->user->balance>=Yii::app()->user->getTransactionsTypes('lifting_up_page')):?>
		<a href="#" onclick="pickup.go('<?=CHtml::normalizeUrl(array('profile/pickup')); ?>'); return false;" class="btn btn-primary btn-block btn-lg"><i class="fa fa-arrow-circle-up"></i>Поднять анкету</a>
	<?php else:?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'action'=>CHtml::normalizeUrl(array('pay/index')),
)); ?>
		<?php echo CHtml::hiddenField('KassaForm[amount]', Yii::app()->user->getTransactionsTypes('lifting_up_page')); ?>
		<input type="submit" value="Пополнить счет и Поднять анкету" class="btn btn-primary btn-block btn-lg" />
<?php $this->endWidget(); ?>
	<?php endif;?>
</div>
</div>