<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.ui.widget.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.fileupload.js'));  ?>
<?php Yii::app()->clientScript->registerScriptFile(Html::jsUrl('jquery.iframe-transport.js'));  ?>

<!-- Modal -->
					<div class="g-hidden" style="display:none;">
						<div class="box-modal" id="uploadPhotoModal">
							<p style="font-weight:bold; color:#246C9E;">Загрузка новой фотографии</p><div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i>закрыть</div>

        <div class="modal-body">
          

 
<script type="text/javascript">
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = (window.location.hostname === 'blueimp.github.com' ||
                window.location.hostname === 'blueimp.github.io') ?
                '//jquery-file-upload.appspot.com/' : 'server/php/';
    $('#fileupload').fileupload({
        url: '<?=CHtml::normalizeUrl(array('photos/upload', 'albumId'=>$_GET['albumId'])); ?>',
        dataType: 'json',
	formData: function (form) {
		
    return [{name: 'CSRFTOKEN',value: '<?php echo Yii::app()->request->csrfToken; ?>'}, {name: 'SESSION_ID',value: '<?php echo Yii::app()->session->sessionID; ?>'}];
},
    add: function (e, data) {
           /* $('.progress').attr(
                'class',
                'progress progress-striped active'
            );*/
        var goUpload = true;
        var uploadFile = data.files[0];
        if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
           // alert('You must select an image file only');
	   YiiAlert.error('Вы должны выбрать только файл изображения.', '402');
           goUpload = false;
        }
        if (uploadFile.size > 10000000) { // 2mb
           // alert('Please upload a smaller image, max size is 2 MB');
	   YiiAlert.error('Пожалуйста, загрузите изображение меньшего размера, максимальный размер равен 10 Мб.', '402');
            goUpload = false;
        }
        if (uploadFile.size < 20000) { // 20kb
           // alert('Please upload a smaller image, max size is 2 MB');
	   YiiAlert.error('Фотография слишком низкого качества.', '402');
            goUpload = false;
        }
        if (goUpload == true) {
        var jqXHR = data.submit()
            .success(function (result, textStatus, jqXHR) {
		
		location.reload(true);
		
/*$('#uploadModal').modal('hide');
            $('#progress').css(
                'width',
                '0%'
            );
		$('#done').prepend(result.html);*/
		})
            .error(function (jqXHR, textStatus, errorThrown) {
		YiiAlert.error(jqXHR.responseText, jqXHR.status);
		})
            .complete(function (result, textStatus, jqXHR) {
           /* $('.progress').attr(
                'class',
                'progress'
            );*/
		});
        }
	
	
	

    },
        progressall: function (e, data) {
		var progress = Math.ceil((data.loaded / data.total) * 100);
            $('#progress').css(
                'width',
                progress + '%'
            );
	    if (progress==100) {
	      $('#progress .sr-only').html(progress+'% Complete');
	    } else {
	      $('#progress .sr-only').html(progress+'% Loading');
	    }
	    
	   //$('#progress').attr("style", "width: "+progress+"%;");
        }
    });
});
</script>

<div>
    
    

    
    
    
    
<span class="btn2 btn-default btn-file" id="UploadPhotos">
                            <i class="fa fa-camera"></i><span>Выбрать файл...</span>
                            <input type="file" name="UploadForm[file]" id="fileupload"/>
                        </span>
<span>или Перетащите фотофайлы прямо в окно браузера.</span>
</div>

<br>
    
<div class="progress">
  <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    <span class="sr-only"></span>
  </div>
</div>

<div style="margin:0px 0 7px 0; color:#E2027F;"><b>Загружайте фотографии на страницу только с вашим изображением<br> и хорошо различимым лицом</b></div>
    
<div style="margin:7px 0 7px 0;"><strong>Запрещено загружать в главный альбом "Фотографии со страницы":</strong></div>
<ul style="font-size:11px;">
    <li>Фотографии без лица;</li>
    <li>Фотографии низкого качества или изменённые в графическом редакторе;</li>
    <li>Групповые фотографии;</li>
    <li>Пейзажи, фото животных, рисунки и «приколы»;</li>
    <li>Чужие фотографии (например, изображения известных людей);</li>
    <li>Порнографические образы (половые органы, половой акт и т.д.);</li>
    <li>Детские фотографии;</li>
    <li>Рекламу;</li>
    <li>Изображения со сценами насилия;</li>
</ul>
    
<div style="margin:7px 0 7px 0;"><strong>В остальных альбомах запрещено размещать:</strong></div>
<ul style="font-size:11px;">
    <li>Чужие и детские фотографии;</li>
    <li>Фотографии содержащие рекламу, посторонние надписи, названия сайтов;</li>
    <li>Фотографии порнографического характера (демонстрация половых органов);</li>
    <li>Фотографии оскорбляющие честь и достоинство пользователей сайта;</li>
    <li>Демонстрирующих сцены насилия над людьми или животными;</li>
    <li>Публичное распространение которых запрещено законом.</li>
</ul>
<div style="margin:7px 0 7px 0;"><strong>Фотография будет помещена в альбом "Разное", если:</strong></div>
<ul style="font-size:11px;">
    <li>На фото не видно или плохо видно Ваше лицо;</li>
    <li>Фото размытое, некачественное;</li>
    <li>Фотография изменена в графическом редакторе или на специальных сайтах.</li>
</ul>
<div style="margin:7px 0 7px 0;"><strong>Фотография будет помещена в альбом "18+", если:</strong></div>
<ul style="font-size:11px;">
    <li>Если на фото изображены обнаженные тела;</li>
</ul>

 
        </div>

						</div>
					</div>