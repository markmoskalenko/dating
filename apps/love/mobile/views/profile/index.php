<?php //Yii::app()->clientScript->registerScriptFile(Html::jsUrl('photo2.js'));  ?>
<?php //Yii::app()->clientScript->registerScriptFile(Html::jsUrl('message.js'));  ?>
<?php //Yii::app()->clientScript->registerScriptFile(Html::jsUrl('upload.js'));  ?>
<?php //Yii::app()->clientScript->registerScriptFile(Html::jsUrl('profile.js'));  ?>
<?php //Yii::app()->clientScript->registerScriptFile(Html::jsUrl('gifts.js'));  ?>
<?php
//Yii::app()->clientScript->registerCssFile(Html::cssUrl('profile.css'));
//if ($this->user->theme) Yii::app()->clientScript->registerCssFile(Html::cssUrl('themes/'.$this->user->theme.'/style.css'));
//$this->breadcrumbs=array($this->user->name);
?>
<!-- рур!-->
            <div class="title row">
                <div class="col-xs-6 col-md-6">
                    <h3 class="pull-left"><?=$this->user->name?></h3>
                </div>    
                <div class="col-xs-6 col-md-6">

			    
                </div>    
            </div>
            <div class="wall row">
                <div class="col-md-12">


<div class="media media-user-profile">

<?php if ($this->user->have_photo==1):?>
						<a href="<?=CHtml::normalizeUrl(array('photos/show', 'userId'=>$this->user->id, 'albumId'=>0, 'photoId'=>$this->user->photoId)); ?>" id="showPhoto" class="pull-left"><img src="<?php echo $this->user->have_photo==1 ? ImageHelper::thumb(75, 75, $this->user->id, $this->user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('photo-activation.jpg');?>" class="media-object" alt="" /></a>
<?php else:?>
	<?php /*/if ($this->isOwn):?>
		<a href="<?php echo CHtml::normalizeUrl(array('photos/upload')); ?>" class="noavatar pull-left"><i class="fa fa-camera"></i><div>Загрузить фотографию</div></a>
	<?php else:*/?>
		<div class="noavatar pull-left"><i class="fa fa-camera"></i></div>
	<?php //endif;?>
<?php endif;?>
  <div class="media-body">
    <h4 class="media-heading"><?=$this->user->name?></h4>
<?php if ($this->user->isOnline) {
	
	if (!$this->user->isMobile) {
		echo '<i class="fa fa-circle"></i>Online';
	} else {
		echo '<i class="fa fa-mobile"></i>Mobile';
	}
	
}
else {
	
	if ($this->user->isMobile) {
		echo '<i class="fa fa-mobile"></i>';
	}
    echo $this->user->sex==1 ? 'заходил ' : 'заходила ';
    echo Html::date($this->user->last_date);
}
?>

<?php if ($this->isOwn):?>
<div style="margin-bottom:2px;margin-top:2px;font-size:11px;">
	<a href="<?php echo CHtml::normalizeUrl(array('profile/editStatus', 'userId'=>$this->user->id)); ?>" id="change_status"<?php echo $this->user->profile->status ? ' style="color:#000;"' : '';?>><?php echo $this->user->profile->status ? $this->user->profile->status : 'изменить статус';?></a>

</div>
<?php elseif ($this->user->profile->status):?>
						<div style="margin-bottom:2px;margin-top:2px;font-size:11px;">
							<?=$this->user->profile->status?>
						</div>
<?php endif;?>

<div><?php echo Html::age($this->user->birthday);?>, <?php echo $this->user->geo->country;?>, <?php echo $this->user->geo->city;?></div>

  </div>
</div>


<?php if ($this->isOwn):?>
<div class="user-profile-menu">
<a href="<?php echo CHtml::normalizeUrl(array('photos/upload')); ?>" class="btn btn-primary btn-block btn-lg"><i class="fa fa-camera"></i>Загрузить фотографию</a>
<?php if ($this->user->isReal):?><a href="<?php echo CHtml::normalizeUrl(array('profile/pickup')); ?>" class="btn btn-primary btn-block btn-lg"><i class="fa fa-arrow-circle-up"></i>Поднять свою страницу «наверх»</a><?php endif;?>
<!--a href="<?php echo CHtml::normalizeUrl(array('edit/index')); ?>" class="btn btn-primary btn-block btn-lg"><i class="fa fa-pencil-square-o"></i>Редактировать страницу</a-->
<?php if ($this->isOwn && !$this->user->isReal):?>
                    <div class="noreal-alert" style="margin-top:5px;margin-bottom:-7px;">
                        <p>Ваша анкета не подтверждена реальностью! Вы пользуетесь ограниченной версией сайта. Большинство функций будет доступно после <a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>">активации анкеты</a>.</p>
                        <a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>" class="btn btn-primary btn-block btn-lg">Получить Real - статус</a>
                    </div>
<?php endif;?>
</div>


<?php elseif (!Yii::app()->user->isGuest):?>
<div class="user-profile-menu">
	<a href="<?php echo CHtml::normalizeUrl(array('messages/dialogue', 'id'=>$this->user->id)); ?>" class="btn btn-primary btn-block btn-lg" id="showModalSendMessage"><i class="fa fa-envelope"></i>Отправить сообщение</a>
<?php if (!$this->isFriend):?>
	<div id="friendshipRequest"><a href="<?php echo CHtml::normalizeUrl(array('friends/apply', 'id'=>$this->user->id)); ?>" class="btn btn-primary btn-block btn-lg" style="margin-top:3px;"><i class="fa fa-plus"></i>Добавить в друзья</a></div>
<?php endif;?>
<?php if ($this->isFriend): ?>
<div style="padding-top:10px;color:#626262;text-align:center; font-size:11px;"><?=$this->user->name?> у Вас в друзьях</div>
<?php endif;?>
</div>
		<?php /*$this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
                        'htmlOptions'=>array('class'=>'nav nav-pills nav-stacked', 'style'=>'margin-top:5px;margin-bottom:5px;'),
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
			'items'=>array(
				array('label'=>'<i class="fa fa-gift pull-right"></i>Отправить подарок', 'url'=>array('gifts/donate', 'id'=>$this->user->id), 'icon'=>'gift', 'itemOptions'=>array('id'=>'donateGifts')),
				array('label'=>$this->user->sex==2 ? '<i class="fa fa-heart right"></i>Понравилась' : '<i class="fa fa-heart pull-right"></i>Понравился', 'url'=>array('profile/addlike', 'userId'=>$this->user->id), 'icon'=>'heart', 'itemOptions'=>array('id'=>'addlike'), 'visible'=>!$this->isLike),
				array('label'=>'<label class="count pull-right"><i class="fa fa-camera"></i><b>'.$this->user->counter->photos.'</b></label>Фотографии со мной', 'url'=>array('photos/index', 'userId'=>$this->user->id), 'icon'=>'camera'),
			)
		));*/ ?>	
<?php else:?>
<div style="padding:3px;margin-bottom:10px;background:#F7F7F7;color:#626262;text-align:center; font-size:13px;">Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать сообщение.</div>
<?php endif;?>	





<?php $this->getPhotos('6'); ?>

                    <div class="title row">
                        <div class="col-xs-6 col-md-6">
                            <h3 class="pull-left">Информация</h3>
                        </div>    
                        <div class="col-xs-6 col-md-6">
                                <?php if ($this->isOwn):?><a class="pull-right addphotos-link" href="<?php echo CHtml::normalizeUrl(array('edit/index')); ?>">Редактировать</a><?php endif;?>	
                        </div>    
                    </div>
                    <div class="data-user row">
                        <div class="col-md-12">
                            <ul>      
                                <li>        
                                    <label>Пол:</label>
                                    <span><?php echo $this->user->sex=='2' ? 'Женский' : 'Мужской';?></span>
                                </li>   
				<?php if (Html::age($this->user->birthday)):?>
				<li>									
					<label>Возраст:</label>
					<span><?php echo Html::age($this->user->birthday).', '.Html::zodiak($this->user->birthday);?></span>
				</li>
				<?php endif;?>

                            </ul>
                            <a id="showFull_information" href="<?php echo CHtml::normalizeUrl(array('profile/info', 'userId'=>$this->user->id)); ?>">Показать полную информацию</a>
                        </div>    
                    </div>
		    
		    
                    <div class="title row">
                        <div class="col-xs-6 col-md-6">
                            <h3 class="pull-left">Другое</h3>
                        </div>    
                        <div class="col-xs-6 col-md-6">
                                
                        </div>    
                    </div>

		    
                        <div class="menu-wall clearfix">
                            <ul>
<?php if (!$this->isOwn):?>
                                <li><a href="<?php echo CHtml::normalizeUrl(array('gifts/donate', 'id'=>$this->user->id)); ?>" style="font-size:14px;"><i class="fa fa-gift" style="font-size:18px;"></i>Отправить подарок</a></li>
<?php endif;?>
<?php if ($this->isOwn):?>
                                <li><a href="<?=CHtml::normalizeUrl(array('profile/like', 'userId'=>$this->user->id)); ?>"><i class="fa fa-heart"></i>Вы понравились<span class="count"><?=$this->user->likes?></span></a></li>
<?php elseif (!Yii::app()->user->isGuest):?>
				<?php if(!$this->isLike):?><li><a href="<?php echo CHtml::normalizeUrl(array('profile/addlike', 'userId'=>$this->user->id)); ?>" id="addlike"><?php echo $this->user->sex==2 ? '<i class="fa fa-heart right"></i>Понравилась' : '<i class="fa fa-heart right"></i>Понравился'?></a></li><?php endif;?>
<?php endif;?>
                                <li><a href="<?php echo CHtml::normalizeUrl(array('photos/index', 'userId'=>$this->user->id)); ?>"><i class="fa fa-camera"></i>Фотографии<span class="count"><?php echo $this->user->counter->photos;?></span></a></li>
                                <li><a href="<?php echo CHtml::normalizeUrl(array('friends/index', 'userId'=>$this->user->id)); ?>"><i class="fa fa-user"></i>Друзья<span class="count"><?php echo $this->user->counter->friends;?></span></a></li>

				<?php if (!Yii::app()->user->isGuest):?><li><a href="#" onclick="$('#dopMenu').show(); $(this).hide();return false;">Ещё...</a></li><?php endif;?>
                            </ul>
			    
			    

<?php if ($this->isOwn):?>
                            <ul id="dopMenu" style="display:none";>
                                <li><a href="<?=CHtml::normalizeUrl(array('profile/like', 'userId'=>$this->user->id)); ?>"><i class="fa fa-heart"></i>Вы понравились<span class="count"><?=$this->user->likes?></span></a></li>

                            </ul>
<?php elseif (!Yii::app()->user->isGuest):?>
		<?php $this->widget('zii.widgets.CMenu',array(
			'id'=>'dopMenu',
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			'htmlOptions'=>array('style'=>'display:none;'),
			//'itemTemplate'=>'<span><span>{menu}</span></span>',
			'items'=>array(
				
				array('label'=>'<i class="fa fa-times"></i>Убрать из друзей', 'url'=>array('friends/index', 'cmd'=>'delete', 'id'=>$this->user->id), 'icon'=>'trash', 'visible'=>$this->isFriend),
				array('label'=>Blacklist::checkBlackByUser($this->user->id)->id ? '<i class="fa fa-check-circle"></i>Разблокировать' : '<i class="fa fa-ban"></i>Заблокировать', 'url'=>array('settings/blacklist', 'bId'=>$this->user->id), 'itemOptions'=>array('id'=>'AddToBlacklist')),
				array('label'=>'<i class="fa fa-warning"></i>Пожаловаться', 'url'=>array('abuse/index', 'userId'=>$this->user->id), 'icon'=>'warning-sign'),
				array('label'=>'Удалить', 'url'=>'#', 'visible'=>Yii::app()->user->role=='admin'),
				array('label'=>'Забанить', 'url'=>'#', 'visible'=>Yii::app()->user->role=='admin'),
			),
		)); ?>
<?php endif;?>
			    
                        </div>




                    <div class="title row">
                        <div class="col-md-12">
                            <h3 class="pull-left">Стена</h3>
                        </div>    
                    </div>
<?php if (/*$this->isOwn OR */Yii::app()->user->model->isReal):?>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'wallForm',
    'htmlOptions'=>array('class'=>'dialogue-form'),
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
    'action'=>$this->user->url,
)); ?>
					<div class="write-msg row" style="padding:2px;">
						<div class="col-md-12" style="padding:2px;">
							<div id="errorSummary" class="alert alert-danger alert-form-validate" style="display:none;"></div>
							<?php echo $form->textArea($wallForm,'message',array('class'=>'form-control', 'style'=>'')); ?>
							<input type="submit" value="ОТПРАВИТЬ" class="btn btn-primary btn-lg" style="width:100px;" onclick="YCWall.add($('#wallForm')); return false;" />
							<span id="loading_sentForm"><i class="fa fa-spinner fa-spin"></i></span>
						</div>
							
					</div>
<?php $this->endWidget(); ?>
<?php elseif (/*!$this->isOwn && */!Yii::app()->user->isGuest):?>
					<div class="write-msg row" style="padding:2px;">
						<div class="col-md-12" style="padding:2px;">
						Ваша анкета не активна!<br>Писать сообщения на стене могут только активные пользователи сайта с статусом Реал.
						<br><a href="<?=CHtml::normalizeUrl(array('activation/index')); ?>" class="">Получить Real - статус</a>
						</div>
							
					</div>
<?php else:?>
					<div class="write-msg row" style="padding:2px;">
						<div class="col-md-12" style="padding:2px;">
						Пожалуйста, <a href="<?php echo CHtml::normalizeUrl(array('user/login')); ?>"><b>войдите</b></a> на сайт или <a href="<?php echo CHtml::normalizeUrl(array('user/register')); ?>"><b>зарегистрируйтесь</b></a>, чтобы написать на стене сообщение.
						</div>
					</div>

<?php endif;?>

                    <div id="container_wall" class="recording-wall">

			<?php $this->renderPartial('wall'); ?>
			

                    </div>    
                </div>
            </div>