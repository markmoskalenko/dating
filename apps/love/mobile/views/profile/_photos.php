<?php if (count($photos)>0): ?>
                    <div class="title row">
                        <div class="col-xs-6 col-md-6">
                            <h3 class="pull-left"><?=Yii::t('app','{n} фотография|{n} фотографии|{n} фотографий', $this->user->counter->photos)?></h3>
                        </div>    
                        <div class="col-xs-6 col-md-6">
                            <a class="pull-right addphotos-link" href="<?php echo CHtml::normalizeUrl(array('photos/index', 'userId'=>$this->user->id)); ?>">Все</a>       
                        </div>    
                    </div>
		    


<div class="row profile-photos">

		<?php foreach($photos as $index=>$row): ?>
		
    <a href="<?=CHtml::normalizeUrl(array('photos/show', 'userId'=>$row->user_id, 'photoId'=>$row->id)); ?>" id="showModalPhotoBox"><img src="<?php echo ImageHelper::thumb(103, 122, $row->user_id, $row->filename, array('method' => 'adaptiveResize'));?>" class="profile-photos-row col-md-2 col-xs-4 col-sm-2 <?php if ($index>'2'):?>hidden-xs<?php endif;?>" alt=""></a>
		
		
		<?php endforeach; ?>
</div>
<?php endif;?>