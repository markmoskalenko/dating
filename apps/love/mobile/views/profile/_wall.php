
<div class="row dialogue">
<div class="recording-wall b-notice" style="margin-left:0px;margin-right:0px;">


<?php foreach($wall as $index=>$row):?>
                <?php if ($index>0):?><hr style="margin-top:5px;margin-bottom:10px;"/><?php endif;?>
		
                <div class="row" id="comment_<?=$row->id?>" style="margin-right:0px;">
                    <div class="col-xs-3 col-sm-2 col-md-2 text-left" style="width:80px;">
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'), '', array('class'=>'')
		), 
		$row->user->url
	);
else:
	echo CHtml::image(Html::imageUrl('camera_s.png'), '', array('class'=>''));
endif;
?>
                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="notice-text">
                                    <div class="head">
<?php
	//echo CHtml::image(Html::imageUrl($row->user->sex==2 ? 'user_female.png' : 'user_male.png'));
	//echo ' ';
	echo CHtml::link(
		$row->user->name ? $row->user->name : 'DELETED', 
		$row->user->url,
		array('rel'=>'ajax1')
	);
?>
<?php if ($row->user->isOnline):?>
								<label class="dialogue-online"><?php if (!$row->user->isMobile):?><i class="fa fa-circle"></i><?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?></label>
<?php endif;?>
<?php if ($row->photoId):?> <span style="color:#A0A0A0;">обновил<?php echo $row->user->sex==2 ? 'а' : ''?> фотографию на странице:</span><?php endif;?></span>

<?php if (Yii::app()->user->checkAccess('deleteComment', array('author_id'=>$row->user->id, 'own_id'=>$this->user->id))):?>
<a class="pull-right" title="удалить" rel="tooltip" href="#" onclick="deleteWall('<?=CHtml::normalizeUrl(array('profile/wall', 'delWallId'=>$row->id)); ?>'); return false;"><i class="fa fa-times"></i></a><?php endif; ?>
				    </div>
				    <div class="message">
								<?php if ($row->photo->id):?>
								<div class="row clearfix">
								<a href="<?=CHtml::normalizeUrl(array('photos/show', 'userId'=>$row->photo->user_id, 'photoId'=>$row->photo->id, 'albumId'=>0)); ?>" id="showModalPhotoBox"><img src="<?php echo ImageHelper::thumb(250, 380, $row->photo->user_id, $row->photo->filename, array('method' => 'adaptiveResize'));?>" class="col-md-6 col-sm-3 col-xs-10" alt=""></a>
	</div>
								<?php endif;?>
				    <div style="margin-top:0px;"><?=Html::message($row->message)?></div></div>
                                    <div class="foot">
                                        <i class="fa fa-clock-o"></i>
                                         <?php echo Html::date($row->date);?>
					 
			 
                                    </div>  
                                </div> 
                            </div>     
                        </div>
                    </div>
                </div> 

<?php endforeach;?>

</div>

<div class="text-center">
<?php $this->widget('ext.bootstrap3.widgets.BsPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
</div>