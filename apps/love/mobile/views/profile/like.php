            <div class="title row">
                <div class="col-md-9 col-xs-9">
                    <h3 class="pull-left"><?=Yii::t('app','{n} оценка|{n} оценки|{n} оценок', $count)?></h3>
                </div>
		<div class="col-xs-3 col-md-3">
                        <a class="pull-right addphotos-link" href="<?php echo CHtml::normalizeUrl(array('profile/index', 'userId'=>$_GET['userId'])); ?>"><i class="fa fa-reply"></i></a>       
                 </div>     
            </div>

	<div class="row dialogue">
	<div>
<div class="recording-wall b-notice" style="margin-left:0px;margin-right:0px;">
<?php foreach($like as $num=>$row):?>
                <?php if ($num>0):?><hr style="margin-top:10px;margin-bottom:10px;"/><?php endif;?>
		
                <div class="row" id="comment_<?=$row->id?>" style="margin-right:0px;">
                    <div class="col-xs-3 col-sm-2 col-md-2 text-left" style="width:80px;">
<?php 
	echo CHtml::link(
		CHtml::image( 
			$row->getImage(55,55, 'camera_s.png'),
			'',
			array('class'=>'ava')
		), 
		$row->url,
		array()
	);
?>
                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="notice-text">
                                    <div class="head">
						<a href="<?=$row->url; ?>"><?=$row->id ? $row->name : 'DELETED'?></a><?php if ($row->isOnline):?><label class="dialogue-online"><?php if (!$row->isMobile):?><i class="fa fa-circle"></i><?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?></label><?php endif;?>
				    </div>
                                </div> 
                            </div>     
                        </div>
                    </div>
                </div> 
<?php endforeach;?>
					</div>

<div class="text-center">
<?php $this->widget('ext.bootstrap3.widgets.BsPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
</div>
</div>