            <div class="title row">
                <div class="col-md-9 col-xs-9">
                    <h3 class="pull-left">Мои друзья</h3>
                </div>
		<div class="col-xs-3 col-md-3">
                        <?php if ($user->name):?><a class="pull-right addphotos-link" href="<?=CHtml::normalizeUrl(array($user->url)); ?>"><i class="fa fa-reply"></i></a><?php endif;?>
                 </div>     
            </div>
<div class="mytabs" style="margin-top:-15px;">
<?php $this->widget('Menu', array(
	'htmlOptions'=>array('class'=>'nav nav-tabs nav-friends'),
    'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'Все<span class="count">'.$this->friendsCounter.'</span>', 'url'=>array('friends/index', 'userId'=>$_GET['userId']), 'linkOptions'=>array('rel'=>'ajax1')),
				array('label'=>'Онлайн<span class="count">'.$this->onlineFriendsCounter.'</span>', 'url'=>array('friends/index', 'section'=>'online', 'userId'=>$_GET['userId']), 'linkOptions'=>array()),
				array('label'=>'Заявки<span class="count">'.Yii::app()->user->requestFriendsCounter.'</span>', 'url'=>array('friends/index', 'section'=>'requests'), 'linkOptions'=>array(), 'visible'=>Yii::app()->user->requestFriendsCounter && $this->isOwn),
				array('label'=>'Запросы<span class="count">'.$this->sentFriendsCounter.'</span>', 'url'=>array('friends/index', 'section'=>'sent'), 'linkOptions'=>array(), 'visible'=>$this->sentFriendsCounter && $this->isOwn),
			),
)); ?>
</div>
<?php if (!$friends): ?>
<p class="text-muted text-center">Нет друзей<?php if ($_GET['section']=='online') echo ' online'?>.</p>
<?php endif;?>
<div class="row dialogue">
<div class="recording-wall b-notice" style="margin-left:0px;margin-right:0px;">
<?php foreach($friends as $num=>$row):?>
                <?php if ($num>0):?><hr style="margin-top:5px;margin-bottom:10px;"/><?php endif;?>
		
                <div class="row" id="comment_<?=$row->id?>" style="margin-right:0px;">
                    <div class="col-xs-3 col-sm-2 col-md-2 text-left" style="width:80px;">
<?php
if ($row->user->id):
	echo CHtml::link(
		CHtml::image( 
			$row->user->getImage(55,55, 'camera_s.png'),
			'',
			array('class'=>'ava')
		), 
		$row->user->url,
		array()
	);
else:
	echo CHtml::image( 
			Html::imageUrl('camera_s.png'),
			'',
			array('class'=>'ava')
		);
endif;
?>
                    </div>
                    <div class="col-xs-8 col-sm-10 col-md-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="notice-text">
                                    <div class="head"><?php
	echo CHtml::link(
		$row->user->name ? $row->user->name : DELETED, 
		$row->user->url,
		array('rel'=>'ajax1')
	);
?>
<?php if ($row->user->isOnline):?>
								<label class="dialogue-online"><?php if (!$row->user->isMobile):?><i class="fa fa-circle"></i><?php else:?><i class="fa fa-mobile"></i>Mobile<?php endif;?></label>
<?php endif;?>
				    </div>
				    <div class="menu-wall clearfix">
		<?php $this->widget('Menu',array(
			//'type'=>'list',
			'htmlOptions'=>array('class'=>''),
			'encodeLabel'=>false,
			'hideEmptyItems'=>false,
			'firstItemCssClass'=>'first',
			'lastItemCssClass'=>'last',
			'items'=>array(
				array('label'=>'<i class="fa fa-envelope"></i>Написать сообщение', 'url'=>array('messages/dialogue', 'id'=>$row->user->id), 'linkOptions'=>array(), 'visible'=>Yii::app()->user->id!=$row->user->id),
				//array('label'=>'<i class="fa fa-camera"></i>Смотреть фотографии', 'url'=>array('photos/index', 'userId'=>$row->user->id, 'albumId'=>0, 'photoId'=>$row->user->photoId), 'linkOptions'=>array('id'=>'showModalPhotoBox')),
				//array('label'=>'<i class="fa fa-gift"></i>Отправить подарок', 'url'=>array('gifts/donate', 'id'=>$row->user->id), 'icon'=>'gift', 'itemOptions'=>array('id'=>'donateGifts'), 'visible'=>Yii::app()->user->id!=$row->user->id),
				array('label'=>'<i class="fa fa-times-circle"></i>Отменить', 'url'=>array('friends/index', 'section'=>'sent', 'cmd'=>'cancel', 'id'=>$row->user->id), 'icon'=>'minus-sign', 'visible'=>$_GET['section']=='sent', 'linkOptions'=>array('confirm'=>'Отменить заявку?')),
				array('label'=>'<i class="fa fa-check"></i>Принять', 'url'=>array('friends/index', 'section'=>'requests', 'cmd'=>'accept', 'id'=>$row->user->id), 'icon'=>'plus', 'visible'=>$_GET['section']=='requests'),
				array('label'=>'<i class="fa fa-times-circle"></i>Отклонить', 'url'=>array('friends/index', 'section'=>'requests', 'cmd'=>'decline', 'id'=>$row->user->id), 'icon'=>'minus-sign', 'visible'=>$_GET['section']=='requests', 'linkOptions'=>array('confirm'=>'Отклонить заявку?')),
				//array('label'=>'<i class="fa fa-times-circle"></i>Убрать из друзей', 'url'=>array('friends/index', 'cmd'=>'delete', 'id'=>$row->friend_userId), 'icon'=>'remove', 'visible'=>($_GET['section']=='' OR $_GET['section']=='online') && $this->isOwn, 'linkOptions'=>array('confirm'=>'Убрать из друзей?')),
			),
		)); ?>
				    </div>
				    <div class="message"></div>

                                </div> 
                            </div>     
                        </div>
                    </div>
                </div> 
<?php endforeach; ?>
</div>

<div class="text-center">
<?php $this->widget('ext.bootstrap3.widgets.BsPager', array(
        //'id'=>'wallPages',
        'pages'=>$pages,
       // 'cssFile'=>Html::cssUrl('pager.css'),
	'htmlOptions'=>array('class'=>'pagination pagination-sm'),
        //'maxButtonCount'=>5,
        'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
	'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
        'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
	'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
        ));
?>
</div>
</div>