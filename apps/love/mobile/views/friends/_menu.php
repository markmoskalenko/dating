
		
		<?php $this->widget('Menu',array(
			'htmlOptions'=>array('class'=>'tabs'),
			'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'Все друзья', 'url'=>array('friends/index', 'userId'=>$_GET['userId']), 'linkOptions'=>array()),
				array('label'=>'Друзья онлайн', 'url'=>array('friends/index', 'section'=>'online', 'userId'=>$_GET['userId']), 'linkOptions'=>array()),
				array('label'=>'Заявки в друзья <span>('.Yii::app()->user->requestFriendsCounter.')</span>', 'url'=>array('friends/index', 'section'=>'requests'), 'linkOptions'=>array(), 'visible'=>Yii::app()->user->requestFriendsCounter && $this->isOwn),
				array('label'=>'Отправленные заявки <span>('.$this->sentFriendsCounter.')</span>', 'url'=>array('friends/index', 'section'=>'sent'), 'linkOptions'=>array(), 'visible'=>$this->sentFriendsCounter && $this->isOwn),
			),
		)); ?>
