
			<div class="gifts clearfix" id="giftsSelect">
<?php foreach($gifts as $row):?>

<div class="row">
<a href="#" rel="selectGift" id="<?php echo $row->id;?>" data-price="<?php echo $row->price;?>" data-price-text="<?=Yii::t('app','{n} монета|{n} монеты|{n} монет', $row->price)?>"><div class="price"><?=Yii::t('app','{n} монета|{n} монеты|{n} монет', $row->price)?></div><img src="<?php echo Html::imageUrl('gifts/gift_'.$row->id.'.png');?>" width="90" height="90" alt=""></a>
</div>
<?php endforeach; ?>
			</div>

			<div id="giftForm" style="display:none; width:545px;">
				
				<div align="center" class="gift_img"><img src="" alt="" id="giftImg" style="margin:15px;" align="absmiddle"><i class="fa fa-angle-right"></i><img src="<?php echo $user->have_photo==1 ? ImageHelper::thumb(90, 90, $user->id, $user->photo, array('method' => 'adaptiveResize')) : Html::imageUrl('camera_s.png');?>" alt="" align="absmiddle" /></div>
				
				
				

					<div class="about-service" id="giftBalance" style="display:none;background: #F3F9FC; padding:12px; font-size:12px;">
						<p>Стоимость подарка: <span id="priceGift"></span></p>
						<p>У Вас не достаточно на счету монет, пожалуйста <a href="<?=CHtml::normalizeUrl(array('pay/index')); ?>"><b>пополните счет</b></a>.</p>

					</div>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'giftSendForm',
    'enableClientValidation'=>false,
    'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->hiddenField($model,'giftId',array('id'=>'giftId')); ?>
					<div class="title">
						<h4 class="left">Ваше сообщение к подарку</h4>
					</div>
					<div class="write-msg">
						<div style="font-size:11px;margin-bottom:6px;">Получатель: <a href="<?php echo $user->url;?>"><?php echo $user->name;?></a></div>
						<?php echo $form->textArea($model,'message',array('class'=>'', 'style'=>'')); ?>
						<div class="foot">
	<div id="errorSummary" class="alert alert-danger" style="display:none;">
		</div><a href="#" id="donateGifts_back"><i class="fa fa-reply"></i>Выбрать другой подарок</a>
							<input type="submit" value="ОТПРАВИТЬ" class="btn small right" onclick="gifts.add($('#giftSendForm')); return false;" />
						</div>	
					</div>
<?php $this->endWidget(); ?>
	
			</div>

<!--рур-->

