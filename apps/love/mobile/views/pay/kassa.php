<?php
$this->pageTitle =Yii::t(Yii::app()->language, 'Пополнение баланса');
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language, 'Пополнение баланса')
);
?>
            <div class="title row">
                <div class="col-md-12 col-xs-12">
                    <h3 class="pull-left">Пополнение баланса</h3>
                </div>   
            </div>

			<div class="section-tabs">
<?php $this->renderPartial('_menu'); ?>


				<div class="tab visible">
<?php if(Yii::app()->request->isPostRequest && $kassaForm->validate()): ?>
<script type="text/javascript">
var timetogo = 2;
var timer = window.setInterval(function()
{
    var str = timetogo;
    $('#counter').text(str);
    
    if (timetogo <= 0)
    {
        $("#goKassa").submit();
         window.clearInterval(timer);
    }
    timetogo--;
}, 1000);
</script> 
<style>
        .vc_text {margin: 10px 0px 10px 0px;font-weight:bold;font-size:11px;}
</style>
<form action="http://businesskassa.com/POST.php" method="post" id="goKassa">
	<input type="hidden" name="xml_encoded" value="<?= $xml_encoded; ?>" />
	<input type="hidden" name="lqsignature" value="<?= $lqsignature; ?>" />
	<input type="submit" class="btn btn-primary btn-lg" value="Оплатить в BusinessKass" style="width:200px;" />
	
	<div class="vc_text">Перенаправление на страницу оплаты через <span id="counter">3</span> сек.</div>
</form>
<div class="vc_text">
<img src="<?php echo Html::imageUrl('pays/sber.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/termin.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/visa.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/abank.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/bank.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/euro.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/liqpay.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/mastercard.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/p24.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/paypal.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/pm.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/svyz.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/w1.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/ya.png');?>" alt="" align="absmiddle" width="24">
<img src="<?php echo Html::imageUrl('pays/zp.png');?>" alt="" align="absmiddle" width="24">
</div>
<?php else: ?>


<div class="r">

<?php $form = $this->beginWidget('CActiveForm', array(
    'htmlOptions'=>array('class'=>'reg-form'),
)); ?>


<div style="margin-bottom:10px;">Cейчас у Вас на счету: <?=Yii::t('app','{n} монета|{n} монеты|{n} монет', Yii::app()->user->balance)?></div>


    	<?php echo $form->radioButtonList($kassaForm,'amount', array(
		//'1'=>'+1 монета',
		'5'=>'+5 монет',
		'10'=>'+10 монет',
		'20'=>'+20 монет',
		'30'=>'+30 монет',
		'50'=>'+50 монет',
		'100'=>'+100 монет',
		)); ?><br/>


	<?php echo $form->error($kassaForm,'amount',array('style'=>'color:red;')); ?>
	
<?php /*echo $form->labelEx($kassaForm,'phone',array('class'=>'col-lg-3 control-label')); ?>

		<?php echo $form->textField($kassaForm, 'phone',array('class'=>'txt-field')); ?>
		<?php echo $form->error($kassaForm,'phone');*/ ?>
	
<div class="form-group">
					<input type="submit" value="Продолжить" class="btn btn-primary btn-lg" style="width:100px;"/> <span id="loading_stat" style="display:none;"><img src='<?=Html::imageUrl('loading.gif')?>' align='absmiddle' /></span>
				</div>	
<?php $this->endWidget(); ?>




</div>
<?php endif; ?>


			</div>	
		</div>