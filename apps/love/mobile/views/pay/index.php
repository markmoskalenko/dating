<?php
$this->pageTitle =Yii::t(Yii::app()->language, 'Пополнение баланса');
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language, 'Пополнение баланса')
);
?>
			<div class="title">
				<h3 class="left"><?php echo Yii::t(Yii::app()->language, 'Пополнение баланса');?></h3>
			</div>	
			<div class="section-tabs">
<?php //$this->renderPartial('_menu'); ?>

				<div class="tab visible">




<div class="r">

<?php if(!$ok): ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'codeForm',
    'htmlOptions'=>array('class'=>'reg-sms'),
    'action'=>CHtml::normalizeUrl(array('transaction/index')),

)); ?>

	<ul id="errorSummary" style="display:none;color:red;">
		</ul>
				<div class="activation">
					<div class="col1">	
						<div style="font-size:14px;margin-bottom:8px;">
							
Чтобы пополнить баланс
						</div>
						
						<div class="inputs" style="margin-bottom:8px;">
							<?php echo $form->labelEx($SMSForm,'country', array('label'=>'Выберите страну')); ?>
							<?php echo $form->dropDownList($SMSForm,'country',$SMSForm->countries, array('class'=>'txt-field')); ?>
							
							<?php echo $form->error($SMSForm,'country',array('style'=>'margin-left:105px;color:red; margin-bottom:5px;')); ?>

						</div>
						
						<div style="font-size:14px;margin-bottom:8px;">
							
Отправьте <span style="color:#CD1B62;">SMS c  текстом <?=$SMSForm->tagPrefix('big');?></span> на короткий <span style="color:#CD1B62;">номер <?=$SMSForm->tagNumber('big');?></span>
						</div>
						
						<div class="inputs" style="margin-bottom:8px;">
							<?php echo $form->labelEx($SMSForm,'code', array('label'=>'Введите код который вам пришел на телефон')); ?>
							<?php echo $form->textField($SMSForm,'code',array('class'=>'txt-field')); ?>
							
							<?php echo $form->error($SMSForm,'code',array('style'=>'margin-left:105px;color:red; margin-bottom:5px;')); ?>

						</div>

						<div class="inputs">
							<input type="submit" value="Пополнить" class="button in-block" />
						</div>	
					</div>	

				</div>	

				
<?php $this->endWidget(); ?>

<?php else: ?>
Ваш счет успешно пополнен. <?php echo CHtml::link('Вернуться назад.',array('transaction/index')); ?>

<?php endif; ?>


</div>


			</div>	
		</div>