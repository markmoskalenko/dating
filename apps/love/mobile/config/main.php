<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/../../config/common.php'),
	array(
                'theme'=>'mobile',
                'preload'=>array(
                        'vc',
                        'iplog',
                ),
                'controllerPath' => dirname(dirname(__FILE__)).'/controllers',
                'viewPath' => dirname(dirname(__FILE__)).'/views',
                
                'import'=>array(
                        'application.mobile.components.*',
                        'application.mobile.filters.*',
                        'ext.bootstrap3.helpers.*',
                        'ext.bootstrap3.widgets.*',
                        'ext.bootstrap3.behaviors.*',
                ),
                'components'=>array(
                        'iplog'=>array(
                            'class'=>'application.mobile.components.CMobileIplog',
                        ),
                        'urlManager'=>array(
                                'showScriptName'=>false,
                                'urlSuffix'=>'',
                                'urlFormat'=>'path',
                                'rules'=>require(dirname(__FILE__).'/urlrules.php'),
                        ),
                        'clientScript' => array(
                            'class'=>'application.mobile.components.MobClientScript',
                            'packages'=>require(dirname(__FILE__).'/packages.php'),
                        ),
                        'log'=>array(
                                'class'=>'CLogRouter',
                                'routes'=>array(
                                        /*array(
                                                'class'=>'CProfileLogRoute',
                                                'enabled'=>true,
                                        ),*/
                                        array(
                                                'class'=>'CFileLogRoute',
                                                //'levels'=>'trace, info, profile, warning, error',
                                                'levels'=>'profile',
                                                //'showInFireBug' => true,
                                                'logFile' => 'mobile.profile.log',
                                        ),
                                        array(
                                                'class'=>'CFileLogRoute',
                                                'levels'=>'warning, error',
                                                //'showInFireBug' => true,
                                                'logFile' => 'mobile.error.log',
                                        ),
                                        array(
                                                'class'=>'CFileLogRoute',
                                                'levels'=>'trace, info',
                                                //'showInFireBug' => true,
                                                'logFile' => 'mobile.info.log',
                                        ),
                                        /* array( // configuration for the toolbar
                                                'class'=>'XWebDebugRouter',
                                                'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle', // default config
                                                //'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
                                                'levels'=>'error, warning, trace, profile, info',
                                                //'allowedIPs'=>array('127.0.0.1','192.168.1.54'),
                                        ),*/
                                ),
                        ),
                ),
	)
);