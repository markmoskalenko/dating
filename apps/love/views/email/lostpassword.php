<table cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tr>
        <td align="center" height="100%" width="100%" valign="top" style="background:url(<?= Html::imageUrl('mail_bg.png')?>) center center #f1f4f7">
            <div style="width:700px;margin: auto;padding:80px 0;">
                <table cellpadding="0" cellspacing="0" height="100%" width="100%" style="font-family:Arial, Helvetica, sans-serif;line-height:1.3;overflow:hidden;border-radius:6px;-moz-border-radius:6;">
                    <tr><td bgcolor="#dfebf2" height="80" align="center"><img border="0" src="<?= Html::imageUrl('logo.png')?>" alt="Логотип" width= "226px" height= "38px"
                                                                              style="margin-top:7px; background-position: 0px -0px!important;"></td></tr>
                    <tr><td bgcolor="#ffffff" align="center" style="padding:1em;">
                            <div style="font-size:20px;margin-bottom:5px;"><strong>Восстановление пароля</strong></div>
                            <div style="font-size:14px;">для восстановления пароля нужно пройти по ссылке ниже.</div>
                            <br>
                            <br>
                            <div>
                                <a href="<?= $domain ?><?php echo CHtml::normalizeUrl(array('/user/changepswd', 'id'=>$userId, 'token'=>$token)); ?>" style="font-size:18px;color:#fff;background-color:#3c79ab;padding:10px 20px;text-decoration:none;border-radius:6px;-moz-border-radius:6;"> Сменить сейчас</a>
                            </div>
                            <br><br>
                        </td></tr>
                    <tr><td bgcolor="#dfebf2" height="80" align="center" style="color:#818181;line-height:1.3;">
                            <span style="font-size:11px;">Наша команда <a style="color:#006699;" href="<?php echo CHtml::normalizeUrl(array('/site/index')); ?>">lovemeet.me</a></span><br>
<!--                            <span style="font-size:9px;">Чтобы отписаться от рассылки, перейдите по <a style="color:#006699;" href="http://test6.it-yes.com/">этой</a> ссылке </span>-->
                        </td></tr>
                </table>
            </div>
        </td>
    </tr>
</table>