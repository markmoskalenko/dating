<?php

return array (
        'Page was deleted or does not exist yet.' => 'Страница удалена, либо еще не создана.',
    'The user has already deleted.' => 'Пользователь уже удален.',
    'User not found.' => 'Пользователь не найден.',
    'You can not add yourself as a friend.' => 'Вы не можете добавить себя в качестве друга.',

'You have already applied for this user.' => 'Вы уже подавали заявку этому пользователю.',
'This user is already in your friends.' => 'Этот пользователь уже у вас в друзьях.',
'This user has already submitted a request to you.' => 'Этот пользователь уже подал вам заявку.',
);
