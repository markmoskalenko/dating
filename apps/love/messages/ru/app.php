<?php

return array (
    'Incorrect username or password.' => 'Неправильное имя пользователя или пароль.',
    
  'Rating' => 'Рейтинг',
  'Photo' => 'Фото',
  'Friends' => 'Друзья',
  'My Friends' => 'Мои Друзья',
  'Online' => 'Сейчас на сайте',
  'I looking' => 'Я ищу',
  'Age' => 'Возраст',
  'Orientation' => 'Ориентации',
  'The purpose of dating' => 'Цель знакомства',
  '{n} person found|Found {n} people|Found {n} people' => 'Найден {n} человек|Найдено {n} человека|Найдено {n} человек',
  'Your request returned no results'=>'Ваш запрос не дал результатов',
  'Try to simplify your search criteria'=>'Попробуйте упростить критерии поиска',
  
  'Today in :time' => 'Сегодня в :time',
  ':date in :time' => ':date в :time',
  
  
  'You have already applied for' => 'Вы уже подавали заявку',
  'Application sent to friends' => 'Заявка в друзья отправлена',
  'User :user you have applied for' => 'Пользователь :user вам уже подал заявку',
  'You already are friends' => 'Вы уже дружите',
  
  
  
  'Name' => 'Имя',
  'Descr' => 'Описание',
  'Subject' => 'Тема',
  'Message' => 'Сообщение',
  'Album' => 'Альбом',
  'All photos' => 'Все фотографии',
  'Photos from' => 'Фотографии со страницы',
  'My photos' => 'Мои фотографии',
  'Edit album' => 'Редактировать альбом',
  'Create album' => 'Создать альбом',
  'Photo :photoNumber of :photoTotal' => 'Фотография :photoNumber из :photoTotal',
  'Access To View' => 'Кто может просматривать этот альбом?',
  'Access To Comment' => 'Кто может комментировать фотографии?',
  'Manager upload photos' => 'Менеджер загрузки фотографий',
  
  'Miscellanea' => 'Разное',
  'Page was deleted or does not exist yet.' => 'Страница удалена либо ещё не создана. ',
);
