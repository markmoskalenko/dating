<?php

class ImMessages extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{im_messages}}';
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'from_user',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}
	
	public function rules()
	{
                return array(
                        array('message', 'required'),
                        
                );
		
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->time = time();
				$this->from_user = Yii::app()->user->id;
			}
			return true;
		}
		return false;
	}
}