<?php

class KassaForm extends CFormModel
{
	public $amount;
	public $phone;
	
	public function rules()
	{
		return array(
			array('amount','required', 'message'=>'Выберите сумму пополения баланса'),
			array('amount', 'numerical'),
			array('amount,', 'in','range'=>range(1,100)),
                        //array('phone', 'required', 'message'=>'Введите номер телефона'),
			//array('phone', 'match',   'pattern'    => '/^[0-9]+$/u'),
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	


}