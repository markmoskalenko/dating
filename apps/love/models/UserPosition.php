<?php

class UserPosition extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{user_position}}';
	}
	
	public function defaultScope()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->time = time();
			
			return true;
		}
		return false;
	}
}