<?php

class GEORegion extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{region}}';
	}
	
	public function listData($country_id)
	{
		$models=GeoRegions::model()->cache(86400)->findAll(array('order'=>'title ASC', 'condition'=>'country_id=:country_id', 'params'=>array(':country_id'=>$country_id)));
		$listData=array();
		foreach($models as $model)
		{
			$value=CHtml::value($model,'region_id');
			$text=CHtml::value($model,'title');
			$listData[$value]=$text;
		}
		return $listData;
		
	}
}