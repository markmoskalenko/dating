<?php

class UserAuthorization extends CActiveRecord
{
        public $old_password;
        public $new_password;
        public $confirm_password;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{user_authorization}}';
	}
	
	public function defaultScope()
	{
		return array(
		);
	}

	public function rules()
	{
                return array(
                        array('email', 'email', 'on'=>'editEmail'),
			array('email', 'unique', 'className'=>'UserAuthorization' ,'on'=>'editEmail'),
			array('email', 'filter', 'filter'=>'mb_strtolower', 'on'=>'editEmail'),
                        array('email', 'required', 'on'=>'editEmail'),
			
                        array('phone', 'required', 'on'=>'editPhone'),
			array('phone', 'unique', 'on'=>'editPhone'),
			array('phone', 'match',   'pattern'    => '/^[0-9]+$/u', 'on'=>'editPhone'),
			
			array('old_password, new_password, confirm_password', 'required', 'on'=>'editPassword'),
			array('new_password', 'length' ,'min' => 3, 'max' => 25 , 'on'=>'editPassword'),
			array('confirm_password', 'compare', 'compareAttribute'=>'new_password', 'on'=>'editPassword'),
			array('old_password','oldPassword', 'on'=>'editPassword'),
                );
		
	}
	
	public function attributeLabels()
	{
		return array(
			'phone'			=> 'Телефон',
			'password' 		=> 'Пароль',
			'confirm_password' 	=> 'Повторите пароль',
			'new_password' 	=> 'Новый пароль',
			'old_password' 	=> 'Старый пароль',
		);
	}
	
	public function oldPassword($attr,$params)
	{
		
		
		if ($this->old_password!=$this->password)
			$this->addError('old_password','Указан неверный старый пароль');
			
	}
	
	public function relations()
	{
		return array(
			'pages' => array(
				self::BELONGS_TO,
				'UserPages',
				'user_id',
				//'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			return true;
		}
		return false;
	}
	
	public static function hashPassword($password)
	{
		return md5($password.'fsjysktrs436hddbnt4gfh4ehff54KJJfE3rgfjgds');
	}

    public function generateRestoreToken()
    {
        return md5($this->user_id.$this->email);
    }

    public static function getByAuthToken($token)
    {
        $oUser = UserAuthorization::model()->find('auth_token = :token', array('token'=>$token));

        if ($oUser){
            $oUser->auth_token = self::generateAuthToken($oUser);
            $oUser->save();
        }

        return $oUser;
    }

    public function getAuthToken()
    {
        if (!$this->auth_token){
            $this->auth_token = self::generateAuthToken($this);
            $this->save();
        }

        return $this->auth_token;
    }

    public static  function generateAuthToken($oUser)
    {
        return md5($oUser->email.$oUser->hash_password.time());
    }

    public static function sendConfirmEmail($userId)
    {
        $oUserA = UserAuthorization::model()->find('user_id = :id', array('id'=>$userId));
        $oUser = User::model()->find($userId);

        if ($oUserA->email_confirm){
            return true;
        }

        if ($oUserA->token == '' || $oUserA->token == null) {
            $oUserA->token = $oUserA->generateRestoreToken();
            $oUserA->save();
        }

        $authToken = $oUserA->getAuthToken();


        $message = new YiiMailMessage;
        $message->view = 'confirmEmail';
        $message->setBody( array('domain'=> 'http://'.Yii::app()->request->domain, 'user'=>$oUser, 'userA'=>$oUserA, 'authToken'=>$authToken), 'text/html');
        $message->subject = 'Подтверждение адреса электронной почты';
        $message->addTo( $oUserA->email );

        $message->from = 'admin@'.Yii::app()->request->domain;

        if(!(Yii::app()->mail->send($message))) {
            throw new CHttpException(500,'Mailer Error.');
        } else {
            return true;
        }

        return false;
    }
}