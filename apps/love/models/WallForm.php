<?php

class WallForm extends CFormModel
{
	public $message;
	
	public function rules()
	{
		return array(
			array('message', 'filter', 'filter'=>'trim'),
			array('message', 'filter', 'filter'=>'strip_tags'),
			array('message', 'required',),
			array('message', 'length', 'min'=>3,),
			array('message', 'length', 'max'=>1500,),
			array('message', 'filter', 'filter' => array($this, 'v')),
			//array('message', 'antiflood'),
		);
	}
	
	public function v($str)
	{
		$str=nl2br($str);
		return preg_replace('/(<br[^>]*>)(?:\s*\1)+/','$1',$str);
	}
	
	public function antiflood($attribute,$params)
	{
		/*$criteria = new CDbCriteria;
		$criteria->select='date';
		$criteria->condition='wrote_user_id=:wrote_user_id AND user_id=:user_id AND ip_address=:ip_address';
		$criteria->params=array(':wrote_user_id'=>Yii::app()->user->id, ':user_id'=>$_GET['id'], ':ip_address'=>XHttpRequest::getUserHostAddress());
		$criteria->order='date desc';
		$result=$this->find($criteria);
		
		$sec=time()-$result[date];*/
		//if ($sec<60)
		//print_r($params);
		$this->addError('message','antiflood');
	}
}