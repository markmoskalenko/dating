<?php

class Notifications extends CActiveRecord
{
	const PHOTO_APPROVED = 1;
	const PHOTO_MOVED = 2;
	const PHOTO_REMOVED = 3;
	const COMMENTED = 5;
	const LIKE = 6;
	const LIKE_PHOTO = 4;
	const FRIEND_REQUEST_ACCEPTED = 7;
	const FRIEND_REQUEST_DECLINE = 8;
	const FRIEND_DELETE = 9;
	const SENT_GIFT = 10;
	const TEXT = 11;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{notifications}}';
	}

	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'from_userId',
				'select'=>'login, sex, name, birthday, photoId, photo, have_photo',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'photo' => array(
				self::BELONGS_TO,
				'Photos',
				'photoId',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'photoComment' => array(
				self::BELONGS_TO,
				'CommentsPhotos',
				'photoCommentId',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}

	public function getDate()
	{
		return Html::date($this->time);
	}


	public static function add($type, $params)
	{
		if (Yii::app()->name!='console') {
			if (Yii::app()->user->id==$params['userId'])
				return;
		}
		
		$notifications=new Notifications;
		$notifications->userId=$params['userId'];
		if (!in_array($type, array(self::PHOTO_APPROVED,
						self::PHOTO_MOVED,
						self::PHOTO_REMOVED))) {
			$notifications->from_userId=Yii::app()->user->id;
		}
		$notifications->photoId=isset($params['photoId']) ? $params['photoId'] : 0;
		$notifications->photoCommentId=isset($params['photoCommentId']) ? $params['photoCommentId'] : 0;
		$notifications->giftId=isset($params['giftId']) ? $params['giftId'] : 0;
		$notifications->text=$params['text'];
		$notifications->type=$type;
		$notifications->save();
		User::model()->updateCounters(array('notifications'=>1),'id=:id',array(':id'=>$params['userId']));
		
		if(Yii::app()->params['noty'])
		{
			$photo=false;
			$text=$notifications->text;
			$user=NULL;
			if ($notifications->from_userId)
				$user=User::model()->findByPk($notifications->from_userId);
			if ($notifications->photoId)
				$photo=Photos::model()->findByPk($notifications->photoId)->getImage('55','55');
			if ($notifications->photoCommentId)
				$text=CommentsPhotos::model()->findByPk($notifications->photoCommentId)->message;
				
			if ($notifications->type==self::SENT_GIFT) {
				//$g=GiftsUsers::model()->findByPk($notifications->giftId);
				$photo=$notifications->giftId;
				//$text=$g->message;
			}
				
			Yii::app()->CURL->run('http://'.Yii::app()->params['noty_hostServer'].'/notifications', false, array(
				'type'=>$notifications->type,
				'userId'=>$notifications->userId,
				'sender_userId'=>$notifications->from_userId,
				'hashUser'=>md5($notifications->userId.'dsbetngaedgc'),
				'text'=>$text,
				'image'=>isset($user->id) ? $user->getImage('55','55') : false,
				'photo'=>$photo,
				'sex'=>isset($user->id) ? $user->sex : false,
				'name'=>isset($user->id) ? $user->name : false));
		}
	}

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->time = time();
			}
			return true;
		}
		return false;
	}
}