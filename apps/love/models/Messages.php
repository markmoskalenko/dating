<?php

class Messages extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{messages}}';
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'to_userId',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}
	
	public function rules()
	{
                return array(
                        array('message', 'required'),
			array('message, subject', 'filter', 'filter'=>'strip_tags'),
			array('message, subject', 'filter', 'filter'=>'trim'),
			array('subject', 'length' , 'max' => 150),
			array('message', 'length' , 'max' => 5000),
                );
		
	}
	
	public function attributeLabels()
	{
		return array(
			'subject' => Yii::t('app', 'Subject'),
			'message' => Yii::t('app', 'Message'),
		);
	}
	
	public function getDialogueUser()
	{
                if ($this->folder=='inbox')
			return $this->user;
		else
			return Yii::app()->user->model;
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				
			}
			return true;
		}
		return false;
	}
	
	protected function afterSave()
	{
		if($this->isNewRecord && $this->folder=="inbox" && Yii::app()->params['noty'])
		{
			$user=User::model()->findByPk($this->to_userId);
			Yii::app()->CURL->run('http://'.Yii::app()->params['noty_hostServer'].'/send', false, array(
				'from_userId'=>$this->from_userId,
				'to_userId'=>$this->to_userId,
				'hashUser'=>md5($this->from_userId.'dsbetngaedgc'),
				//'url'=>CHtml::normalizeUrl(array('messages/dialogue', 'dialogue')),
				'image'=>$user->getImage('55','55'),
				'name'=>$user->name,
				'message'=>$this->message));
			
			$mailDelivery=new MailDelivery;
			
			$mailDelivery->to_userId=$this->from_userId;
			$mailDelivery->view='noty_messages';
			$mailDelivery->view='noty_messages';
			
			$mailDelivery->save();
			
		}
	}
}