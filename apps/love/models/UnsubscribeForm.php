<?php

class UnsubscribeForm extends CFormModel
{
	public $phone;
	public $email;
	public $message;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// имя
			array('phone', 'required', 'message'=>'Введите ваш номер телефона для отписки.'),
			array('phone', 'match', 'pattern'=>'/(^(\+7)(9|8)[0-9]{9}$)/i', 'message'=>'Номер абонента должен быть в формате +79056543201'),
			//array('phone', 'filter', 'filter'=>array($this, 'phoneFilter')),
			//array('phone', 'stopSub'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'phone'			=> 'Ваш номер телефона',
			'email'			=> 'Ваш email',
			'message'			=> 'Сообщение',
		);
	}
	
	public function phoneFilter($value)
	{
		return '7'.substr(str_replace("+","",trim($value)),1,10);
	}
	
	/*public function stopSub($attribute,$params)
	{
		$this->addError('phone',Yii::t('app','У Вас нет подписок на Контент.'));
	}*/
	
	public function unsubscribe()
	{
		$skey='dsg544shrehb5e4bsgfj';
		$serviceId='3342';
		$action='CLOSE';
		
		$url='http://sub-bill.ru/http/subscr_pl/sub_api.php';
		
		$data = array(
			'action' => $action,
			'serviceId' => $serviceId,
			'msisdn' => $this->phoneFilter($this->phone),
			'hash' => md5($skey.$action.$serviceId.$this->phoneFilter($this->phone)),
		);
		
		foreach($data as $key=>$value)
		{
			$i++;
			if ($i==1)
				$url.="?";
			else
				$url.="&";
				
			$url.=$key."=".$value;
		}
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_TIMEOUT,80);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_COOKIEJAR,'cookies.txt');
		curl_setopt($ch,CURLOPT_COOKIEFILE,'cookies.txt');
		return CJSON::decode(curl_exec($ch));
	}
}