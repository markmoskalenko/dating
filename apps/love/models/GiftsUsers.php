<?php

class GiftsUsers extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{gifts_users}}';
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'sent_userId',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}
	
	public function rules()
	{
		return array(
			array('giftId', 'required',),
			array('message', 'length', 'min'=>3,),
			array('message', 'length', 'max'=>1500,),
			array('message', 'filter', 'filter'=>'strip_tags'),
			array('userId', 'safe'),
			array('price', 'checkBalance'),
			//array('message', 'antiflood'),
		);
	}
	
	public function checkBalance($attribute,$params)
	{
		$gift=Gifts::model()->findByPk($this->giftId);
		if (Yii::app()->user->balance>=$gift->price) {
			Yii::app()->user->model->saveAttributes(array('balance'=>new CDbExpression('`balance`-'.$gift->price)));
		} else {
			$this->addError('price','refill_your_account');
		}
		

	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->sent_userId = Yii::app()->user->id;
				$this->date = time();
			}
			return true;
		}
		return false;
	}
}