<?php

class User extends CActiveRecord
{
        public $email;
        public $password;
        public $old_password;
        public $new_password;
        public $confirm_password;
	public $verifyCode;

	public $day_birth;
	public $month_birth;
	public $year_birth;
	public $ItemsBirthday;
    public $agree_rules;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{users}}';
	}

	public function relations()
	{
		return array(
			'profile' => array(
				self::BELONGS_TO,
				'Profiles',
				'id',
				//'joinType' => 'LEFT OUTER JOIN',
			),
			'auth' => array(
				self::BELONGS_TO,
				'UserAuthorization',
				'id',
				//'joinType' => 'LEFT OUTER JOIN',
			),
			'counter' => array(
				self::BELONGS_TO,
				'UserCounters',
				'id',
				//'joinType' => 'LEFT OUTER JOIN',
			),
			'position' => array(
				self::BELONGS_TO,
				'UserPosition',
				'id',
				//'order'=>'position.time DESC',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'country' => array(
				self::BELONGS_TO,
				'GEOCountry',
				'country_id',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'region' => array(
				self::BELONGS_TO,
				'GEORegion',
				'region_id',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'city' => array(
				self::BELONGS_TO,
				'GEOCity',
				'city_id',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'like' => array(
				self::HAS_ONE,
				'UserLikes',
				'userId',
				'order'=>'like.time DESC',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'likePhoto' => array(
				self::HAS_ONE,
				'Like',
				'userId',
				'order'=>'likePhoto.time DESC',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'notification' => array(
				self::HAS_ONE,
				'Notifications',
				'from_userId',
				'order'=>'notification.id DESC',
				//'joinType' => 'LEFT OUTER JOIN',
			),
			'fave' => array(
				self::HAS_ONE,
				'UserLikes',
				'own_userId',
				'order'=>'fave.time DESC',
				'joinType' => 'LEFT OUTER JOIN',
			),
			'blacklist' => array(
				self::HAS_ONE,
				'Blacklist',
				'black_userId',
				'order'=>'blacklist.id DESC',
			),
			'photo' => array(
				self::BELONGS_TO,
				'Photos',
				'photoId',
				//'joinType' => 'LEFT OUTER JOIN',
			),
			'image' => array(
				self::BELONGS_TO,
				'Photos',
				'photoId',
				//'joinType' => 'LEFT OUTER JOIN',
			),
			'bot' => array(
				self::HAS_ONE,
				'Bots',
				'user_id',
				//'joinType' => 'INNER JOIN',
			),
            'photos' => array(
                self::HAS_MANY,
                'Photos',
                'user_id',
            ),
            'visitors' => array(
                self::HAS_MANY,
                'UserVisits',
                'visited_user_id'
            )
		);
	}

	public function defaultScope()
	{
		return array(
			
		);
	}

	public function rules()
	{
                return array(
			//рега
                        array('name, email, password, confirm_password,verifyCode', 'required', 'on'=>'register'),
                        array('confirm_password', 'compare', 'compareAttribute'=>'password', 'on'=>'register'),
                        array('email', 'email', 'on'=>'register, editEmail'),
			array('email', 'unique', 'className'=>'UserAuthorization' ,'on'=>'register, editEmail'),
			array('email', 'filter', 'filter'=>'mb_strtolower'),
			array('sex', 'in', 'range'=>array('1','2'),'on'=>'register,edit'),
			array('login', 'match',   'pattern'    => '/^[A-Za-z0-9_]+$/u', 'on'=>'editLogin'),
			array('login', 'length' ,'min' => 5, 'max' => 25 , 'on'=>'editLogin'),
			array('login', 'unique', 'on'=>'editLogin'),
			array('name', 'length' ,'min' => 3, 'max' => 25 , 'on'=>'register'),
			array('password', 'length' ,'min' => 3, 'max' => 25 , 'on'=>'register'),
			array('verifyCode','myCaptcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on'=>'register'),
			array('country_id,region_id,city_id', 'safe', 'on'=>array('register','editMain')),

           // Регистрация на теме meetdate
            array('name, email, password', 'required', 'on'=>'registerMeetDate'),
            array('agree_rules', 'compare','compareValue'=>1, 'on'=>'registerMeetDate', 'message'=>'Для регистрации необходимо согласиться с правилами сайта.'),
            array('email', 'email', 'on'=>'registerMeetDate'),
            array('email', 'unique', 'className'=>'UserAuthorization' ,'on'=>'registerMeetDate'),
            array('email', 'filter', 'filter'=>'mb_strtolower', 'on'=>'registerMeetDate'),
            array('sex', 'in', 'range'=>array('1','2'),'on'=>'registerMeetDate'),
            array('name', 'length' ,'min' => 3, 'max' => 25 , 'on'=>'registerMeetDate'),
            array('password', 'length' ,'min' => 3, 'max' => 25 , 'on'=>'registerMeetDate'),

			// редактировать
			array('name', 'required', 'message'=>'Введите ваше имя.', 'on'=>'edit'),
			array('birthday', 'required', 'on'=>'edit'),
			array('country_id', 'required', 'on'=>'edit', 'message'=>'Необходимо заполнить страну.'),
			array('region_id', 'required', 'on'=>'edit', 'message'=>'Необходимо заполнить регион.'),
			array('city_id', 'required', 'on'=>'edit', 'message'=>'Необходимо заполнить город.'),
			array('name', 'length', 'min' => 3 , 'max' => 25 , 'on'=>'register,edit'),
			array('name', 'match',   'pattern'    => '/^[A-Za-zа-яА-Я\s+]+$/u', 'message'=>'В имени нельзя использовать цифры и знаки.', 'on'=>'register,edit'),
			array('name', 'filter', 'filter'=>'trim', 'on'=>'register,edit'),
			array('name', 'filter', 'filter'=>array("Html", "ucwords"), 'on'=>'register,edit'),
			//array('birthday', 'date', 'format'=>'yyyy-MM-dd', 'on'=>'edit'),
			array('ItemsBirthday', 'safe', 'on'=>'edit'),
			array('birthday','birthdayValidate', 'on'=>'edit'),
			
			//array('birthday', 'type', 'type' => 'date', 'dateFormat' => 'yyyy-MM-dd', 'on'=>'edit'),
			
			// смена старого пароля
			array('old_password, new_password, confirm_password', 'required', 'on'=>'editPassword'),
			array('new_password', 'length' ,'min' => 3, 'max' => 25 , 'on'=>'editPassword'),
			array('confirm_password', 'compare', 'compareAttribute'=>'new_password', 'on'=>'editPassword'),
			array('old_password','oldPassword', 'on'=>'editPassword'),
			
			
                        array('login', 'required', 'on'=>'editLogin'),
                        array('email', 'required', 'on'=>'editEmail'),
                        array('phone', 'required', 'on'=>'editPhone'),
			array('phone', 'unique', 'on'=>'editPhone'),
			array('phone', 'match',   'pattern'    => '/^[0-9]+$/u', 'on'=>'editPhone'),
			
			array('login', 'match',  'not'=>true, 'pattern'    => '/^(messages|friends|admin|administrator|photos|notifications|guests|fave|pay|settings|top|search|video|gifts|edit|profile|groups|journals|meetings|gifts|activation|support)[^>]+$/u', 'on'=>'register, editLogin'),
			array('login', 'match',  'not'=>true, 'pattern'    => '/^(messages|friends|admin|administrator|photos|notifications|guests|fave|pay|settings|top|search|video|gifts|edit|profile|groups|journals|meetings|gifts|activation|support)+$/u', 'on'=>'register, editLogin'),
                );
		
	}
	
	public function myCaptcha($attr,$params)
	{
		if(Yii::app()->request->isAjaxRequest)
			return;
		
		CValidator::createValidator('captcha', $this, $attr, $params)->validate($this);
	}
	
	public function birthdayValidate($attr,$params)
	{
		if (!$this->ItemsBirthday[Day] OR !$this->ItemsBirthday[Year] OR !$this->ItemsBirthday[Month])
			$this->addError('birthday','День рождение не указано.');
	}
	
	public function oldPassword($attr,$params)
	{
		
		
		if ($this->hashPassword($this->old_password)!=$this->password)
			$this->addError('old_password','Указан неверный старый пароль');
			
	}
	
	public function getIsOnline()
	{
		$a=(time()-$this->last_date)/60;
		if ($a<=15)
			return true;
		else
			return false;
	}

	/*public function getIsMobile()
	{
		return $this->is_mobile;
	}*/
	
	public function getUrl()
	{
		//return Yii::app()->createUrl('profile/index', array('name'=>$this->login));
		//return CHtml::normalizeUrl('/'.$this->login);
		if ($this->login)
			return CHtml::normalizeUrl(array('/profile/index', 'user'=>$this->login));
		else
			return CHtml::normalizeUrl(array('/profile/index', 'userId'=>$this->id));
	}
	
	public function getImage($width, $height, $default='')
	{
		if ($this->have_photo==1)
			return ImageHelper::thumb($width, $height, $this->id, $this->photo, array('method' => 'adaptiveResize'));
		elseif ($default)
			return Html::imageUrl($default);
		else
			return false;
	}
	
	
	public function getGeo()
	{
		$geo = new stdClass();
		/*if (Yii::app()->user->role!='admin') {
			if ($this->isBot && Yii::app()->vc->optionsDomain->isGeo) {
				$geo->country=Yii::app()->geo->country;
				$geo->city=Yii::app()->geo->city;
				return $geo;
			}
		}*/
		$geo->country=$this->country->name;
		//$geo->region=$this->region->name;
		$geo->city=$this->city->name;
		return $geo;
	}
	
	public function attributeLabels()
	{
		return array(
			'name'			=> 'Ваше Имя',
			'login'			=> 'Логин',
			'password' 		=> 'Пароль',
			'confirm_password' 	=> 'Повторите пароль',
			'new_password' 	=> 'Новый пароль',
			'old_password' 	=> 'Старый пароль',
			'country_id' 	=> 'Страна',
			'region_id' 	=> 'Регион',
			'city_id' 	=> 'Город',
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->balance='0';
				//$this->login='user_'.Html::translitIt($this->name).'_'.$this->id;
				//$this->subaccount_id = 0;
				$this->reg_date = time();
				//$this->password = $this->hashPassword($this->password);
			}
			
                   
                   if (is_array($this->ItemsBirthday)) $this->birthday= $this->ItemsBirthday[Year].'-'.$this->ItemsBirthday[Month].'-'.$this->ItemsBirthday[Day];
//echo $this->ItemsBirthday[Year].'-'.$this->ItemsBirthday[Month].'-'.$this->ItemsBirthday[Day];
			
			return true;
		}
		return false;
	}
	
	protected function afterSave()
	{
		if($this->isNewRecord)
		{
			$userAuthorization=new UserAuthorization;
			$userAuthorization->user_id=$this->id;
			$userAuthorization->email=$this->email;
			$userAuthorization->password=$this->password;
			$userAuthorization->hash_password = UserAuthorization::hashPassword($this->password);
			$userAuthorization->save();
			
			$profiles=new Profiles;
			$profiles->user_id=$this->id;
			$profiles->save();
			
			$userPosition=new UserPosition;
			$userPosition->user_id=$this->id;
			$userPosition->save();
			
			$userPosition=new UserCounters;
			$userPosition->user_id=$this->id;
			$userPosition->save();
			
			$userPages=new UserPages;
			$userPages->user_id=$this->id;
			$userPages->name=$this->login;
			$userPages->save();
			
			$userPages=new UserSettings;
			$userPages->user_id=$this->id;
			$userPages->save();
			
			Notifications::add(Notifications::TEXT, array(
				'userId'=>$this->id,
				'text'=>'Добро пожаловать на сайт знакомств для взрослых.',
			));
			
			// присваниваем ботов к юзеру но только если не является оплата МТ подписки
			//if (Yii::app()->vc->optionsDomain->typePaymentRU!='mt')
				Bots::createCommandByUserId($this->id);
		}
	}
	
	public function hashPassword($password)
	{
		//return md5($password);
		return $password;
	}
	
	public static function getRangedown($start, $end, $defaultSitting='')
	{
		$range = array();
		if (!empty($defaultSitting)) $range[]=$defaultSitting;
				for ($i=$start; $i<=$end; $i++) 
				$range[$i]=$i;
				return $range;
	}
	
	public static function findSpotlight($limit=9)
	{
		$criteria=new CDbCriteria;
		$criteria->condition='t.have_photo=1 AND t.isReal=1 AND t.isBot=0 AND t.sex=:sex AND t.country_id=:country_id';
		$criteria->params=array(':sex'=>Yii::app()->user->model->sex==1 ? 2 : 1, ':country_id'=>Yii::app()->user->model->country_id);
		$criteria->order='position.time DESC';
		$criteria->limit=$limit;
		$criteria->with='position';
		return User::model()->findAll($criteria);
	}

    public function getYears(){
        $date = DateTime::createFromFormat("Y-m-d", $this->day_birth);
        return $date->format("Y");
    }

    public static function notNew($userId)
    {
        $user = User::model()->find('id = :id', array('id'=>$userId));
        $user->not_new = true;
        if ($user->save()) {
            return true;
        }

        return false;
    }

    public function addCoins()
    {
        $this->add_coins++;
        if ($this->add_coins > 30 || $this->add_coins < 1) {
            $this->add_coins = 1;
        }

        $this->balance += $this->add_coins;
        if ($this->save()) {
            return true;
        }

        return false;
    }

    public static function getTop()
    {
        $criteria=new CDbCriteria(array(
            'condition'=>'t.have_photo=1 AND t.isReal=1 AND t.isBot=0',
            'with'=>array(
                'position'
            ),
            'order'=>'position.time DESC',
        ));

        $profiles=User::model()->findAll($criteria);
        return $profiles;
    }

    public function getUserApprovedPhotos()
    {
        $criteria=new CDbCriteria(array(
            'condition'=>'t.approve=1 AND user_id ='.$this->id,
            'order'=>'t.sort ASC',
        ));

        $photos=Photos::model()->findAll($criteria);
        return $photos;
    }

    public function getTodayVisitors() {
        return UserVisits::model()->findAllBySql(
            'SELECT * FROM user_visits
            WHERE visited_user_id = :id
            AND DATE(visit_date) >= DATE(NOW())
            GROUP BY user_id
            ORDER BY visit_date DESC
            LIMIT 10',
            array('id' => Yii::app()->user->id));
    }
}