<?php

class UserCounters extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{user_counters}}';
	}
	
	public function defaultScope()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
		);
	}
	
	public static function recalculationPhotos($user_id)
	{
		UserCounters::model()->findByPk($user_id)->saveAttributes(array('photos'=>Photos::model()->userById($user_id)->count(), 'update_photos'=>time()));
	}
	
	public static function recalculationFriends($user_id)
	{
		UserCounters::model()->findByPk($user_id)->saveAttributes(array('friends'=>Friends::model()->userById($user_id)->count()));
	}
	
	public static function recalculationAlbums($user_id)
	{
		UserCounters::model()->findByPk($user_id)->saveAttributes(array('albums'=>Albums::model()->userById($user_id)->count()));
	}
	
	public static function recalculationWall($user_id)
	{
		UserCounters::model()->findByPk($user_id)->saveAttributes(array('wall'=>Wall::model()->userById($user_id)->count(), 'update_wall'=>time()));
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			return true;
		}
		return false;
	}
}