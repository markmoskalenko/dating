<?php

class UserLikes extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{likes_users}}';
	}

	public function getDate()
	{
		return Html::date($this->time);
	}

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->time=time();
			}
			return true;
		}
		return false;
	}
}