<?php

class ProfileFieldValues extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{profile_field_values}}';
	}
	
	public function relations()
	{
		return array(
			'field' => array(
				self::BELONGS_TO,
				'ProfileField',
				'field_id',
				'joinType' => 'INNER JOIN',
				/*'with'=>array(
					'section'
				)*/
			),
		);
	}
}