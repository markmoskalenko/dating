<?php

class Smtp extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'smtp';
    }

    public function relations()
    {
        return array(
        );
    }

    public function rules()
    {
        return array(
            array('host, username, password', 'required'),
            array('host, username, password, port, encryption', 'filter', 'filter'=>'trim'),
            array('host, username, password, port, encryption', 'length' , 'max' => 255),
        );

    }

    public function attributeLabels()
    {
        return array(
            'host' => Yii::t('app', 'Хост'),
            'username' => Yii::t('app', 'Имя пользователя'),
            'password' => Yii::t('app', 'Пароль'),
            'port' => Yii::t('app', 'Порт'),
            'encryption' => Yii::t('app', 'Encryption'),
        );
    }

    public function getDialogueUser()
    {
        if ($this->folder=='inbox')
            return $this->user;
        else
            return Yii::app()->user->model;
    }

    public static function getTransportOptions()
    {
        $criteria = new CDbCriteria;
        $res = Smtp::model()->find($criteria);
        if ($res){
            $res = $res->getAttributes();
            if (isset($res['id']))
                unset($res['id']);
            return $res;
        }

        return null;
    }
}