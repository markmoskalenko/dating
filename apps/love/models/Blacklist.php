<?php

class Blacklist extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{blacklist}}';
	}

	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'from_userId',
				'select'=>'login, sex, name, birthday, photoId, photo',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}

	public static function checkBlackByUser($id)
	{
		return Blacklist::model()->find('userId=:userId AND black_userId=:black_userId', array(':userId'=>Yii::app()->user->id, ':black_userId'=>$id));
	}
	
	public static function checkByUser($id)
	{
		return Blacklist::model()->find('userId=:userId AND black_userId=:black_userId', array(':userId'=>$id, ':black_userId'=>Yii::app()->user->id));
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				
			}
			return true;
		}
		return false;
	}
}