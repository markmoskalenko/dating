<?php

class Friends extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{friends}}';
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'friend_userId',
				'select'=>'name, photo, sex, login, have_photo, photoId, isMobile, last_date',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}
	
	public function rules()
	{
                return array(
                );
		
	}
	
	public function user()
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`userId`='.Yii::app()->user->id,
		));
		return $this;
	}
	
	public function userById($user_id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`userId`='.$user_id,
		));
		return $this;
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->time = time();
			}
			return true;
		}
		return false;
	}
	
	protected function afterSave()
	{
		parent::afterSave();
		UserCounters::recalculationFriends($this->userId);
		UserCounters::recalculationFriends($this->friend_userId);
	}
	
	protected function afterDelete()
	{
		parent::afterDelete();
		UserCounters::recalculationFriends($this->userId);
		UserCounters::recalculationFriends($this->friend_userId);
	}
}