<?php

class Chat extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{chat}}';
	}
	
	public function defaultScope()
	{
		return array(
		);
	}
	
	public function rules()
	{
		return array(
			array('message', 'required', 'message'=>'Напишите сообщение'),
			array('message', 'length', 'min'=>3, 'tooShort'=>'Слишком короткое сообщение'),
			//array('message', 'length', 'max'=>1500,),
			array('message', 'filter', 'filter'=>'trim'),
			array('message', 'filter', 'filter'=>'strip_tags'),
			array('message', 'filter', 'filter' => array($this, 'v')),
			array('message', 'checkBalance'),
			//array('message', 'antiflood'),
		);
	}

	public function checkBalance($attribute,$params)
	{
		if (Yii::app()->user->getTransactionsTypes('chat')>=Yii::app()->user->balance) {
			$this->addError('code',Yii::t('app','Недостаточно монет.'));
		}
	}
	
	public function v($str)
	{
		/*$arr=explode(" ", $str);
		foreach($arr as $t) {
			$str.=" ".substr($t,0,20);
		}*/
		
		return substr($str,0,500);
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'userId',
				'joinType' => 'LEFT OUTER JOIN',
				/*'with'=>array(
					'profile'=>array(
						'select'=>'name, lastname',
						'joinType' => 'LEFT OUTER JOIN'
					),
				),*/
			),
		);
	}
	
	public function getChat($limit=6)
	{
		$criteria = new CDbCriteria;
		$criteria->with=array('user'=>array('with'=>array('country', 'city')));
		
		if (Yii::app()->user->isGuest OR !Yii::app()->user->model->isReal)
			$criteria->addCondition('`user`.`isBot`=1');

		$criteria->order='t.id DESC';
		$criteria->limit=$limit;
		return $this->findAll($criteria);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->date = time();
				$this->ip_address=Yii::app()->request->userHostAddress;
			}
			return true;
		}
		return false;
	}
}