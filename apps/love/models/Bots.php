<?php

class Bots extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{bots}}';
	}
	
	public function rules()
	{
                return array(
			array('messages', 'safe'),
                );
	}
	
	public static function createCommandByUserId($user_id)
	{
		foreach (Bots::model()->findAll(array('condition'=>'messages!=""', 'order'=>'rand()', 'limit'=>rand(6,16))) as $row) {
			$interval_posts=array();
			$msgArray=explode(";", $row->messages);
			foreach ($msgArray as $kys=>$rowMsgs) {
				$interval_posts[]=time()+rand(60,600);
			}
			asort($interval_posts);
			
			$botsUsers=new BotsUsers();
			$botsUsers->user_id=$user_id;
			$botsUsers->bot_user_id=$row->user_id;
			$botsUsers->message_max=count($msgArray);
			$botsUsers->interval_posts=implode(";",$interval_posts);
			$botsUsers->save();
		}
	}
}