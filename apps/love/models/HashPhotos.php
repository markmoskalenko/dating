<?php

class HashPhotos extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{hash_photos}}';
	}
	
	public static function create($hash)
	{
		$hashPhotos=new HashPhotos;
		$hashPhotos->hash=$hash;
		$hashPhotos->save();
	}
}