<?php

class IMContacts extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{im_contacts}}';
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'contact_user_id',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				//$this->time = time();
			}
			return true;
		}
		return false;
	}
}