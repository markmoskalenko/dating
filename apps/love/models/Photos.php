<?php

class Photos extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{photos}}';
	}
	
	public function rules()
	{
                return array(
			array('descr', 'length' , 'max' => 125 , 'on'=>'edit'),
			array('descr', 'filter', 'filter'=>'strip_tags', 'on'=>'edit'),
			array('descr', 'filter', 'filter'=>'trim', 'on'=>'edit'),
			array('album_id', 'safe', 'on'=>'edit'),
                );	
	}

	public function attributeLabels()
	{
		return array(
			'descr'			=> Yii::t('app', 'Descr'),
			'album_id'			=> Yii::t('app', 'Album'),
			
		);
	}
	
	public function defaultScope()
	{
		return array(

		);
	}
	
	public function getNormalizeUrl()
	{
		$params=array();
		if (isset($_GET['albumId'])) $params['albumId']=$_GET['albumId'];
		return CHtml::normalizeUrl(array_merge(array('photos/index', 'userId'=>$this->user_id, 'photoId'=>$this->id), $params));
	}
	
	public function getImage($width, $height)
	{
		return ImageHelper::thumb($width, $height, $this->user_id, $this->filename, array('method' => 'adaptiveResize'));
	}
	
	public function getImageSource()
	{
		return Html::contentUrl('1/u'.$this->user_id.'/'.$this->filename);
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'user_id',
				'joinType' => 'LEFT OUTER JOIN',
				'select'=>'name, sex, login, have_photo, photo, photoId, country_id, region_id, city_id, birthday, isReal',
				/*'with'=>array(
					'profile'=>array(
						'select'=>'name, lastname',
						'joinType' => 'LEFT OUTER JOIN'
					),
				),*/
			),
			'album' => array(
				self::BELONGS_TO,
				'Albums',
				'album_id',
				'joinType' => 'LEFT OUTER JOIN',
				'select'=>'name, system, censor',
			),
			'fave' => array(
				self::HAS_ONE,
				'Like',
				'like_photoId',
				//'order'=>'fave.time DESC',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}
	
	public function user()
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`user_id`='.Yii::app()->user->id,
		));
		return $this;
	}
	
	public function userById($user_id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`user_id`='.$user_id,
		));
		return $this;
	}
	
	public function albumById($album_id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`album_id`='.$album_id,
		));
		return $this;
	}
	
	public function orderBySort()
	{
		$this->getDbCriteria()->mergeWith(array(
			'order'=>'`t`.`sort` DESC',
		));
		return $this;
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->user_id = Yii::app()->user->getId();
				$this->date = time();
			}
			return true;
		}
		return false;
	}
	
	protected function afterSave()
	{
		parent::afterSave();
		UserCounters::recalculationPhotos($this->user_id);
	}
	
	protected function afterDelete()
	{
		parent::afterDelete();
		UserCounters::recalculationPhotos($this->user_id);
	}
}