<?php

class Configs extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{configs}}';
	}
	
	public function defaultScope()
	{
		return array(
			
		);
	}
 
	public function rules()
	{
                return array(
			array('name,value,description', 'filter', 'filter'=>'trim'),
                        array('name,value,description', 'required'),
			array('name', 'match', 'pattern' => '/^[A-Za-z0-9_\s,]+$/u'),
			array('name', 'unique'),
                );
		
	}
}