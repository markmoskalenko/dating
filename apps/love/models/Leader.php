<?php

class Leader extends CActiveRecord
{
    const MINUTES = 5;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{leaders}}';
    }

    public function relations()
    {
        return array(
            'user' => array(
                self::BELONGS_TO,
                'User',
                'user_id',
                'joinType' => 'LEFT OUTER JOIN',
            ),
            'photo' => array(
                self::BELONGS_TO,
                'Photos',
                'photo_id',
                'joinType' => 'LEFT OUTER JOIN',
            ),
        );
    }

    public function rules()
    {
        return array(
            array('user_id, rate, date_start', 'required'),
        );

    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', '#'),
            'rate' => Yii::t('app', 'Ставка'),
            'date_start' => Yii::t('app', 'Дата ставки'),
            'left_time' => Yii::t('app', 'Остаток времени'),
            'is_current' => Yii::t('app', 'Лидер'),
            'user' => Yii::t('app', 'Пользователь'),
            'photo' => Yii::t('app', 'Фото'),
        );
    }

    public function getDialogueUser()
    {
        if ($this->folder=='inbox')
            return $this->user;
        else
            return Yii::app()->user->model;
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord){
            $this->left_time = $this->leftTimeByRate($this->rate);
            $model = self::model()->find('user_id = :user', array('user'=>$this->user_id));
            if (!$model){
                return true;
            }

            $this->rate += $model->rate;
            $this->addLeftTime($model->left_time);

            $model->delete();
        }

        self::refreshCurrentLeader();

        $this->date_start = date('Y-m-d H:i:s');

        return true;
    }

    protected function afterSave()
    {
        $current = self::getCurrent();

        if ($this != $current && $this->rate > $current->rate){
            self::setCurrent($this->user_id);
        }

        return true;
    }

    public static function setCurrent($userId)
    {
        $sql = 'UPDATE '.self::tableName().' SET is_current=0 WHERE is_current=1';
        Yii::app()->db->createCommand($sql)->execute();

        $sql = 'UPDATE '.self::tableName().' SET is_current=1 WHERE user_id='.$userId;
        Yii::app()->db->createCommand($sql)->execute();

        return true;
    }

    public static function getCurrent()
    {
        return self::model()->find('is_current = 1');
    }

    private function leftTimeByRate($rate)
    {
        $time1 = new DateTime('00:00:00');
        $time = new DateTime('00:00:00');
        $time->modify('+'.$rate*self::MINUTES.' minutes');
        $razn = ((int)$time->format('U'))-((int)$time1->format('U'));

        return $this->getLeftTimeBySeconds($razn);
    }

    private function getLeftTimeBySeconds($seconds)
    {
        $minutes = intval($seconds/60);
        $sec = $seconds-$minutes*60;
        $hours = intval($minutes/60);
        $minutes = $minutes-$hours*60;

        if ($hours < 10){
            $hours = '0'.$hours;
        }
        if ($minutes < 10){
            $minutes = '0'.$minutes;
        }
        if ($sec < 10){
            $sec = '0'.$sec;
        }

        return $hours.':'.$minutes.':'.$sec;
    }

    public function getLeftSeconds()
    {
        $aLT = explode(':', $this->left_time);

        return ($aLT[0]*3600) + ($aLT[1]*60) + $aLT[2];
    }

    public function addLeftTime($leftTime)
    {
        $aLT = explode(':', $leftTime);

        if (count($aLT) != 3) {
            return false;
        }

        $incSeconds = ($aLT[0]*3600) + ($aLT[1]*60) + $aLT[2];

        $this->left_time = $this->getLeftTimeBySeconds($this->getLeftSeconds()+$incSeconds);

        return true;
    }

    public static function getLeaders()
    {
        return self::model()->findAll(
            'left_time > "00:00:00" ORDER BY rate DESC LIMIT 5'
        );
    }

    public static function refreshCurrentLeader()
    {
        $model = self::model()->find('is_current = 1');
        if (!$model) {
            self::setNewLeader();
            return true;
        }

        $dt = new DateTime();
        $dtOld = DateTime::createFromFormat('Y-m-d H:i:s', $model->date_start);
        $seconds = (intval($dt->format('U'))) - (intval($dtOld->format('U')));


        $aLT = explode(':', $model->left_time);
        $oldSeconds = ($aLT[0] * 3600) + ($aLT[1] * 60) + $aLT[2];

        $seconds = $oldSeconds - $seconds;

        if ($seconds <= 0) {
            $leftTime = '00:00:00';
        } else {
            $leftTime = $model->getLeftTimeBySeconds($seconds);
        }

        print('Current Leader left time: '. $leftTime);

        if ($seconds <= 0) {
            $leftTime = "00:00:00";
        }

        $sql = 'UPDATE ' . self::tableName() . ' SET date_start="' . $dt->format('Y-m-d H:i:s') . '", left_time="' . $leftTime . '" WHERE id=' . $model->id;
        Yii::app()->db->createCommand($sql)->execute();

        if ($seconds <= 0) {
            self::setNewLeader();
        }


        return true;
    }

    public static function setNewLeader()
    {
        $model = self::model()->find(
            'left_time > "00:00:00" ORDER BY rate DESC'
        );

        $sql = 'UPDATE ' . self::tableName() . ' SET is_current=0;';

        if ($model) {
            $sql .= 'UPDATE ' . self::tableName() . ' SET date_start="'.date('Y-m-d H:i:s').'", is_current=1 WHERE user_id=' . $model->user_id;
        }
        Yii::app()->db->createCommand($sql)->execute();
    }
}