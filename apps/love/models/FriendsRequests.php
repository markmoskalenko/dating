<?php

class FriendsRequests extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{friends_requests}}';
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'',
				'joinType' => 'LEFT OUTER JOIN',
			),
		);
	}
	
	public function rules()
	{
                return array(
                );
		
	}
	
	public function getUser()
	{
		if ($this->userReq)
			return $this->userReq;
		else
			return $this->user1;
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->time = time();
			}
			return true;
		}
		return false;
	}
	
	protected function afterSave()
	{
		if($this->isNewRecord && Yii::app()->params['noty'])
		{
			$user=User::model()->findByPk($this->friend_userId);
			Yii::app()->CURL->run('http://'.Yii::app()->params['noty_hostServer'].'/friendsRequests', false, array(
				'from_userId'=>$this->userId,
				'to_userId'=>$this->friend_userId,
				'hashUser'=>md5($this->userId.'dsbetngaedgc'),
				//'url'=>CHtml::normalizeUrl(array('messages/dialogue', 'dialogue')),
				'image'=>$user->getImage('55','55'),
				'name'=>$user->name));
		}
	}
}