<?php

class Profiles extends CActiveRecord
{
    public $ages = array(
        1 => array(
            'min' => '18',
            'max' => '20',
        ),
        2 => array(
            'min' => '21',
            'max' => '25',
        ),
        3 => array(
            'min' => '26',
            'max' => '30',
        ),
        4 => array(
            'min' => '31',
            'max' => '35',
        ),
        5 => array(
            'min' => '36',
            'max' => '40',
        ),
        6 => array(
            'min' => '41',
            'max' => '50',
        ),
        7 => array(
            'min' => '51',
            'max' => '60',
        ),
        8 => array(
            'min' => '61',
            'max' => '80',
        ),
    );

        public $confirm_password;
	public $verifyCode;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{profiles}}';
	}

	public function relations()
	{
		return array(

		);
	}

	public function defaultScope()
	{
		return array(
			
		);
	}

	public function rules()
	{
                return array(

			// знакомтсва
			array('lookfor,lookfor_age,target,iam,marital,sponsor,children', 'safe', 'on'=>'editMeet'),
			array('waitingfor', 'length' , 'max' => 250 , 'on'=>'editMeet'),
			// типаж
			array('physique,coloreye,colorscin,head,race,onbody,breastsize', 'safe', 'on'=>'editType'),
			array('height', 'numerical', 'allowEmpty'=>true, 'min' =>'100', 'max' =>'250', 'on'=>'editType'),
			array('weight', 'numerical', 'allowEmpty'=>true, 'min' =>'30', 'max' =>'400', 'on'=>'editType'),
			array('membersize', 'numerical', 'allowEmpty'=>true, 'min' =>5, 'max' => 30 , 'on'=>'editType'),
			//array('membersize,membercircle', 'numerical', 'allowEmpty'=>true, 'min' =>'5', 'max' =>'30', 'on'=>'editType'),
			// обо мне
			array('smoke,drink,drug,circumstance,habit,daymode,flang', 'safe', 'on'=>'editAboutme'),
			// секс
			array('sextime,insex,excitesinsex,hesex', 'safe', 'on'=>'editSex'),
			// основное
			array('sex,orientation,birthday', 'safe', 'on'=>'editMain'),
			array('name', 'length' ,'min' => 3, 'max' => 25 , 'on'=>'editMain'),
			//array('country_id', 'exist', 'className' => 'GeoCountries','allowEmpty'=>true, 'on'=>array('editMain')),
			//array('city','length' , 'max' => 30, 'on'=>'editMain'),
			// дичное
			array('about,interests,activities,music,films,tv,books,games,quotes,waitingfor','length' , 'max' => 500, 'on'=>'editPersonal'),
			array('about,interests,activities,music,films,tv,books,games,quotes,waitingfor', 'filter', 'filter'=>'strip_tags', 'on'=>'editPersonal'),
			array('about,interests,activities,music,films,tv,books,games,quotes,waitingfor', 'filter', 'filter'=>'trim', 'on'=>'editPersonal'),
			
			
			array('status', 'length' ,'max' => 350 , 'on'=>'updateStatus'),
			array('status', 'filter', 'filter'=>'strip_tags', 'on'=>'updateStatus'),
			array('status', 'filter', 'filter'=>'trim', 'on'=>'updateStatus'),
                );
		
	}
	
	public function myCaptcha($attr,$params)
	{
		if(Yii::app()->request->isAjaxRequest)
			return;
		
		CValidator::createValidator('captcha', $this, $attr, $params)->validate($this);
	}
	
	/*public function attributeLabels()
	{
		return array(
			'login'			=> 'Логин',
			'password' 		=> 'Пароль',
			'confirm_password' 	=> 'Повторите пароль',
			
			'lookfor'			=> 'Познакомлюсь:',
			'lookfor_age' 		=> 'Желаемый возраст:',
			'target' 	=> 'Цель знакомства:',
			'iam' 	=> 'Как я знакомлюсь:',
			'marital' 	=> 'Семейное положение:',
			'sponsor'	=> 'Материальная поддержка:',
			'children' 	=> 'Есть ли у вас дети?',
			'waitingfor'	=> 'Кого я хочу найти:',
			
			'height'	=> 'Рост:',
			'weight'	=> 'Вес:',
			'physique'	=> 'Телосложение:',
			'membersize'	=> 'Размеры члена:',
			'membercircle'	=> 'Размеры члена (окружность):',
			'coloreye'	=> 'Цвет глаз:',
			'colorscin'	=> 'Цвет кожи:',
			'head'	=> 'Волосы на голове:',
			'race'	=> 'Внешность:',
			'onbody'	=> 'На теле есть:',
			
			
			'smoke'	=> 'Отношение к курению:',
			'drink'	=> 'Отношение к алкоголю:',
			'drug'	=> 'Отношение к наркотикам:',
			'circumstance'	=> 'Материальное положение:',
			'habit'	=> 'Жилищные условия:',
			'daymode' => 'Режим дня:',
			'flang' => 'Владение иностранными языками:',
			
			
			'sextime' => 'Как часто вы бы хотели заниматься сексом:',
			'insex' => 'В сексе мне нравится:',
		);
	}*/
	
	public function generateAttributeLabel($name)
	{
		return Yii::t('profile_field', $name);
	}
	
	public function getAttributesArray()
	{
		return array(
			'lookfor'=>'editMeet',
			'lookfor_age'=>'editMeet',
			'target'=>'editMeet',
			'onbody'=>'editType',
			'flang'=>'editAboutme',
			'insex'=>'editSex',
			'excitesinsex'=>'editSex'
		);
	}
	
	public function getUsername()
	{
		if ($this->name)
			return $this->name;
		//elseif ($this->fname && $this->lname)
		//	return $this->fname.' '.$this->lname;
		else
			return $this->login;
	}
	
	protected function afterFind()
	{
		if (!Yii::app()->request->isPostRequest)
		{
			foreach($this->getAttributesArray() as $key=>$value) {
				if ($this->$key) {
					$this->$key=explode(',', $this->$key);
				}
			}
			//
			
			//$this->birthday=array('y'=>$dr[0],'m'=>$dr[1],'d'=>$dr[2]);
			//print_r($this->birthday);
			//Yii::log(print_r($this->tableSchema,true));
			//print_r($this->attributeNames(),true));
		}
		parent::afterFind();
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{

			} else {
				
				foreach($this->getAttributesArray() as $key=>$value) {
					if ($this->scenario==$value){
						if (is_array($this->$key)) {
							$this->$key=implode(',', $this->$key);
						} else
							$this->$key='';
					}
				}
				if ($this->scenario=='editMain'){
				//$this->birthday = date('Y-m-d', mktime(0, 0, 0, $this->birthday[m], $this->birthday[d], $this->birthday[y]));
				//echo $this->birthday;
				}
			}
			return true;
		}
		return false;
	}
	
	protected function afterSave()
	{
		if (in_array($this->scenario, array('editMeet','editType','editAboutme','editSex'))) {
			foreach($this->getAttributesArray() as $key=>$value) {
				if ($this->$key) {
					$this->$key=explode(',', $this->$key);
				}
			}
		}
	}
	
	public function hashPassword($password)
	{
		//return md5($password);
		return $password;
	}
}