<?php

class Albums extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{albums}}';
	}
	
	public function rules()
	{
                return array(
			array('name,access_to_view,access_to_comment', 'required', 'on'=>'create, edit'),
			array('name', 'length' ,'max' => 35 , 'on'=>'create, edit'),
			array('descr', 'length' , 'max' => 125 , 'on'=>'create, edit'),
			array('name, descr', 'filter', 'filter'=>'strip_tags', 'on'=>'create, edit'),
			array('name, descr', 'filter', 'filter'=>'trim', 'on'=>'create, edit'),
			//array('name, descr', 'match',   'pattern'    => '/^[A-Za-z0-9�-��-�\s+]+$/u', 'on'=>'create, edit'),
			array('censor', 'safe', 'on'=>'create, edit'),
			//array('name, descr', 'match',   'pattern'    => '/^[A-z][\w]+$/',),
			// �������
			//array('email', 'filter', 'filter'=>'mb_strtolower'),
                );	
	}

	public function attributeLabels()
	{
		return array(
			'name'			=> Yii::t('app', 'Name'),
			'descr'			=> Yii::t('app', 'Descr'),
			'access_to_view'	=> Yii::t('app', 'Access To View'),
			'access_to_comment'	=> Yii::t('app', 'Access To Comment'),
		);
	}
	
        public function relations()
        {
                return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'user_id',
				'joinType' => 'LEFT OUTER JOIN',
				'select'=>'login, sex'
			),
			'photo' => array(
				self::HAS_ONE,
				'Photos',
				'album_id',
				'order'=>'photo.sort DESC',
				'joinType' => 'LEFT OUTER JOIN',
				'select'=>'name, user_id, filename',
			),
			/*'profile' => array(
				self::BELONGS_TO,
				'UserProfiles',
				'user_id',
				'joinType' => 'INNER JOIN',
			),
			'photo' => array(
				self::HAS_ONE,
				'Photos',
				'album_id',
				'joinType' => 'LEFT OUTER JOIN',
			),*/
                );
        }
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->date = time();
				$this->updated = time();
			} else {
				$this->updated = time();
			}
			return true;
		}
		return false;
	}

	public function user()
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`user_id`='.Yii::app()->user->id,
		));
		return $this;
	}
	
	public function userById($id=0)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`user_id`='.intval($id ? $id : Yii::app()->user->id),
		));
		return $this;
	}
	
	public function listData()
	{
		$criteria = new CDbCriteria;
		$criteria->condition='`t`.`user_id`='.Yii::app()->user->id;
		$criteria->order='t.name ASC';
		$models=$this->findAll($criteria);
		$listData=array();
		foreach($models as $model)
		{
			$value=CHtml::value($model,'id');
			$text=CHtml::value($model,'name');
			$listData[$value]=$text;
		}
		return $listData;
		
	}
	
	protected function afterSave()
	{
		parent::afterSave();
		UserCounters::recalculationAlbums($this->user_id);
	}
	
	protected function afterDelete()
	{
		parent::afterDelete();
		UserCounters::recalculationAlbums($this->user_id);
	}
}