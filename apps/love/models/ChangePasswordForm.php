<?php

class ChangePasswordForm extends CFormModel
{
    public $user_id;
    public $password;
    public $confirm_password;

    public function rules()
    {
        return array(
            array('user_id, password, confirm_password', 'required'),
            array('password', 'length' ,'min' => 3, 'max' => 25),
            array('confirm_password', 'compare', 'compareAttribute'=>'password'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'password' => 'Пароль',
            'confirm_password' => 'Повторите пароль',
        );
    }
}