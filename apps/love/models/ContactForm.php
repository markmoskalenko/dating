<?php

class ContactForm extends CFormModel
{
	public $name;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// имя
			array('name, email, subject, body, verifyCode', 'required', 'on'=>array('support')),
			array('name, email, body, verifyCode', 'required', 'on'=>array('nospam')),
			// email has to be a valid email address
			array('email', 'email', 'on'=>array('support','nospam')),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!extension_loaded('gd'), 'on'=>array('support','nospam')),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'name'			=> 'Ваше имя',
			'email' 		=> 'Ваш E-mail',
			'subject' 		=> 'Тема сообщения',
			'body' 		=> 'Сообщение',
			'verifyCode' 	=> 'Код с картинки',
			

		);
	}
}