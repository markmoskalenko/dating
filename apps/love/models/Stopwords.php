<?php

class Stopwords extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{stopwords}}';
	}
	
	public function rules()
	{
                return array(
			//����
                        array('word', 'required'),
			array('replacement', 'safe'),
                );
	}
}