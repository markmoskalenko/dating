<?php

class Wall extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{wall}}';
	}
	
	public function defaultScope()
	{
		return array(
			//'condition'=>'activation=1 AND (type_id=2 OR type_id=3)',
			//'order'=>'date DESC',
			//'limit'=>'15',
		);
	}
	
	public function rules()
	{
		return array(
			//array('message', 'required',),
			//array('message', 'length', 'min'=>3,),
			//array('message', 'length', 'max'=>1500,),
			//array('message', 'filter', 'filter'=>'strip_tags'),
			//array('message', 'antiflood'),
		);
	}

	public function antiflood($attribute,$params)
	{
		/*$criteria = new CDbCriteria;
		$criteria->select='date';
		$criteria->condition='wrote_user_id=:wrote_user_id AND user_id=:user_id AND ip_address=:ip_address';
		$criteria->params=array(':wrote_user_id'=>Yii::app()->user->id, ':user_id'=>$_GET['id'], ':ip_address'=>XHttpRequest::getUserHostAddress());
		$criteria->order='date desc';
		$result=$this->find($criteria);
		
		$sec=time()-$result[date];*/
		//if ($sec<60)
		//print_r($params);
		$this->addError('message','antiflood');
	}
	
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'User',
				'written_userId',
				'joinType' => 'LEFT OUTER JOIN',
				'select' => 'id, login, name, sex, have_photo, photo, photoId',
				/*'with'=>array(
					'profile'=>array(
						'select'=>'name, lastname',
						'joinType' => 'LEFT OUTER JOIN'
					),
				),*/
			),
			'photo' => array(
				self::BELONGS_TO,
				'Photos',
				'photoId',
				'joinType' => 'LEFT OUTER JOIN',
				'select' => 'id, user_id, filename',
			),
		);
	}
	

	
	public function getUsername()
	{
		if ($this->user->name)
			$name=$this->user->name;
		else
			$name=$this->user->login;
			
		return $name ? $name : 'DELETED';
	}
	
	public function findByUserId($userId, $limit)
	{
		//$dependency = new CDbCacheDependency('SELECT MAX(date) FROM site_wall WHERE user_id='.intval($_GET['user_id'] ? $_GET['user_id'] : Yii::app()->user->getId()).' LIMIT 1');
		
		
		$criteria = new CDbCriteria;

		$criteria->condition='`t`.`user_id`=:user_id';
		$criteria->params=array(':user_id'=>$userId);
		//$criteria->limit=$limit;
		
		$count = Wall::model()->/*cache(10000, $dependency, 2)->*/count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize=$limit;
		$pages->pageVar='wall';
		$pages->route='profile/index';
		$pages->params=array('user_id'=>$userId);
		//$pages->setCurrentPage('3'); // ���������� ����
		$pages->applyLimit($criteria);
//echo $count;
		
		$result=Wall::model()->with(array('user'=>array('select'=>'login, img, sex')))->findAll($criteria);
//print_r($result);
		if ($result===NULL) return $result;
		
		return array("data"=>$result, "pages"=>$pages);
	}
	
	public function user()
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`userId`='.Yii::app()->user->id,
		));
		return $this;
	}
	
	public function userById($user_id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'`t`.`userId`='.$user_id,
		));
		return $this;
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->written_userId = Yii::app()->user->id;
				$this->date = time();
			}
			return true;
		}
		return false;
	}
	
	protected function afterSave()
	{
		parent::afterSave();
		UserCounters::recalculationWall($this->userId);
	}
	
	protected function afterDelete()
	{
		parent::afterDelete();
		UserCounters::recalculationWall($this->userId);
	}
}