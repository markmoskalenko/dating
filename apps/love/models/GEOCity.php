<?php

class GEOCity extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{city}}';
	}
	
	public function listData($region_id)
	{
		$models=GeoCity::model()->cache(86400)->findAll(array('order'=>'title ASC', 'condition'=>'region_id=:region_id', 'params'=>array(':region_id'=>$region_id)));
		$listData=array();
		foreach($models as $model)
		{
			$value=CHtml::value($model,'city_id');
			$text=CHtml::value($model,'title');
			$listData[$value]=$text;
		}
		return $listData;
		
	}
}