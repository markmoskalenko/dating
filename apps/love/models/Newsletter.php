<?php

class Newsletter extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'newsletters';
    }

    public function relations()
    {
        return array(
        );
    }

    public function rules()
    {
        return array(
            array('title, check_code, email_list_code, template_variables, template', 'required'),
        );

    }

    public function attributeLabels()
    {
        return array(
            'title' => Yii::t('app', 'Заголовок'),
            'check_code' => Yii::t('app', 'Код проверки'),
            'email_list_code' => Yii::t('app', 'Код списка Email адресов'),
            'template_variables' => Yii::t('app', 'Код описания переменных шаблона'),
            'template' => Yii::t('app', 'Код шаблона'),
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('check_code',$this->check_code,true);
        $criteria->compare('email_list_code',$this->email_list_code,true);
        $criteria->compare('template_variables',$this->template_variables,true);
        $criteria->compare('template',$this->template,true);

        $sort = new CSort;
        $sort->defaultOrder = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort
        ));
    }

    public static function startNewsletters($id = null)
    {
        if ($id){
            $newsletters = Newsletter::model()->findAll('id = :id', array('id'=>$id));
        } else {
            $newsletters = Newsletter::model()->findAll();
        }
        $messages_sent = 0;
        foreach ($newsletters as $one){
            if (!eval($one->check_code)) {
                continue;
            }

            if ((is_array(eval($one->email_list_code)) && count(eval($one->email_list_code)) < 1) || !is_array(eval($one->email_list_code))){
                continue;
            }

            $emails = eval($one->email_list_code);

            foreach($emails as $e){
                if (strstr($e, '@') == '@mail.ru'){
                    $hourDate = (new DateTime('-1 hour'))->format('Y-m-d H:i:s');
                    $sql = 'SELECT COUNT(*) FROM mail_send_log WHERE mail LIKE "%@mail.ru%" AND date > "'.$hourDate.'"';
                    $countEmails = Yii::app()->db->createCommand($sql)->queryAll();
                    if ($countEmails['count'] > 1000){
                        continue;
                    }
                }

                $criteria = new CDbCriteria();
                $criteria->condition = 'email = :e';
                $criteria->params = array(':e'=>$e);
                $oUser = UserAuthorization::model()->find($criteria);

                if( !$oUser || $oUser->no_subscribe ) continue;

                $continue = false;

                eval($one->template_variables);

                if( $continue == true ) continue 1;

                echo 'SEND: '.$e."\r\n";

                ob_start();
                eval('?>' . $one->template);
                $template = ob_get_contents();
                ob_end_clean();

                $authToken = $oUser->getAuthToken();

                $template = str_replace('{token}',$authToken, $template);

                $template = str_replace('{user_id}',$oUser->user_id, $template);
                $template = str_replace('{user_email}',$oUser->email, $template);

                $message = new YiiMailMessage;
                $message->setBody( $template, 'text/html');
                $message->subject = $one->title;
                $message->addTo( $e );

                $message->from = 'admin@mail.ru';

                if (Yii::app()->mail->send($message)){
                    $mLog = new MailSendLog();
                    $mLog->email = $e;
                    $mLog->date = date('Y-m-d H:i:s');
                    $mLog->save();
                    $messages_sent++;
                }
            }

        }

        return $messages_sent;
    }
}