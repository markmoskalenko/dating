<?php

class UserPages extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{user_pages}}';
	}
	
	public function defaultScope()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			return true;
		}
		return false;
	}
}