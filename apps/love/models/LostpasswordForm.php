<?php

class LostpasswordForm extends CFormModel
{
	public $email;
	public $verifyCode;

        public function rules()
        {
                return array(
                        array('email', 'required'),
                        array('email', 'email'),
			//array('email', 'haveEmail'),
			array('email', 'filter', 'filter'=>'mb_strtolower'),
			
			array('verifyCode','captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
                );
        }
	
	public function attributeLabels()
	{
		return array(
		);
	}
	
	public function captcha1($attribute,$params)
	{
		
			//$this->addError('verifyCode', 'cap');
	}
	
	/*public function haveEmail($attribute,$params)
	{
		$user = User::model()->find('email=:email', array(':email'=>$this->email));
		if ($user) {
			$email = Yii::app()->email;
			$email->to = $user->email;
		}
		else
			$this->addError('email', 'A user with this email was not found');
	}*/
}