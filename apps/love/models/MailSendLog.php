<?php

class MailSendLog extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'mail_send_log';
    }

    public function relations()
    {
        return array(
        );
    }

    public function rules()
    {
        return array(
            array('email, date', 'required'),
        );

    }

    public function attributeLabels()
    {
        return array(
            'email' => Yii::t('app', 'Email'),
            'date' => Yii::t('app', 'Дата'),
        );
    }
}