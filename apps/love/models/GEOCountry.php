<?php

class GEOCountry extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{country}}';
	}
	
	public function listData()
	{
		$models=GeoCountries::model()->cache(86400)->findAll(array('order'=>'sort=0, sort ASC, title ASC'));
		$listData=array();
		foreach($models as $model)
		{
			$value=CHtml::value($model,'country_id');
			$text=CHtml::value($model,'title');
			$listData[$value]=$text;
		}
		return $listData;
		
	}
}