<?php

class UserSettings extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{user_settings}}';
	}
	
	public function rules()
	{
                return array(
			array('messages_only_from_real,email_notify_friends,email_notify_messages,email_notify,sms_notify,sound_notify', 'in', 'range'=>array('0','1')),
                );
		
	}
	
	public function defaultScope()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			
			return true;
		}
		return false;
	}
}