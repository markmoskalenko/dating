<?php

class ProfileField extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{profile_field}}';
	}
	
	public function relations()
	{
		return array(
			'section' => array(
				self::BELONGS_TO,
				'ProfileFieldSection',
				'section_id',
				'joinType' => 'INNER JOIN',
			),
			'values' => array(
				self::BELONGS_TO,
				'ProfileFieldSection',
				'field_id',
				'joinType' => 'INNER JOIN',
			),
		);
	}
	
	public function getForms($model)
	{
		$fields=ProfileField::model()->findAll(array(
			'condition'=>'scenario=:scenario',
			'params'=>array('scenario'=>$model->scenario),
			'order'=>'sort DESC, id ASC'
		));
		
		if (!$fields) return array();
		return $fields;
	}
	
	public function getListData($id=3)
	{
		foreach(ProfileFieldValues::model()->with('field')->findAll(array('condition'=>'t.value!=0 AND field.id='.$id, 'order'=>'`t`.`sort` DESC, `t`.`id` ASC')) as $row) {
			$field_values[$row->value]=Yii::t('profile_field_values', $row->depending_on_sex==1 ? $row->field->name.'_'.$row->value.'_'.Yii::app()->user->sex : $row->field->name.'_'.$row->value);
		}
		//print_r($field_values);
		return $field_values;
		//return CHtml::listData(,'','');
	}
}