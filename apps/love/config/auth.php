<?php

return array(
    
        // ��������
        'deleteOwnComment' => array(
                'type' => CAuthItem::TYPE_OPERATION,
                'description' => '',
                'bizRule' => 'return Yii::app()->user->id == $params["own_id"];',
                'data' => null,
                'children' => array(
                        'deleteComment',
                ),
        ),
        'deleteMyComment' => array(
                'type' => CAuthItem::TYPE_OPERATION,
                'description' => '',
                'bizRule' => 'return Yii::app()->user->id == $params["author_id"];',
                'data' => null,
                'children' => array(
                        'deleteComment',
                ),
        ),
        
        //������
        'deleteComment' => array(
                'type' => CAuthItem::TYPE_TASK,
                'description' => '',
                'bizRule' => null,
                'data' => null,
        ),
        'activation' => array(
                'type' => CAuthItem::TYPE_TASK,
                'description' => '',
                'bizRule' => null,
                'data' => null,
        ),
       /* 'buyStatusReal' => array(
                'type' => CAuthItem::TYPE_TASK,
                'description' => '',
                'bizRule' => null,
                'data' => null,
        ),*/
        
        // ���� 
        'guest' => array(
                'type' => CAuthItem::TYPE_ROLE,
                'description' => 'Guest',
                'bizRule' => 'return Yii::app()->user->isGuest;',
                'data' => null
        ),
        'userUnreal' => array(
                'type' => CAuthItem::TYPE_ROLE,
                'description' => 'User',
                'children' => array(
                        'deleteOwnComment',
                        'deleteMyComment',
                        'activation',
                ),
                'bizRule' => null,
                'data' => null
        ),
        'user' => array(
                'type' => CAuthItem::TYPE_ROLE,
                'description' => 'User',
                'children' => array(
                        'deleteOwnComment',
                        'deleteMyComment',
                ),
                'bizRule' => null,
                'data' => null
        ),
        'moderator' => array(
                'type' => CAuthItem::TYPE_ROLE,
                'description' => 'Moderator',
                'children' => array(
                        'user',
                        'deleteComment',
                ),
                'bizRule' => null,
                'data' => null
        ),
        'admin' => array(
                'type' => CAuthItem::TYPE_ROLE,
                'description' => 'Administrator',
                'children' => array(
                        'user',
                        'deleteComment',
                ),
                'bizRule' => null,
                'data' => null
        ),
);