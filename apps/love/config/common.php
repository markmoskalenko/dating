<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

//Yii::setPathOfAlias('filters',YiiBase::getPathOfAlias('application').'/filters');
//echo Yii::getPathOfAlias('webroot').'/filters';
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');
Yii::setPathOfAlias('vc', dirname(__FILE__) . '/../../../extensions/vipconvert');
Yii::setPathOfAlias('geoip', dirname(__FILE__) . '/../../../extensions/geoip');
Yii::setPathOfAlias('smtpmail', dirname(__FILE__) . '/../../../extensions/smtpmail');
Yii::setPathOfAlias('mail', dirname(__FILE__) . '/../extensions/yii-mail');
YiiBase::setPathOfAlias('uploadsroot', dirname(__FILE__) . '/../../../');

$db = require(__DIR__ . '/db.php');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    //'name'=>'',
    'charset' => 'utf-8',
    'language' => 'ru',
    'timeZone' => 'Europe/Moscow',

    // preloading 'log' component
    'preload' => array(
//        'log',
        'conf',
        //'jQueryAddress',
    ),

    // Enable modules
    'modules' => array(),

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.helpers.*',
        'application.filters.*',
        'application.models.test.*',
        'application.extensions.captchaExtended.*',
        'application.extensions.yii-mail.*',
    ),
    'aliases' => array(
        //If you used composer your path should be
        'xupload' => 'ext.vendor.Asgaroth.xupload',
        //If you manually installed it
        'xupload' => 'ext.xupload',

        //'bootstrap' => 'application.modules.bootstrap',
    ),
    //'defaultController'=>'site',

    // application components
    'components' => array(
        /*'viewRenderer'=>array(
'class'=>'CPradoViewRenderer',
),*/
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'faceDetector' => array(
            'class' => 'ext.FaceDetector.CFaceDetector',
        ),
        'request' => array(
            'class' => 'EHttpRequest',
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
        ),
        'session' => array(
            'class' => 'CDbHttpSession',
            'connectionID' => 'db',
            'autoCreateSessionTable' => true,
        ),
        /*'cache'=>array(
            'class'=>'system.caching.CMemCache',
            'servers'=>array(
                array('host'=>'127.0.0.1', 'port'=>11211),
            ),
        ),*/
        'cache' => array(
            'class' => 'system.caching.CFileCache',
            //'class'=>'system.caching.CDbCache',
            // 'directoryLevel'=>2
        ),

        'db' => $db,

        'sphinx' => array(
            'class' => 'ext.DGSphinxSearch.DGSphinxSearch',
            'server' => '127.0.0.1',
            'port' => 3312,
            'maxQueryTime' => 30,
            'enableProfiling' => true,
            'enableResultTrace' => true,
            'fieldWeights' => array(
                'name' => 10000,
                'keywords' => 100,
            ),
        ),
        'user' => array(
            'class' => 'WebUser',
            'loginUrl' => array('user/login'),
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'authManager' => array(
            'class' => 'PhpAuthManager',
            'authFile' => dirname(__FILE__) . '/auth.php',
            'defaultRoles' => array('guest'),
            'showErrors' => YII_DEBUG
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),

        'coreMessages' => array(
            'basePath' => 'apps/love/messages',
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'CLinkPager' => array(
                    'maxButtonCount' => 10,
                    'cssFile' => false,
                ),
                'ActiveForm' => array(// 'class'=>'application.helpers.EActiveForm',
                ),
            ),
        ),

        'clientScript' => array(
            'class' => 'NLSClientScript',
            //'packages'=>require(dirname(__FILE__).'/packages.php'),
        ),
        'jQueryAddress' => array(
            'class' => 'ext.jQueryAddress.JQueryAddress',
        ),
        /*'startSite'=>array(
                'class'=>'application.components.StartSite',
        ),*/
        /*'vc'=>array(
                'class'=>'vc.components.VCStart',
        ),*/
        'conf' => array(
            'class' => 'Configer',
        ),
        /*'email'=>array(
                'class'=>'application.extensions.email.Email',
                'delivery'=>'debug', //Will use the php mailing function.
                //May also be set to 'debug' to instead dump the contents of the email into the view
        ),
        'geo' => array (
                'class' => 'application.extensions.GeoIP.CGeoIP',
                'debug' => false,
        ),*/
        'geo' => array(
            'class' => 'geoip.CGeoIP',
            'debug' => false,
        ),
        'Smtpmail' => array(
            'class' => 'smtpmail.PHPMailer',
            'Host' => "lifemeetmail.biz",
            'Username' => 'life@lifemeetmail.biz',
            'Password' => 'jsOc7ZZ8',
            'Mailer' => 'smtp',
            'Port' => 25,
            'SMTPAuth' => true,
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.MyMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => '',
                'password' => '',
                'port' => '465',
                'encryption'=>'tls'
            ),
            'viewPath' => 'application.views.email', 'logging' => true, 'dryRun' => false
        ),
        'CURL' => array(
            'class' => 'application.extensions.curl.Curl',
            //you can setup timeout,http_login,proxy,proxylogin,cookie, and setOPTIONS

            //eg.
            'options' => array(
                'timeout' => 1,

                /* 'cookie'=>array(
                 'set'=>'cookie'
             ),*/


                'setOptions' => array(),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => require(dirname(__FILE__) . '/params.php'),
);