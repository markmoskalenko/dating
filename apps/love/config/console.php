<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
$db = require(__DIR__ . '/db.php');
Yii::setPathOfAlias('mail', dirname(__FILE__) . '/../extensions/yii-mail');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Console Application',
    'timeZone' => 'Europe/Moscow',
    'language' => 'ru',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.helpers.*',
        'application.filters.*',
        'application.models.test.*',
        'application.extensions.captchaExtended.*',
        'application.extensions.yii-mail.*',
    ),
    'components' => array(
        'mail' => array(
            'class' => 'ext.yii-mail.MyMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => '',
                'password' => '',
                'port' => '465',
                'encryption'=>'tls'
            ),
            'viewPath' => 'application.views.email', 'logging' => true, 'dryRun' => false
        ),
        'db' => $db,
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    //'levels'=>'trace, info, profile, warning, error',
                    'levels' => 'profile',
                    //'showInFireBug' => true,
                    'logFile' => 'console.profile.log',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'warning, error',
                    //'showInFireBug' => true,
                    'logFile' => 'console.error.log',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'trace, info',
                    //'showInFireBug' => true,
                    'logFile' => 'console.info.log',
                ),
            ),
        ),
    ),
);