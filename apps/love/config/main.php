<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/common.php'),
	array(
                'theme'=>'meetdate',//'meetdate',
                'preload'=>array(
                        'vc',
                        'iplog',
                        //'jQueryAddress',
                ),
                'components'=>array(
                        'iplog'=>array(
                            'class'=>'application.components.CIplog',
                        ),
                        'urlManager'=>array(
                                'showScriptName'=>false,
                                'urlSuffix'=>'',
                                'urlFormat'=>'path',
                                'rules'=>require(dirname(__FILE__).'/urlrules.php'),      
                        ),
                        'log'=>array(
                                'class'=>'CLogRouter',
                                'routes'=>array(
                                        array(
                                                'class'=>'CProfileLogRoute',
                                                'enabled'=>true,
                                        ),
                                        array(
                                                'class'=>'CWebLogRoute',
                                                //'levels'=>'trace, info, profile, warning, error',
                                                'levels'=>'info, profile, warning, error',
                                                //'showInFireBug' => true
                                        ),
                                        array(
                                                'class'=>'CFileLogRoute',
                                                //'levels'=>'trace, info, profile, warning, error',
                                                'levels'=>'profile',
                                                //'showInFireBug' => true,
                                                'logFile' => 'app.profile.log',
                                        ),
                                        array(
                                                'class'=>'CFileLogRoute',
                                                'levels'=>'warning, error',
                                                //'showInFireBug' => true,
                                                'logFile' => 'app.error.log',
                                        ),
                                        array(
                                                'class'=>'CFileLogRoute',
                                                'levels'=>'info',
                                                //'showInFireBug' => true,
                                                'logFile' => 'app.info.log',
                                        ),
                                        /* array( // configuration for the toolbar
                                                'class'=>'XWebDebugRouter',
                                                'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle', // default config
                                                //'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
                                                'levels'=>'error, warning, trace, profile, info',
                                                //'allowedIPs'=>array('127.0.0.1','192.168.1.54'),
                                        ),*/
                                ),
                        ),
                ),
	)
);