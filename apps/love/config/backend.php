<?php

return CMap::mergeArray(
    require(dirname(__FILE__) . '/common.php'),
    array(
        'theme' => 'backend',

        'controllerPath' => dirname(dirname(__FILE__)) . '/backend/controllers',
        'viewPath' => dirname(dirname(__FILE__)) . '/backend/views',

        'import' => array(
            'application.backend.components.*',
            'application.backend.filters.*',
            'application.backend.models.*',
        ),
        'components' => array(
            'urlManager' => array(
                'showScriptName' => true,
                'urlSuffix' => '',
                'urlFormat' => 'path',
                'rules' => require(dirname(__FILE__) . '/../backend/config/urlrules.php'),
            ),
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'class' => 'CProfileLogRoute',
                        'enabled' => true,
                    ),
                    array(
                        'class' => 'CFileLogRoute',
                        //'levels'=>'trace, info, profile, warning, error',
                        'levels' => 'profile',
                        //'showInFireBug' => true,
                        'logFile' => 'backend.profile.log',
                    ),
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'warning, error',
                        //'showInFireBug' => true,
                        'logFile' => 'backend.error.log',
                    ),
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'trace, info',
                        //'showInFireBug' => true,
                        'logFile' => 'backend.info.log',
                    ),
                    /* array( // configuration for the toolbar
                            'class'=>'XWebDebugRouter',
                            'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle', // default config
                            //'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
                            'levels'=>'error, warning, trace, profile, info',
                            //'allowedIPs'=>array('127.0.0.1','192.168.1.54'),
                    ),*/
                ),
            ),
        ),
    )
);