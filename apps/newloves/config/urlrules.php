<?php

// this contains the application url rules
return array(
        '/'=>array('site/index', 'urlSuffix'=>'', ),
        'admin/user/<user_id:[0-9]+>/<_c>'=>array('admin/<_c>/index', 'urlSuffix'=>'', 'defaultParams'=>array(), 'urlSuffix'=>''),
        'admin/user/<user_id:[0-9]+>/<_c>/<_a>'=>array('admin/<_c>/<_a>', 'urlSuffix'=>'', 'defaultParams'=>array(), 'urlSuffix'=>''),
        
        'admin'=>array('admin/default/index', 'urlSuffix'=>'', ),
        'admin/<_c>'=>array('admin/<_c>/index', 'urlSuffix'=>'', 'defaultParams'=>array(), 'urlSuffix'=>''),
        'admin/<_c>/<_a>/<id:\d+>'=>array('admin/<_c>/<_a>', 'urlSuffix'=>''),
        'admin/<_c>/<_a>'=>array('admin/<_c>/<_a>', 'urlSuffix'=>'', 'defaultParams'=>array(), 'urlSuffix'=>''),
        
        'register'=>array('user/register', 'urlSuffix'=>'', 'defaultParams'=>array()),
        'login'=>array('user/login', 'urlSuffix'=>'', 'defaultParams'=>array()),
        'lostpassword'=>array('user/lostpassword', 'urlSuffix'=>'', 'defaultParams'=>array()),
        'logout'=>array('user/logout', 'urlSuffix'=>'', 'defaultParams'=>array()),
        
        //'<_c>,<section:[a-z]+>'=>array('<_c>/index', 'urlSuffix'=>''),
        
             
        //'page/<user:\w+>'=>array('profile/index', 'urlSuffix'=>'', 'defaultParams'=>array()),
        
       // 'albums/<userId:\d+>/<albumId:\d+>'=>array('albums/index', 'urlSuffix'=>''),
        
        'id<userId:[0-9]+>'=>array('profile/index', 'urlSuffix'=>''),
        
        'im<id:[0-9]+>'=>array('im/chat', 'urlSuffix'=>''),
        
        'app<id:[0-9]+>'=>array('apps/play', 'urlSuffix'=>''),
        
        //'photo<userId:\d+>_<photoId:[0-9]+>'=>array('photos/show', 'urlSuffix'=>''),

        'album<userId:[0-9]+>_<albumId:[0-9]+>/photo<photoId:[0-9]+>'=>array('photos/index', 'urlSuffix'=>''),
        'photo<userId:[0-9]+>_<photoId:[0-9]+>'=>array('photos/index', 'urlSuffix'=>''),
        'album<userId:[0-9]+>_<albumId:[0-9]+>'=>array('photos/index', 'urlSuffix'=>''),
        //'photo/u<userId:[0-9]+>/a<albumId:[0-9]+>/p<photoId:[0-9]+>'=>array('photos/index', 'urlSuffix'=>''),
        
        '<_c:(photos|friends)><userId:[0-9]+>'=>array('<_c>/index', 'urlSuffix'=>''),
        
        
        '<_c:(search|photos|friends|messages|edit|settings|fave|notifications|guests|pay|video|top|gifts|activation|support|apps)>'=>array('<_c>/index', 'urlSuffix'=>'', 'defaultParams'=>array()),
        '<_c:(search|photos|friends|messages|edit|settings|fave|notifications|guests|pay|video|top|gifts|activation|support)+>/<_a>'=>array('<_c>/<_a>', 'urlSuffix'=>'', 'defaultParams'=>array()),
        
        '<_c:(rules)>/<page:[A-Za-z0-9_]+>'=>array('<_c>/index', 'urlSuffix'=>''),
        
        array(
                'class' => 'application.components.ProfilePageUrlRule',
                'connectionID' => 'db',
        ),
        
        //'<_c>'=>array('<_c>/index', 'urlSuffix'=>'', 'defaultParams'=>array()),
        //'<_c>/<_a>'=>array('<_c>/<_a>', 'urlSuffix'=>'', 'defaultParams'=>array()),
);