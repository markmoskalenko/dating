<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/../../love/config/common.php'),
	array(
                'theme'=>'thesexmir',
                'preload'=>array(
                        'vc',
                        'iplog',
                        //'jQueryAddress',
                ),
                'controllerPath' => dirname(dirname(__FILE__)).'/controllers',
                'viewPath' => dirname(dirname(__FILE__)).'/views',
                'controllerMap' => array(
                        'pay' => array(
                                'class' => 'application.controllers.PayController'
                        ),
                        'profile' => array(
                                'class' => 'application.controllers.ProfileController'
                        ),
                        'activation' => array(
                                'class' => 'application.controllers.ActivationController'
                        ),
                        'api' => array(
                                'class' => 'application.controllers.ApiController'
                        ),
                        'appApi' => array(
                                'class' => 'application.controllers.AppApiController'
                        ),
                        'apps' => array(
                                'class' => 'application.controllers.AppsController'
                        ),
                        'chat' => array(
                                'class' => 'application.controllers.ChatController'
                        ),
                        'edit' => array(
                                'class' => 'application.controllers.EditController'
                        ),
                        'fave' => array(
                                'class' => 'application.controllers.FaveController'
                        ),
                        'friends' => array(
                                'class' => 'application.controllers.FriendsController'
                        ),
                        'gifts' => array(
                                'class' => 'application.controllers.GiftsController'
                        ),
                        'guests' => array(
                                'class' => 'application.controllers.GuestsController'
                        ),
                        'messages' => array(
                                'class' => 'application.controllers.MessagesController'
                        ),
                        'notifications' => array(
                                'class' => 'application.controllers.NotificationsController'
                        ),
                        'photos' => array(
                                'class' => 'application.controllers.PhotosController'
                        ),
                        'rules' => array(
                                'class' => 'application.controllers.RulesController'
                        ),
                        'search' => array(
                                'class' => 'application.controllers.SearchController'
                        ),
                        'settings' => array(
                                'class' => 'application.controllers.SettingsController'
                        ),
                        'site' => array(
                                'class' => 'application.controllers.SiteController'
                        ),
                        'support' => array(
                                'class' => 'application.controllers.SupportController'
                        ),
                        'top' => array(
                                'class' => 'application.controllers.TopController'
                        ),
                        'unsubscribe' => array(
                                'class' => 'application.controllers.UnsubscribeController'
                        ),
                        'user' => array(
                                'class' => 'application.controllers.UserController'
                        ),
                ),
                'components'=>array(
                        'iplog'=>array(
                            'class'=>'application.components.CIplog',
                        ),
                        'urlManager'=>array(
                                'showScriptName'=>false,
                                'urlSuffix'=>'',
                                'urlFormat'=>'path',
                                'rules'=>require(dirname(__FILE__).'/urlrules.php'),      
                        ),
                        'log'=>array(
                                'class'=>'CLogRouter',
                                'routes'=>array(
                                        array(
                                                'class'=>'CFileLogRoute',
                                                //'levels'=>'trace, info, profile, warning, error',
                                                'levels'=>'profile',
                                                //'showInFireBug' => true,
                                                'logFile' => 'app.profile.log',
                                        ),
                                        array(
                                                'class'=>'CFileLogRoute',
                                                'levels'=>'warning, error',
                                                //'showInFireBug' => true,
                                                'logFile' => 'app.error.log',
                                        ),
                                        array(
                                                'class'=>'CFileLogRoute',
                                                'levels'=>'info',
                                                //'showInFireBug' => true,
                                                'logFile' => 'app.info.log',
                                        ),
                                        /* array( // configuration for the toolbar
                                                'class'=>'XWebDebugRouter',
                                                'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle', // default config
                                                //'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
                                                'levels'=>'error, warning, trace, profile, info',
                                                //'allowedIPs'=>array('127.0.0.1','192.168.1.54'),
                                        ),*/
                                ),
                        ),
                ),
	)
);